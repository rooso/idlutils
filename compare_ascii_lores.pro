pro compare_ascii_lores
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ce programme compare les profils ascii de l'output 00150 ;
;et de l'output lores 00178 (le plus proche en temps)     ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  files = find_file('/Users/oroos/Post-stage/LOPs00150/density_profile*.ascii',count=nf)
  files_lores = find_file('/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/LOPs00178/lores*.ascii',count=nf_lores)

  if nf ne nf_lores then stop

  window, 1
  plot, [0.002,50], [1d-6,1d7], /nodata, /xlog, /ylog, /xstyle, /ystyle, xtitle='Depth [kpc]', ytitle='Density [H/cc]'
  al_legend, ['Hires', 'Lores'], color=[!black,!red]

  for k = 0, nf-1 do begin

     percentage = fix(float(k+1)/nf*100)   
     if k mod 100 eq 0 then print, percentage, ' %'

     ascii = read_file_ascii(files(k))
     ascii_lores = read_file_ascii(files_lores(k))
     
     oplot, ascii(*,0), ascii(*,1), color=!black
     oplot, ascii_lores(*,0), ascii_lores(*,1), color=!red
     
;stop
;erase
     
     if k eq 0 then begin
        min_den = min(ascii(*,1))
        max_den = max(ascii(*,1))
        min_dpth = min(ascii(*,0))
        max_dpth = max(ascii(*,0))
        
        min_den_lores = min(ascii_lores(*,1))
        max_den_lores = max(ascii_lores(*,1))
        min_dpth_lores = min(ascii_lores(*,0))
        max_dpth_lores = max(ascii_lores(*,0))
     endif else begin
        min_den = [min_den,min(ascii(*,1))]
        max_den = [max_den,max(ascii(*,1))]
        min_dpth = [min_dpth,min(ascii(*,0))]
        max_dpth = [max_dpth,max(ascii(*,0))]
        
        min_den_lores = [min_den_lores,min(ascii_lores(*,1))]
        max_den_lores = [max_den_lores,max(ascii_lores(*,1))]
        min_dpth_lores = [min_dpth_lores,min(ascii_lores(*,0))]
        max_dpth_lores = [max_dpth_lores,max(ascii_lores(*,0))]
     endelse
  endfor

  print, 'Min/max density :', min(min_den), max(max_den)
  print, 'Min/max density lores :', min(min_den_lores), max(max_den_lores) ;H/cc

  print, 'Min/max depth :', min(min_dpth), max(max_dpth)
  print, 'Min/max depth lores :', min(min_dpth_lores), max(max_dpth_lores) ;kpc

;IDL output :
;Min/max density :   3.9870862e-06       396446.40
;Min/max density lores :   1.6501002e-06       81823.395
;Min/max depth :       0.0000000       24.661255
;Min/max depth lores :       0.0000000       24.719238
end
