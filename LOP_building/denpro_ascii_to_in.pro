;lecture des LOPs et creation des scripts avec les profils de densite
;pour Cloudy
FUNCTION read_lop, file, sim, factor, res, filling_factor

;lire les donnees
print, file
ascii = read_file_ascii(file)
sorted_depth = ascii(*,0)
sorted_density = ascii(*,1)

if factor ne '' then file = str_replace(file, res+'density', 'x'+factor+'_'+res+'density')

;preparing data for Cloudy

;correction de la densite (car on a des densites moyennes)
sorted_density = sorted_density/filling_factor ;;;avant d'utiliser une densite cloudy, il faudra TOUJOURS remultiplier par ff !!!

;cloudy reads logs + depth in cm and rho in cm^-3
print, "premier point : ", sorted_depth(0)
cloudy_depth = alog10(double(sorted_depth)/3.24d-22) ; in log(cm)
cloudy_density = alog10(double(sorted_density)) ; in H/cc = cm^-3

t_debut = where(sorted_depth eq 0., nt_debut)
if nt_debut gt 0 then cloudy_depth[t_debut] = -30. ;;le premier point cloudy est arbitraire et est a -35.
;;si le premier vrai point amr est a 0, cela pose un probleme (valeurs
;;doivent etre triees par pfdeur croissante)

;print, cloudy_depth 
taille = n_elements(cloudy_depth)
print, "taille", taille

;sortie du script lisible par cloudy : spectre realiste de Sy1 (AGN +
;BB + transmitted) + profil de densite issu de la LOP

;On veut : L_tot = 10^44.5/45.5/46.5 erg/s = L_AGN + somme L_BB_i + L_transmis avec
;L_AGN + L_BB = L_transmis et L_AGN/L_BB_i donne par l'ajustement improved_final_realistic_sy1_spectrum

;       L_BB = 4*pi*r_0^2*sigma*T^4

;on veut commencer le calcul du transfert de rayonnement au niveau de
;la premiere cellule
radius_inner = cloudy_depth(0)
if cloudy_depth(0) eq -30 then radius_inner = cloudy_depth(1)
radius_outer = cloudy_depth(taille-1) - 0.05

;predire la profondeur a laquelle la limite d'ionisation sera
;atteinte en utilisant le critere 10^22 cm-2
lg = fltarr(taille)
lg(0) = 10^cloudy_depth(0)
column_density = 0

;troncature des lignes trop longues pour cloudy
taille_500 = 0
if taille ge 497 then begin
   taille_500 = taille
   taille = 497
   print, "/!\ LOP tronquee a 500 paires"
   radius_outer = cloudy_depth(taille-1) - 0.05
if radius_outer gt max(cloudy_depth(0:taille-1)) then radius_outer = cloudy_depth(taille-2) - 0.05
endif

first_depth = -35.

;;;L_tot = L_incident + L_transmis = 44.5 (corresp a L_X = 43.5)
;;;avec L_transmis = L_incident = L_AGN + somme L_BB
;;;L_BB en fonction de L_AGN donne par realistic_AGN.in

file_in = str_replace(file, 'ascii', 'in')
 n=strpos(file,sim+'_LOP')+9
m=strpos(file_in,'.in')
len=m-n
lop_name=strmid(file_in,n,len)
;print, lop_name
;stop

openw, data, file_in, /get_lun
fill = fill_AGN_SED(data, radius_inner, radius_outer, factor, lop_name)
printf, data, 'filling factor ', filling_factor
printf, data, 'dlaw table //density is divided by the filling factor'
printf, data, 'continue', first_depth, cloudy_density(0) ;;premier point exige par cloudy, densite du premier vrai point (en double, du coup)

for i = 0, taille-1 do begin
      if cloudy_depth(i) eq !VALUES.F_INFINITY then print, 'Warning, infinite value for LOP ', lop_name
      
if i eq 0 then begin
if cloudy_depth(0) ne -30 then printf, data, 'continue', cloudy_depth(i), cloudy_density(i)
endif else begin
printf, data, 'continue', cloudy_depth(i), cloudy_density(i)
endelse
      if i gt 0 then lg(i) = 10^cloudy_depth(i)-10^cloudy_depth(i-1)
      ;print, lg(i)
   if column_density lt 1e22 then begin
        column_density = column_density +  10^(cloudy_density(i))*filling_factor*lg(i)
        i_critique = i
        ;print, column_density
   endif
endfor


if factor eq '' then begin
if taille_500 gt 0 then begin
file_trunc = str_replace(file, 'density_profile', 'trunc500')

openw, trunc500, file_trunc, /get_lun
print, 'Attention, ', taille_500-497, ' points ont ete tronques'
printf, trunc500, 'Attention, ', taille_500-497, ' points ont ete tronques'
print, 'Les points restants sont : '
printf, trunc500, 'Les points restants sont : '
for j = taille, taille_500-1 do begin
printf, trunc500, cloudy_depth(j), cloudy_density(j)
endfor
close, trunc500
free_lun, trunc500
endif

file_rion = str_replace(file, 'density_profile', 'rion_prediction')
file_rion = str_replace(file_rion, 'ascii', 'txt')

openw, rion, file_rion, /get_lun
print, "LOP ", lop_name, " : La densite de colonne critique ", column_density, " cm^-2 est atteinte a une profondeur de ", 10^cloudy_depth(i_critique), " cm (log = ", cloudy_depth(i_critique), "), soit ", 10^cloudy_depth(i_critique)*3.24e-22, " kpc."
printf, rion, "LOP ", lop_name, " : La densite de colonne critique ", column_density, " cm^-2 est atteinte a une profondeur de ", 10^cloudy_depth(i_critique), " cm (log = ", cloudy_depth(i_critique), "), soit ", 10^cloudy_depth(i_critique)*3.24e-22, " kpc."
if column_density lt 1e22 then begin
    print, "LOP ", lop_name, " : Attention, la limite d'ionisation n'est pas passee !"
    printf, rion, "LOP ", lop_name, " : Attention, la limite d'ionisation n'est pas passee !"
endif
close, rion
free_lun, rion
endif

file_save = str_replace(file_in, '.in', '')
n=strpos(file_save, 'LOPs'+sim)+10
file_save=strmid(file_save, n, strlen(file_save))

if factor eq '' then nzone = '4400'
if factor eq '10' then nzone = '5500'
if factor eq '100' then nzone = '6600'

printf, data, 'end of dlaw table'
printf, data, 'abundances ISM no grains no qheat'
printf, data, 'grains ISM function sublimation'
printf, data, 'no grain qheat'
printf, data, 'iterate to convergence'
printf, data, '//save line labels "line_labels.txt"'
printf, data, 'save lines, emissivity "'+file_save+'.em_intrinsic" last //default == intrinsinc'
printf, data, 'TOTL	 1216A'
printf, data, 'H  1	 1026A'
printf, data, 'TOTL	 4861A'
printf, data, 'O  3	 5007A'
printf, data, 'H  1	 6563A'
printf, data, 'N  2	 6584A'
printf, data, 'O  1	 6300A'
printf, data, 'S  2	 6720A'
printf, data, 'Ne 3	 3869A'
printf, data, 'TOTL	 3727A'
printf, data, 'Ne 5	 3426A'
printf, data, 'Si 7	2.481m'
printf, data, 'H  1	2.166m'
printf, data, 'Si 6	1.963m'
printf, data, 'Al 9	2.040m'
printf, data, 'Ca 8	2.321m'
printf, data, 'Ne 6	7.652m'
printf, data, 'end of lines'
printf, data, 'save lines, emissivity emergent "'+file_save+'.em_emergent" last //default == intrinsinc'
printf, data, 'TOTL	 1216A'
printf, data, 'H  1	 1026A'
printf, data, 'TOTL	 4861A'
printf, data, 'O  3	 5007A'
printf, data, 'H  1	 6563A'
printf, data, 'N  2	 6584A'
printf, data, 'O  1	 6300A'
printf, data, 'S  2	 6720A'
printf, data, 'Ne 3	 3869A'
printf, data, 'TOTL	 3727A'
printf, data, 'Ne 5	 3426A'
printf, data, 'Si 7	2.481m'
printf, data, 'H  1	2.166m'
printf, data, 'Si 6	1.963m'
printf, data, 'Al 9	2.040m'
printf, data, 'Ca 8	2.321m'
printf, data, 'Ne 6	7.652m'
printf, data, 'end of lines'
printf, data, 'set nend '+nzone
;printf, data, 'save overview "'+file_save+'.ovr" last'
printf, data, 'save continuum  "'+file_save+'.con" units micron last'
;printf, data, 'save lines, array  "'+file_save+'.lin" units micron last'
printf, data, 'save element Hydrogen  "'+file_save+'_H.el" last'
printf, data, 'save element Oxygen  "'+file_save+'_O.el" last'
printf, data, 'save physical conditions  "'+file_save+'.phyc" last'
printf, data, 'print last'
if taille_500 gt 0 then printf, data, '//Cette LOP a ete tronquee de ',taille_500-497,' points.'
close, data

free_lun, data

return, 0

end


pro denpro_ascii_to_in, sim=sim, part=part, lmax=lmax, boxlen=boxlen, ff=ff
;;;attention au nom pour les LOP lmax autre que 13 ou boxlen autre que 50 kpc

if not keyword_set(sim) then sim = '00100'
if not keyword_set(part) then part = '*'
if not keyword_set(lmax) then lmax = 13
if not keyword_set(boxlen) then boxlen = 50 ;kpc
if not keyword_Set(ff) then ff = 1./5.

filenames = '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part+'_*'
res ='' 

if lmax eq 12 then begin
res = 'lores_' 
filenames = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/LOPs'+sim+'/'+res+'density_profile_'+sim+'_LOP*'+part+'_*'
endif

files = find_file(filenames+'.ascii',count = nfiles)

for k = 0, nfiles-1 do begin
    y=read_lop(files(k),sim, '', res, ff)
    y=read_lop(files(k),sim, '10', res, ff)
    y=read_lop(files(k),sim, '100', res, ff)

endfor

print, 'Fin du programme. Tout est OK.'

end


