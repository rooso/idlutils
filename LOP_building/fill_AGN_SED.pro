function fill_AGN_SED, data, radius_inner, radius_outer, factor, lop_name

if factor eq '' then begin
printf, data, 'title Realistic AGN+central pc spectrum for LOP number '
printf, data, '///', lop_name ;attention ici pour la lecture du fichier .in par certains programmes
printf, data, 'AGN T = 5.5e5 k, a(ox) = -0.7, a(uv)=-0.3 a(x)=-0.48'
printf, data, 'luminosity (total) 44.12 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=3e2 K , radius 18.20 //luminosities adjusted so that Ltot = 44.5'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, '////////autres bb pour combler vide IR et rattraper pente FIR'
printf, data, 'blackbody, T=90 K , radius 18.90 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=200 K , radius 18.39 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=900 K , radius 17.10'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=1400 K , radius 16.10' 
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=2900 K , radius 14.98 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'CMB'
printf, data, 'table read "../realistic_AGN_transmitted.txt"'
printf, data, 'luminosity (total) 44.2 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'sphere'
endif

if factor eq '10' then begin
printf, data, 'title Realistic AGNx10+central pc spectrum for LOP number '
printf, data, '///', lop_name
printf, data, 'AGN T = 5.5e5 k, a(ox) = -0.7, a(uv)=-0.3 a(x)=-0.48'
printf, data, 'luminosity (total) 45.10 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=3e2 K , radius 18.69 //luminosities adjusted so that Ltot = 45.5'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, '////////autres bb pour combler vide IR et rattraper pente FIR'
printf, data, 'blackbody, T=90 K , radius 19.59 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=200 K , radius 18.89 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=900 K , radius 17.59'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=1400 K , radius 16.59' 
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=2900 K , radius 16.19 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'CMB'
printf, data, 'table read "../x10_realistic_AGN_transmitted.txt"'
printf, data, 'luminosity (total) 45.2 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'sphere'
endif

if factor eq '100' then begin
printf, data, 'title Realistic AGNx100+central pc spectrum for LOP number '
printf, data, '///', lop_name
printf, data, 'AGN T = 5.5e5 k, a(ox) = -0.7, a(uv)=-0.3 a(x)=-0.48'
printf, data, 'luminosity (total) 46.10 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=3e2 K , radius 19.19 //luminosities adjusted so that Ltot = 46.5'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, '////////autres bb pour combler vide IR et rattraper pente FIR'
printf, data, 'blackbody, T=90 K , radius 20.09 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=200 K , radius 19.39 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=900 K , radius 18.09'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=1400 K , radius 17.09' 
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=2900 K , radius 16.69 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'CMB'
printf, data, 'table read "../x100_realistic_AGN_transmitted.txt"'
printf, data, 'luminosity (total) 46.2 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'sphere'

endif

return, 0

end
