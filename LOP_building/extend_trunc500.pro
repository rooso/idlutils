pro extend_trunc500

file='/Users/oroos/Post-stage/Extend_trunc500/list_trunc500_and_outer_r_and_number_x100'

openr, lun, file, /get_lun
line=''

number = dblarr(file_lines(file))
count = 0

for i = 0, file_lines(file)-1 do begin
readf, lun, line
pos1 = strpos(line, ' de ')
pos2 = strpos(line, 'points.')
number(i) = double(strmid(line,pos1+4,pos2-pos1-5))
;print, number(i)
if strmatch(line, '*x100*') eq 1 then count = count+1
endfor

print, 'Il y a ', n_elements(number), ' lignes tronquees arretees au rayon de troncature.'
t = where(number lt 50, nt)
print, nt, ' lignes sont tronquees de moins de 50 points' ;mais ca on s'en fout
t = where(number eq 0, nt)
print, nt, ' lignes ont pile 497 points (==pas tronquees == celle ou le readcol fait une erreur juste apres)' 
print, count, ' lignes sont avec la luminosite x100'

;quelle est la densite de colonne a traiter restante ???

window, 0
plothist, number, bin=5, xr=[-10,300], yr=[0,6], /xstyle, title='Histogramme du nombre de points tronques'

files_trunc = findfile('/Users/oroos/Post-stage/Extend_trunc500/trunc500*.ascii', count=nf_trunc)

if nf_trunc ne file_lines(file) then stop

final_colden = dblarr(nf_trunc)

for k = 0, nf_trunc-1 do begin
readcol, files_trunc(k), depth, density, /silent

if n_elements(depth) gt 1 then begin
density = (10d^density)*1./5.
depth = 10d^depth

length = dblarr(n_elements(depth))
colden = dblarr(n_elements(depth))

length(0) = depth(0)
colden(0) = length(0)*density(0)

for j = 1, n_elements(depth)-1 do begin
length(j) = depth(j)-depth(j-1)
colden(j) = colden(j-1) + length(j)*density(j)
endfor

final_colden(k) = max(colden)

endif else begin
final_colden(k) = 0
endelse
endfor

window, 1
plothist, alog10(final_colden), bin=0.5, xr=[17,26], yr=[0,30], /xstyle, title='Histogramme de la densite de colonne a la vraie fin des lignes tronquees'

t = where(final_colden lt 1d20, nt)
print, nt, ' lignes ont une densite de colonne restante inferieure a 10^20 H/cc'
t = where(final_colden lt 1d22, nt)
print, nt, ' lignes ont une densite de colonne restante inferieure a 10^22 H/cc'
print, 'Lignes >= 10^22 H/cc : '

files_in = files_trunc
if count eq nf_trunc then strreplace, files_in, 'trunc500_', 'x100_density_profile_'
if count eq nf_trunc then strreplace, files_in, '.ascii', '.in'

print, files_in[where(final_colden ge 1d22)]
end
