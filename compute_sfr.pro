function compute_sfr, file_in, threshold=threshold, rhoSFR=rhoSFR, SFR=SFR, lmax=lmax, boxlen=boxlen
;pour calculer T_post_cloudy = max(T_amr,T_cloudy), il faut
;re-echantillonner T_amr sur T_cloudy, puis coller T_amr sur la partie
;non calculee par cloudy (parce que T_cloudy < 4,000 K) avec un
;sur-echantillonnage imitant celui de cloudy.

  yes_plot = 1                  ;afficher les plots ? 1/0
  
  if not keyword_set(lmax) then lmax = 13
  if not keyword_set(boxlen) then boxlen = 50 ;kpc
  
  
  if keyword_set(SFR) then begin
     if SFR eq 1 then rhoSFR = 0
  endif
  if keyword_set(rhoSFR) then begin
     if rhoSFR eq 1 then SFR = 0
  endif
  
;print, file_in

;;lire les donnees interessantes
  phyc = read_file_phyc(file_in)
  
  if strmatch(file_in, '*transp[4567]*') eq 1 then begin
     file_in = str_replace(file_in, 'transp[4567]', 'density')
  endif
  if strmatch(file_in, 'x1*_') eq 1 then begin
     file_in = str_replace(file_in, 'x1*_', '')
  endif
  amr = read_file_ascii(file_in)
  
  depth_cloudy = phyc(*,0)
  depth_amr = amr(*,0)
  
  density_cloudy = phyc(*,2)
  density_amr = amr(*,1)
  
  T_cloudy = phyc(*,1)
  T_amr = amr(*,5)
  
;;re-echantillonner
  taille_cloudy = n_elements(T_cloudy)
  taille_amr = n_elements(T_amr)
  
;print, 'taille cloudy ', taille_cloudy
;print, 'taille amr ', taille_amr
  
  density_resampled = fltarr(taille_cloudy)
  T_resampled = fltarr(taille_cloudy)
  
  T_post_cloudy = fltarr(taille_cloudy)
  density_post_cloudy = fltarr(taille_cloudy)
  density_init = fltarr(taille_cloudy)
  T_init = fltarr(taille_cloudy)
  
        ;;;interpoler comme cloudy
  xx = alog10(depth_amr[1:-1])                                   ;depth_amr
  yy = alog10(density_amr[1:-1])                                 ;density_amr
  density_resampled = 10d^(interpol(yy,xx,alog10(depth_cloudy))) ;density resampled onto Cloudy depth variable
  
  yy2 = alog10(T_amr[1:-1])                                 ;temperature_amr
  T_resampled = 10d^(interpol(yy2,xx,alog10(depth_cloudy))) ;temperature resampled onto Cloudy depth variable
  
  t = where(T_resampled eq 0, nt)
  if nt gt 0 then begin
     print, 'nb de zeros', nt
     stop
  endif
  
  t=where(finite(T_resampled) eq 0, nt)
  if nt gt 0 then begin
     print, 'probleme d echantillonnage !'
     stop
  endif
  
;help, T_resampled
;help, density_resampled
  
;T_cloudy : temperatures cloudy (grille cloudy)
;T_resample : temperature amr re-echantillonnee sur la grille cloudy
  
;prendre le maximum entre la valeur initiale et la valeur calculee par
;cloudy (temperature d'equilibre)
  for i = 0, taille_cloudy-1 do begin
     T_post_cloudy(i) = max([T_resampled(i),T_cloudy(i)])
     ;if max([T_resampled(i),T_cloudy(i)]) eq T_cloudy(i) then density_post_cloudy(i) = density_cloudy(i)
     ;if max([T_resampled(i),T_cloudy(i)]) eq T_resampled(i) then
 density_post_cloudy(i) = density_resampled(i)
  endfor
  
;help, T_post_cloudy
;help, density_post_cloudy
  
  t_fill_with_amr_values = where(depth_amr gt max(depth_cloudy), nt_fill)
  
;print, 'max depth cloudy ', max(depth_cloudy)
;print, 'max depth amr ', max(depth_amr)
  
;;;sur-echantillonner la partie amr restante pour avoir un espacement
;;;similaire a la grille cloudy et avoir un sfr comparable (eviter le
;;;changement vert/bleu a l'interface grille cloudy/grille amr
;;;sur la carte du sfr)
;cette interface est genante car elle n'est pas au meme endroit
;pour les differentes luminosites et donc la carte de sfr initial
;semble changer
;NB : pas de pb pour le rho_SFR, qui ne depend pas de
;l'echantillonnage, contrairement au SFR
  
  dpth = congrid([1.0001*max(depth_cloudy),depth_amr[t_fill_with_amr_values ]],11*(taille_amr+1),/interp)
  dn   = congrid([density_amr[t_fill_with_amr_values(0)],density_amr[t_fill_with_amr_values ]],11*(taille_amr+1),/interp)
  tpr =  congrid([T_amr[t_fill_with_amr_values(0)],T_amr[t_fill_with_amr_values ]],11*(taille_amr+1),/interp)
  
  dpth = dpth(uniq(dpth))       ;se debarrasser des profondeurs en double a la fin
  dn = dn(uniq(dpth))
  tpr = tpr(uniq(dpth))
  
  depth_post_cloudy = [depth_cloudy,dpth]
  T_post_cloudy = [T_post_cloudy, tpr]           ;temperature post, puis temperature amr sur-echantillonnee
  density_post_cloudy = [density_post_cloudy,dn] ;densite post puis densite amr sur-echantillonnee
  depth_init = depth_post_cloudy                 ; c'etait quand meme le but
  T_init = [T_resampled, tpr]                    ;temperature amr re-echantillonnee sur grille cloudy puis temperature amr sur-echantillonnee
  density_init = [density_resampled,dn]          ;densite amr re-chantillonnee sur grille cloudy puis densite amr sur-echantillonnee
  
  taille_post_cloudy = n_elements(depth_post_cloudy)
  
;print, 'taille post cloudy ', taille_post_cloudy
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(50.d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_post_cloudy ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_post_cloudy[t_rho]
     t_temp = where(T_post_cloudy[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then T_post_cloudy[t_rho(t_temp)] = 900
  endif
  t_rho = where(density_init ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_init[t_rho]
     t_temp = where(T_init[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then T_init[t_rho(t_temp)] = 900
  endif
  
  if yes_plot eq 1 then begin
!p.multi=0
     window, 1
     plot, depth_post_cloudy, density_post_cloudy, /xlog, /ylog, xr=[0.002,20], yr=[1d-7,1d6], title='density', /xstyle, /ystyle
     oplot, depth_post_cloudy, density_init, color=!red
     oplot, depth_cloudy, density_cloudy, color=!green, thick=4
     oplot, depth_amr, density_amr, color=!blue
     oplot, depth_post_cloudy, density_post_cloudy
     
     window, 3
     plot, depth_post_cloudy, T_post_cloudy, /xlog, /ylog, xr=[0.002,20], yr=[1d2,1d10], title='temperature', /xstyle, /ystyle
     oplot, depth_post_cloudy, T_init, color=!red
     oplot, depth_cloudy, T_cloudy, color=!green, thick=4
     oplot, depth_amr, T_amr, color=!blue
     oplot, depth_post_cloudy, T_post_cloudy
  endif
  
;1 H/cc = 1 x m_H x 1e6 kg/m3
;       = 1.67e-19 kg/m3
;G = 6.67e-11 m3/kg/s2
  n=1.5
  epsilon=0.01
  G = 6.67d-11
  hcc_to_kgm3 = 1.67d-21
  alpha = sqrt(32.*G/(3.*!dpi))
  kgsm3_to_Msunyrpc3 = 1./(2.17d-27)
  kpc_to_m = 1./(3.24d-20)
  scale_sfr_max = 0.2455        ;=1.67d-27/2d30*(1./3024d-22)^3*1d-9
  
;;valeurs trop grande pour idl...
  scale = 0.475                 ;pour avoir le SFR en Msun/yr
;=alpha*(H/cc_to_kg/m3)^1.5*(kpc_to_m)^3*(kg/s_to_Msun/yr)
;=sqrt(32*6.67e-11/(3*pi))*(1.67e-27/1e-6)^1.5*(1/3.24e-20)^3*365*24*3600/2e30
  
;print, 'threshold ', threshold
  
  if rhoSFR eq 1 then begin
                                ;rhoSFR avant : valeurs AMR resampled
     rhoSFR_avant = density_init*0.0
     t_sfr = where((density_init ge threshold) and (T_init le 1e4), nt_sfr)
     if nt_sfr gt 0 then rhoSFR_avant[t_sfr] = epsilon*alpha*(density_init[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3*1d-3 ;Ms/yr/kpc3  
     
;rhoSFR apres
     rhoSFR_apres = density_post_cloudy*0.0
     t_sfr = where((density_post_cloudy ge threshold) and (T_post_cloudy le 1e4), nt_sfr)
     if nt_sfr gt 0 then rhoSFR_apres[t_sfr] = epsilon*alpha*(density_post_cloudy[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3*1d-3 ;Ms/yr/kpc3
     
     if yes_plot eq 1 then begin
        window, 5
        plot, depth_post_cloudy, rhoSFR_apres, /ylog, xr=[0.002,10], yr=[1d-20,1d-10], title='rhosfr', /xstyle, /ystyle, /xlog, thick=4
        oplot, depth_post_cloudy, rhoSFR_avant, color=!red
        print, minmax(rhoSFR_avant)
        print, minmax(rhoSFR_apres)
        if strmatch(file_in,'*dk*') eq 1 then stop
     endif
     
     return, [total(rhoSFR_avant),total(rhoSFR_apres)] ;mais la somme des rhoSFR ne correspond a rien...
  endif
  
  if SFR eq 1 then begin
     
;profondeur pour calculer le sfr
     depth_centered = depth_amr*0
     depth_centered(0) = depth_amr(0)
     for k = 1, taille_amr-1 do begin
        depth_centered(k) = (depth_amr(k)+depth_amr(k-1))/2.
     endfor
     
     depth_centered_lop = depth_post_cloudy*0
     depth_centered_lop(0) = depth_post_cloudy(0)
     for k = 1, taille_post_cloudy-1 do begin
        depth_centered_lop(k) = (depth_post_cloudy(k)+depth_post_cloudy(k-1))/2.
     endfor
     
;;;re-echantillonnage de depth_centered pour avoir les 2 autres
;;;dimensions dans la grille cloudy 
     
     xx = alog10(depth_amr[1:-1])   
     yy = alog10(depth_centered[1:-1]) 
     depth_centered_resampled = 10d^interpol(yy, xx, alog10(depth_post_cloudy))
     t=where(finite(depth_centered_resampled) eq 0, nt)
     if nt gt 0 then stop
     
     SFR_tot_avant = density_init*0.0
     for i = 0, taille_post_cloudy-1 do begin
        if density_init(i) ge threshold and T_init(i) le 1d4 then begin
           if i eq 0 then begin
              volume = (depth_centered_lop(0)*depth_centered_resampled(0)^2)
              SFR_tot_avant(0) = epsilon*(density_init(0))^n*volume*scale ;Msun/yr
              SFR_max = 0.3*9.9*scale_sfr_max*density_init(0)*volume
              if SFR_tot_avant(0) gt SFR_max then SFR_tot_avant(0) = SFR_max
           endif
           if i ne 0 then begin
              volume = ((depth_centered_lop(i)-depth_centered_lop(i-1))*(depth_centered_resampled(i)-depth_centered_resampled(i-1))^2)
              SFR_tot_avant(i) = epsilon*(density_init(i))^n*volume*scale ;Msun/yr
              SFR_max = 0.3*9.9*scale_sfr_max*density_init(i)*volume
              if SFR_tot_avant(i) gt SFR_max then SFR_tot_avant(i) = SFR_max
           endif
        endif
     endfor
     SFRavant_total = total(SFR_tot_avant) 
     
     SFR_tot_apres = density_post_cloudy*0.0
     for i = 0, taille_post_cloudy-1 do begin
        if density_post_cloudy(i) ge threshold and T_post_cloudy(i) le 1d4 then begin
           if i eq 0 then begin
              volume = (depth_centered_lop(0)*depth_centered_resampled(0)^2)
              SFR_tot_apres(0) = epsilon*(density_post_cloudy(0))^n*volume*scale ;Msun/yr
              SFR_max = 0.3*9.9*scale_sfr_max*density_post_cloudy(0)*volume
              if SFR_tot_apres(0) gt SFR_max then SFR_tot_apres(0) = SFR_max
           endif
           if i ne 0 then begin
              volume = ((depth_centered_lop(i)-depth_centered_lop(i-1))*(depth_centered_resampled(i)-depth_centered_resampled(i-1))^2)
              SFR_tot_apres(i) = epsilon*(density_post_cloudy(i))^n*volume*scale ;Msun/yr
              SFR_max = 0.3*9.9*scale_sfr_max*density_post_cloudy(i)*volume
              if SFR_tot_apres(i) gt SFR_max then SFR_tot_apres(i) = SFR_max
           endif
        endif
     endfor
     SFRapres_total = total(SFR_tot_apres)
     
     if yes_plot eq 1 then begin
        window, 5
        plot, depth_post_cloudy, SFR_tot_apres, /ylog, xr=[0.002,10], yr=[1d-17,1d-6], title='sfr', /xstyle, /ystyle, /xlog, thick=4
        oplot, depth_post_cloudy, SFR_tot_avant, color=!red
        ;print, minmax(SFR_tot_avant), total(SFR_tot_avant)
        ;print, minmax(SFR_tot_apres), total(SFR_tot_apres)
        ;if strmatch(file_in,'*dk*') eq 1 then stop
     endif
     
     return, [SFRavant_total,SFRapres_total,(SFRavant_total-SFRapres_total)/SFRavant_total]
  endif
  
  if SFR eq 0 and rhoSFR eq 0 then return, -42

end


window, 7
plot, depth_post_cloudy, (rhoSFR_avant-rhoSFR_apres)/rhoSFR_avant, /xlog, /ylog
oplot, depth_post_cloudy, (rhoSFR_avant-rhoSFR_apres)/rhoSFR_avant, color=!red  
print, minmax((rhoSFR_avant-rhoSFR_apres)/rhoSFR_avant,/Nan)
print, avg((rhoSFR_avant-rhoSFR_apres)/rhoSFR_avant,/Nan) 


