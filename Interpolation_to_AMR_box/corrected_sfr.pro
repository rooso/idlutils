pro corrected_sfr, sfr_threshold=sfr_threshold, sim=sim, factor=factor, lmax=lmax, boxlen=boxlen

  if not keyword_set(lmax) then lmax = '13'
  if not keyword_Set(boxlen) then boxlen = 50. ;kpc
  if not keyword_Set(sfr_threshold) then sfr_threshold = 'sfr10'
  if not keyword_Set(sim) then sim = '00100'

  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
  endif else begin
     nm = ''
  endelse
  
  if lmax eq '12' then begin
     dir = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/'
     dir_sink = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/'
     sim_with = ['00119', '00178', '00197', '00225']
     sim_no   = ['', '', '', '']
     res='lores_'
  endif else begin
     dir = '/Users/oroos/Post-stage/'
     dir_sink = '/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/'
     sim_with = ['00075w', '00100', '00108', '00150', '00170', '00210']
     sim_no   = ['00075n', '00085', '00090', '00120', '00130', '00148']
     res=''
  endelse
  
  for sim_ii = 0,  n_elements(sim_with)-1 do if strmatch(sim, sim_with(sim_ii)) eq 1 then break
  
  print, 'Restauration du fichier AMR '+sim_with(sim_ii)
  file_name_amr = dir_sink+'gas_part_'+sim+'.ascii.lmax'+lmax
  file_name_amr_sav = dir+'LOPs'+sim_with(sim_ii)+'/'+res+'amr_'+sim_with(sim_ii)+'_all.sav'
  dat_file_info = file_info(file_name_amr)
  sav_file_info = file_info(file_name_amr_sav)
  if dat_file_info.mtime gt sav_file_info.mtime then begin
     readcol, file_name_amr, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr, format='(D,D,D,D,D,D,D,D,D,I)'
     save,  x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr, file=file_name_amr_sav
  endif else begin
     restore, file_name_amr_sav
  endelse
  print, 'Fin de la restauration du fichier AMR '+sim_with(sim_ii)
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(boxlen*1d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_amr ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_amr[t_rho]
     t_temp = where(temperature_amr[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_amr[t_rho(t_temp)] = 900
  endif
  
  
  print, 'Restauration du fichier POST-CLOUDY '+nm+sim_with(sim_ii)
  special_dir = '/Users/oroos/Post-stage/'
  file_name_post = findfile(special_dir+nm+res+'cloudy_near_amr_'+sim_with(sim_ii)+'_all.dat',count=nf)
  if nf ne 1 then begin
     special_dir = '/Volumes/TRANSCEND/'
     file_name_post = findfile(special_dir+nm+res+'cloudy_near_amr_'+sim_with(sim_ii)+'_all.dat',count=nf)
     if nf ne 1 then stop
  endif
  file_name_post_sav = '/Volumes/TRANSCEND/'+nm+res+'cloudy_near_'+sim_with(sim_ii)+'_all.sav'
  dat_file_info = file_info(file_name_post)
  sav_file_info = file_info(file_name_post_sav)
  if dat_file_info.mtime gt sav_file_info.mtime then begin
     readcol, file_name_post,  x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A5,A35)'
     save,  x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, file=file_name_post_sav
  endif else begin
     restore, file_name_post_sav
  endelse
  print, 'Fin de la restauration du fichier POST-CLOUDY '+nm+sim_with(sim_ii)
  
  
;jusqu'ici OK **************************************

  if sfr_threshold eq 'sfr100' then begin
     SFR_amr_with_all = double(SFR100_amr)
     SFR_hybrid_all = double(SFR100_hybrid)
     SFR_cloudy_all = double(SFR100_cloudy)
  endif
  if sfr_threshold eq 'sfr10' then begin
     SFR_amr_with_all = double(SFR10_amr)
     SFR_hybrid_all = double(SFR10_hybrid)
     SFR_cloudy_all = double(SFR10_cloudy)
  endif
  if sfr_threshold eq 'sfr1' then begin
     SFR_amr_with_all = double(SFR1_amr)
     SFR_hybrid_all = double(SFR1_hybrid)
     SFR_cloudy_all = double(SFR1_cloudy)
  endif
  if sfr_threshold eq 'sfr0' then begin
     SFR_amr_with_all = double(SFR0_amr)
     SFR_hybrid_all = double(SFR0_hybrid)
     SFR_cloudy_all = double(SFR0_cloudy)
  endif
  in_cell_all = in_cell
  
  density_amr_with_all = double(density_amr)
  
  if lmax ne '12' then begin
     print, 'Restauration du fichier AMR sans FB '+sim_no(sim_ii)
     file_name_dat =  '/Users/oroos/Post-stage/LOPs'+sim_no(sim_ii)+'/initial_individual_sfr_'+sim_no(sim_ii)+'_all.dat'
     
     dat_file_info = file_info(file_name_dat)
     sav_file_info = file_info('/Users/oroos/Post-stage/LOPs'+sim_no(sim_ii)+'/initial_individual_sfr_'+sim_no(sim_ii)+'_all_dat.sav')
     
     if dat_file_info.mtime gt sav_file_info.mtime then begin
        readcol, '/Users/oroos/Post-stage/LOPs'+sim_no(sim_ii)+'/initial_individual_sfr_'+sim_no(sim_ii)+'_all.dat', SFR100_amr_no, SFR10_amr_no, SFR1_amr_no, SFR0_amr_no, cell_size_no, density_amr_no, format='(D,D,D,D,D,D)'
        save,  SFR100_amr_no, SFR10_amr_no, SFR1_amr_no, SFR0_amr_no, cell_size_no, density_amr_no, file='/Users/oroos/Post-stage/LOPs'+sim_no(sim_ii)+'/initial_individual_sfr_'+sim_no(sim_ii)+'_all_dat.sav'
     endif else begin
        restore,  '/Users/oroos/Post-stage/LOPs'+sim_no(sim_ii)+'/initial_individual_sfr_'+sim_no(sim_ii)+'_all_dat.sav'
     endelse 
     
     density_amr_no_all = double(density_amr_no)
     
     if sfr_threshold eq 'sfr100' then SFR_amr_no_all = double(SFR100_amr_no)
     if sfr_threshold eq 'sfr10' then SFR_amr_no_all = double(SFR10_amr_no)
     if sfr_threshold eq 'sfr1' then SFR_amr_no_all = double(SFR1_amr_no)
     if sfr_threshold eq 'sfr0' then SFR_amr_no_all = double(SFR0_amr_no)
     print, 'Fin de la restauration du fichier AMR sans FB '+sim_no(sim_ii)
  endif

;jusqu'ici OK **************************************

  
  print, 'Construction des histogrammes'
  
  help, sfr_amr_with_all
  help, sfr_hybrid_all
  
  cell_size_amr = double(cell_size_amr)
  if lmax ne '12' then cell_size_no = double(cell_size_no)


  cell_size_amr_cube = cell_size_amr^3
  if lmax ne '12' then cell_size_no_cube = cell_size_no^3
  
;test1 = sfr_amr_with_all[where(in_cell eq 1)] - sfr_hybrid_all[where(in_cell eq 1)]
;if min(test1) lt 0 then stop ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
;ce test est compris dans le suivant
  
  test4 = sfr_amr_with_all[where(in_cell ge 1)] - sfr_hybrid_all[where(in_cell ge 1)]
  if min(test4) lt 0 then stop  ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
  
  print, 'Pas de SFRapres > SFRavant (ok.)'
  
  
;--------------------------------------------------------------
  
;Comment corriger le sfr des cellules amr dont le point cloudy le plus
;proche est a 1 ou 4 taille de cellule de distance pour avoir le sfr
;des toutes les cellules de la boite a partir des histogrammes de
;densite et de sfr ??
  
;soit [rho]01 l'ensemble des rho du bin de densite [rho0;rho1]
;soit [sfr]01 l'ensemble des sfr correspondant
  
;en comparant l'histogramme de densite de toutes les cellules
;et celui des cellules 1 ou 4 uniquement, on sait que dans le bin de
;densite [rho0;rho1] on rate exactement 1-X01 cellules, ou X01 =
;N_ref/N_n_1 ou 4 

;on fait la correction pour chaque valeur de sfr N_4 ou N_1 en
;multipliant par le facteur X01 les sfr contenus dans ce bin de
;densite

;ensuite, on reconstruit l'histogramme

;atention toutefois a la gymnastique due aux logs /!\ 
;/!\/!\ le sfr nul (majorite des cellules) est mis a dummy_zero_sfr (valeur non
;atteinte par les vrais sfr non nul) par souci de compacite des
;histogrammes logs. il faut compter ces points dans le calcul du
;nombre de cellules mais les remettre a 0 ou les ignorer pour calculer
;le sfr total de la boite

;--------------------------------------------------------------

;definition des 'in_cell' et in_4_cells'
  t_4 = where(in_cell_all ge 1, nt_4) ;equivalent  t = where(x_lop ge (x_amr-4*cell_size_amr/2.) and x_lop lt (x_amr+4*cell_size_amr/2.) 
                                ; and y_lop ge (y_amr-4*cell_size_amr/2.) and y_lop lt (y_amr+4*cell_size_amr/2.)
                                ; and z_lop ge (z_amr-4*cell_size_amr/2.) and z_lop lt (z_amr+4*cell_size_amr/2.))
  t_1 = where(in_cell_all eq 1, nt_1) ;equivalent  t = where(x_lop ge (x_amr-cell_size_amr/2.) and x_lop lt (x_amr+cell_size_amr/2.)
                                ; and y_lop ge (y_amr-cell_size_amr/2.) and y_lop lt (y_amr+cell_size_amr/2.)
                                ; and z_lop ge (z_amr-cell_size_amr/2.) and z_lop lt (z_amr+cell_size_amr/2.))
  print, 'nt1 ', nt_1, ' nt4 ', nt_4
  
;correction de l'histogramme de densite
  log_density_amr_with_all = alog10(density_amr_with_all)
  if lmax ne '12' then log_density_amr_no_all = alog10(density_amr_no_all)
  
  
;faire attention aux sfr nuls (log -> infini)
  dummy_zero_sfr = floor(alog10(min([min(sfr_hybrid_all[where(sfr_hybrid_all ne 0)]/cell_size_amr_cube[where(sfr_hybrid_all ne 0)]),min(sfr_amr_with_all[where(sfr_amr_with_all ne 0)]/cell_size_amr_cube[where(sfr_amr_with_all ne 0)])])))
  
  if lmax ne '12' then dummy_zero_sfr = floor(alog10(min([min(sfr_hybrid_all[where(sfr_hybrid_all ne 0)]/cell_size_amr_cube[where(sfr_hybrid_all ne 0)]),min(sfr_amr_with_all[where(sfr_amr_with_all ne 0)]/cell_size_amr_cube[where(sfr_amr_with_all ne 0)]), min(sfr_amr_no_all[where(sfr_amr_no_all ne 0)]/cell_size_no_cube[where(sfr_amr_no_all ne 0)])])))

  print, 'Le SFR nul apparaitra avec la valeur ', dummy_zero_sfr, ' sur l histo.'
  
                                ;prendre en compte les sfr nuls (la
                                ;majorite !) -- valeur arbitraire et
                                ;               reconnaissable 

;jusqu'ici OK **************************************


  rhosfr_amr_with_all = sfr_amr_with_all/cell_size_amr_cube
  if lmax ne '12' then rhosfr_amr_no_all = sfr_amr_no_all/cell_size_no_cube
  rhosfr_hybrid_all = sfr_amr_with_all/cell_size_amr_cube



  log_rhosfr_amr_with_all = alog10(rhosfr_amr_with_all)
  t = where(sfr_amr_with_all eq 0)
  log_rhosfr_amr_with_all[t] = dummy_zero_sfr 
  cell_size_amr_cube_forlogwith = cell_size_amr_cube ;a zero pour ne pas avoir a corriger
  cell_size_amr_cube_forlogwith[t] = 0
  
  if lmax ne '12' then begin  
     log_rhosfr_amr_no_all = alog10(rhosfr_amr_no_all)
     t = where(sfr_amr_no_all eq 0)
     log_rhosfr_amr_no_all[t] = dummy_zero_sfr
     cell_size_amr_cube_forlogno = cell_size_no_cube ;a zero pour ne pas avoir a corriger
     cell_size_amr_cube_forlogno[t] = 0
  endif
  
  log_rhosfr_hybrid_all = alog10(rhosfr_hybrid_all)
  t = where(sfr_hybrid_all eq 0)
  log_rhosfr_hybrid_all[t] = dummy_zero_sfr
  cell_size_amr_cube_forloghybrid = cell_size_amr_cube ;a zero pour ne pas avoir a corriger
  cell_size_amr_cube_forloghybrid[t] = 0

;jusqu'ici OK **************************************
;------------------------- test hist2d

  bins2d = [0.3,0.3]

  mn=min(log_density_amr_with_all)
  mn=[mn,alog10(boxlen/2^fix(lmax))] ;BOXLEN, LMAX

  mx=max(log_density_amr_with_all)
  mx=[mx,max(alog10(cell_size_amr))]

  if lmax ne '12' then begin
     mn=min([min(log_density_amr_with_all),min(log_density_amr_no_all)])
     mn=[mn,alog10(boxlen/2^fix(lmax))] ;BOXLEN, LMAX
     mx=max([max(log_density_amr_with_all),max(log_density_amr_no_all)])
     mx=[mx,max([max(alog10(cell_size_amr)),max(alog10(cell_size_no))])]
  endif

;jusqu'ici ??? **************************************
;stop  


  test=hist_nd(transpose([[log_density_amr_with_all],[alog10(cell_size_amr)]]),bins2d, min=mn, max=mx, rev=ri)
  test_4=hist_nd(transpose([[log_density_amr_with_all[t_4]],[alog10(cell_size_amr[t_4])]]),bins2d, min=mn, max=mx, rev=ri_4)
  test_1=hist_nd(transpose([[log_density_amr_with_all[t_1]],[alog10(cell_size_amr[t_1])]]),bins2d, min=mn, max=mx, rev=ri_1)
  rgx=[mn(0),mx(0)]
  rgy=[mn(1),mx(1)]

  print, rgx, rgy

;       REVERSE_INDICES: Set to a named variable to receive the
;         reverse indices, for mapping which points occurred in a
;         given bin.  Note that this is a 1-dimensional reverse index
;         vector (see HISTOGRAM).  E.g., to find the indices of points
;         which fell in a histogram bin [i,j,k], look up:
;
;             ind=[i+nx*(j+ny*k)]
;             ri[ri[ind]:ri[ind+1]-1]
;
;         See also ARRAY_INDICES for converting in the other
;         direction.
;here, 2d -> i, nx, j
;nx = number of bins of first dimension (=(size(test))[1])

;jusqu'ici CASSE **************************************

  den_bins2d=dblarr((size(test))[1])
  size_bins2d=dblarr((size(test))[2])

  offset = 0.5
  nbin   = long((mx-mn)/bins2d+1)

  den_bins2d = mn(0) + (LINDGEN(nbin(0))+offset)*bins2d(0)
  size_bins2d = mn(1) + (LINDGEN(nbin(1))+offset)*bins2d(1)

  test_corrected_1 = test_1*0.
  test_corrected_4 = test_4*0.

;jusqu'ici CASSE

;nx = (size(test))[1]
;for j=0,n_elements(size_bins2d)-1 do begin
;for i=0,n_elements(den_bins2d)-1 do begin
; ind=[i+nx*j]
;;if ri[ind] ne ri[ind+1] then  ri[ri[ind]:ri[ind+1]-1]=42 ;test pour voir si tous les bins sont atteints ; ok
;if ri_1[ind] ne ri_1[ind+1] then  test_corrected_1(i,j) = n_elements(ri_1[ri_1[ind]:ri_1[ind+1]-1])*double(test(i,j))/double(test_1(i,j)) 
;endfor
;endfor     ;version boucle des lignes suivantes (devra etre utilisee pour la correction du sfr)

  correction2d_1 = test_1*0.
  correction2d_4 = test_4*0.

  correction2d_1[where(test_1 ne 0)] = double(test[where(test_1 ne 0)])/double(test_1[where(test_1 ne 0)])
  correction2d_4[where(test_4 ne 0)] = double(test[where(test_4 ne 0)])/double(test_4[where(test_4 ne 0)])

  log_test=alog10(test)
  t=where(test eq 0)
  log_test[t]=-1

  log_test_4=alog10(test_4)
  t=where(test_4 eq 0)
  log_test_4[t]=-1

  log_test_1=alog10(test_1)
  t=where(test_1 eq 0)
  log_test_1[t]=-1

  log_diff_4_corrected=alog10(abs(test-round(test_4*correction2d_4)))
  t=where(abs(test-round(test_4*correction2d_4)) eq 0)
  log_diff_4_corrected[t]=-1

  log_diff_1_corrected=alog10(abs(test-round(test_1*correction2d_1)))
  t=where(abs(test-round(test_1*correction2d_1)) eq 0)
  log_diff_1_corrected[t]=-1

  ;stop

  if max(log_diff_4_corrected) ne -1 then begin ;s'il y a des bins vides a n=4, agrandir le critere a n=8 juste pour ceux-la

     t = where(in_cell_all eq 0)
     t_8 = where(x_lop[t] ge (x_amr[t]-8*cell_size_amr[t]/2.) and x_lop[t] lt (x_amr[t]+8*cell_size_amr[t]/2.) and y_lop[t] ge (y_amr[t]-8*cell_size_amr[t]/2.) and y_lop[t] lt (y_amr[t]+8*cell_size_amr[t]/2.) and z_lop[t] ge (z_amr[t]-8*cell_size_amr[t]/2.) and z_lop[t] lt (z_amr[t]+8*cell_size_amr[t]/2.))


     test_8=hist_nd(transpose([[log_density_amr_with_all[t_8]],[alog10(cell_size_amr[t_8])]]),bins2d, min=mn, max=mx, rev=ri_8)

     correction2d_8 = test_8*0.

     correction2d_8[where(test_8 ne 0)] = double(test[where(test_8 ne 0)])/double(test_8[where(test_8 ne 0)])


     log_test_8=alog10(test_8)
     t=where(test_8 eq 0)
     log_test_8[t]=-1

     log_diff_8_corrected=alog10(abs(test-round(test_8*correction2d_8)))
     t=where(abs(test-round(test_8*correction2d_8)) eq 0)
     log_diff_8_corrected[t]=-1


  endif
;a partir d'ici PROBLEME **************************************


;;;;test
;t_16 = where(x_lop ge (x_amr-16*cell_size_amr/2.) and x_lop lt (x_amr+16*cell_size_amr/2.) and y_lop ge (y_amr-16*cell_size_amr/2.) and y_lop lt (y_amr+16*cell_size_amr/2.) and z_lop ge (z_amr-16*cell_size_amr/2.) and z_lop lt (z_amr+16*cell_size_amr/2.))
;test_16=hist_nd(transpose([[log_density_amr_with_all[t_16]],[alog10(cell_size_amr[t_16])]]),bins2d, min=mn, max=mx, rev=ri_16)
;correction2d_16 = test_16*0.
;correction2d_16[where(test_16 ne 0)] = double(test[where(test_16 ne 0)])/double(test_16[where(test_16 ne 0)])
;log_test_16=alog10(test_16)
;t=where(test_16 eq 0)
;log_test_16[t]=-1
;log_diff_16_corrected=alog10(abs(test-round(test_16*correction2d_16)))
;t=where(abs(test-round(test_16*correction2d_16)) eq 0)
;log_diff_16_corrected[t]=-1

;;;;;pour le 00108x1,sfr100 : nb d'elements de t_16 = 2*nb d'elements de t_8 mais aucun point manquant recupere (cellules de de faible densite donc pas tres grave)


  window, 1 
  loadct,39
  plotimage, log_test, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)'  
  window, 10
  plotimage, log_test_4, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)' 
  window, 11
  plotimage, log_test_1, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)'
  window, 12
  plotimage, log_diff_1_corrected, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : log(ref-corr)'
  window, 13
  plotimage, log_diff_4_corrected, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : log(ref-corr)'

  ;stop

  window,3
  ttest=fltarr((size(test))[1])
  for k=0,(size(test))[2]-1 do ttest += test(*,k)
  plot, ttest, psym=10, /ylog
  for k=0,(size(test))[2]-1 do oplot, test(*,k), psym=10, color=k*1000*randomu(seed)

  window, 2
  plothist, alog10(cell_size_amr), bin=0.3d, title='Cell sizes : all cells/in 4 cells/in 1 cell', /ylog, xr=[-3,0], yr=[1,1e8], /ystyle, min=-7d, omin=mino, omax=maxo, reverse_indices=rev_in, /l64
  plothist, alog10(cell_size_amr), size_bins, sizenumber_per_bin, bin=0.3d, /overplot, min=mino, max=maxo, color=!green
  plothist, alog10(cell_size_amr[t_4]), size4_bins, sizenumber4_per_bin,bin=0.3d, /overplot, min=mino, max=maxo, color=!magenta
  plothist, alog10(cell_size_amr[t_1]), size1_bins, sizenumber1_per_bin,bin=0.3d, /overplot, min=mino, max=maxo, color=!yellow
  if lmax ne '12' then plothist, alog10(cell_size_no), bin=0.3d, /overplot, min=mino, max=maxo, color=!blue


  test_totrho_1 = 0.
  test_totrho_4 = 0.
  test_totrho = 0.

  test_totsfr_1 = 0.
  test_totsfr_4 = 0.

  check_test_totsfr_1 = 0.
  check_test_totsfr_4 = 0.
  check_test_totsfr = 0.

  check_test_totrhosfr_1 = 0.
  check_test_totrhosfr_4 = 0.
  check_test_totrhosfr = 0.

  cntr4 = 0
  cntr11 = 0                    ; compter le nombre de bins vides corriges par les bins de reference
  cntr12 = 0 

  nx = (size(test))[1]

  for i=0,n_elements(den_bins2d)-1 do begin
     for j=0,n_elements(size_bins2d)-1 do begin
        ind=[i+nx*j]
;if ri[ind] ne ri[ind+1] then  ri[ri[ind]:ri[ind+1]-1]=42 ;test pour voir si tous les bins sont atteints ; ok
        if ri_1[ind] ne ri_1[ind+1] then  test_totrho_1 = test_totrho_1 + total(correction2d_1(i,j)*density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])
        if ri_4[ind] ne ri_4[ind+1] then  test_totrho_4 = test_totrho_4 + total(correction2d_4(i,j)*density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])
        if ri[ind]   ne ri[ind+1]   then  test_totrho   = test_totrho   + total(density_amr_with_all[ri[ri[ind]:ri[ind+1]-1]])

        if ri_1[ind] ne ri_1[ind+1] then  check_test_totsfr_1 = check_test_totsfr_1 + total(correction2d_1(i,j)*sfr_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])
        if ri_4[ind] ne ri_4[ind+1] then  check_test_totsfr_4 = check_test_totsfr_4 + total(correction2d_4(i,j)*sfr_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])
        if ri[ind]   ne ri[ind+1]   then  check_test_totsfr   = check_test_totsfr   + total(sfr_amr_with_all[ri[ri[ind]:ri[ind+1]-1]])

        if ri_1[ind] ne ri_1[ind+1] then  check_test_totrhosfr_1 = check_test_totrhosfr_1 + total(correction2d_1(i,j)*rhosfr_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])
        if ri_4[ind] ne ri_4[ind+1] then  check_test_totrhosfr_4 = check_test_totrhosfr_4 + total(correction2d_4(i,j)*rhosfr_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])
        if ri[ind]   ne ri[ind+1]   then  check_test_totrhosfr   = check_test_totrhosfr   + total(rhosfr_amr_with_all[ri[ri[ind]:ri[ind+1]-1]])

        if ri_1[ind] ne ri_1[ind+1] then  test_totsfr_1 = test_totsfr_1 + total(correction2d_1(i,j)*sfr_hybrid_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])
        if ri_4[ind] ne ri_4[ind+1] then  test_totsfr_4 = test_totsfr_4 + total(correction2d_4(i,j)*sfr_hybrid_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])

        if max(log_diff_4_corrected) ne -1 then begin
;comment corriger les bins vides en N=4 et N=1 ?? prendre le bin de densite de reference
           if ri_4[ind] eq ri_4[ind+1] and ri_8[ind] ne ri_8[ind+1] then begin ;si on loupe un bin de densite dans N=4, alors on prend le bin N=8. s'il n'existe pas, tant pis
              test_totrho_4 = test_totrho_4 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              check_test_totrhosfr_4 = check_test_totrhosfr_4 + total(correction2d_8(i,j)*rhosfr_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              check_test_totsfr_4 = check_test_totsfr_4 + total(correction2d_8(i,j)*sfr_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              test_totsfr_4 = test_totsfr_4 + total(correction2d_8(i,j)*sfr_hybrid_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=4
              cntr4 += 1
           endif
        endif

        if ri_1[ind] eq ri_1[ind+1] and ri_4[ind] ne ri_4[ind+1] then begin          ;si on loupe un bin de densite dans N=1, alors on prend le bin amr N=4
           t_41 = where(in_cell_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]] gt 1, nt_41) ; ne pas recompter les cellules deja comptees
           if nt_41 ne 0 then begin

              test_totrho_1 = test_totrho_1 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41])
              check_test_totrhosfr_1 = check_test_totrhosfr_1 + total(correction2d_4(i,j)*(rhosfr_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41])
              check_test_totsfr_1 = check_test_totsfr_1 + total(correction2d_4(i,j)*(sfr_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41])
              test_totsfr_1 = test_totsfr_1 + total(correction2d_4(i,j)*(sfr_hybrid_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=1 
              cntr11 += 1
           endif
        endif

        if max(log_diff_4_corrected) ne -1 then begin
           if ri_1[ind] eq ri_1[ind+1] and ri_4[ind] eq ri_4[ind+1] and ri_8[ind] ne ri_8[ind+1] then begin ;si on loupe un bin de densite dans N=1 et N=4, alors on prend le bin N=8. s'il n'existe pas, tant pis (tres probablement des bins de tres faible densite qui ne contribuent pas au sfr)
              test_totrho_1 = test_totrho_1 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              check_test_totrhosfr_1 = check_test_totrhosfr_1 + total(correction2d_8(i,j)*rhosfr_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              check_test_totsfr_1 = check_test_totsfr_1 + total(correction2d_8(i,j)*sfr_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])
              test_totsfr_1 = test_totsfr_1 + total(correction2d_8(i,j)*sfr_hybrid_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=1 ou 4
              cntr12 += 1
           endif
        endif
     endfor
  endfor                        ;version boucle des lignes precedentes



  print, total(density_amr_with_all), test_totrho, test_totrho_1, test_totrho_4
  print, total(sfr_amr_with_all), check_test_totsfr, check_test_totsfr_1, check_test_totsfr_4
  print, cntr11, cntr12, cntr4
;stop

;j=0
;ind=fltarr(nx)
;for i=0,n_elements(den_bins2d)-1 do  ind(i)=[i+nx*j]
;for i=0,n_elements(den_bins2d)-1 do if ri_1[ind(i)] ne ri_1[ind(i)+1] then  test_totrho_1 = test_totrho_1 + correction2d_1(i,j)*density_amr_with_all[t_1[ri_1[ri_1[ind(i)]:ri_1[ind(i)+1]-1]]]



;----------------------------------- fin test hist2d

  window, 30
  plothist, log_density_amr_with_all, bin=0.3d, title='Density : all cells/in 4 cells/in 1 cell', /ylog, xr=[-7,7], yr=[1,1e8], /ystyle, min=-7d, omin=mino, omax=maxo, /l64
  plothist, log_density_amr_with_all, bin=0.3d, amr_density_bins, number_per_bin, color=!red, /overplot, min=mino, max=maxo, reverse_indices=rev_in, /l64
  if lmax ne '12' then plothist, log_density_amr_no_all, bin=0.3d, color=!blue, /overplot, min=mino, max=maxo, /l64
  plothist, log_density_amr_with_all[t_4], amr4_density_bins, number4_per_bin, bin=0.3d, /overplot, color=!magenta, min=mino, max=maxo, reverse_indices=rev_in4, /l64
  plothist, log_density_amr_with_all[t_1], amr1_density_bins, number1_per_bin, bin=0.3d, /overplot, color=!orange, min=mino, max=maxo, reverse_indices=rev_in1, /l64
  p_den = !p & x_den = !x & y_den = !y
  
  check_sfr_corrige = rhosfr_amr_with_all*0.d
  for ii = 0, n_elements(number_per_bin)-1 do $
     IF rev_in[ii] NE rev_in[ii+1] THEN $ 
;pour l'ensemble des sfr compris dans chaque bin de densite,
;prendre la valeur et la multiplier par la correction, divisee par le
;nombre d'elements dans chaque bin (exactement comme pour la densite)
     check_sfr_corrige[rev_in[rev_in[ii]:rev_in[ii+1]-1]] = rhosfr_amr_with_all[rev_in[rev_in[ii]:rev_in[ii+1]-1]]*cell_size_amr_cube[rev_in[rev_in[ii]:rev_in[ii+1]-1]]

  check_sfr_corrige[where(sfr_amr_with_all eq 0)] = 0 ;normalement pas necessaire

;prendre en compte les bins vides pour in_cell ou in_4_cells
  missing_values1 = setdifference(round(100.*amr_density_bins),round(100.*amr1_density_bins), count=n_missing1)
  missing_values4 = setdifference(round(100.*amr_density_bins),round(100.*amr4_density_bins), count=n_missing4)
  
  number_per_bin_adj1 = number_per_bin
  number_per_bin_adj4 = number_per_bin
  
  if n_missing1 ne 0 then begin
     for kk = 0, n_missing1-1 do $
        if kk eq 0 then rem = where(round(100.d*amr_density_bins) eq missing_values1(kk), nrem) $
        else rem = [rem,where(round(100.d*amr_density_bins) eq missing_values1(kk), nrem)]
     remove, rem, number_per_bin_adj1
  endif
  
  if n_missing4 ne 0 then begin
     for kk = 0, n_missing4-1 do begin
        if kk eq 0 then rem = where(round(100.d*amr_density_bins) eq missing_values4(kk), nrem) $
        else rem = [rem,where(round(100.d*amr_density_bins) eq missing_values4(kk), nrem)]
     endfor
     remove, rem, number_per_bin_adj4
  endif
  
;print, amr_density_bins
;print, amr4_density_bins
;print, amr1_density_bins
  
;correction pour avoir le sfr sur toute la boite
;hypothese : tous les points amr compris entre rho_i et rho_i+1 = 2*rho_i ont la meme distribution de temperature
; NB : 10^0.3 ~= 2, d'ou la largeur des bins
  
;print, amr_density_bins
  
  
;calculer la correction 
  N_rho_1 = number1_per_bin*1d
  N_rho_4 = number4_per_bin*1d
  
  R = rev_in4                   ;calcul du facteur de correction
  for i = 0, n_elements(number_per_bin_adj4)-1 do $
     IF R[i] NE R[i+1] THEN N_rho_4(i) = n_elements(R[R[I] : R[i+1]-1])*(double(number_per_bin_adj4(i))/double(number4_per_bin(i))) 
  
  rho_corrige_4 = log_density_amr_with_all[t_4]*0.d ;calcul de la densite corrigee
  for ii = 0, n_elements(number4_per_bin)-1 do $
     IF rev_in4[ii] NE rev_in4[ii+1] THEN $
        rho_corrige_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = density_amr_with_all[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*N_rho_4(ii)/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])

  
  R = rev_in1
  for i = 0L, n_elements(number_per_bin_adj1)-1 do $
     IF R[i] NE R[i+1] THEN N_rho_1(i) = n_elements(R[R[I] : R[i+1]-1])*(double(number_per_bin_adj1(i))/double(number1_per_bin(i)))    

  rho_corrige_1 = log_density_amr_with_all[t_1]*0.d
  for ii = 0, n_elements(number1_per_bin)-1 do $
     IF rev_in1[ii] NE rev_in1[ii+1] THEN $
        rho_corrige_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = density_amr_with_all[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*N_rho_1(ii)/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]) 


                                ;trace des histogrammes corriges
  oplot, amr4_density_bins, N_rho_4, color=!dred, psym=10
  oplot, amr1_density_bins, N_rho_1, color=!brown, psym=10 ;ok. les deux histo corriges recouvrent parfaitement l'histo total
                                ;aux bins vides pres
  
;correction coherente ?
  help, density_amr_with_all
  print, total(ulong64(number_per_bin))
  print, total(ulong64(N_rho_1)), total(ulong64(N_rho_4))
  print, ulong64(n_elements(density_amr_with_all))-total(ulong64(number_per_bin))
  print, ulong64(n_elements(density_amr_with_all))-total(ulong64(N_rho_1)), ulong64(n_elements(density_amr_with_all))-total(ulong64(N_rho_4))
;les points qui manquent ici viennent des bins de densite non couverts
;par les N=4 ou N=1... comment faire ? ---> pas tres grave (qq
;dizaines sur 23 millions) et on ne peut pas les corriger car
;il n'y a pas de points
  
  print, total(density_amr_with_all)
  print, total(rho_corrige_1)
  print, total(rho_corrige_4)   ;valeur coherentes
  print, 'Erreur : ', abs(total(density_amr_with_all)-total(rho_corrige_1))/total(density_amr_with_all)*100d, ' % et ',  abs(total(density_amr_with_all)-total(rho_corrige_4))/total(density_amr_with_all)*100d, ' %.'
                                ;peut-on diminuer cette erreur ??

  
;sanity check
;test1 = rhosfr_amr_with_all[t_1]*cell_size_amr_cube[t_1] - rhosfr_hybrid_all[t_1]*cell_size_amr_cube[t_1]
;if min(test1) lt 0 then stop ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
;ce test est compris dans le suivant
  
  test4 = rhosfr_amr_with_all[t_4]*cell_size_amr_cube[t_4] - rhosfr_hybrid_all[t_4]*cell_size_amr_cube[t_4]
  if min(test4) lt 0 then stop  ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
  
  
;test1 = rhosfr_amr_with_all[t_1]- rhosfr_hybrid_all[t_1]
;if min(test1) lt 0 then stop ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
;ce test est compris dans le suivant
  
;test4 = rhosfr_amr_with_all[t_4] - rhosfr_hybrid_all[t_4]
;if min(test4) lt 0 then stop ;si le sfr apres est plus grand que le sfr avant alors il y a un probleme
;inutile, inclus dans les precedents
  
  print, 'Pas de SFRapres > SFRavant (ok.)'
  
;appliquer la correction au sfr

  check_rhosfr_corrige_4 = rhosfr_amr_with_all[t_4]*0.d
  rhosfr_corrige_4 = rhosfr_hybrid_all[t_4]*0.d
  check_sfr_corrige_4 = rhosfr_amr_with_all[t_4]*0.d
  sfr_corrige_4 = rhosfr_hybrid_all[t_4]*0.d
  check_number_4 = rhosfr_hybrid_all[t_4]*0.d

;for ii = 0, n_elements(number4_per_bin)-1 do IF rev_in4[ii] NE rev_in4[ii+1] THEN $

  for ii = 0, n_elements(number4_per_bin)-1 do begin
     IF rev_in4[ii] NE rev_in4[ii+1] THEN begin

        check_rhosfr_corrige_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = rhosfr_amr_with_all[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*double(N_rho_4(ii))/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])

        rhosfr_corrige_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = rhosfr_hybrid_all[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*double(N_rho_4(ii))/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])

        check_sfr_corrige_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = rhosfr_amr_with_all[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*cell_size_amr_cube[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*double(N_rho_4(ii))/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])

        sfr_corrige_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = rhosfr_hybrid_all[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*cell_size_amr_cube[t_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]]]*double(N_rho_4(ii))/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])

;verif :
        check_number_4[rev_in4[rev_in4[ii]:rev_in4[ii+1]-1]] = double(N_rho_4(ii))/n_elements(rev_in4[rev_in4[ii]:rev_in4[ii+1]-1])


     endif
  endfor

  sfr_corrige_4[where(sfr_hybrid_all[t_4] eq 0)] = 0
  check_sfr_corrige_4[where(sfr_amr_with_all[t_4] eq 0)] = 0
  rhosfr_corrige_4[where(sfr_hybrid_all[t_4] eq 0)] = 0
  check_rhosfr_corrige_4[where(sfr_amr_with_all[t_4] eq 0)] = 0


;if abs(total(check_number_4) - total(n_rho_4)) gt 1d-4 then stop

  check_rhosfr_corrige_1 = rhosfr_amr_with_all[t_1]*0.d
  rhosfr_corrige_1 = rhosfr_hybrid_all[t_1]*0.d
  check_sfr_corrige_1 = rhosfr_amr_with_all[t_1]*0.d
  sfr_corrige_1 = rhosfr_hybrid_all[t_1]*0.d
  check_number_1 = rhosfr_hybrid_all[t_1]*0.d


;for ii = 0, n_elements(number1_per_bin)-1 do IF rev_in1[ii] NE rev_in1[ii+1] THEN $

  for ii = 0, n_elements(number1_per_bin)-1 do begin
     IF rev_in1[ii] NE rev_in1[ii+1] THEN begin

        check_rhosfr_corrige_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = rhosfr_amr_with_all[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*double(N_rho_1(ii))/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1])

        rhosfr_corrige_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = rhosfr_hybrid_all[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*double(N_rho_1(ii))/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1])

        check_sfr_corrige_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = rhosfr_amr_with_all[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*cell_size_amr_cube[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*double(N_rho_1(ii))/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1])

;pour l'ensemble des sfr compris dans chaque bin de densite,
;prendre la valeur et la multiplier par la correction, divisee par le
;nombre d'elements dans chaque bin (exactement comme pour la densite)
        sfr_corrige_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = rhosfr_hybrid_all[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*cell_size_amr_cube[t_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]]]*double(N_rho_1(ii))/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1])

;verif :
        check_number_1[rev_in1[rev_in1[ii]:rev_in1[ii+1]-1]] = double(N_rho_1(ii))/n_elements(rev_in1[rev_in1[ii]:rev_in1[ii+1]-1])


     endif
  endfor

  sfr_corrige_1[where(sfr_hybrid_all[t_1] eq 0)] = 0 ;normalement pas necessaire
  check_sfr_corrige_1[where(sfr_amr_with_all[t_1] eq 0)] = 0
  rhosfr_corrige_1[where(sfr_hybrid_all[t_1] eq 0)] = 0
  check_rhosfr_corrige_1[where(sfr_amr_with_all[t_1] eq 0)] = 0


;if abs(total(check_number_1) - total(n_rho_1)) gt 1d-4 then stop ;doivent etre egaux sinon on a perdu des points en chemin
;nb : check_number_1 n'est pas entier, sinon gros problemes d'arrondi

  print, 'Total corrected_Sfr4 (histo1d) : ', total(sfr_corrige_4), ' Ms/yr.'
  print, 'Total corrected_Sfr1 (histo1d) : ', total(sfr_corrige_1), ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with4 (check, histo1d) : ', total(check_sfr_corrige_4), ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with1 (check, histo1d) : ', total(check_sfr_corrige_1), ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with (check, histo1d) : ', total(check_sfr_corrige), ' Ms/yr.'
  print, 'Total SFR_amr_with : ',   total(sfr_amr_with_all), ' Ms/yr.'
  if lmax ne '12' then print, 'Total SFR_amr_no : ',     total(sfr_amr_no_all), ' Ms/yr.'

  print, 'Error on total density : (4 and 1, histo1d) ',  abs(total(density_amr_with_all)-total(rho_corrige_4))/total(density_amr_with_all)*100d, ' % and ',  abs(total(density_amr_with_all)-total(rho_corrige_1))/total(density_amr_with_all)*100d, ' %.'
  print, 'Number of missing points : (4 and 1, histo1d) ', total(double(number_per_bin))-total(double(n_rho_4)), ' and ', total(double(number_per_bin))-total(double(n_rho_1)), ' (', (total(double(number_per_bin))-total(double(n_rho_4)))/double(n_elements(in_cell))*100d, ' % and ',  (total(double(number_per_bin))-total(double(n_rho_1)))/double(n_elements(in_cell))*100d, ' %).'
  print, 'Error on total rhoSFR : (4 and 1, histo1d) ',  (total(rhosfr_amr_with_all[where(sfr_amr_with_all ne 0)])-total(check_rhosfr_corrige_4))/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' % and ',  (total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])-total(check_rhosfr_corrige_1))/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' %.'
  print, 'Error on total SFR : (4 and 1, histo1d) ',  (total(sfr_amr_with_all)-total(check_sfr_corrige_4))/total(SFR_amr_with_all)*100d, ' % and ',  (total(SFR_amr_with_all)-total(check_sfr_corrige_1))/total(SFR_amr_with_all)*100d, ' %.'
  print, 'Number of missing points : (4 and 1, histo1d) ', total(double(number_per_bin))-total(double(check_number_4)), ' and ', total(double(number_per_bin))-total(double(check_number_1)), ' (', (total(double(number_per_bin))-total(double(check_number_4)))/double(n_elements(in_cell))*100d, ' % and ' , (total(double(number_per_bin))-total(double(check_number_1)))/double(n_elements(in_cell))*100d, ' %).'
  print, '-------------'
  print, 'Total corrected_Sfr4 (histo2d) : ', test_totsfr_4, ' Ms/yr.'
  print, 'Total corrected_Sfr1 (histo2d) : ', test_totsfr_1, ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with4 (check, histo2d) : ', check_test_totsfr_4, ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with1 (check, histo2d) : ', check_test_totsfr_1, ' Ms/yr.'
  print, 'Total corrected_sfr_amr_with (check, histo2d) : ', check_test_totsfr, ' Ms/yr.'
  print, 'Total SFR_amr_with : ',   total(sfr_amr_with_all), ' Ms/yr.'
  if lmax ne '12' then print, 'Total SFR_amr_no : ',     total(sfr_amr_no_all), ' Ms/yr.'

  print, 'Error on total density : (4 and 1, histo2d) ',  abs(total(density_amr_with_all)-test_totrho_4)/total(density_amr_with_all)*100d, ' % and ',  abs(total(density_amr_with_all)-test_totrho_1)/total(density_amr_with_all)*100d, ' %.'
  print, 'Number of missing points : (4 and 1, histo2d) ', total(double(number_per_bin))-ulong64(total(correction2d_4*test_4)), ' and ', total(double(number_per_bin))-ulong64(total(correction2d_1*test_1)), ' (', (total(double(number_per_bin))-ulong64(total(correction2d_4*test_4)))/double(n_elements(in_cell))*100d, ' % and ',  (total(double(number_per_bin))-ulong64(total(correction2d_1*test_1)))/double(n_elements(in_cell))*100d, ' %).'
  print, 'Error on total rhoSFR : (4 and 1, histo2d) ',  (total(rhosfr_amr_with_all[where(sfr_amr_with_all ne 0)])-check_test_totrhosfr_4)/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' % and ',  (total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])-check_test_totrhosfr_1)/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' %.'
  print, 'Error on total SFR : (4 and 1, histo2d) ',  (total(sfr_amr_with_all)-check_test_totsfr_4)/total(SFR_amr_with_all)*100d, ' % and ',  (total(SFR_amr_with_all)-check_test_totsfr_1)/total(SFR_amr_with_all)*100d, ' %.'
  print, 'Number of missing points : (4 and 1, histo2d) : same.'


;comment ameliorer le calcul ? pourquoi l'erreur s'amplifie (a ce point) ?
  
  
  openw, lun, nm+res+'corrected_total_sfr_'+sim_with(sim_ii)+'-'+sim_no(sim_ii)+'_threshold_'+sfr_threshold+'.txt', /get_lun
  printf, lun, 'Total corrected_Sfr4 (histo1d) : ', total(sfr_corrige_4), ' Ms/yr.'
  printf, lun, 'Total corrected_Sfr1 (histo1d) : ', total(sfr_corrige_1), ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with4 (check, histo1d) : ', total(check_sfr_corrige_4), ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with1 (check, histo1d) : ', total(check_sfr_corrige_1), ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with (check, histo1d) : ', total(check_sfr_corrige), ' Ms/yr.'
  printf, lun, 'Total SFR_amr_with : ',   total(sfr_amr_with_all), ' Ms/yr.'
  if lmax ne '12' then printf, lun, 'Total SFR_amr_no : ',     total(sfr_amr_no_all), ' Ms/yr.'

  printf, lun, 'Error on total density : (4 and 1, histo1d) ',  abs(total(density_amr_with_all)-total(rho_corrige_4))/total(density_amr_with_all)*100d, ' % and ',  abs(total(density_amr_with_all)-total(rho_corrige_1))/total(density_amr_with_all)*100d, ' %.'
  printf, lun, 'Number of missing points : (4 and 1, histo1d) ', total(double(number_per_bin))-total(double(n_rho_4)), ' and ', total(double(number_per_bin))-total(double(n_rho_1)), ' (', (total(double(number_per_bin))-total(double(n_rho_4)))/double(n_elements(in_cell))*100d, ' % and ',  (total(double(number_per_bin))-total(double(n_rho_1)))/double(n_elements(in_cell))*100d, ' %).'
  printf, lun, 'Error on total rhoSFR : (4 and 1, histo1d) ',  (total(rhosfr_amr_with_all[where(sfr_amr_with_all ne 0)])-total(check_rhosfr_corrige_4))/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' % and ',  (total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])-total(check_rhosfr_corrige_1))/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' %.'
  printf, lun, 'Error on total SFR : (4 and 1, histo1d) ',  (total(sfr_amr_with_all)-total(check_sfr_corrige_4))/total(SFR_amr_with_all)*100d, ' % and ',  (total(SFR_amr_with_all)-total(check_sfr_corrige_1))/total(SFR_amr_with_all)*100d, ' %.'
  printf, lun, 'Number of missing points : (4 and 1, histo1d) ', total(double(number_per_bin))-total(double(check_number_4)), ' and ', total(double(number_per_bin))-total(double(check_number_1)), ' (', (total(double(number_per_bin))-total(double(check_number_4)))/double(n_elements(in_cell))*100d, ' % and ' , (total(double(number_per_bin))-total(double(check_number_1)))/double(n_elements(in_cell))*100d, ' %).'
  printf, lun, '-------------'
  printf, lun, 'Total corrected_Sfr4 (histo2d) : ', test_totsfr_4, ' Ms/yr.'
  printf, lun, 'Total corrected_Sfr1 (histo2d) : ', test_totsfr_1, ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with4 (check, histo2d) : ', check_test_totsfr_4, ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with1 (check, histo2d) : ', check_test_totsfr_1, ' Ms/yr.'
  printf, lun, 'Total corrected_sfr_amr_with (check, histo2d) : ', check_test_totsfr, ' Ms/yr.'
  printf, lun, 'Total SFR_amr_with : ',   total(sfr_amr_with_all), ' Ms/yr.'
  if lmax ne '12' then printf, lun, 'Total SFR_amr_no : ',     total(sfr_amr_no_all), ' Ms/yr.'

  printf, lun, 'Error on total density : (4 and 1, histo2d) ',  abs(total(density_amr_with_all)-test_totrho_4)/total(density_amr_with_all)*100d, ' % and ',  abs(total(density_amr_with_all)-test_totrho_1)/total(density_amr_with_all)*100d, ' %.'
  printf, lun, 'Number of missing points : (4 and 1, histo2d) ', total(double(number_per_bin))-ulong64(total(correction2d_4*test_4)), ' and ', total(double(number_per_bin))-ulong64(total(correction2d_1*test_1)), ' (', (total(double(number_per_bin))-ulong64(total(correction2d_4*test_4)))/double(n_elements(in_cell))*100d, ' % and ',  (total(double(number_per_bin))-ulong64(total(correction2d_1*test_1)))/double(n_elements(in_cell))*100d, ' %).'
  printf, lun, 'Error on total rhoSFR : (4 and 1, histo2d) ',  (total(rhosfr_amr_with_all[where(sfr_amr_with_all ne 0)])-check_test_totrhosfr_4)/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' % and ',  (total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])-check_test_totrhosfr_1)/total(rhoSFR_amr_with_all[where(sfr_amr_with_all ne 0)])*100d, ' %.'
  printf, lun, 'Error on total SFR : (4 and 1, histo2d) ',  (total(sfr_amr_with_all)-check_test_totsfr_4)/total(SFR_amr_with_all)*100d, ' % and ',  (total(SFR_amr_with_all)-check_test_totsfr_1)/total(SFR_amr_with_all)*100d, ' %.'
  printf, lun, 'Number of missing points : (4 and 1, histo2d) : same.'

  close, lun
  free_lun, lun


;histogrammes non corriges
  bin_sfr = 0.12d0              ;(no more) adjusted to match the number of density bins ;utile ?
  
  window, 29
  plothist, log_rhoSFR_amr_with_all, bin=bin_sfr, title='rhoSFR no FB / with FB / with FB + CLOUDY : all cells', /ylog, yr=[1,1e8], /ystyle, omin=mino, omax=maxo, /l64
  if lmax ne '12' then plothist, log_rhoSFR_amr_no_all, bin=bin_sfr, color=!blue, /overplot, min=mino, max=maxo, /l64
  plothist, log_rhoSFR_amr_with_all, bin=bin_sfr, sfr_bins, sfrnumber_per_bin, color=!green, /overplot, min=mino, max=maxo, /l64
;plothist, log_SFR_hybrid_all, bin=bin_sfr, color=!red, /overplot, min=mino, max=maxo ;not corrected
  p_sfr = !p & x_sfr = !x & y_sfr = !y
  
  help, sfr_bins
  help, amr_density_bins
;;stop
  
  sfrnumber_per_bin_adj4 = sfrnumber_per_bin
  sfrnumber_per_bin_adj1 = sfrnumber_per_bin
  
  window, 28
  plothist, log_rhoSFR_amr_with_all[t_4], bin=bin_sfr, title='rhoSFR before/after CLOUDY  : in 4 cells', /ylog, yr=[1,1e8], /ystyle, omin=mino, omax=maxo, /l64
  if nt_4 gt 0 then plothist, log_rhoSFR_amr_with_all[t_4], check_sfr4_bins, check_sfrnumber4_per_bin,  bin=bin_sfr, /overplot, color=!green, min=mino, max=maxo, rev=check_Rev_Sfr4, /l64
  if nt_4 gt 0 then plothist, log_rhoSFR_hybrid_all[t_4],  sfr4_bins, sfrnumber4_per_bin,  bin=bin_sfr, /overplot, color=!red, min=mino, max=maxo, rev=Rev_Sfr4, /l64
  
;reconstruction de l'histogramme
  n_sfr_4 = hist1d(log_rhosfr_hybrid_all[t_4], check_number_4, bins=bin_sfr, obin=x, min=mino, max=maxo, /l64) ;histogramme pondere
  check_n_sfr_4 = hist1d(log_rhosfr_amr_with_all[t_4], check_number_4, bins=bin_sfr, obin=check_x, min=mino, max=maxo, /l64)

;for k = 0, n_elements(sfr4_bins)-1 do begin
;if abs(x(k) - sfr4_bins(k)) gt 1d-5 then stop
;if abs(check_x(k) - check_sfr4_bins(k)) gt 1d-5 then stop
;endfor

  help, number_per_bin
  help, sfrnumber4_per_bin
  help, check_sfrnumber4_per_bin
  
  
  oplot, sfr4_bins, N_SFR_4, color=!dred, psym=10
  oplot, check_sfr4_bins, check_n_sfr_4, color=!cyan, psym=10
  
  
  print, total(n_sfr_4)
  print, total(check_n_sfr_4)
  print, total(ulong64(n_rho_4))
  print, total(ulong64(sfrnumber_per_bin)) ;devraient etre egaux
  
;stop ;;;;;;;;
  
  window, 27
  plothist, log_rhoSFR_amr_with_all[t_1], bin=bin_sfr, title='rhoSFR before/after CLOUDY : in 1 cell', /ylog, yr=[1,1e8], /ystyle, omin=mino, omax=maxo, /l64
  if nt_1 gt 0 then plothist, log_rhoSFR_amr_with_all[t_1], check_sfr1_bins, check_sfrnumber1_per_bin, bin=bin_sfr, /overplot, color=!green, min=mino, max=maxo, rev=check_Rev_Sfr1, /l64
  if nt_1 gt 0 then plothist, log_rhoSFR_hybrid_all[t_1],  sfr1_bins, sfrnumber1_per_bin, bin=bin_sfr, /overplot, color=!red, min=mino, max=maxo, rev=Rev_Sfr1, /l64

;reconstruction de l'histogramme
  n_sfr_1 = hist1d(log_rhosfr_hybrid_all[t_1], check_number_1, bins=bin_sfr, obin=x, min=mino, max=maxo) ;histogramme pondere
  check_n_sfr_1 = hist1d(log_rhosfr_amr_with_all[t_1], check_number_1, bins=bin_sfr, obin=check_x, min=mino, max=maxo)

;for k = 0, n_elements(sfr1_bins)-1 do begin
;if abs(x(k) - sfr1_bins(k)) gt 1d-5 then stop
;if abs(check_x(k) - check_sfr1_bins(k)) gt 1d-5 then stop
;endfor

  
  help, number_per_bin
  help, sfrnumber1_per_bin
  help, check_sfrnumber1_per_bin
  
  
  oplot, sfr1_bins, N_SFR_1, color=!brown, psym=10
  oplot, check_sfr1_bins, check_N_SFR_1, color=!cyan, psym=10


  wset, 29
  !p = p_sfr & !x = x_sfr & !y = y_sfr
  oplot, sfr4_bins, N_SFR_4, color=!dred, psym=10
  oplot, sfr1_bins, N_SFR_1, color=!brown, psym=10
  
  oplot, check_sfr4_bins, check_N_SFR_4, color=!cyan, psym=10
  oplot, check_sfr1_bins, check_N_SFR_1, color=!cyan, psym=10
  
;verification de la coherence
  help, SFR_amr_with_all
  print, total(ulong64(sfrnumber_per_bin))
  print, ulong64(total(ulong64(N_sfr_1))), ulong64(total(ulong64(N_sfr_4)))
  print, total(ulong64(N_sfr_1))-total(ulong64(sfrnumber_per_bin)), total(ulong64(N_sfr_4))-total(ulong64(sfrnumber_per_bin))
  
  
  
end
