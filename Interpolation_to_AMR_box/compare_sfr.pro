function read_corrected_mass, nm, sim_w, sim_n, part, dir, res

  if part ne '' then begin
     if strmatch(part, '*_*') eq 0 then part = '_'+part
  endif

  print, dir+'Total_heated-ionized_mass/'+nm+res+'corrected_total_mass_'+sim_w+'-'+sim_n+part+'.txt'
  openr, lun, dir+'Total_heated-ionized_mass/'+nm+res+'corrected_total_mass_'+sim_w+'-'+sim_n+part+'.txt', /get_lun
  line = ''
  for k = 0, 3 do readf, lun, line ;sauter les sommes des densite
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  check_tot_mass = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  corrected_mass_all = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  corrected_mass_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  corrected_mass_n4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  ionized_mass_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  ionized_mass_n4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  readf, lun, line
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  heated_sup1K_mass_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'M_sun')
  len = ap - avt
  heated_sup1K_mass_n4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  readf, lun, line
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  check_tot_volume = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  corrected_volume_all = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  corrected_volume_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  corrected_volume_n4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  ionized_volume_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  ionized_volume_n4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  readf, lun, line
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  heated_sup1K_volume_n1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'kpc^3')
  len = ap - avt
  heated_sup1K_volume_n4 = double(strmid(line,avt+3,len-3))

  close, lun
  free_lun, lun

  return, [check_tot_mass, check_tot_volume, corrected_mass_all, corrected_volume_all, corrected_mass_n1, corrected_volume_n1, corrected_mass_n4, corrected_volume_n4, ionized_mass_n1, ionized_volume_n1, ionized_mass_n4, ionized_volume_n4, heated_sup1K_mass_n1, heated_sup1K_volume_n1, heated_sup1K_mass_n4, heated_sup1K_volume_n4]

end


function read_corrected_sfr, sfr_threshold, nm, sim_w, sim_n, dir, res
  
  openr, lun, dir+'Total_corrected_SFR/'+nm+res+'corrected_total_sfr_'+sim_w+'-'+sim_n+'_threshold_'+sfr_threshold+'.txt', /get_lun
  line = ''
  for k = 0, 16 do readf, lun, line ;sauter toutes les lignes de la correction avec l'histo 1d des densites
  if res eq '' then readf, lun, line
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  sfr_corrected_N4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  sfr_corrected_N1 = double(strmid(line,avt+3,len-3)) 
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  check_with_N4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  check_with_N1 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  check_with_all = double(strmid(line,avt+3,len-3))
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  total_with = double(strmid(line,avt+3,len-3)) 
if res eq '' then begin
  readf, lun, line
  avt = strpos(line, ' : ')
  ap = strpos(line, 'Ms/yr')
  len = ap - avt
  total_no = double(strmid(line,avt+3,len-3))
endif else begin
total_no = 0
endelse
  readf, lun, line
  avt = strpos(line, 'd) ')     ;faire la suite pour les erreurs
  ap = strpos(line, '% and')
  len = ap - avt
  err_den_N4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  ap = strpos(line, '%.')
  len = ap
  err_den_N1 = double(strmid(line,0,len))
  readf, lun, line
  avt = strpos(line, 'd) ') 
  ap = strpos(line, 'and')
  len = ap - avt
  nmissing_N4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  ap = strpos(line, '(')
  len = ap
  nmissing_N1 = double(strmid(line,0,len))
  readf, lun, line
  avt = strpos(line, 'd) ') 
  ap = strpos(line, '% and')
  len = ap - avt
  err_rhosfr_N4 = double(strmid(line,avt+3,len-3))
  readf, lun, line
  ap = strpos(line, '%.')
  len = ap
  err_rhosfr_N1 = double(strmid(line,0,len))
  readf, lun, line
  avt = strpos(line, 'd) ') 
  ap = strpos(line, '% and')
  len = ap - avt
  err_sfr_N4 = double(strmid(line,avt+3,len-3))
  avt = strpos(line, '% and')
  len = strlen(line) - avt
  err_sfr_N1 = double(strmid(line,avt+5,len-5))

  close, lun
  free_lun, lun

  return, [sfr_corrected_N4,sfr_corrected_N1,total_with,total_no,check_with_N4,check_with_N1]

end 


function plot_all_snapshots, param, thick, sfr_threshold, dir
  
  files_with = findfile(dir+'outputs/outputs_with/thermal_mass_sfr_*.dat', count=n_with)
  sfr100_with = dblarr(n_with)
  sfr10_with = dblarr(n_with)
  sfr1_with = dblarr(n_with)
  sfr0_with = dblarr(n_with)
  gas_mass_with = dblarr(n_with)
  
  readcol, dir+'outputs/outputs_with/time_list_with', snapshots_with, time_with, n_stars_with, format=('A5,D,D'), /silent
  time_with = time_with - time_with(0)
  
  files_no = findfile(dir+'outputs/outputs_no/initial_mass_sfr_*.dat', count=n_no)
  sfr100_no = dblarr(n_no)
  sfr10_no = dblarr(n_no)
  sfr1_no = dblarr(n_no)
  sfr0_no = dblarr(n_no)
  gas_mass_no = dblarr(n_no)
  
  readcol, dir+'outputs/outputs_no/time_list_no', snapshots_no, time_no, n_stars_no, format=('A5,D,D'), /silent
  time_no = time_no - time_no(0)
;1 snapshot n'est pas complet : 00140 (no)
  t = where(snapshots_no ne '00140', nt)
  if nt gt 0 and nt eq n_no-1 then begin
     print, 'Le snapshot 00140_no est ignore'
     time_no = time_no[t]
  endif
  
  n_files = max([n_with,n_no])
  
  
  for j = 0, n_files-1 do begin
     
     if j lt n_no then begin
        openr, no, files_no(j), /get_lun
        
        line1=''
        readf, no, line1
        line2=''
        readf, no, line2
        line1=''
        readf, no, line1
        line2=''
        readf, no, line2
        line_mass = ''
        readf, no, line_mass
;print, line_mass
        avt = strpos(line_mass, 'AMR')
        ap = strpos(line_mass, 'M_sun')
        len = ap - avt
        gas_mass_no(j) = double(strmid(line_mass,avt+4,len-4))
;print, gas_mass_no(j)
        
        line_sfr = ''
        readf, no, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr100_no(j) = double(strmid(line_sfr,avt+5,len-5))
;print, 'SFR100', line_sfr, 'FIN'
;print, 'SFR100', sfr100_no(j), ' FIN'
        readf, no, line1
        readf, no, line1
        readf, no, line1
        readf, no, line1
        line_sfr = ''
        readf, no, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr10_no(j) = double(strmid(line_sfr,avt+5,len-5))
;print, 'SFR10', line_sfr, 'FIN'
;print, 'SFR10', sfr10_no(j), ' FIN'
        readf, no, line1
        readf, no, line1
        readf, no, line1
        readf, no, line1
        line_sfr = ''
        readf, no, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr1_no(j) = double(strmid(line_sfr,avt+5,len-5))
;print, 'SFR1', line_sfr, 'FIN'
;print, 'SFR1', sfr1_no(j), ' FIN'
        readf, no, line1
        readf, no, line1
        readf, no, line1
        readf, no, line1
        line_sfr = ''
        readf, no, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr0_no(j) = double(strmid(line_sfr,avt+5,len-5))
;print, 'SFR0', line_sfr, 'FIN'
;print, 'SFR0', sfr0_no(j), ' FIN'
        
        close, no   
        free_lun, no
     endif
     if j lt n_with then begin
        openr, with, files_with(j), /get_lun
        
        line1=''
        readf, with, line1
        line2=''
        readf, with, line2
        line1=''
        readf, no, line1
        line2=''
        readf, no, line2
        
        line_mass = ''
        readf, with, line_mass
        avt = strpos(line_mass, 'AMR')
        ap = strpos(line_mass, 'M_sun')
        len = ap - avt
        gas_mass_with(j) = double(strmid(line_mass,avt+4,len-4))
        
        line_sfr = ''
        readf, with, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr100_with(j) = double(strmid(line_sfr,avt+5,len-5))
        readf, with, line1
        readf, with, line1
        readf, with, line1
        readf, with, line1
        line_sfr = ''
        readf, with, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr10_with(j) = double(strmid(line_sfr,avt+5,len-5))
        readf, with, line1
        readf, with, line1
        readf, with, line1
        readf, with, line1
        line_sfr = ''
        readf, with, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr1_with(j) = double(strmid(line_sfr,avt+5,len-5))
        readf, with, line1
        readf, with, line1
        readf, with, line1
        readf, with, line1
        line_sfr = ''
        readf, with, line_sfr
        avt = strpos(line_sfr, 'AMR')
        ap = strpos(line_sfr, 'M_sun')
        len = ap - avt
        sfr0_with(j) = double(strmid(line_sfr,avt+5,len-5))

        
        close, with   
        free_lun, with
     endif
  endfor

  
  if param eq 'sfr' then begin
     print, 'sfr'
     if sfr_threshold eq 'sfr100' then oplot, time_no, sfr100_no, psym=-3, color=!dblue, thick=thick
     if sfr_threshold eq 'sfr10' then oplot, time_no, sfr10_no, psym=-3, color=!dblue, thick=thick
     if sfr_threshold eq 'sfr1' then oplot, time_no, sfr1_no, psym=-3, color=!dblue, thick=thick
     if sfr_threshold eq 'sfr0' then oplot, time_no, sfr0_no, psym=-3, color=!dblue, thick=thick
     if sfr_threshold eq 'sfr100' then oplot, time_with, sfr100_with, psym=-3, color=!dgreen, thick=thick
     if sfr_threshold eq 'sfr10' then oplot, time_with, sfr10_with, psym=-3, color=!dgreen, thick=thick
     if sfr_threshold eq 'sfr1' then oplot, time_with, sfr1_with, psym=-3, color=!dgreen, thick=thick
     if sfr_threshold eq 'sfr0' then oplot, time_with, sfr0_with, psym=-3, color=!dgreen, thick=thick
  endif
  if param eq 'mass' then begin
     oplot, time_no, gas_mass_no, psym=-3, color=!dblue, thick=thick
     oplot, time_with, gas_mass_with, psym=-3, color=!dgreen, thick=thick
  endif
  
;print, time_no, time_with
;print, sfr100_no, sfr100_with
;print, sfr10_no, sfr10_with
;print, sfr1_no, sfr1_with
;print, sfr0_no, sfr0_with

end


function read_file_near, file
  
  openr, std, file, /get_lun
  
  params=dblarr(79)

;print, file, ' ??'
  
  line1=''
  for i = 0, 4 do begin
     readf, std, line1
  endfor
  readf, std, line1             ;line mass
                                ;print, line1, ' FIN'
  len = strpos(line1, 'M_sun')
  params(0) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr100
                                ;print, line1, ' FIN'
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(1) = double(strmid(line1,avt+5,len-5))
  readf, std, line1             ;line sfr10
                                ;print, line1, ' FIN'
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(2) = double(strmid(line1,avt+5,len-5))
  readf, std, line1             ;line sfr1
                                ;print, line1, ' FIN'
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(3) = double(strmid(line1,avt+5,len-5))
  readf, std, line1             ;line sfr0
                                ;print, line1, ' FIN'
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(4) = double(strmid(line1,avt+5,len-5))
  for i = 0, 3 do begin
     readf, std, line1
  endfor
  readf, std, line1             ;line mass
  len = strpos(line1, 'M_sun')
                                ;print, line1, ' FIN'
  params(5) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line mass
  avt = strpos(line1, ') :')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(6) = double(strmid(line1,avt+3,len-5))
  readf, std, line1
  readf, std, line1             ;line heated mass
  avt = strpos(line1, ') :')
                                ;print, line1, ' FIN'
  params(7) = double(strmid(line1,avt+3,strlen(line1)))
  readf, std, line1
  readf, std, line1             ;line heated mass
  avt = strpos(line1, ') :')
  params(8) = double(strmid(line1,avt+3,strlen(line1)))
  readf, std, line1
  readf, std, line1
  readf, std, line1             ;line sfr100
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(9) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr100
  len = strpos(line1, 'M_sun')
  params(10) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr100
  len = strpos(line1, 'M_sun')
  params(11) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr10
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(12) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr10
  len = strpos(line1, 'M_sun')
  params(13) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr10
  len = strpos(line1, 'M_sun')
  params(14) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr1
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(15) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr1
  len = strpos(line1, 'M_sun')
  params(16) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr1
  len = strpos(line1, 'M_sun')
  params(17) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr0
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(18) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr0
  len = strpos(line1, 'M_sun')
  params(19) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr0
  len = strpos(line1, 'M_sun')
  params(20) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1
  readf, std, line1             ;line mass
  avt = strpos(line1, 'R :')
  params(21) = double(strmid(line1,avt+3,strlen(line1)))
  readf, std, line1
  readf, std, line1
  readf, std, line1             ;line mass
  avt = strpos(line1, ') :')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(22) = double(strmid(line1,avt+3,len-5))
  readf, std, line1
  readf, std, line1             ;line heated mass
  avt = strpos(line1, ') :')
  params(23) = double(strmid(line1,avt+3,strlen(line1)))
  readf, std, line1
  readf, std, line1             ;line heated mass
  avt = strpos(line1, ') :')
  params(24) = double(strmid(line1,avt+3,strlen(line1)))
  readf, std, line1
  readf, std, line1
  readf, std, line1             ;line sfr100
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(25) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr100
  len = strpos(line1, 'M_sun')
  params(26) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr100
  len = strpos(line1, 'M_sun')
  params(27) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr10
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(28) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr10
  len = strpos(line1, 'M_sun')
  params(29) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr10
  len = strpos(line1, 'M_sun')
  params(30) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr1
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(31) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr1
  len = strpos(line1, 'M_sun')
  params(32) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr1
  len = strpos(line1, 'M_sun')
  params(33) = double(strmid(line1,0,len-1))
  readf, std, line1             ;line sfr0
  avt = strpos(line1, 'AMR')
  ap = strpos(line1, 'M_sun')
  len = ap - avt
  params(34) = double(strmid(line1,avt+5,len-5))
  readf, std, line1
  readf, std, line1             ;line sfr0
  len = strpos(line1, 'M_sun')
  params(35) = double(strmid(line1,0,len-1))
  readf, std, line1
  readf, std, line1             ;line sfr0
  len = strpos(line1, 'M_sun')
  params(36) = double(strmid(line1,0,len-1))
  for i = 0, 5 do begin
     readf, std, line1
  endfor
  readf, std, line1             ;line nb
  avt = strpos(line1, 'les')
  ap = strpos(line1, 'cellules')
  len = ap - avt
  params(37) = double(strmid(line1,avt+3,len-3))
;print, line1
  readf, std, line1             ;line nb
  params(38) = double(line1)
;print, line1
  readf, std, line1
  readf, std, line1
  readf, std, line1             ;line nb
  params(39) = double(line1)
;print, line1
  for i = 0, 3 do begin
     readf, std, line1
  endfor
  readf, std, line1             ;line nb heated
  params(40) = double(line1)
;print, line1
  readf, std, line1
  readf, std, line1             ;line nb heated
  avt = strpos(line1, ') :')
  params(41) = double(strmid(line1,avt+3,strlen(line1)))
;print, line1
  readf, std, line1
  readf, std, line1             ;line nb heated
  avt = strpos(line1, ') :')
  params(42) = double(strmid(line1,avt+3,strlen(line1)))
;print, line1
  readf, std, line1
  readf, std, line1
                                ;print, line1, ' FIN'
  readf, std, line1             ;line nb sfr100
                                ;print, line1, ' FIN'
  avt = strpos(line1, 'AMR :')
  ap = strpos(line1, 'AMR in_cell :')
  len = ap - avt
  params(43) = double(strmid(line1,avt+5,len-5))
  avt = ap
  params(44) = double(strmid(line1,avt+13,strlen(line1)))
  readf, std, line1
                                ;print, line1, ' FIN'
  avt = strpos(line1,'AMR in_4cell :')
  params(45) = double(strmid(line1,avt+14,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'CLOUDY :')
  ap = strpos(line1, 'CLOUDY in_cell :')
  len = ap - avt
  params(46) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(47) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'CLOUDY in_4cell :')
  params(48) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'HYBRID :')
  ap = strpos(line1, 'HYBRID in_cell :')
  len = ap - avt
  params(49) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(50) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'HYBRID in_4cell :')
  params(51) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1
  readf, std, line1             ; line nb sfr10
  avt = strpos(line1, 'AMR :')
  ap = strpos(line1, 'AMR in_cell :')
  len = ap - avt
  params(52) = double(strmid(line1,avt+5,len-5))
  avt = ap
  params(53) = double(strmid(line1,avt+13,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'AMR in_4cell :')
  params(54) = double(strmid(line1,avt+14,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'CLOUDY :')
  ap = strpos(line1, 'CLOUDY in_cell :')
  len = ap - avt
  params(55) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(56) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'CLOUDY in_4cell :')
  params(57) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'HYBRID :')
  ap = strpos(line1, 'HYBRID in_cell :')
  len = ap - avt
  params(58) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(59) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'HYBRID in_4cell :')
  params(60) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1
  readf, std, line1             ; line nb sfr1
  avt = strpos(line1, 'AMR :')
  ap = strpos(line1, 'AMR in_cell :')
  len = ap - avt
  params(61) = double(strmid(line1,avt+5,len-5))
  avt = ap
  params(62) = double(strmid(line1,avt+13,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'AMR in_4cell :')
  params(63) = double(strmid(line1,avt+14,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'CLOUDY :')
  ap = strpos(line1, 'CLOUDY in_cell :')
  len = ap - avt
  params(64) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(65) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'CLOUDY in_4cell :')
  params(66) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'HYBRID :')
  ap = strpos(line1, 'HYBRID in_cell :')
  len = ap - avt
  params(67) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(68) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'HYBRID in_4cell :')
  params(69) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1
  readf, std, line1             ; line nb sfr0
  avt = strpos(line1, 'AMR :')
  ap = strpos(line1, 'AMR in_cell :')
  len = ap - avt
  params(70) = double(strmid(line1,avt+5,len-5))
  avt = ap
  params(71) = double(strmid(line1,avt+13,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'AMR in_4cell :')
  params(72) = double(strmid(line1,avt+14,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'CLOUDY :')
  ap = strpos(line1, 'CLOUDY in_cell :')
  len = ap - avt
  params(73) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(74) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'CLOUDY in_4cell :')
  params(75) = double(strmid(line1,avt+17,strlen(line1)))
  readf, std, line1 
  avt = strpos(line1, 'HYBRID :')
  ap = strpos(line1, 'HYBRID in_cell :')
  len = ap - avt
  params(76) = double(strmid(line1,avt+8,len-8))
  avt = ap
  params(77) = double(strmid(line1,avt+16,strlen(line1)))
  readf, std, line1
  avt = strpos(line1,'HYBRID in_4cell :')
  params(78) = double(strmid(line1,avt+17,strlen(line1)))

  close, std
  free_lun, std

;print, file, ' ok !!'
;stop
  
  return, params
  
end

pro compare_sfr, ps=ps, sfr_threshold=sfr_threshold, n_criterium=n_criterium, ionized=ionized, heated=heated, part=part, reservoir=reservoir, lmax=lmax, boxlen=boxlen, lores=lores
  
;ce programme compare le sfr avec FB (thermal+radiatif) et sans FB (du
;tout) au cours du temps

  if not keyword_set(lores) then lores = 0
  if not keyword_set(lmax) then lmax = 13
  if not keyword_set(boxlen) then boxlen = 50 ;kpc
  IF not KEYWORD_SET(ionized) then ionized = 0
  IF not KEYWORD_SET(heated) then heated = 0
  IF not KEYWORD_SET(ionized) and not KEYWORD_SET(heated) then begin
     ionized = 1
     heated = 1
  endif

  IF not KEYWORD_SET(sfr_threshold) then sfr_threshold = 'sfr10'
  IF not KEYWORD_SET(n_criterium) then n_criterium = 4
  IF not KEYWORD_SET(ps) then ps = 0

  if not keyword_set(part) then part = '' ;part peut etre egal a disk ou a reservoir

  if sfr_threshold eq 'sfr100' then thr = '100'
  if sfr_threshold eq 'sfr10'  then thr =  '10'
  if sfr_threshold eq 'sfr1'   then thr =   '1'
  if sfr_threshold eq 'sfr0'   then thr =   '0'
  name_plot = 'SFR threshold : n > '+thr+' H/cc'

;stop

  if lmax eq 12 then begin
     dir = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/'
                                ;dir_sink = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/'
     sim_with = ['00119', '00178', '00197', '00225']
     sim_no = ['', '', '', '']
     time_with = [173.809, 207.229, 219.254, 240.054]
     res='lores_'
  endif else begin

     dir = '/Users/oroos/Post-stage/'
                                ;dir_sink = '/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/'
     sim_with = ['00075w', '00100', '00108', '00150', '00170', '00210']
     sim_no   = ['00075n', '00085', '00090', '00120', '00130', '00148']
                                ;time    = [     155,     168,     174,     208,     219,     240] ;Myrs
     
     time_no   = [157.318, 168.944, 174.523, 208.071, 219.562, 240.076]
     time_with = [154.296, 168.637, 174.212, 207.636, 219.975, 240.526]

     time_no = time_no - 151.65
     res = ''
;sim_with = ['00210']
;sim_no = ['00148']
;time_with = [240.526]
;time_no = [240.076]
  endelse
  
;soustraire t_0 = 151 Myrs (temps ou le FB a ete eteint dans la simu 'NO')
  time_with = time_with - 151.65

  tout = 1
  if tout eq 1 then begin

     if lmax ne 12 then begin
        files_no_fb = findfile(dir+'LOPs*/initial_mass_sfr_*.dat', count=n_no_fb) 
        sfr100_no_fb = dblarr(n_no_fb)
        sfr10_no_fb = dblarr(n_no_fb)
        sfr1_no_fb = dblarr(n_no_fb)
        sfr0_no_fb = dblarr(n_no_fb)
        gas_mass_no_fb = dblarr(n_no_fb)
        time_no_fb = dblarr(n_no_fb)
        
        print, n_no_fb, ' fichiers initial_mass_sfr trouves. (sans FB dans la simu)'
                                ;print, files_no_fb
     endif
     
     files_thermal_only = findfile(dir+'LOPs*/thermal_mass_sfr_*.dat', count=n_thermal_only) 
     sfr100_thermal_only = dblarr(n_thermal_only)
     sfr10_thermal_only = dblarr(n_thermal_only)
     sfr1_thermal_only = dblarr(n_thermal_only)
     sfr0_thermal_only = dblarr(n_thermal_only)
     gas_mass_thermal_only = dblarr(n_thermal_only)
     time_thermal_only = dblarr(n_thermal_only)
     
     print, n_thermal_only, ' fichiers thermal_mass_sfr trouves. (avec FB dans la simu / avant cloudy)'   
                                ;print, files_thermal_only
     
     if lmax ne 12 then begin
        if n_no_fb ne n_thermal_only then begin
           print, 'Le nombre de snapshots avec et sans FB thermique n est pas le meme !!'
           stop
        endif
     endif
     
     
     for i = 0, n_elements(sim_with)-1 do begin
        t_with = where(strmatch(files_thermal_only, '*'+sim_with(i)+'*') eq 1, nt_with)
        if lmax ne 12 then t_no   = where(strmatch(files_no_fb       , '*'+sim_no(i)+'*')   eq 1, nt_no)
        
        if nt_with le 0 then begin
           print, 'Attention, les fichiers des simulations ', sim_with(i), ' n ont pas ete trouves !!'
           stop
        endif

        
        if lmax ne 12 then begin
           if nt_no le 0 then begin
              print, 'Attention, les fichiers des simulations ', sim_no(i), ' n ont pas ete trouves !!'
              stop
           endif
        endif
        
        
        if lmax ne 12 then time_no_fb[t_no] = time_no(i)
        time_thermal_only[t_with] = time_with(i)
     endfor
     
;         if lmax ne 12 then print, time_no_fb
;print, time_thermal_only
     
     for j = 0, n_thermal_only-1 do begin

        if lmax ne 12 then begin
           openr, no, files_no_fb(j), /get_lun
           line_no=''
           for t = 0, 2 do readf, no, line_no
           avt = strpos(line_no, 'AMR')
           ap = strpos(line_no, 'M_sun')
           len = ap - avt
           gas_mass_no_fb(j) = double(strmid(line_no,avt+5,len-5))
           readf, no, line_no
           avt = strpos(line_no, 'AMR')
           ap = strpos(line_no, 'M_sun')
           len = ap - avt
           sfr100_no_fb(j) = double(strmid(line_no,avt+5,len-5))
           for t = 0, 3 do readf, no, line_no
           avt = strpos(line_no, 'AMR')
           ap = strpos(line_no, 'M_sun')
           len = ap - avt
           sfr10_no_fb(j) = double(strmid(line_no,avt+5,len-5))
           for t = 0, 3 do readf, no, line_no
           avt = strpos(line_no, 'AMR')
           ap = strpos(line_no, 'M_sun')
           len = ap - avt
           sfr1_no_fb(j) = double(strmid(line_no,avt+5,len-5))
           for t = 0, 3 do readf, no, line_no
           avt = strpos(line_no, 'AMR')
           ap = strpos(line_no, 'M_sun')
           len = ap - avt
           sfr0_no_fb(j) = double(strmid(line_no,avt+5,len-5))
           close, no
           free_lun, no
        endif

        openr, with, files_thermal_only(j), /get_lun
        line_with=''
        for t = 0, 2 do readf, with, line_with
        avt = strpos(line_with, 'AMR')
        ap = strpos(line_with, 'M_sun')
        len = ap - avt
        gas_mass_thermal_only(j) = double(strmid(line_with,avt+5,len-5))
        readf, with, line_with
        avt = strpos(line_with, 'AMR')
        ap = strpos(line_with, 'M_sun')
        len = ap - avt
        sfr100_thermal_only(j) = double(strmid(line_with,avt+5,len-5))
        for t = 0, 3 do readf, with, line_with
        avt = strpos(line_with, 'AMR')
        ap = strpos(line_with, 'M_sun')
        len = ap - avt
        sfr10_thermal_only(j) = double(strmid(line_with,avt+5,len-5))
        for t = 0, 3 do readf, with, line_with
        avt = strpos(line_with, 'AMR')
        ap = strpos(line_with, 'M_sun')
        len = ap - avt
        sfr1_thermal_only(j) = double(strmid(line_with,avt+5,len-5))
        for t = 0, 3 do readf, with, line_with
        avt = strpos(line_with, 'AMR')
        ap = strpos(line_with, 'M_sun')
        len = ap - avt
        sfr0_thermal_only(j) = double(strmid(line_with,avt+5,len-5))
        close, with
        free_lun, with
     endfor
     
     n_with = n_elements(SIM_with)
     
     files_post_cloudy = findfile(dir+'LOPs*/'+res+'mass_sfr_*.dat', count=n_post_cloudy) 
     files_post_cloudy_x10 = findfile(dir+'LOPs*/x10_'+res+'mass_sfr_*.dat', count=n_post_cloudy_x10) 
     files_post_cloudy_x100 = findfile(dir+'LOPs*/x100_'+res+'mass_sfr_*.dat', count=n_post_cloudy_x100) 
     
     final_params = dblarr(79,n_with)
     final_params_x10 = dblarr(79,n_with)
     final_params_x100 = dblarr(79,n_with)
     
     
                                ;print, files_post_cloudy
     
     n_post_cloudy_all = max([n_post_cloudy,n_post_cloudy_x10,n_post_cloudy_x100])
     
     if n_post_cloudy eq 0 or n_post_cloudy_x10 eq 0 or n_post_cloudy_x100 eq 0 then begin
        print, 'Attention, il manque un regime de luminosite'
        stop
     endif
     
     for j = 0, n_post_cloudy_all-1 do begin
        
        
;params===
;mass_amr_all_partial(j)               0 ;total should be same as gas_mass_no_fb
;sfr100_amr_all_partial(j)             1 ;total should be same as sfr_no_fb
;sfr10_amr_all_partial(j)              2
;sfr1_amr_all_partial(j)               3
;sfr0_amr_all_partial(j)               4
        
        
;mass_amr_in4cell_partial(j)           5
;mass_cloudy_in4cell_partial(j)        6
;heated_mass_amr_in4cell_partial(j)    7
;heated_mass_cloudy_in4cell_partial(j) 8
;sfr100_amr_in4cell_partial(j)         9
;sfr100_hybrid_in4cell_partial(j)      10
;sfr100_cloudy_in4cell_partial(j)      11
;sfr10_amr_in4cell_partial(j)          12
;sfr10_hybrid_in4cell_partial(j)       13
;sfr10_cloudy_in4cell_partial(j)       14
;sfr1_amr_in4cell_partial(j)           15
;sfr1_hybrid_in4cell_partial(j)        16
;sfr1_cloudy_in4cell_partial(j)        17
;sfr0_amr_in4cell_partial(j)           18
;sfr0_hybrid_in4cell_partial(j)        19
;sfr0_cloudy_in4cell_partial(j)        20

;mass_amr_incell_partial(j)            21
;mass_cloudy_incell_partial(j)         22
;heated_mass_amr_incell_partial(j)     23
;heated_mass_cloudy_incell_partial(j)  24
;sfr100_amr_incell_partial(j)          25
;sfr100_hybrid_incell_partial(j)       26
;sfr100_cloudy_incell_partial(j)       27
;sfr10_amr_incell_partial(j)           28
;sfr10_hybrid_incell_partial(j)        29
;sfr10_cloudy_incell_partial(j)        30
;sfr1_amr_incell_partial(j)            31
;sfr1_hybrid_incell_partial(j)         32
;sfr1_cloudy_incell_partial(j)         33
;sfr0_amr_incell_partial(j)            34
;sfr0_hybrid_incell_partial(j)         35
;sfr0_cloudy_incell_partial(j)         36

;nb_all_partial(j)                     37 ;total should equal number of lines in corresponding cloudy_near*.dat
;nb_in4cell_partial(j)                 38
;nb_incell_partial(j)                  39
;nb_heated_all_partial(j)              40
;nb_heated_in4cell_partial(j)          41
;nb_heated_incell_partial(j)           42
;nb_sfr100_amr_all_partial(j)          43
;nb_sfr100_amr_incell_partial(j)       44
;nb_sfr100_amr_in4cell_partial(j)      45
;nb_sfr10_amr_all_partial(j)           46 
;nb_sfr10_amr_incell_partial(j)        47
;nb_sfr10_amr_in4cell_partial(j)       48  
;nb_sfr1_amr_all_partial(j)            49
;nb_sfr1_amr_incell_partial(j)         50 
;nb_sfr1_amr_in4cell_partial(j)        51
;nb_sfr0_amr_all_partial(j)            52 
;nb_sfr0_amr_incell_partial(j)         53
;nb_sfr0_amr_in4cell_partial(j)        54 
;nb_sfr100_cloudy_all_partial(j)       55
;nb_sfr100_cloudy_incell_partial(j)    56
;nb_sfr100_cloudy_in4cell_partial(j)   57
;nb_sfr10_cloudy_all_partial(j)        58
;nb_sfr10_cloudy_incell_partial(j)     59
;nb_sfr10_cloudy_in4cell_partial(j)    60
;nb_sfr1_cloudy_all_partial(j)         61
;nb_sfr1_cloudy_incell_partial(j)      62
;nb_sfr1_cloudy_in4cell_partial(j)     63
;nb_sfr0_cloudy_all_partial(j)         64
;nb_sfr0_cloudy_incell_partial(j)      65
;nb_sfr0_cloudy_in4cell_partial(j)     66
;nb_sfr100_hybrid_all_partial(j)       67
;nb_sfr100_hybrid_incell_partial(j)    68
;nb_sfr100_hybrid_in4cell_partial(j)   69
;nb_sfr10_hybrid_all_partial(j)        70
;nb_sfr10_hybrid_incell_partial(j)     71
;nb_sfr10_hybrid_in4cell_partial(j)    72
;nb_sfr1_hybrid_all_partial(j)         73
;nb_sfr1_hybrid_incell_partial(j)      74
;nb_sfr1_hybrid_in4cell_partial(j)     75
;nb_sfr0_hybrid_all_partial(j)         76
;nb_sfr0_hybrid_incell_partial(j)      77
;nb_sfr0_hybrid_in4cell_partial(j)     78

        if j eq 0 then begin
           read_params = read_file_near(files_post_cloudy(j)) 
           read_params_x10 = read_file_near(files_post_cloudy_x10(j)) 
           read_params_x100 = read_file_near(files_post_cloudy_x100(j)) 
        endif else begin
           if j lt n_post_cloudy then read_params = [[read_params],[read_file_near(files_post_cloudy(j))]]
           if j lt n_post_cloudy_x10 then read_params_x10 = [[read_params_x10],[read_file_near(files_post_cloudy_x10(j))]]
           if j lt n_post_cloudy_x100 then read_params_x100 = [[read_params_x100],[read_file_near(files_post_cloudy_x100(j))]]
        endelse
;print,  files_post_cloudy(j)
        
     endfor
     help, read_params
     
     s=size(read_params)
     if s[0] ne 2 then stop else nb = s[2]
     
     
;bug dans le nombre de lignes de chaque fichier :
     read_params(37,*) = read_params(37,*) + 1
     read_params_x10(37,*) = read_params_x10(37,*) + 1
     read_params_x100(37,*) = read_params_x100(37,*) + 1
     if lmax eq 12 then correct_numbers = [12500396, 13285670, 13205737, 13219528] else correct_numbers = [23487106, 23383786, 23521294, 23192154, 22538389, 22495588]
     yes_std = intarr(n_elements(sim_with))
     yes_x10 = intarr(n_elements(sim_with))
     yes_x100 = intarr(n_elements(sim_with))
     
     
     for i = 0, n_elements(sim_with)-1 do begin
        
;print, sim_with(i)
        
        t_std = where(strmatch(files_post_cloudy, '*'+sim_with(i)+'*') eq 1, nt_std)
        t_x10 = where(strmatch(files_post_cloudy_x10, '*'+sim_with(i)+'*') eq 1, nt_x10)
        t_x100 = where(strmatch(files_post_cloudy_x100, '*'+sim_with(i)+'*') eq 1, nt_x100)      
;if nt_std gt 0 then print, files_post_cloudy[t_std]
        
        for j = 0, nb-1 do begin
           if nt_std gt 0 then final_params(j,i) = total(read_params[j,t_std])             ;& if nt_std gt 0 and j eq 37 then print, read_params[j,t_std]
           if nt_x10 gt 0 then final_params_x10(j,i) = total(read_params_x10[j,t_x10])     ;& if nt_x10 gt 0 and j eq 37 then print, read_params_x10[j,t_x10]
           if nt_x100 gt 0 then final_params_x100(j,i) = total(read_params_x100[j,t_x100]) ;& if nt_x100 gt 0 and j eq 37 then print, read_params_x100[j,t_x100]
        endfor
        
;print, sim_with(i)
        if nt_std gt 0 and  final_params(37,i)      eq correct_numbers(i) then yes_std(i) = 1 ;est-ce que les totaux sont complets ?
        if nt_x10 gt 0 and  final_params_x10(37,i)  eq correct_numbers(i) then yes_x10(i) = 1
        if nt_x100 gt 0 and final_params_x100(37,i) eq correct_numbers(i) then yes_x100(i) = 1
        
     endfor
     
     t_ok      = where(yes_std eq 1, nt_ok)
     t_ok_x10  = where(yes_x10 eq 1, nt_ok_x10)
     t_ok_x100 = where(yes_x100 eq 1, nt_ok_x100)
     
     affiche = [ $
               ['On doit avoir : ', strtrim(correct_numbers,2)], $
               ['On a : std :    ', strtrim((transpose(final_params(37,*))),2)], $
               ['On prend :      ', strtrim((yes_std*transpose(final_params(37,*))),2)], $
               ['On a : x10 :    ', strtrim((transpose(final_params_x10(37,*))),2)], $
               ['On prend :      ', strtrim((yes_x10*transpose(final_params_x10(37,*))),2)], $
               ['On a : x100 :   ', strtrim((transpose(final_params_x100(37,*))),2)], $
               ['On prend :      ', strtrim((yes_x100*transpose(final_params_x100(37,*))),2)]]
     
     print, affiche
     
     
;n'afficher que les valeurs totales completes
     for i = 0, n_elements(sim_with)-1 do begin
        final_params(*,i) = final_params(*,i)*yes_std(i)
        final_params_x10(*,i) = final_params_x10(*,i)*yes_x10(i)
        final_params_x100(*,i) = final_params_x100(*,i)*yes_x100(i)
     endfor


     if n_criterium eq 4 then begin   
        print, 'mass fraction in4cells      (%) ',    transpose(final_params(5,*)/final_params(0,*))*100
        print, 'mass fraction in4cells_x10  (%) ',    transpose(final_params_x10(5,*)/final_params_x10(0,*))*100   
        print, 'mass fraction in4cells_x100 (%) ',    transpose(final_params_x100(5,*)/final_params_x100(0,*))*100  
     endif  
     if n_criterium eq 1 then begin 
        print, 'mass fraction in1cell       (%) ',    transpose(final_params(21,*)/final_params(0,*))*100
        print, 'mass fraction in1cell_x10   (%) ',    transpose(final_params_x10(21,*)/final_params_x10(0,*))*100 
        print, 'mass fraction in1cell_x100  (%) ',    transpose(final_params_x100(21,*)/final_params_x100(0,*))*100
     endif   
     
;corrected_values of sfr
     corrected_sfr_N4 = dblarr(n_elements(time_thermal_only))
     corrected_sfr_N1 = dblarr(n_elements(time_thermal_only))
     verif_sfr_with = dblarr(n_elements(time_thermal_only))
     if lmax ne 12 then verif_sfr_no = dblarr(n_elements(time_thermal_only))
     corrected_verif_n4 = dblarr(n_elements(time_thermal_only))
     corrected_verif_n1 = dblarr(n_elements(time_thermal_only))
     
     corrected_sfr_N4_x10 = dblarr(n_elements(time_thermal_only))
     corrected_sfr_N1_x10 = dblarr(n_elements(time_thermal_only))
     verif_sfr_with_x10 = dblarr(n_elements(time_thermal_only))
     if lmax ne 12 then verif_sfr_no_x10 = dblarr(n_elements(time_thermal_only))
     corrected_verif_n4_x10 = dblarr(n_elements(time_thermal_only))
     corrected_verif_n1_x10 = dblarr(n_elements(time_thermal_only))
     
     corrected_sfr_N4_x100 = dblarr(n_elements(time_thermal_only))
     corrected_sfr_N1_x100 = dblarr(n_elements(time_thermal_only))
     verif_sfr_with_x100 = dblarr(n_elements(time_thermal_only))
     if lmax ne 12 then verif_sfr_no_x100 = dblarr(n_elements(time_thermal_only))
     corrected_verif_n4_x100 = dblarr(n_elements(time_thermal_only))
     corrected_verif_n1_x100 = dblarr(n_elements(time_thermal_only))
     
     
     for kk = 0, n_elements(time_thermal_only)-1 do begin
        correction=read_corrected_sfr(sfr_threshold, '', sim_with(kk), sim_no(kk), dir, res)
        corrected_sfr_N4(kk) = correction(0)
        corrected_sfr_N1(kk) = correction(1)
        verif_sfr_with(kk) = correction(2)
        if lmax ne 12 then  verif_sfr_no(kk) = correction(3)
        corrected_verif_n4(kk) = correction(4)
        corrected_verif_n1(kk) = correction(5)
        
        correction_x10=read_corrected_sfr(sfr_threshold, 'x10_', sim_with(kk), sim_no(kk), dir, res)
        corrected_sfr_N4_x10(kk) = correction_x10(0)
        corrected_sfr_N1_x10(kk) = correction_x10(1)
        verif_sfr_with_x10(kk) = correction_x10(2)
        if lmax ne 12 then verif_sfr_no_x10(kk) = correction_x10(3)
        corrected_verif_n4_x10(kk) = correction_x10(4)
        corrected_verif_n1_x10(kk) = correction_x10(5)

        correction_x100=read_corrected_sfr(sfr_threshold, 'x100_', sim_with(kk), sim_no(kk), dir, res)
        corrected_sfr_N4_x100(kk) = correction_x100(0)
        corrected_sfr_N1_x100(kk) = correction_x100(1)
        verif_sfr_with_x100(kk) = correction_x100(2)
        if lmax ne 12 then verif_sfr_no_x100(kk) = correction_x100(3)
        corrected_verif_n4_x100(kk) = correction_x100(4)
        corrected_verif_n1_x100(kk) = correction_x100(5)
     endfor
     
;corrected_values of mass and volume
     corrected_mass_N4 = dblarr(n_elements(time_thermal_only))
     corrected_mass_N1 = dblarr(n_elements(time_thermal_only))
     verif_mass_with = dblarr(n_elements(time_thermal_only))
     ionized_mass_N4 = dblarr(n_elements(time_thermal_only))
     ionized_mass_N1 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N4 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N1 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N4 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N1 = dblarr(n_elements(time_thermal_only))
     verif_volume_with = dblarr(n_elements(time_thermal_only))
     ionized_volume_N4 = dblarr(n_elements(time_thermal_only))
     ionized_volume_N1 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N4 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N1 = dblarr(n_elements(time_thermal_only))
     
     corrected_mass_N4_x10 = dblarr(n_elements(time_thermal_only))
     corrected_mass_N1_x10 = dblarr(n_elements(time_thermal_only))
     verif_mass_with_x10 = dblarr(n_elements(time_thermal_only))
     ionized_mass_N4_x10 = dblarr(n_elements(time_thermal_only))
     ionized_mass_N1_x10 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N4_x10 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N1_x10 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N4_x10 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N1_x10 = dblarr(n_elements(time_thermal_only))
     verif_volume_with_x10 = dblarr(n_elements(time_thermal_only))
     ionized_volume_N4_x10 = dblarr(n_elements(time_thermal_only))
     ionized_volume_N1_x10 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N4_x10 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N1_x10 = dblarr(n_elements(time_thermal_only))

     corrected_mass_N4_x100 = dblarr(n_elements(time_thermal_only))
     corrected_mass_N1_x100 = dblarr(n_elements(time_thermal_only))
     verif_mass_with_x100 = dblarr(n_elements(time_thermal_only))
     ionized_mass_N4_x100 = dblarr(n_elements(time_thermal_only))
     ionized_mass_N1_x100 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N4_x100 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_mass_N1_x100 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N4_x100 = dblarr(n_elements(time_thermal_only))
     corrected_volume_N1_x100 = dblarr(n_elements(time_thermal_only))
     verif_volume_with_x100 = dblarr(n_elements(time_thermal_only))
     ionized_volume_N4_x100 = dblarr(n_elements(time_thermal_only))
     ionized_volume_N1_x100 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N4_x100 = dblarr(n_elements(time_thermal_only))
     heated_sup1K_volume_N1_x100 = dblarr(n_elements(time_thermal_only))

     for kk = 0, n_elements(time_thermal_only)-1 do begin
        correction=read_corrected_mass('', sim_with(kk), sim_no(kk),part, dir, res)
        corrected_mass_N4(kk) = correction(6)
        corrected_mass_N1(kk) = correction(4)
        verif_mass_with(kk) = correction(0)
        ionized_mass_N4(kk) = correction(10)
        ionized_mass_N1(kk) = correction(8)
        heated_sup1K_mass_N4(kk) = correction(14)
        heated_sup1K_mass_N1(kk) = correction(12)
        corrected_volume_N4(kk) = correction(7)
        corrected_volume_N1(kk) = correction(5)
        verif_volume_with(kk) = correction(1)
        ionized_volume_N4(kk) = correction(11)
        ionized_volume_N1(kk) = correction(9)
        heated_sup1K_volume_N4(kk) = correction(15)
        heated_sup1K_volume_N1(kk) = correction(13)
        
;                 0                1                 2                     3                   4                     5                 6                  7    
;;return, [check_tot_mass, check_tot_volume, corrected_mass_all, corrected_volume_all, corrected_mass_n1, corrected_volume_n1, corrected_mass_n4, corrected_volume_n4,
;         8                9               10                11                    12                   13                    14                   15 
;ionized_mass_n1, ionized_volume_n1, ionized_mass_n4, ionized_volume_n4, heated_sup1K_mass_n1, heated_sup1K_volume_n1, heated_sup1K_mass_n4, heated_sup1K_volume_n4]

        
        correction_x10=read_corrected_mass('x10_', sim_with(kk), sim_no(kk),part, dir, res)
        corrected_mass_N4_x10(kk) = correction_x10(6)
        corrected_mass_N1_x10(kk) = correction_x10(4)
        verif_mass_with_x10(kk) = correction_x10(0)
        ionized_mass_N4_x10(kk) = correction_x10(10)
        ionized_mass_N1_x10(kk) = correction_x10(8)
        heated_sup1K_mass_N4_x10(kk) = correction_x10(14)
        heated_sup1K_mass_N1_x10(kk) = correction_x10(12)
        corrected_volume_N4_x10(kk) = correction_x10(7)
        corrected_volume_N1_x10(kk) = correction_x10(5)
        verif_volume_with_x10(kk) = correction_x10(1)
        ionized_volume_N4_x10(kk) = correction_x10(11)
        ionized_volume_N1_x10(kk) = correction_x10(9)
        heated_sup1K_volume_N4_x10(kk) = correction_x10(15)
        heated_sup1K_volume_N1_x10(kk) = correction_x10(13)

        correction_x100=read_corrected_mass('x100_', sim_with(kk), sim_no(kk),part, dir, res)
        corrected_mass_N4_x100(kk) = correction_x100(6)
        corrected_mass_N1_x100(kk) = correction_x100(4)
        verif_mass_with_x100(kk) = correction_x100(0)
        ionized_mass_N4_x100(kk) = correction_x100(10)
        ionized_mass_N1_x100(kk) = correction_x100(8)
        heated_sup1K_mass_N4_x100(kk) = correction_x100(14)
        heated_sup1K_mass_N1_x100(kk) = correction_x100(12)
        corrected_volume_N4_x100(kk) = correction_x100(7)
        corrected_volume_N1_x100(kk) = correction_x100(5)
        verif_volume_with_x100(kk) = correction_x100(1)
        ionized_volume_N4_x100(kk) = correction_x100(11)
        ionized_volume_N1_x100(kk) = correction_x100(9)
        heated_sup1K_volume_N4_x100(kk) = correction_x100(15)
        heated_sup1K_volume_N1_x100(kk) = correction_x100(13)

     endfor
;--------------------------------------------------------------------------------------------------------------------------------------------------------

     !x.margin=[7,2]   
     !y.margin=[4,1]

     if ps eq 0 then thick=2 else thick=25
     if ps eq 0 then symsize=1 else symsize=2
     if ps eq 0 then lin=1 else lin=0.3

;parametres en fonction du temps

     yes=1
     if yes eq 1 then begin
        if ps eq 1 then begin
           ps_start, res+'n='+strtrim(n_criterium,2)+'_corrected_'+sfr_threshold+'Hcc_vs_time.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20, /helvetica, /bold
           !p.charsize=5 
           !p.charthick=2
           !x.charsize=1
           !y.charsize=1
           !x.thick=30
           !y.thick=30
           !p.thick=4
        endif

;n'afficher que les CI ??
        only_before_RT = 0      ; 0 == afficher tout   

if lmax ne 12 then sf_rg = [15,40] else sf_rg = [0,25]

        if ps eq 0 then window, 0, ypos=425
        plot, [-6,98], sf_rg, psym=1, /nodata, xtitle='Time [Myrs]', ytitle='SFR [M'+sunsymbol()+' yr'+textoidl('^{-1}')+'] (> '+strmid(sfr_threshold,3,strlen(sfr_threshold)-3)+' cm'+textoidl('^{-3}')+')', /ystyle, /xstyle
        if lmax ne 12 then plt = plot_all_snapshots('sfr', thick, sfr_threshold, dir)
        psym=[-1,-2,-4,-5,-6,-7]   
        for kk = 0, n_elements(time_thermal_only)-1 do begin
           
           if lmax ne 12 then begin     
              if sfr_threshold eq 'sfr100' then plots, time_no_fb(kk), sfr100_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr10' then plots, time_no_fb(kk), sfr10_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr1' then plots, time_no_fb(kk), sfr1_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr0' then plots, time_no_fb(kk), sfr0_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
              plots, time_no_fb(kk), verif_sfr_no(kk), psym=psym(kk), color=!blue, thick=thick
              plots, time_no_fb(kk), verif_sfr_no_x10(kk), psym=psym(kk), color=!blue, thick=thick
              plots, time_no_fb(kk), verif_sfr_no_x100(kk), psym=psym(kk), color=!blue, thick=thick 
           endif
           
           if sfr_threshold eq 'sfr100' then plots, time_thermal_only(kk), sfr100_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
           if sfr_threshold eq 'sfr10' then plots, time_thermal_only(kk), sfr10_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
           if sfr_threshold eq 'sfr1' then plots, time_thermal_only(kk), sfr1_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
           if sfr_threshold eq 'sfr0' then plots, time_thermal_only(kk), sfr0_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
           if sfr_threshold eq 'sfr100' then begin
              plots, time_thermal_only(kk), ((final_params(1,kk))), psym=psym(kk), color=!green, thick=thick     ;doivent se superposer a sfr_thermal_only
              plots, time_thermal_only(kk), ((final_params_x10(1,kk))), psym=psym(kk), color=!green, thick=thick ;SFR100
              plots, time_thermal_only(kk), ((final_params_x100(1,kk))), psym=psym(kk), color=!green, thick=thick
           endif
           if sfr_threshold eq 'sfr10' then begin 
              plots, time_thermal_only(kk), ((final_params(2,kk))), psym=psym(kk), color=!green, thick=thick     ;doivent se superposer a sfr_thermal_only
              plots, time_thermal_only(kk), ((final_params_x10(2,kk))), psym=psym(kk), color=!green, thick=thick ;SFR10
              plots, time_thermal_only(kk), ((final_params_x100(2,kk))), psym=psym(kk), color=!green, thick=thick
           endif
           if sfr_threshold eq 'sfr1' then begin                                                                 ;need transpose if array
              plots, time_thermal_only(kk), ((final_params(3,kk))), psym=psym(kk), color=!green, thick=thick     ;doivent se superposer a sfr_thermal_only
              plots, time_thermal_only(kk), ((final_params_x10(3,kk))), psym=psym(kk), color=!green, thick=thick ;SFR1
              plots, time_thermal_only(kk), ((final_params_x100(3,kk))), psym=psym(kk), color=!green, thick=thick
           endif
           if sfr_threshold eq 'sfr0' then begin
              plots, time_thermal_only(kk), ((final_params(4,kk))), psym=psym(kk), color=!green, thick=thick     ;doivent se superposer a sfr_thermal_only
              plots, time_thermal_only(kk), ((final_params_x10(4,kk))), psym=psym(kk), color=!green, thick=thick ;SFR0
              plots, time_thermal_only(kk), ((final_params_x100(4,kk))), psym=psym(kk), color=!green, thick=thick
           endif
           plots, time_thermal_only(kk), verif_sfr_with(kk), psym=psym(kk), color=!green, thick=thick
           plots, time_thermal_only(kk), verif_sfr_with_x10(kk), psym=psym(kk), color=!green, thick=thick
           plots, time_thermal_only(kk), verif_sfr_with_x100(kk), psym=psym(kk), color=!green, thick=thick 

        endfor

        if lmax ne 12 then begin  
           if sfr_threshold eq 'sfr100' then err_bars = avg(abs(sfr100_thermal_only - sfr100_no_fb))
           if sfr_threshold eq 'sfr10' then err_bars = avg(abs(sfr10_thermal_only - sfr10_no_fb))
           if sfr_threshold eq 'sfr1' then err_bars = avg(abs(sfr1_thermal_only - sfr1_no_fb))
           if sfr_threshold eq 'sfr0' then err_bars = avg(abs(sfr0_thermal_only - sfr0_no_fb))
           
           if sfr_threshold eq 'sfr100' then errplot, time_thermal_only, sfr100_thermal_only-err_bars, sfr100_thermal_only+err_bars, color=!green, thick=thick
           if sfr_threshold eq 'sfr10' then errplot, time_thermal_only, sfr10_thermal_only-err_bars, sfr10_thermal_only+err_bars, color=!green, thick=thick
           if sfr_threshold eq 'sfr1' then errplot, time_thermal_only, sfr1_thermal_only-err_bars, sfr1_thermal_only+err_bars, color=!green, thick=thick
           if sfr_threshold eq 'sfr0' then errplot, time_thermal_only, sfr0_thermal_only-err_bars, sfr0_thermal_only+err_bars, color=!green, thick=thick
        endif else begin
           err_bars = fltarr(n_elements(time_thermal_only))
        endelse     


;-------------------------------------------------------------------------------------------corrected sfr
        if only_before_RT eq 0 then begin

           if n_criterium eq 1 then begin   
              oplot, time_thermal_only, corrected_sfr_N1, psym=-3, color=!red, thick=thick, line=5, syms=2*symsize
              oplot, time_thermal_only, corrected_sfr_N1_x10, psym=-3, color=!red, thick=thick, line=2, syms=2*symsize
              oplot, time_thermal_only, corrected_sfr_N1_x100, psym=-3, color=!red, thick=thick, line=4, syms=2*symsize
              err_bars_corr_n1 = abs(corrected_verif_n1-verif_sfr_with)+err_bars
              err_bars_corr_n1_x10 = abs(corrected_verif_n1_x10-verif_sfr_with_x10)+err_bars
              err_bars_corr_n1_x100 = abs(corrected_verif_n1_x100-verif_sfr_with_x100)+err_bars
              errplot, time_thermal_only-1.5, corrected_sfr_N1 - err_bars_corr_n1, corrected_sfr_N1 + err_bars_corr_n1, color=!red, thick=thick
              errplot, time_thermal_only, corrected_sfr_N1_x10 - err_bars_corr_n1_x10, corrected_sfr_N1_x10 + err_bars_corr_n1_x10, color=!red, thick=thick
              errplot, time_thermal_only+1.5, corrected_sfr_N1_x100 - err_bars_corr_n1_x100, corrected_sfr_N1_x100 + err_bars_corr_n1_x100, color=!red, thick=thick

                                ; for kk = 0, n_elements(time_no_fb)-1 do begin
                                ;  plots, time_thermal_only(kk)-1.5, corrected_sfr_N1(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
                                ;  plots, time_thermal_only(kk), corrected_sfr_N1_x10(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
                                ;  plots, time_thermal_only(kk)+1.5, corrected_sfr_N1_x100(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
;endfor


           endif
           if n_criterium eq 4 then begin
              oplot, time_thermal_only, corrected_sfr_N4, psym=-3, color=!red, thick=thick, line=5, syms=2*symsize
              oplot, time_thermal_only, corrected_sfr_N4_x10, psym=-3, color=!red, thick=thick, line=2, syms=2*symsize
              oplot, time_thermal_only, corrected_sfr_N4_x100, psym=-3, color=!red, thick=thick, line=4  , syms=2*symsize
              err_bars_corr_n4 = abs(corrected_verif_n4-verif_sfr_with)+err_bars
              err_bars_corr_n4_x10 = abs(corrected_verif_n4_x10-verif_sfr_with_x10)+err_bars
              err_bars_corr_n4_x100 = abs(corrected_verif_n4_x100-verif_sfr_with_x100)+err_bars
              errplot, time_thermal_only-1.5, corrected_sfr_N4 - err_bars_corr_n4, corrected_sfr_N4 + err_bars_corr_n4, color=!red, thick=thick
              errplot, time_thermal_only, corrected_sfr_N4_x10 - err_bars_corr_n4_x10, corrected_sfr_N4_x10 + err_bars_corr_n4_x10, color=!red, thick=thick
              errplot, time_thermal_only+1.5, corrected_sfr_N4_x100 - err_bars_corr_n4_x100, corrected_sfr_N4_x100 + err_bars_corr_n4_x100, color=!red, thick=thick

                                ;for kk = 0, n_elements(time_no_fb)-1 do begin
                                ; plots, time_thermal_only(kk)-1.5, corrected_sfr_N4(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
                                ; plots, time_thermal_only(kk), corrected_sfr_N4_x10(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
                                ; plots, time_thermal_only(kk)+1.5, corrected_sfr_N4_x100(kk), psym=psym(kk), color=!red, thick=thick, syms=2*symsize
;endfor
           endif   

;--------------------------------------------------------------------------------------------------

;remettre les points par dessus   
           for kk = 0, n_elements(time_no_fb)-1 do begin
              
              if sfr_threshold eq 'sfr100' then plots, time_thermal_only(kk), sfr100_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr10' then plots, time_thermal_only(kk), sfr10_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr1' then plots, time_thermal_only(kk), sfr1_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize
              if sfr_threshold eq 'sfr0' then plots, time_thermal_only(kk), sfr0_thermal_only(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize

           endfor

           if lmax ne 12 then begin
              if sfr_threshold eq 'sfr100' then errplot, time_thermal_only, sfr100_thermal_only-err_bars, sfr100_thermal_only+err_bars, color=!green, thick=thick
              if sfr_threshold eq 'sfr10' then errplot, time_thermal_only, sfr10_thermal_only-err_bars, sfr10_thermal_only+err_bars, color=!green, thick=thick
              if sfr_threshold eq 'sfr1' then errplot, time_thermal_only, sfr1_thermal_only-err_bars, sfr1_thermal_only+err_bars, color=!green, thick=thick
              if sfr_threshold eq 'sfr0' then errplot, time_thermal_only, sfr0_thermal_only-err_bars, sfr0_thermal_only+err_bars, color=!green, thick=thick
              
              for kk = 0, n_elements(time_no_fb)-1 do begin
                 
                 if sfr_threshold eq 'sfr100' then plots, time_no_fb(kk), sfr100_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
                 if sfr_threshold eq 'sfr10' then plots, time_no_fb(kk), sfr10_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
                 if sfr_threshold eq 'sfr1' then plots, time_no_fb(kk), sfr1_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
                 if sfr_threshold eq 'sfr0' then plots, time_no_fb(kk), sfr0_no_fb(kk), psym=psym(kk), color=!blue, thick=thick, syms=2*symsize
                 
              endfor
           endif
        endif
;-------------------------------------------------------------------------------------------
        
        items = ['No feedback', 'Thermal FB',  'Initial FB + RT :', '    1 x L'+textoidl('_{AGN}'), '  10 x L'+textoidl('_{AGN}'), '100 x L'+textoidl('_{AGN}') ]
        psyms = [0, 0, 2,0,0,0]
        colors = [!dblue, !dgreen, !red, !red,!red,!red]
        linestyles = [0,0,0,5,2,4]
        syms = 2*symsize

if lmax ne 12 then bottom = 1 else bottom = 0
                                ; Add the legend.
        AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, bottom=bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, background_color=!white, syms=syms, linsize=lin

if lmax ne 12 then begin
        if ps eq 0 then begin
           x = 6.3
           yb = 23.1
           yg = 21.9
        endif else begin
           x = 8.6
           yb = 23.5
           yg = 22.22
        endelse

        plots, x,yb, psym=2, color=!blue, thick=thick, syms=syms
        plots, x,yg, psym=2, color=!green, thick=thick, syms=syms  
endif

        if ps eq 1 then ps_end, /png
        if ps eq 1 then print, 'Conversion en png...'
     endif
     ;stop

     
     fleche_bas = 1d-6
     fleche_haut = 5d-6

     !x.margin=[8,2]   
     !y.margin=[4,1]

     psyms=[-1,-2,-4,-5,-6,-7] 
     hsize=-0.2

     yes = 1
     if yes eq 1 then begin
        if ps eq 1 then begin
           ps_start, res+'n='+strtrim(n_criterium,2)+'_ratios_'+sfr_threshold+'Hcc_vs_luminosity.eps', /encapsulated, /color, /decomposed, /cm, xsize=20, ysize=20, /helvetica, /bold
           !p.charsize=5 
           !p.charthick=2
           !x.charsize=1
           !y.charsize=1
           !x.thick=30
           !y.thick=30
           !p.thick=4
        endif

        if ps eq 0 then window, 2, ypos=425
        plot, [44.2,46.8], [0,1], xtitle='log Total luminosity of AGN [erg s'+textoidl('^{-1}')+']', ytitle='Relative reduction of SFR : |'+textoidl('\Delta SFR')+'|/SFR'+textoidl('_i'), /nodata, /ylog, yr=[fleche_bas,1d-1], /ystyle, /xstyle

        if lmax ne 12 then begin
           if sfr_threshold eq 'sfr100' then plots, [44.2,46.8], max(abs(1d - sfr100_thermal_only/sfr100_no_fb)), color=!green, thick=thick, line=5
           if sfr_threshold eq 'sfr10'  then plots, [44.2,46.8], max(abs(1d - sfr10_thermal_only/sfr10_no_fb)), color=!green, thick=thick, line=5
           if sfr_threshold eq 'sfr1'   then plots, [44.2,46.8], max(abs(1d - sfr1_thermal_only/sfr1_no_fb)), color=!green, thick=thick, line=5
           if sfr_threshold eq 'sfr0'   then plots, [44.2,46.8], max(abs(1d - sfr0_thermal_only/sfr0_no_fb)), color=!green, thick=thick, line=5
        endif
        for kk = 0, n_elements(time_thermal_only)-1 do begin
                                ;erase
                                ;plot, [44,47], [0,1], xtitle='Total luminosity of AGN (erg/s)', ytitle='1 - SFR_f/SFR_i', /nodata, /ylog, yr=[fleche_bas,1]              

                                ; if sfr_threshold eq 'sfr100' then plots, 44.25, abs(1d - sfr100_thermal_only(kk)/sfr100_no_fb(KK)), psym=psyms(kk), color=!green, thick=thick, syms=2*symsize
                                ; if sfr_threshold eq 'sfr10' then plots, 44.25, abs(1d - sfr10_thermal_only(kk)/sfr10_no_fb(KK)), psym=psyms(kk), color=!green, thick=thick, syms=2*symsize
                                ; if sfr_threshold eq 'sfr1' then plots, 44.25, abs(1d - sfr1_thermal_only(kk)/sfr1_no_fb(KK)), psym=psyms(kk), color=!green, thick=thick, syms=2*symsize
                                ; if sfr_threshold eq 'sfr0' then plots, 44.25, abs(1d - sfr0_thermal_only(kk)/sfr0_no_fb(KK)), psym=psyms(kk), color=!green, thick=thick, syms=2*symsize
           if n_criterium eq 4 then begin
              oplot, [44.5,45.5,46.5], [1d - corrected_sfr_N4(kk)/corrected_verif_N4(kk),1d - corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk),1d - corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N4(kk)/corrected_verif_N4(kk) eq 1 then  oplot, [44.5, 45.5], [fleche_haut,1d - corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk)], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk) eq 1 then  oplot, [44.5,45.5], [1d - corrected_sfr_N4(kk)/corrected_verif_N4(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk) eq 1 then  oplot, [46.5,45.5], [1d - corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk) eq 1 then  oplot, [45.5,46.5], [1d - corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              plots, 44.5, 1d - corrected_sfr_N4(kk)/corrected_verif_N4(kk), psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              plots, 45.5, 1d - corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk), psym=psyms(kk), color=!red, thick=thick , syms=2*symsize
              plots, 46.5, 1d - corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk), psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N4(kk)/corrected_verif_N4(kk) eq 1 then  arrow, 44.5, fleche_haut, 44.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N4(kk)/corrected_verif_N4(kk) eq 1 then  plots, 44.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk) eq 1 then arrow, 45.5, fleche_haut, 45.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N4_x10(kk)/corrected_verif_N4_x10(kk) eq 1 then  plots, 45.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk) eq 1 then arrow, 46.5, fleche_haut, 46.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N4_x100(kk)/corrected_verif_N4_x100(kk) eq 1 then  plots, 46.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
           endif
           if n_criterium eq 1 then begin
              oplot, [44.5,45.5,46.5], [1d - corrected_sfr_N1(kk)/corrected_verif_N1(kk),1d - corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk),1d - corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N1(kk)/corrected_verif_N1(kk) eq 1 then  oplot, [44.5, 45.5], [fleche_haut,1d - corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk)], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk) eq 1 then  oplot, [44.5,45.5], [1d - corrected_sfr_N1(kk)/corrected_verif_N1(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk) eq 1 then  oplot, [46.5,45.5], [1d - corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              if corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk) eq 1 then  oplot, [45.5,46.5], [1d - corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk),fleche_haut], psym=-3, color=!gray, thick=thick, line=kk
              plots, 44.5, 1d - corrected_sfr_N1(kk)/corrected_verif_N1(kk), psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              plots, 45.5, 1d - corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk), psym=psyms(kk), color=!red, thick=thick , syms=2*symsize
              plots, 46.5, 1d - corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk), psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N1(kk)/corrected_verif_N1(kk) eq 1 then arrow, 44.5, fleche_haut, 44.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N1(kk)/corrected_verif_N1(kk) eq 1 then  plots, 44.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk) eq 1 then arrow, 45.5, fleche_haut, 45.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N1_x10(kk)/corrected_verif_N1_x10(kk) eq 1 then  plots, 45.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
              if corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk) eq 1 then arrow, 46.5, fleche_haut, 46.5, fleche_bas, color=!red, thick=thick, /data, /solid, hsize=hsize
              if corrected_sfr_N1_x100(kk)/corrected_verif_N1_x100(kk) eq 1 then  plots, 46.5, fleche_haut, psym=psyms(kk), color=!red, thick=thick, syms=2*symsize
           endif
           
;print, 'pause'
;read, go_on
        endfor
        if ps eq 1 then ps_end, /png
        if ps eq 1 then print, 'Conversion en png...'
     endif
     
;     print, 'pause'
;     read, go_on
;     
;     window, 10 
;     plot, [0,100], [0,12], psym=-1, /nodata, xtitle='Time (Myrs)', ytitle='SFR (M_sun/yr) : comparaison avant/apres in 4 cells', /ystyle, title=name_plot
;     if sfr_threshold eq 'sfr100' then begin
;        oplot, time_thermal_only, (transpose(final_params(9,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR100 AMR in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(9,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(9,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(10,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR100 HYBRID in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(10,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(10,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(11,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR100 CLOUDY in 4 cells
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(11,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only, (transpose(final_params_x100(11,*))), psym=-1, color=!magenta, thick=thick, line=2
;;---> SFR cloudy calcule a partir de la temperature cloudy et de la
;;densite cloudy... resamplage + pb quand la cellule amr la plus proche
;;est loin !
;     endif
;     if sfr_threshold eq 'sfr10' then begin
;        oplot, time_thermal_only, (transpose(final_params(12,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR10 AMR in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(12,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(12,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(13,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR10 HYBRID in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(13,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(13,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(14,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR10 CLOUDY in 4 cells
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(14,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only, (transpose(final_params_x100(14,*))), psym=-1, color=!magenta, thick=thick, line=2
;;---> SFR cloudy calcule a partir de la temperature cloudy et de la
;;densite cloudy... resamplage + pb quand la cellule amr la plus proche
;;est loin !
;     endif
;     if sfr_threshold eq 'sfr1' then begin
;        oplot, time_thermal_only, (transpose(final_params(15,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR1 AMR in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(15,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(15,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(16,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR1 HYBRID in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(16,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(16,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(17,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR1 CLOUDY in 4 cells
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(17,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only, (transpose(final_params_x100(17,*))), psym=-1, color=!magenta, thick=thick, line=2
;;---> SFR cloudy calcule a partir de la temperature cloudy et de la
;;densite cloudy... resamplage + pb quand la cellule amr la plus proche
;;est loin !
;     endif
;     if sfr_threshold eq 'sfr0' then begin
;        oplot, time_thermal_only, (transpose(final_params(18,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR0 AMR in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(18,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(18,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(19,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR0 HYBRID in 4 cells
;        oplot, time_thermal_only, (transpose(final_params_x10(19,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(19,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(20,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR0 CLOUDY in 4 cells
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(20,*))), psym=-1, color=!magenta, thick=thick, line=1
;     ;oplot, time_thermal_only, (transpose(final_params_x100(20,*))), psym=-1, color=!magenta, thick=thick, line=2
;;---> SFR cloudy calcule a partir de la temperature cloudy et de la
;;densite cloudy... resamplage + pb quand la cellule amr la plus proche
;;est loin !
;     endif
;     
;     items = ["SFR AMR", "SFR HYBRID" , "Seyfert", "Weak Quasar", "Strong Quasar" ]
;     
;     colors = [!green, !red, !black, !black, !black]   
;     psyms = [1, 1, -1, -1, -1] 
;     lines = [0, 0, 0, 1, 2] 
;     
;                                ; Add the legend.
;     AL_Legend, items,  psym=psyms, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, linsize=0.1, line=lines, background=!white
;     
;     
;     window, 20 
;     plot, [0,100], [0,5], psym=-1, /nodata, xtitle='Time (Myrs)', ytitle='SFR (M_sun/yr) : comparaison avant/apres in 1 cell', /ystyle, title=name_plot
;     if sfr_threshold eq 'sfr100' then begin
;        oplot, time_thermal_only, (transpose(final_params(25,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR100 AMR in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(25,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(25,*))), psym=-1, color=!green, thick=thick+2, line=2
;
;        oplot, time_thermal_only, (transpose(final_params(26,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR100 HYBRID in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(26,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(26,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(27,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR100 CLOUDY in 1 cell
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(27,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only,
;                                ;(transpose(final_params_x100(27,*))),
;                                ;psym=-1, color=!magenta, thick=thick,
;                                ;line=2
;     endif
;     if sfr_threshold eq 'sfr10' then begin
;        oplot, time_thermal_only, (transpose(final_params(28,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR10 AMR in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(28,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(28,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(29,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR10 HYBRID in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(29,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(29,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(30,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR10 CLOUDY in 1 cell
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(30,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only,
;                                ;(transpose(final_params_x100(30,*))),
;                                ;psym=-1, color=!magenta,
;                                ;thick=thick, line=2
;     endif
;     if sfr_threshold eq 'sfr1' then begin
;        oplot, time_thermal_only, (transpose(final_params(31,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR1 AMR in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(31,*))), psym=-1, color=!green, thick=thick+2, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(31,*))), psym=-1, color=!green, thick=thick+2, line=2
;        
;        oplot, time_thermal_only, (transpose(final_params(32,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR1 HYBRID in 1 cell
;        oplot, time_thermal_only, (transpose(final_params_x10(32,*))), psym=-1, color=!red, thick=thick, line=1
;        oplot, time_thermal_only, (transpose(final_params_x100(32,*))), psym=-1, color=!red, thick=thick, line=2
;        
;                                ;oplot, time_thermal_only, (transpose(final_params(33,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR1 CLOUDY in 1 cell
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(33,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only,
;                                ;(transpose(final_params_x100(33,*))),
;                                ;psym=-1, color=!magenta, thick=thick,
;                                ;line=2
;     endif
;     if sfr_threshold eq 'sfr0' then begin
; oplot, time_thermal_only, (transpose(final_params(34,*))), psym=-1, color=!green, thick=thick+2, line=0 ;SFR0 AMR in 1 cell
; oplot, time_thermal_only, (transpose(final_params_x10(34,*))), psym=-1, color=!green, thick=thick+2, line=1
; oplot, time_thermal_only, (transpose(final_params_x100(34,*))), psym=-1, color=!green, thick=thick+2, line=2
; 
; oplot, time_thermal_only, (transpose(final_params(35,*))), psym=-1, color=!red, thick=thick, line=0 ;SFR0 HYBRID in 1 cell
; oplot, time_thermal_only, (transpose(final_params_x10(35,*))), psym=-1, color=!red, thick=thick, line=1
; oplot, time_thermal_only, (transpose(final_params_x100(35,*))), psym=-1, color=!red, thick=thick, line=2
; 
;                                ;oplot, time_thermal_only, (transpose(final_params(36,*))), psym=-1, color=!magenta, thick=thick, line=0 ;SFR0 CLOUDY in 1 cell
;                                ;oplot, time_thermal_only, (transpose(final_params_x10(36,*))), psym=-1, color=!magenta, thick=thick, line=1
;                                ;oplot, time_thermal_only,
;                                ;(transpose(final_params_x100(36,*))),
;                                ;psym=-1, color=!magenta, thick=thick,
;                                ;line=2
;endif
;     items = ["SFR AMR", "SFR HYBRID" , "Seyfert", "Weak Quasar", "Strong Quasar" ]
;     
;     colors = [!green, !red, !black, !black, !black]   
;     psyms = [1, 1, -1, -1, -1] 
;     lines = [0, 0, 0, 1, 2] 
;     
;                                ; Add the legend.
;     AL_Legend, items,  psym=psyms, Color=colors, /right, textcolor=!black, outline_color=!black, /clear, thick=thick, linsize=0.1, line=lines, background=!white
;     
;     print, 'pause'
;     read, go_on
     
     if ionized eq 1 then nm_ion = 'ionized_' else nm_ion = ''
     if heated  eq 1 then nm_htd = 'heated_'  else nm_htd = ''

     if ionized eq 1 and heated eq 0 then title = 'Ionized'
     if ionized eq 0 and heated eq 1 then title = 'Heated'
     if ionized eq 1 and heated eq 1 then title = 'Heated and ionized'

     psyms=[-1,-2,-4,-5,-6,-7]      

     if ps eq 1 then begin
        ps_start, res+'n='+strtrim(n_criterium,2)+'_corrected_'+nm_ion+nm_htd+'mass_vs_time'+part+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=20, ysize=20, /helvetica, /bold
        !p.charsize=5 
        !p.charthick=2
        !x.charsize=1
        !y.charsize=1
        !x.thick=30
        !y.thick=30
        !p.thick=4
     endif
     if ps eq 0 then window, 1  
     plot, [-1,91], [1d4,5d10], psym=-1, /nodata, xtitle='Time [Myrs]', ytitle='Gas mass [M_sun]'+repstr(part,'_',' '), /ystyle, /ylog, /xstyle
     if lmax ne 12 then plt = plot_all_snapshots('mass', thick, sfr_threshold, dir)
     for kk = 0, n_elements(time_thermal_only)-1 do begin

        if lmax ne 12 then plots, time_no_fb(kk), gas_mass_no_fb(kk), psym=psyms(kk), color=!blue, thick=thick, syms=2*symsize
        plots, time_thermal_only(kk), gas_mass_thermal_only(kk), psym=psyms(kk), color=!green, thick=thick , syms=2*symsize
        plots, time_thermal_only(kk), (final_params(0,kk)), psym=psyms(kk), color=!green, thick=thick, line=5, syms=2*symsize ;doivent se superposer a gas_mass_thermal_only
        plots, time_thermal_only(kk), (final_params_x10(0,kk)), psym=psyms(kk), color=!green, thick=thick, line=2, syms=2*symsize
        plots, time_thermal_only(kk), (final_params_x100(0,kk)), psym=psyms(kk), color=!green, thick=thick, line=4, syms=2*symsize

        plots, time_thermal_only(kk), verif_mass_with(kk), psym=psyms(kk), color=!green, thick=thick, line=5, syms=2*symsize ;doivent se superposer a gas_mass_thermal_only
        plots, time_thermal_only(kk), verif_mass_with_x10(kk), psym=psyms(kk), color=!green, thick=thick, line=2, syms=2*symsize
        plots, time_thermal_only(kk), verif_mass_with_x100(kk), psym=psyms(kk), color=!green, thick=thick, line=4, syms=2*symsize

        if n_criterium eq 4 then begin
           if ionized eq 1 then begin
              plots, time_thermal_only(kk)-1.5, ionized_mass_N4(kk), psym=psyms(kk), color=!red, thick=thick, line=5, syms=2*symsize ;
              plots, time_thermal_only(kk), ionized_mass_N4_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
              plots, time_thermal_only(kk)+1.5, ionized_mass_N4_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=4, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, time_thermal_only(kk)-1.5, heated_sup1K_mass_N4(kk), psym=psyms(kk), color=!orange, thick=thick, line=5, syms=2*symsize ;
              plots, time_thermal_only(kk), heated_sup1K_mass_N4_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
              plots, time_thermal_only(kk)+1.5, heated_sup1K_mass_N4_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=4, syms=2*symsize
           endif
        endif

        if n_criterium eq 1 then begin
           if ionized eq 1 then begin
              plots, time_thermal_only(kk)-1.5, ionized_mass_N1(kk), psym=psyms(kk), color=!red, thick=thick, line=5, syms=2*symsize ;
              plots, time_thermal_only(kk), ionized_mass_N1_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
              plots, time_thermal_only(kk)+1.5, ionized_mass_N1_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=4, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, time_thermal_only(kk)-1.5, heated_sup1K_mass_N1(kk), psym=psyms(kk), color=!orange, thick=thick, line=5, syms=2*symsize ;
              plots, time_thermal_only(kk), heated_sup1K_mass_N1_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
              plots, time_thermal_only(kk)+1.5, heated_sup1K_mass_N1_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=4, syms=2*symsize
           endif
        endif
     endfor


     if n_criterium eq 4 then begin
        if ionized eq 1 then begin
           oplot, time_thermal_only, ionized_mass_N4, psym=0, color=!red, thick=thick, line=5, syms=2*symsize ;
           oplot, time_thermal_only, ionized_mass_N4_x10, psym=0, color=!red, thick=thick, line=2, syms=2*symsize
           oplot, time_thermal_only, ionized_mass_N4_x100, psym=0, color=!red, thick=thick, line=4, syms=2*symsize
        endif
        if heated eq 1 then begin
           oplot, time_thermal_only, heated_sup1K_mass_N4, psym=0, color=!orange, thick=thick, line=5, syms=2*symsize ;
           oplot, time_thermal_only, heated_sup1K_mass_N4_x10, psym=0, color=!orange, thick=thick, line=2, syms=2*symsize
           oplot, time_thermal_only, heated_sup1K_mass_N4_x100, psym=0, color=!orange, thick=thick, line=4, syms=2*symsize
        endif
     endif

     if n_criterium eq 1 then begin
        if ionized eq 1 then begin
           oplot, time_thermal_only, ionized_mass_N1, psym=0, color=!red, thick=thick, line=5, syms=2*symsize ;
           oplot, time_thermal_only, ionized_mass_N1_x10, psym=0, color=!red, thick=thick, line=2, syms=2*symsize
           oplot, time_thermal_only, ionized_mass_N1_x100, psym=0, color=!red, thick=thick, line=4, syms=2*symsize
        endif
        if heated eq 1 then begin
           oplot, time_thermal_only, heated_sup1K_mass_N1, psym=0, color=!orange, thick=thick, line=5, syms=2*symsize ;
           oplot, time_thermal_only, heated_sup1K_mass_N1_x10, psym=0, color=!orange, thick=thick, line=2, syms=2*symsize
           oplot, time_thermal_only, heated_sup1K_mass_N1_x100, psym=0, color=!orange, thick=thick, line=4, syms=2*symsize
        endif
     endif


     if lmax ne 12 then  err_bars = avg(abs(gas_mass_no_fb - gas_mass_thermal_only)) else err_bars = fltarr(n_elements(time_thermal_only))
     if lmax ne 12 then errplot, time_thermal_only,  gas_mass_thermal_only-err_bars,  gas_mass_thermal_only+err_bars, color=!green, thick=thick

     if n_criterium eq 4 then begin

        err_bars_n4 =  abs(gas_mass_thermal_only-corrected_mass_n4)+err_bars
        err_bars_n4_x10 =  abs(gas_mass_thermal_only-corrected_mass_n4_x10)+err_bars
        err_bars_n4_x100 =  abs(gas_mass_thermal_only-corrected_mass_n4_x100)+err_bars

        if ionized eq 1 then begin
           errplot, time_thermal_only-1.5,  ionized_mass_n4-err_bars_n4,  ionized_mass_n4+err_bars_n4, color=!red, thick=thick
           errplot, time_thermal_only,  ionized_mass_n4_x10-err_bars_n4_x10,  ionized_mass_n4_x10+err_bars_n4_x10, color=!red, thick=thick
           errplot, time_thermal_only+1.5,  ionized_mass_n4_x100-err_bars_n4_x100,  ionized_mass_n4_x100+err_bars_n4_x100, color=!red, thick=thick
        endif
        
        if heated eq 1 then begin
           errplot, time_thermal_only-1.5,  heated_sup1K_mass_n4-err_bars_n4,  heated_sup1K_mass_n4+err_bars_n4, color=!orange, thick=thick
           errplot, time_thermal_only,  heated_sup1K_mass_n4_x10-err_bars_n4_x10,  heated_sup1K_mass_n4_x10+err_bars_n4_x10, color=!orange, thick=thick
           errplot, time_thermal_only+1.5,  heated_sup1K_mass_n4_x100-err_bars_n4_x100,  heated_sup1K_mass_n4_x100+err_bars_n4_x100, color=!orange, thick=thick
        endif
     endif

     if n_criterium eq 1 then begin
        
        err_bars_n1 =  abs(gas_mass_thermal_only-corrected_mass_n1)+err_bars
        err_bars_n1_x10 =  abs(gas_mass_thermal_only-corrected_mass_n1_x10)+err_bars
        err_bars_n1_x100 =  abs(gas_mass_thermal_only-corrected_mass_n1_x100)+err_bars

        if ionized eq 1 then begin
           errplot, time_thermal_only-1.5,  ionized_mass_n1-err_bars_n1,  ionized_mass_n1+err_bars_n1, color=!red, thick=thick
           errplot, time_thermal_only,  ionized_mass_n1_x10-err_bars_n1_x10,  ionized_mass_n1_x10+err_bars_n1_x10, color=!red, thick=thick
           errplot, time_thermal_only+1.5,  ionized_mass_n1_x100-err_bars_n1_x100,  ionized_mass_n1_x100+err_bars_n1_x100, color=!red, thick=thick
        endif
        
        if heated eq 1 then begin
           errplot, time_thermal_only-1.5,  heated_sup1K_mass_n1-err_bars_n1,  heated_sup1K_mass_n1+err_bars_n1, color=!orange, thick=thick
           errplot, time_thermal_only,  heated_sup1K_mass_n1_x10-err_bars_n1_x10,  heated_sup1K_mass_n1_x10+err_bars_n1_x10, color=!orange, thick=thick
           errplot, time_thermal_only+1.5,  heated_sup1K_mass_n1_x100-err_bars_n1_x100,  heated_sup1K_mass_n1_x100+err_bars_n1_x100, color=!orange, thick=thick
        endif
     endif

     if ionized eq 1 and heated eq 0 then begin
        items = ['No feedback', 'Thermal FB',  'Initial FB + RT : ionized', '    1 x L'+textoidl('_{AGN}'), '  10 x L'+textoidl('_{AGN}'), '100 x L'+textoidl('_{AGN}') ]
        psyms = [1, 1, 1, 0, 0, 0]
        colors = [!blue, !green, !red, !red, !red, !red]
        linestyles = [0,0,0,5,2,4]
        syms = 2*symsize
     endif
     if ionized eq 0 and heated eq 1 then begin
        items = ['No feedback', 'Thermal FB',  'Initial FB + RT : heated', '    1 x L'+textoidl('_{AGN}'), '  10 x L'+textoidl('_{AGN}'), '100 x L'+textoidl('_{AGN}') ]
        psyms = [1, 1, 1, 0, 0, 0]
        colors = [!blue, !green, !orange, !orange, !orange, !orange]
        linestyles = [0,0,0,5,2,4]
        syms = 2*symsize
     endif
     if ionized eq 1 and heated eq 1 then begin
        items = ['No feedback', 'Thermal FB',  'Initial FB + RT : heated', 'Initial FB + RT : ionized', '    1 x L'+textoidl('_{AGN}'), '  10 x L'+textoidl('_{AGN}'), '100 x L'+textoidl('_{AGN}') ]
        psyms = [1, 1, 1, 1, 0, 0, 0]
        colors = [!blue, !green, !orange, !red, !black, !black, !black]
        linestyles = [0,0,0,0,5,2,4]
        syms = 2*symsize
     endif
                                ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, syms=syms, linsize=lin, back=!white

     if ps eq 1 then ps_end, /png
     if ps eq 1 then print, 'Conversion en png...'

;---------------------------------------
     !x.margin=[10,3]   
     !y.margin=[4,1]

     if ps eq 1 then begin
        ps_start, res+'n='+strtrim(n_criterium,2)+'_corrected_'+nm_ion+nm_htd+'mass_ratios_vs_luminosity'+part+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=20, ysize=20, /helvetica, /bold
        !p.charsize=5 
        !p.charthick=2
        !x.charsize=1
        !y.charsize=1
        !x.thick=30
        !y.thick=30
        !p.thick=4
     endif

     if ionized eq 1 and heated eq 0 then yr = [1d-4,1]
     if ionized eq 0 and heated eq 1 then yr = [1d-6,1]
     if ionized eq 1 and heated eq 1 then yr = [1d-4,2]

     if ps eq 0 then window, 31 ; [1.2,1.8]                                                           ;+repstr(part,'_',' ')
     plot, [44,47], yr, psym=-1, /nodata, xtitle='Log Total AGN Luminosity [erg s'+textoidl('^{-1}')+']', ytitle=title+' mass fraction', /ystyle, /ylog, /xstyle

     psyms=[-1,-2,-4,-5,-6,-7] 

     for kk = 0, n_elements(time_thermal_only)-1 do begin

                                ;plots, 44.25, gas_mass_thermal_only(kk)/gas_mass_no_fb(kk), psym=psym(kk), color=!green, thick=thick, syms=2*symsize

        if n_criterium eq 4 then begin
           if ionized eq 1 then oplot, [44.5,45.5,46.5], [ionized_mass_N4(kk)/corrected_mass_n4(kk),ionized_mass_N4_x10(kk)/corrected_mass_n4_x10(kk),ionized_mass_N4_x100(kk)/corrected_mass_n4_x100(kk)], psym=-3, color=!dgray, thick=thick, line=kk
           if heated eq 1 then oplot, [44.5,45.5,46.5], [heated_sup1K_mass_N4(kk)/corrected_mass_n4(kk),heated_sup1K_mass_N4_x10(kk)/corrected_mass_n4_x10(kk),heated_sup1K_mass_N4_x100(kk)/corrected_mass_n4_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk

           if ionized eq 1 then begin
              plots, 44.5, ionized_mass_N4(kk)/corrected_mass_n4(kk), psym=psyms(kk), color=!red, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, ionized_mass_N4_x10(kk)/corrected_mass_n4_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=1, syms=2*symsize
              plots, 46.5, ionized_mass_N4_x100(kk)/corrected_mass_n4_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, 44.5, heated_sup1K_mass_N4(kk)/corrected_mass_n4(kk), psym=psyms(kk), color=!orange, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, heated_sup1K_mass_N4_x10(kk)/corrected_mass_n4_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=1, syms=2*symsize
              plots, 46.5, heated_sup1K_mass_N4_x100(kk)/corrected_mass_n4_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
           endif
        endif

        if n_criterium eq 1 then begin
           if ionized eq 1 then oplot, [44.5,45.5,46.5], [ionized_mass_N1(kk)/corrected_mass_n1(kk),ionized_mass_N1_x10(kk)/corrected_mass_n1_x10(kk),ionized_mass_N1_x100(kk)/corrected_mass_n1_x100(kk)], psym=-3, color=!dgray, thick=thick, line=kk
           if heated eq 1 then oplot, [44.5,45.5,46.5], [heated_sup1K_mass_N1(kk)/corrected_mass_n1(kk),heated_sup1K_mass_N1_x10(kk)/corrected_mass_n1_x10(kk),heated_sup1K_mass_N1_x100(kk)/corrected_mass_n1_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk
           if ionized eq 1 then begin
              plots, 44.5, ionized_mass_N1(kk)/corrected_mass_n1(kk), psym=psyms(kk), color=!red, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, ionized_mass_N1_x10(kk)/corrected_mass_n1_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=1, syms=2*symsize
              plots, 46.5, ionized_mass_N1_x100(kk)/corrected_mass_n1_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, 44.5, heated_sup1K_mass_N1(kk)/corrected_mass_n1(kk), psym=psyms(kk), color=!orange, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, heated_sup1K_mass_N1_x10(kk)/corrected_mass_n1_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=1, syms=2*symsize
              plots, 46.5, heated_sup1K_mass_N1_x100(kk)/corrected_mass_n1_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
           endif
        endif
     endfor


     if ps eq 1 then ps_end, /png
     if ps eq 1 then print, 'Conversion en png...'
;---------------------------------------
     if ps eq 1 then begin
        ps_start, res+'n='+strtrim(n_criterium,2)+'_corrected_'+nm_ion+nm_htd+'volume_ratios_vs_luminosity'+part+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=20, ysize=20, /helvetica, /bold
        !p.charsize=5 
        !p.charthick=2
        !x.charsize=1
        !y.charsize=1
        !x.thick=30
        !y.thick=30
        !p.thick=4
     endif
     if ps eq 0 then window, 30, ypos=425 ; [1.2,1.8]                                                      +repstr(part,'_',' ')
     plot, [44,47], yr, psym=-1, /nodata, xtitle='Log Total AGN Luminosity [erg s'+textoidl('^{-1}')+']', ytitle=title+' volume fraction', /ystyle, /ylog, /xstyle

     for kk = 0, n_elements(time_thermal_only)-1 do begin

        if n_criterium eq 4 then begin
           if ionized eq 1 then oplot, [44.5,45.5,46.5], [ionized_volume_N4(kk)/corrected_volume_n4(kk),ionized_volume_N4_x10(kk)/corrected_volume_n4_x10(kk),ionized_volume_N4_x100(kk)/corrected_volume_n4_x100(kk)], psym=-3, color=!dgray, thick=thick, line=kk
           if heated eq 1 then oplot, [44.5,45.5,46.5], [heated_sup1K_volume_N4(kk)/corrected_volume_n4(kk),heated_sup1K_volume_N4_x10(kk)/corrected_volume_n4_x10(kk),heated_sup1K_volume_N4_x100(kk)/corrected_volume_n4_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk

           if ionized eq 1 then begin
              plots, 44.5, ionized_volume_N4(kk)/corrected_volume_n4(kk), psym=psyms(kk), color=!red, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, ionized_volume_N4_x10(kk)/corrected_volume_n4_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=1, syms=2*symsize
              plots, 46.5, ionized_volume_N4_x100(kk)/corrected_volume_n4_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, 44.5, heated_sup1K_volume_N4(kk)/corrected_volume_n4(kk), psym=psyms(kk), color=!orange, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, heated_sup1K_volume_N4_x10(kk)/corrected_volume_n4_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=1, syms=2*symsize
              plots, 46.5, heated_sup1K_volume_N4_x100(kk)/corrected_volume_n4_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
           endif
        endif

        if n_criterium eq 1 then begin
           if ionized eq 1 then oplot, [44.5,45.5,46.5], [ionized_volume_N1(kk)/corrected_volume_n1(kk),ionized_volume_N1_x10(kk)/corrected_volume_n1_x10(kk),ionized_volume_N1_x100(kk)/corrected_volume_n1_x100(kk)], psym=-3, color=!dgray, thick=thick, line=kk
           if heated eq 1 then oplot, [44.5,45.5,46.5], [heated_sup1K_volume_N1(kk)/corrected_volume_n1(kk),heated_sup1K_volume_N1_x10(kk)/corrected_volume_n1_x10(kk),heated_sup1K_volume_N1_x100(kk)/corrected_volume_n1_x100(kk)], psym=-3, color=!gray, thick=thick, line=kk

           if ionized eq 1 then begin
              plots, 44.5, ionized_volume_N1(kk)/corrected_volume_n1(kk), psym=psyms(kk), color=!red, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, ionized_volume_N1_x10(kk)/corrected_volume_n1_x10(kk), psym=psyms(kk), color=!red, thick=thick, line=1, syms=2*symsize
              plots, 46.5, ionized_volume_N1_x100(kk)/corrected_volume_n1_x100(kk), psym=psyms(kk), color=!red, thick=thick, line=2, syms=2*symsize
           endif
           if heated eq 1 then begin
              plots, 44.5, heated_sup1K_volume_N1(kk)/corrected_volume_n1(kk), psym=psyms(kk), color=!orange, thick=thick, line=0, syms=2*symsize ;
              plots, 45.5, heated_sup1K_volume_N1_x10(kk)/corrected_volume_n1_x10(kk), psym=psyms(kk), color=!orange, thick=thick, line=1, syms=2*symsize
              plots, 46.5, heated_sup1K_volume_N1_x100(kk)/corrected_volume_n1_x100(kk), psym=psyms(kk), color=!orange, thick=thick, line=2, syms=2*symsize
           endif
        endif
     endfor


     if ps eq 1 then ps_end, /png
     if ps eq 1 then print, 'Conversion en png...'
;---------------------------------------
     
;     stop
;     print, 'pause'
;     read, go_on 
;     
;;heated mass
;     window, 11
;     plot, [0,100], [0.0005,30], psym=-1, /nodata, xtitle='Time (Myrs)', ytitle='% of heated gas mass : comparaison in 4 cells/in 1 cell', /ystyle, /ylog
;     oplot, time_thermal_only, (transpose(final_params(7,*)/final_params(5,*)))*100, psym=-1, color=!magenta, thick=thick, line=0 ; AMR in 4 cells
;     oplot, time_thermal_only, (transpose(final_params_x10(7,*)/final_params_x10(5,*)))*100, psym=-1, color=!magenta, thick=thick, line=1
;     oplot, time_thermal_only, (transpose(final_params_x100(7,*)/final_params_x100(5,*)))*100, psym=-1, color=!magenta, thick=thick, line=2
;     
;;oplot, time_thermal_only, (transpose(final_params(8,*)/final_params(6,*)))*100, psym=-2, color=!dmagenta, thick=thick, line=0 ; CLOUDY in 4 cells
;;oplot, time_thermal_only, (transpose(final_params_x10(8,*)/final_params_x10(6,*)))*100, psym=-2, color=!dmagenta, thick=thick, line=1
;;oplot, time_thermal_only, (transpose(final_params_x100(8,*)/final_params_x100(6,*)))*100, psym=-2, color=!dmagenta, thick=thick, line=2
;;mieux vaut utiliser la densite amr que la densite cloudy pour faire
;;les calculs
;     
;     oplot, time_thermal_only, (transpose(final_params(23,*)/final_params(21,*)))*100, psym=-1, color=!orange, thick=thick, line=0 ; AMR in 1 cell
;     oplot, time_thermal_only, (transpose(final_params_x10(23,*)/final_params_x10(21,*)))*100, psym=-1, color=!orange, thick=thick, line=1
;     oplot, time_thermal_only, (transpose(final_params_x100(23,*)/final_params_x100(21,*)))*100, psym=-1, color=!orange, thick=thick, line=2
;     
;;oplot, time_thermal_only, (transpose(final_params(24,*)/final_params(22,*)))*100, psym=-2, color=!dorange, thick=thick, line=0 ; CLOUDY in 1 cell
;;oplot, time_thermal_only, (transpose(final_params_x10(24,*)/final_params_x10(22,*)))*100, psym=-2, color=!dorange, thick=thick, line=1
;;oplot, time_thermal_only, (transpose(final_params_x100(24,*)/final_params_x100(22,*)))*100, psym=-2, color=!dorange, thick=thick, line=2
;     items = ["Cloudy point in 4 cells" , "Cloudy point in cell", "Seyfert", "Weak Quasar", "Strong Quasar" ]
;     
;     colors = [ !magenta, !orange, !black, !black, !black]   
;     psyms = [1, 1, -1, -1, -1] 
;     lines = [0, 0, 0, 1, 2] 
;     
;     
;                                ; Add the legend.
;     AL_Legend, items,  psym=psyms, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, linsize=0.1, line=lines
;     
;     print, 'pause'
;     read, go_on
;     
                                ;histogramme du sfr
                                ;if ps eq 1 then begin
                                ;   ps_start, sfr_threshold+'_histogram.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=30
                                ;   !p.charsize=4 
                                ;   !p.charthick=2
                                ;endif
                                ;if ps eq 0 then window, 5
                                ;plothist, [15,40], bin=2, /nodata, xtitle='SFR (M_sun/yr) : comparaison sans FB et avec FB (simu)', yr=[0,5], title=name_plot
                                ;if sfr_threshold eq 'sfr100' then plothist, sfr100_no_fb, bin=2, color=!blue, /boxplot, /overplot, thick=thick
                                ;if sfr_threshold eq 'sfr10' then plothist, sfr10_no_fb, bin=2, color=!blue, /boxplot, /overplot, thick=thick
                                ;if sfr_threshold eq 'sfr1' then plothist, sfr1_no_fb, bin=2, color=!blue, /boxplot, /overplot, thick=thick
                                ;if sfr_threshold eq 'sfr0' then plothist, sfr0_no_fb, bin=2, color=!blue, /boxplot, /overplot, thick=thick
                                ;if sfr_threshold eq 'sfr100' then plothist, sfr100_thermal_only, bin=2, color=!green, /overplot, /boxplot, thick=thick
                                ;if sfr_threshold eq 'sfr10' then plothist, sfr10_thermal_only, bin=2, color=!green, /overplot, /boxplot, thick=thick
                                ;if sfr_threshold eq 'sfr1' then plothist, sfr1_thermal_only, bin=2, color=!green, /overplot, /boxplot, thick=thick
                                ;if sfr_threshold eq 'sfr0' then plothist, sfr0_thermal_only, bin=2, color=!green, /overplot, /boxplot, thick=thick
                                ;if ps eq 1 then ps_end, /png
                                ;if ps eq 1 then print, 'Conversion en png...'
     
     
;print, 'pause'
;read, go_on
;
;if ps eq 1 then begin
;        ps_start, res+'number_of_heated_cells.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=30
;        !p.charsize=4 
;        !p.charthick=2
;     endif
;if ps eq 0 then window, 3
;     plot, time_thermal_only, [1e-3,100], /nodata, xtitle='Time (Myrs)', ytitle='% of heated cells : all cells/in 4 cells/in 1 cell', /ylog
;     oplot, time_thermal_only, (transpose(final_params(40,*)/final_params(37,*)))*100,  color=!red , thick=thick, line=0,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x10(40,*)/final_params_x10(37,*)))*100, color=!red, thick=thick, line=1,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x100(40,*)/final_params_x100(37,*)))*100, color=!red, thick=thick, line=2,  psym=-1
;
; oplot, time_thermal_only, (transpose(final_params(41,*)/final_params(38,*)))*100,  color=!magenta , thick=thick, line=0,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x10(41,*)/final_params_x10(38,*)))*100, color=!magenta, thick=thick, line=1,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x100(41,*)/final_params_x100(38,*)))*100, color=!magenta, thick=thick, line=2,  psym=-1
;
; oplot, time_thermal_only, (transpose(final_params(42,*)/final_params(39,*)))*100,  color=!orange , thick=thick, line=0,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x10(42,*)/final_params_x10(39,*)))*100, color=!orange, thick=thick, line=1,  psym=-1
;     oplot, time_thermal_only, (transpose(final_params_x100(42,*)/final_params_x100(39,*)))*100, color=!orange, thick=thick, line=2,  psym=-1
; items = ["All cells", "Cloudy point in 4 cells" , "Cloudy point in cell", "Seyfert", "Weak Quasar", "Strong Quasar" ]
;                                             
;     colors = [!red, !magenta, !orange, !black, !black, !black]   
;     psyms = [1, 1, 1, -1, -1, -1] 
;     lines = [0, 0, 0, 0, 1, 2] 
;                                              
;                                ; Add the legend.
;     AL_Legend, items,  psym=psyms, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, linsize=0.1, line=lines
;
;     if ps eq 1 then ps_end, /png
;     if ps eq 1 then print, 'Conversion en png...'
     
  endif

  stop  
end



;plot de la mediane de la reduction relative du SFR, avec rms en barres d'erreur pour comparer high et lores.
;IDL> window, 1  
;IDL> plot, [0.0014,0.0066,0.023], psym=1, thick=2, xr=[-0.1,2.1], yr=[-0.1,0.1] ;medianes pour x1,x10,x100 hires                       
;IDL> errplot, [0.0014,0.0066,0.023]-[0.0016,0.0088,0.025],  [0.0014,0.0066,0.023]+[0.0016,0.0088,0.025] ;rms pour x1,x10,x100 pour hires
;IDL> oplot, [0.00088,0.0049,0.017], color=!red ;medianes pour x1,x10,x100 lores 
;IDL> errplot, [0.00088,0.0049,0.017]-[0.0049,0.023,0.052], [0.00088,0.0049,0.017]+[0.0049,0.023,0.052], color=!red ;medianes pour x1,x10,x100 lores
;-----> rms plus grande pour lores mais parfaitement compatible.
