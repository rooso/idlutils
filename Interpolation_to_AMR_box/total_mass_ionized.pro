pro total_mass_ionized, factor=factor, ps=ps, sim=sim, disk=disk, reservoir=reservoir, lmax=lmax, boxlen=boxlen

  if not keyword_Set(lmax) then lmax = '13'
  if not keyword_set(boxlen) then boxlen = 50.
  if not keyword_set(factor) then factor = ''
  if not keyword_set(ps) then ps = 0 
  if not keyword_Set(sim) then sim = '00100'
  if not keyword_Set(disk) then disk = 0
  if not keyword_set(reservoir) then reservoir = 0

  plt = 0                       ; afficher les graphes ? 1 : 0

;restreindre le calcul des fractions chauffees/ionisees a 2kpc
;au-dessus et en-dessous du disk ? 1 (oui), 0 (non)

  if not keyword_set(reservoir) then reservoir = 0 ;;;;faire les fractions de masse pour le gaz a 0.3-10 H/cc (reservoir de HI, formerait des * dans 100-200 Myrs)
  if reservoir eq 1 then nreservoir = '_reservoir' else nreservoir = ''

  if not keyword_set(disk) then disk = 0
  if disk eq 1 then ndisk = '_disk' else ndisk = ''

;if reservoir eq 1 then disk = 0 ;;;utile ?

  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
  endif else begin
     nm = ''
  endelse

  true_ps = ps
  true_nm = nm

  if lmax eq '12' then begin
     dir = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/'
     dir_sink = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/'
     sim_with = ['00119', '00178', '00197', '00225']
     sim_no   = ['', '', '', '']
     res='lores_'
  endif else begin
     dir = '/Users/oroos/Post-stage/'
     dir_sink = '/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/'
     sim_with = ['00075w', '00100', '00108', '00150', '00170', '00210']
     sim_no   = ['00075n', '00085', '00090', '00120', '00130', '00148']
     res=''
  endelse
  

;sim_with = sim_no

  for iii = 0,  n_elements(sim_with)-1 do if strmatch(sim, sim_with(iii)) eq 1 then break
;fb_case = 'No' ; 'With'
;lmax='13'

  print, 'Restauration du fichier AMR '+sim_with(iii)
  file_name_amr = dir_sink+'gas_part_'+sim+'.ascii.lmax'+lmax
  file_name_amr_sav = dir+'LOPs'+sim_with(iii)+'/'+res+'amr_'+sim_with(iii)+'_all.sav'
  dat_file_info = file_info(file_name_amr)
  sav_file_info = file_info(file_name_amr_sav)
  if dat_file_info.mtime gt sav_file_info.mtime then begin
     readcol, file_name_amr, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr, format='(D,D,D,D,D,D,D,D,D,I)'
     save,  x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr, file=file_name_amr_sav
  endif else begin
     restore, file_name_amr_sav
  endelse
  print, 'Fin de la restauration du fichier AMR '+sim_with(iii)

;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(boxlen*1d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_amr ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_amr[t_rho]
     t_temp = where(temperature_amr[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_amr[t_rho(t_temp)] = 900
  endif

;/!\/!\/!\/!\/!\/!\ SEULE LA TEMPERATURE (ET DONC LE SFR,
;calcul'e ensuite) A ETE CORRIGEE POUR LE POLYTROPE DE JEANS
;L'IONISATION N'EST PAS CORRIGEE !

  nm = true_nm

  print, 'Restauration du fichier POST-CLOUDY '+nm+sim_with(iii)
  special_dir = '/Users/oroos/Post-stage/'
  file_name_post = findfile(special_dir+nm+res+'cloudy_near_amr_'+sim_with(iii)+'_all.dat',count=nf)
  if nf ne 1 then begin
     special_dir = '/Volumes/TRANSCEND/'
     file_name_post = findfile(special_dir+nm+res+'cloudy_near_amr_'+sim_with(iii)+'_all.dat',count=nf)
     if nf ne 1 then stop
  endif
  file_name_post_sav = '/Volumes/TRANSCEND/'+nm+res+'cloudy_near_'+sim_with(iii)+'_all.sav'
  dat_file_info = file_info(file_name_post)
  sav_file_info = file_info(file_name_post_sav)
  if dat_file_info.mtime gt sav_file_info.mtime then begin
     readcol, file_name_post,  x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A5,A35)'
     save,  x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, file=file_name_post_sav
  endif else begin
     restore, file_name_post_sav
  endelse
  print, 'Fin de la restauration du fichier POST-CLOUDY '+nm+sim_with(iii)
  

;--------------------------------restreindre le calcul aux reservoirs de HI ?
  if reservoir eq 1 then begin

     t_reservoir = where(density_amr ge 0.3 and density_amr le 10, nt_reservoir) ;kpc
     if nt_reservoir gt 0 then begin
        x_amr =   x_amr[t_reservoir]
        y_amr =   y_amr[t_reservoir]
        z_amr =   z_amr[t_reservoir]
        vx_amr =   vx_amr[t_reservoir]
        vy_amr =   vy_amr[t_reservoir]
        vz_amr =   vz_amr[t_reservoir]
        density_amr =   density_amr[t_reservoir]
        temperature_amr =   temperature_amr[t_reservoir]
        cell_size_amr =   cell_size_amr[t_reservoir]
        ilevel_amr =   ilevel_amr[t_reservoir]
        distance_min =   distance_min[t_reservoir]
        x_lop =   x_lop[t_reservoir]
        y_lop =   y_lop[t_reservoir]
        z_lop =   z_lop[t_reservoir]
        theta_lop =   theta_lop[t_reservoir]
        phi_lop =   phi_lop[t_reservoir]
        depth_lop =   depth_lop[t_reservoir]
        density_lop =   density_lop[t_reservoir]
        temperature_post =   temperature_post[t_reservoir]
        Hneutre_lop =   Hneutre_lop[t_reservoir]
        Oneutre_lop =   Oneutre_lop[t_reservoir]
        Hb_4861A_lop =   Hb_4861A_lop[t_reservoir]
        OIII_5007A_lop =   OIII_5007A_lop[t_reservoir]
        Ha_6563A_lop =   Ha_6563A_lop[t_reservoir]
        NII_6584A_lop =   NII_6584A_lop[t_reservoir]
        SFR100_amr =   SFR100_amr[t_reservoir]
        SFR100_cloudy =   SFR100_cloudy[t_reservoir]
        SFR100_hybrid =   SFR100_hybrid[t_reservoir]
        SFR10_amr =   SFR10_amr[t_reservoir]
        SFR10_cloudy =   SFR10_cloudy[t_reservoir]
        SFR10_hybrid =   SFR10_hybrid[t_reservoir]
        SFR1_amr =   SFR1_amr[t_reservoir]
        SFR1_cloudy =   SFR1_cloudy[t_reservoir]
        SFR1_hybrid =   SFR1_hybrid[t_reservoir]
        SFR0_amr =   SFR0_amr[t_reservoir]
        SFR0_cloudy =   SFR0_cloudy[t_reservoir]
        SFR0_hybrid =   SFR0_hybrid[t_reservoir]
        in_cell =   in_cell[t_reservoir]
        flag =   flag[t_reservoir]
        name =   name[t_reservoir]
     endif
  endif



;---------------------------------restreindre le calcul a un tranche
;de 2*2kpc d'epaisseur autour du plan median xy de la boite ???
  if disk eq 1 then begin

     center = boxlen/2.         ;kpc
     thickness = 4d             ;kpc

     t_disk = where(abs(z_amr - center) le thickness/2., nt_disk) ;kpc
     if nt_disk gt 0 then begin
        x_amr =   x_amr[t_disk]
        y_amr =   y_amr[t_disk]
        z_amr =   z_amr[t_disk]
        vx_amr =   vx_amr[t_disk]
        vy_amr =   vy_amr[t_disk]
        vz_amr =   vz_amr[t_disk]
        density_amr =   density_amr[t_disk]
        temperature_amr =   temperature_amr[t_disk]
        cell_size_amr =   cell_size_amr[t_disk]
        ilevel_amr =   ilevel_amr[t_disk]
        distance_min =   distance_min[t_disk]
        x_lop =   x_lop[t_disk]
        y_lop =   y_lop[t_disk]
        z_lop =   z_lop[t_disk]
        theta_lop =   theta_lop[t_disk]
        phi_lop =   phi_lop[t_disk]
        depth_lop =   depth_lop[t_disk]
        density_lop =   density_lop[t_disk]
        temperature_post =   temperature_post[t_disk]
        Hneutre_lop =   Hneutre_lop[t_disk]
        Oneutre_lop =   Oneutre_lop[t_disk]
        Hb_4861A_lop =   Hb_4861A_lop[t_disk]
        OIII_5007A_lop =   OIII_5007A_lop[t_disk]
        Ha_6563A_lop =   Ha_6563A_lop[t_disk]
        NII_6584A_lop =   NII_6584A_lop[t_disk]
        SFR100_amr =   SFR100_amr[t_disk]
        SFR100_cloudy =   SFR100_cloudy[t_disk]
        SFR100_hybrid =   SFR100_hybrid[t_disk]
        SFR10_amr =   SFR10_amr[t_disk]
        SFR10_cloudy =   SFR10_cloudy[t_disk]
        SFR10_hybrid =   SFR10_hybrid[t_disk]
        SFR1_amr =   SFR1_amr[t_disk]
        SFR1_cloudy =   SFR1_cloudy[t_disk]
        SFR1_hybrid =   SFR1_hybrid[t_disk]
        SFR0_amr =   SFR0_amr[t_disk]
        SFR0_cloudy =   SFR0_cloudy[t_disk]
        SFR0_hybrid =   SFR0_hybrid[t_disk]
        in_cell =   in_cell[t_disk]
        flag =   flag[t_disk]
        name =   name[t_disk]
     endif
  endif


  in_cell_all = in_cell

  density_amr_with_all = double(density_amr)
  temperature_amr_with_all = double(temperature_amr)

  cell_size_amr = double(cell_size_amr)

  cell_size_amr_cube = cell_size_amr^3

  Hneutre = double(Hneutre_lop)

  ps = true_ps

  t_4 = where(in_cell_all ge 1, nt_4)
  t_1 = where(in_cell_all eq 1, nt_1)

  log_density_amr_with_all = alog10(density_amr_with_all)


  bins2d = [0.3,0.3]

  mn=min(log_density_amr_with_all)
  mn=[mn,alog10(boxlen/2^lmax)]

  mx=max(log_density_amr_with_all)
  mx=[mx,max(alog10(cell_size_amr))]
  
  test=hist_nd(transpose([[log_density_amr_with_all],[alog10(cell_size_amr)]]),bins2d, min=mn, max=mx, rev=ri)
  test_4=hist_nd(transpose([[log_density_amr_with_all[t_4]],[alog10(cell_size_amr[t_4])]]),bins2d, min=mn, max=mx, rev=ri_4)
  test_1=hist_nd(transpose([[log_density_amr_with_all[t_1]],[alog10(cell_size_amr[t_1])]]),bins2d, min=mn, max=mx, rev=ri_1)
  rgx=[mn(0),mx(0)]
  rgy=[mn(1),mx(1)]


  den_bins2d=dblarr((size(test))[1])
  size_bins2d=dblarr((size(test))[2])

  offset = 0.5
  nbin   = long((mx-mn)/bins2d+1)

  den_bins2d = mn(0) + (LINDGEN(nbin(0))+offset)*bins2d(0)
  size_bins2d = mn(1) + (LINDGEN(nbin(1))+offset)*bins2d(1)

  test_corrected_1 = test_1*0.
  test_corrected_4 = test_4*0.

  correction2d_1 = test_1*0.
  correction2d_4 = test_4*0.

  correction2d_1[where(test_1 ne 0)] = double(test[where(test_1 ne 0)])/double(test_1[where(test_1 ne 0)])
  correction2d_4[where(test_4 ne 0)] = double(test[where(test_4 ne 0)])/double(test_4[where(test_4 ne 0)])

  log_test=alog10(test)
  t=where(test eq 0)
  log_test[t]=-1

  log_test_4=alog10(test_4)
  t=where(test_4 eq 0)
  log_test_4[t]=-1

  log_test_1=alog10(test_1)
  t=where(test_1 eq 0)
  log_test_1[t]=-1

  log_diff_4_corrected=alog10(abs(test-round(test_4*correction2d_4)))
  t=where(abs(test-round(test_4*correction2d_4)) eq 0)
  log_diff_4_corrected[t]=-1

  log_diff_1_corrected=alog10(abs(test-round(test_1*correction2d_1)))
  t=where(abs(test-round(test_1*correction2d_1)) eq 0)
  log_diff_1_corrected[t]=-1

  if max(log_diff_4_corrected) ne -1 then begin
     t = where(in_cell_all eq 0)
     t_8 = where(x_lop[t] ge (x_amr[t]-8*cell_size_amr[t]/2.) and x_lop[t] lt (x_amr[t]+8*cell_size_amr[t]/2.) and y_lop[t] ge (y_amr[t]-8*cell_size_amr[t]/2.) and y_lop[t] lt (y_amr[t]+8*cell_size_amr[t]/2.) and z_lop[t] ge (z_amr[t]-8*cell_size_amr[t]/2.) and z_lop[t] lt (z_amr[t]+8*cell_size_amr[t]/2.))


     test_8=hist_nd(transpose([[log_density_amr_with_all[t_8]],[alog10(cell_size_amr[t_8])]]),bins2d, min=mn, max=mx, rev=ri_8)

     correction2d_8 = test_8*0.

     correction2d_8[where(test_8 ne 0)] = double(test[where(test_8 ne 0)])/double(test_8[where(test_8 ne 0)])


     log_test_8=alog10(test_8)
     t=where(test_8 eq 0)
     log_test_8[t]=-1

     log_diff_8_corrected=alog10(abs(test-round(test_8*correction2d_8)))
     t=where(abs(test-round(test_8*correction2d_8)) eq 0)
     log_diff_8_corrected[t]=-1


  endif else begin
     ri_8 = ri_4
  endelse

  if plt eq 1 then begin
     window, 1 
     loadct,39
     plotimage, log_test, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)'  
     window, 10
     plotimage, log_test_4, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)' 
     window, 11
     plotimage, log_test_1, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : Histogram (logscale)'
     window, 12
     plotimage, log_diff_1_corrected, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : log(ref-corr)'
     window, 13
     plotimage, log_diff_4_corrected, range=[-1,max(log_test)], imgxr=rgx, imgyr=rgy, xtitle='log Density', ytitle='log Cell size', title='Colors : log(ref-corr)'

     window, 2
     plothist, alog10(cell_size_amr), bin=0.3d, title='Cell sizes : all cells/in 4 cells/in 1 cell', /ylog, xr=[-3,0], yr=[1,1e8], /ystyle, min=-7d, omin=mino, omax=maxo, reverse_indices=rev_in, /l64
     plothist, alog10(cell_size_amr), size_bins, sizenumber_per_bin, bin=0.3d, /overplot, min=mino, max=maxo, color=!green
     plothist, alog10(cell_size_amr[t_4]), size4_bins, sizenumber4_per_bin,bin=0.3d, /overplot, min=mino, max=maxo, color=!magenta
     plothist, alog10(cell_size_amr[t_1]), size1_bins, sizenumber1_per_bin,bin=0.3d, /overplot, min=mino, max=maxo, color=!yellow
  endif


  scale_vol = 1.

  test_totrho_1 = 0.
  test_totrho_4 = 0.
  test_totrho = 0.

  total_mass_1 = 0.
  total_mass_4 = 0.
  total_mass = 0.

  total_mass_ionized_1 = 0.
  total_mass_ionized_4 = 0.

  total_mass_heated_1 = 0.
  total_mass_heated_4 = 0.

  total_vol_1 = 0.
  total_vol_4 = 0.
  total_vol = 0.

  total_vol_ionized_1 = 0.
  total_vol_ionized_4 = 0.

  total_vol_heated_1 = 0.
  total_vol_heated_4 = 0.


  print, minmax(Hneutre)        ;/!\ avant correction N=4/N=1 !
  t_05 = where(Hneutre lt 0.5, nt_05)
  t_01 = where(Hneutre lt 0.1, nt_01)
  t_001 = where(Hneutre lt 0.01, nt_001)
  print, nt_05, nt_01, nt_001
  ion_crit = 0.1

  print, minmax(temperature_post)
  t_heat = where(round(temperature_post - temperature_amr_with_all) gt 1, nt_heat)
  print, nt_heat

  window, 1
  plothist, alog10(Hneutre), bin=.1, Hneutre_bins, number_per_bin, /ylog, yr=[1d-1,1d8], min=-9
  tneutre = where(Hneutre lt 0.1,ntneutre)
  if ntneutre gt 0 then plothist, alog10(Hneutre[where(Hneutre lt 0.1)]), bin=.1, /overplot, color=!red 

  window, 2
  plothist, alog10(temperature_post), bin=.1, tpost_bins, tpost_number_per_bin, /ylog, yr=[1d-1,1d8], min=-9
  if nt_heat gt 0 then plothist, alog10(temperature_post[t_heat]), bin=.1, /overplot, color=!red 
  plothist, alog10(temperature_amr_with_all), bin=.1, /overplot, color=!green 
  if nt_heat gt 0 then plothist, alog10(temperature_amr_with_all[t_heat]), bin=.1, /overplot, color=!red, line=1 
  
  cntr4 = 0
  cntr11 = 0                    ; compter le nombre de bins vides corriges par les bins de reference
  cntr12 = 0 

  nb_heated = 0
  nb_heated_1 = 0
  nb_heated_4 = 0

  nb_ionized = 0
  nb_ionized_1 = 0
  nb_ionized_4 = 0

;;;;parametres physiques du gaz chauffe
  T_amr_chauffe_n4 = -42
  T_post_chauffe_n4 = -42
  rho_amr_chauffe_n4 = -42
  size_amr_chauffe_n4 = -42
  sfr100_amr_chauffe_n4 = -42
  sfr10_amr_chauffe_n4 = -42
  sfr1_amr_chauffe_n4 = -42
  sfr0_amr_chauffe_n4 = -42
  sfr100_post_chauffe_n4 = -42
  sfr10_post_chauffe_n4 = -42
  sfr1_post_chauffe_n4 = -42
  sfr0_post_chauffe_n4 = -42
  Hneutre_chauffe_n4 = -42
  T_amr_chauffe_n1 = -42
  T_post_chauffe_n1 = -42
  rho_amr_chauffe_n1 = -42
  size_amr_chauffe_n1 = -42
  sfr100_amr_chauffe_n1 = -42
  sfr10_amr_chauffe_n1 = -42
  sfr1_amr_chauffe_n1 = -42
  sfr0_amr_chauffe_n1 = -42
  sfr100_post_chauffe_n1 = -42
  sfr10_post_chauffe_n1 = -42
  sfr1_post_chauffe_n1 = -42
  sfr0_post_chauffe_n1 = -42
  Hneutre_chauffe_n1 = -42

;;;;parametres physiques du gaz ionise
  T_amr_ionise_n4 = -42
  T_post_ionise_n4 = -42
  rho_amr_ionise_n4 = -42
  size_amr_ionise_n4 = -42
  sfr100_amr_ionise_n4 = -42
  sfr10_amr_ionise_n4 = -42
  sfr1_amr_ionise_n4 = -42
  sfr0_amr_ionise_n4 = -42
  sfr100_post_ionise_n4 = -42
  sfr10_post_ionise_n4 = -42
  sfr1_post_ionise_n4 = -42
  sfr0_post_ionise_n4 = -42
  Hneutre_ionise_n4 = -42
  T_amr_ionise_n1 = -42
  T_post_ionise_n1 = -42
  rho_amr_ionise_n1 = -42
  size_amr_ionise_n1 = -42
  sfr100_amr_ionise_n1 = -42
  sfr10_amr_ionise_n1 = -42
  sfr1_amr_ionise_n1 = -42
  sfr0_amr_ionise_n1 = -42
  sfr100_post_ionise_n1 = -42
  sfr10_post_ionise_n1 = -42
  sfr1_post_ionise_n1 = -42
  sfr0_post_ionise_n1 = -42
  Hneutre_ionise_n1 = -42

  scale_mass = 1.67d-21/(3.24d-20)^3/2.0d30

  nx = (size(test))[1]

  for i=0,n_elements(den_bins2d)-1 do begin
     for j=0,n_elements(size_bins2d)-1 do begin
        ind=[i+nx*j]
;if ri[ind] ne ri[ind+1] then  ri[ri[ind]:ri[ind+1]-1]=42 ;test pour voir si tous les bins sont atteints ; ok
        if ri[ind]   ne ri[ind+1]   then begin
           test_totrho   = test_totrho + total(density_amr_with_all[ri[ri[ind]:ri[ind+1]-1]])
           total_mass    = total_mass  + total(density_amr_with_all[ri[ri[ind]:ri[ind+1]-1]]*cell_size_amr_cube[ri[ri[ind]:ri[ind+1]-1]]*scale_mass)
           total_vol     = total_vol   + total(                                              cell_size_amr_cube[ri[ri[ind]:ri[ind+1]-1]]*scale_vol)
        endif      
        
        if ri_1[ind] ne ri_1[ind+1] then begin
           test_totrho_1 = test_totrho_1 + total(correction2d_1(i,j)*density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])
           total_mass_1  = total_mass_1  + total(correction2d_1(i,j)*density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]*cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]*scale_mass)
           total_vol_1   = total_vol_1   + total(correction2d_1(i,j)                                                         *cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]*scale_vol)
           
           t_ion1 = where(Hneutre[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]] lt ion_crit, nt_ion1)
           if nt_ion1 gt 0 then begin
              total_mass_ionized_1 = total_mass_ionized_1 + total(correction2d_1(i,j)*(density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]*cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]*scale_mass)
              total_vol_ionized_1  = total_vol_ionized_1 +  total(correction2d_1(i,j)*(                           1.                           *cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]*scale_vol)


              if n_elements(T_amr_ionise_n1) eq 1 then begin
                 T_amr_ionise_n1 = (temperature_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 T_post_ionise_n1 = (temperature_post[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 rho_amr_ionise_n1 = (density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 size_amr_ionise_n1 = (cell_size_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr100_amr_ionise_n1 = (SFR100_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr10_amr_ionise_n1 = (SFR10_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr1_amr_ionise_n1 = (SFR1_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr0_amr_ionise_n1 = (SFR0_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr100_post_ionise_n1 = (SFR100_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr10_post_ionise_n1 = (SFR10_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr1_post_ionise_n1 = (SFR1_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 sfr0_post_ionise_n1 = (SFR0_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]
                 Hneutre_ionise_n1 = (Hneutre[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]

              endif else begin
                 T_amr_ionise_n1 = [T_amr_ionise_n1, (temperature_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 T_post_ionise_n1 = [T_post_ionise_n1, (temperature_post[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 rho_amr_ionise_n1 = [rho_amr_ionise_n1, (density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 size_amr_ionise_n1 = [size_amr_ionise_n1, (cell_size_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr100_amr_ionise_n1 = [sfr100_amr_ionise_n1, (SFR100_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr10_amr_ionise_n1 = [sfr10_amr_ionise_n1, (SFR10_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr1_amr_ionise_n1 = [sfr1_amr_ionise_n1, (SFR1_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr0_amr_ionise_n1 = [sfr0_amr_ionise_n1, (SFR0_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr100_post_ionise_n1 = [sfr100_post_ionise_n1, (SFR100_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr10_post_ionise_n1 = [sfr10_post_ionise_n1, (SFR10_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr1_post_ionise_n1 = [sfr1_post_ionise_n1, (SFR1_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 sfr0_post_ionise_n1 = [sfr0_post_ionise_n1, (SFR0_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]
                 Hneutre_ionise_n1 = [Hneutre_ionise_n1, (Hneutre[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_ion1]]

              endelse



           endif         
           t_heated1 = where(round(temperature_post[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]] - temperature_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]) gt 1, nt_heated1)
           if nt_heated1 gt 0 then begin
              total_mass_heated_1 = total_mass_heated_1 + total(correction2d_1(i,j)*(density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]]*cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]*scale_mass)
              total_vol_heated_1  = total_vol_heated_1  + total(correction2d_1(i,j)*(                             1.                         *cell_size_amr_cube[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]*scale_vol)


              if n_elements(T_amr_chauffe_n1) eq 1 then begin
                 T_amr_chauffe_n1 = (temperature_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 T_post_chauffe_n1 = (temperature_post[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 rho_amr_chauffe_n1 = (density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 size_amr_chauffe_n1 = (cell_size_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr100_amr_chauffe_n1 = (SFR100_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr10_amr_chauffe_n1 = (SFR10_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr1_amr_chauffe_n1 = (SFR1_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr0_amr_chauffe_n1 = (SFR0_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr100_post_chauffe_n1 = (SFR100_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr10_post_chauffe_n1 = (SFR10_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr1_post_chauffe_n1 = (SFR1_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 sfr0_post_chauffe_n1 = (SFR0_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]
                 Hneutre_chauffe_n1 = (Hneutre[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]

              endif else begin
                 T_amr_chauffe_n1 = [T_amr_chauffe_n1, (temperature_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 T_post_chauffe_n1 = [T_post_chauffe_n1, (temperature_post[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 rho_amr_chauffe_n1 = [rho_amr_chauffe_n1, (density_amr_with_all[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 size_amr_chauffe_n1 = [size_amr_chauffe_n1, (cell_size_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr100_amr_chauffe_n1 = [sfr100_amr_chauffe_n1, (SFR100_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr10_amr_chauffe_n1 = [sfr10_amr_chauffe_n1, (SFR10_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr1_amr_chauffe_n1 = [sfr1_amr_chauffe_n1, (SFR1_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr0_amr_chauffe_n1 = [sfr0_amr_chauffe_n1, (SFR0_amr[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr100_post_chauffe_n1 = [sfr100_post_chauffe_n1, (SFR100_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr10_post_chauffe_n1 = [sfr10_post_chauffe_n1, (SFR10_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr1_post_chauffe_n1 = [sfr1_post_chauffe_n1, (SFR1_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 sfr0_post_chauffe_n1 = [sfr0_post_chauffe_n1, (SFR0_hybrid[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]
                 Hneutre_chauffe_n1 = [Hneutre_chauffe_n1, (Hneutre[t_1[ri_1[ri_1[ind]:ri_1[ind+1]-1]]])[t_heated1]]

              endelse
           endif       
           
           nb_heated_1 += nt_heated1
           nb_ionized_1 += nt_ion1
           
        endif

        if ri_4[ind] ne ri_4[ind+1] then begin
           
           test_totrho_4 = test_totrho_4 + total(correction2d_4(i,j)*density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])
           total_mass_4  = total_mass_4  + total(correction2d_4(i,j)*density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*scale_mass)
           total_vol_4   = total_vol_4   + total(correction2d_4(i,j)                                                         *cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*scale_vol)
           
           t_ion4 = where(Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]] lt ion_crit, nt_ion4)
           if nt_ion4 gt 0 then begin
              total_mass_ionized_4 = total_mass_ionized_4 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]*scale_mass)
              total_vol_ionized_4  = total_vol_ionized_4  + total(correction2d_4(i,j)*(                        1.                              *cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]*scale_vol)

              if n_elements(T_amr_ionise_n4) eq 1 then begin
                 T_amr_ionise_n4 = (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 T_post_ionise_n4 = (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 rho_amr_ionise_n4 = (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 size_amr_ionise_n4 = (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr100_amr_ionise_n4 = (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr10_amr_ionise_n4 = (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr1_amr_ionise_n4 = (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr0_amr_ionise_n4 = (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr100_post_ionise_n4 = (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr10_post_ionise_n4 = (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr1_post_ionise_n4 = (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 sfr0_post_ionise_n4 = (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]
                 Hneutre_ionise_n4 = (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]

              endif else begin
                 T_amr_ionise_n4 = [T_amr_ionise_n4, (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 T_post_ionise_n4 = [T_post_ionise_n4, (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 rho_amr_ionise_n4 = [rho_amr_ionise_n4, (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 size_amr_ionise_n4 = [size_amr_ionise_n4, (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr100_amr_ionise_n4 = [sfr100_amr_ionise_n4, (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr10_amr_ionise_n4 = [sfr10_amr_ionise_n4, (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr1_amr_ionise_n4 = [sfr1_amr_ionise_n4, (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr0_amr_ionise_n4 = [sfr0_amr_ionise_n4, (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr100_post_ionise_n4 = [sfr100_post_ionise_n4, (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr10_post_ionise_n4 = [sfr10_post_ionise_n4, (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr1_post_ionise_n4 = [sfr1_post_ionise_n4, (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 sfr0_post_ionise_n4 = [sfr0_post_ionise_n4, (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]
                 Hneutre_ionise_n4 = [Hneutre_ionise_n4, (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_ion4]]

              endelse
           endif  
           t_heated4 = where(round(temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]] - temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]) gt 1, nt_heated4)
           if nt_heated4 gt 0 then begin
              total_mass_heated_4 = total_mass_heated_4 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]*scale_mass)
              total_vol_heated_4  = total_vol_heated_4  + total(correction2d_4(i,j)*(                          1.                            *cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]*scale_vol)


              if n_elements(T_amr_chauffe_n4) eq 1 then begin
                 T_amr_chauffe_n4 = (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 T_post_chauffe_n4 = (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 rho_amr_chauffe_n4 = (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 size_amr_chauffe_n4 = (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr100_amr_chauffe_n4 = (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr10_amr_chauffe_n4 = (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr1_amr_chauffe_n4 = (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr0_amr_chauffe_n4 = (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr100_post_chauffe_n4 = (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr10_post_chauffe_n4 = (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr1_post_chauffe_n4 = (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 sfr0_post_chauffe_n4 = (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]
                 Hneutre_chauffe_n4 = (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]

              endif else begin
                 T_amr_chauffe_n4 = [T_amr_chauffe_n4, (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 T_post_chauffe_n4 = [T_post_chauffe_n4, (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 rho_amr_chauffe_n4 = [rho_amr_chauffe_n4, (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 size_amr_chauffe_n4 = [size_amr_chauffe_n4, (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr100_amr_chauffe_n4 = [sfr100_amr_chauffe_n4, (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr10_amr_chauffe_n4 = [sfr10_amr_chauffe_n4, (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr1_amr_chauffe_n4 = [sfr1_amr_chauffe_n4, (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr0_amr_chauffe_n4 = [sfr0_amr_chauffe_n4, (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr100_post_chauffe_n4 = [sfr100_post_chauffe_n4, (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr10_post_chauffe_n4 = [sfr10_post_chauffe_n4, (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr1_post_chauffe_n4 = [sfr1_post_chauffe_n4, (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 sfr0_post_chauffe_n4 = [sfr0_post_chauffe_n4, (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]
                 Hneutre_chauffe_n4 = [Hneutre_chauffe_n4, (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_heated4]]

              endelse
           endif

           nb_heated_4 += nt_heated4
           nb_ionized_4 += nt_ion4

;stop
           
        endif   


        
        further_correction = 1
        if further_correction eq 1 then begin
;comment corriger les bins vides en N=4 et N=1 ?? prendre le bin de densite de reference
           if ri_4[ind] eq ri_4[ind+1] and ri_8[ind] ne ri_8[ind+1] then begin ;si on loupe un bin de densite dans N=4, alors on prend le bin N=8. s'il n'existe pas, tant pis
              test_totrho_4 = test_totrho_4 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=4
              total_mass_4 = total_mass_4 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*scale_mass)
              total_vol_4 = total_vol_4 + total(correction2d_8(i,j)*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*scale_vol)
              
              
              t_ion8 = where(Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]] lt ion_crit, nt_ion8)
              if nt_ion8 gt 0 then total_mass_ionized_4 = total_mass_ionized_4 + total(correction2d_8(i,j)*(density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]*scale_mass)
              if nt_ion8 gt 0 then total_vol_ionized_4 = total_vol_ionized_4 + total(correction2d_8(i,j)*(cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]*scale_vol)
              
              if nt_ion8 gt 0 then begin


                 if n_elements(T_amr_ionise_n4) eq 1 then begin
                    T_amr_ionise_n4 = (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    T_post_ionise_n4 = (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    rho_amr_ionise_n4 = (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    size_amr_ionise_n4 = (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr100_amr_ionise_n4 = (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr10_amr_ionise_n4 = (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr1_amr_ionise_n4 = (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr0_amr_ionise_n4 = (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr100_post_ionise_n4 = (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr10_post_ionise_n4 = (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr1_post_ionise_n4 = (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr0_post_ionise_n4 = (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    Hneutre_ionise_n4 = (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]

                 endif else begin
                    T_amr_ionise_n4 = [T_amr_ionise_n4, (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    T_post_ionise_n4 = [T_post_ionise_n4, (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    rho_amr_ionise_n4 = [rho_amr_ionise_n4, (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    size_amr_ionise_n4 = [size_amr_ionise_n4, (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr100_amr_ionise_n4 = [sfr100_amr_ionise_n4, (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr10_amr_ionise_n4 = [sfr10_amr_ionise_n4, (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr1_amr_ionise_n4 = [sfr1_amr_ionise_n4, (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr0_amr_ionise_n4 = [sfr0_amr_ionise_n4, (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr100_post_ionise_n4 = [sfr100_post_ionise_n4, (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr10_post_ionise_n4 = [sfr10_post_ionise_n4, (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr1_post_ionise_n4 = [sfr1_post_ionise_n4, (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr0_post_ionise_n4 = [sfr0_post_ionise_n4, (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    Hneutre_ionise_n4 = [Hneutre_ionise_n4, (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]

                 endelse

              endif
              
              t_heated8 = where(round(temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]] - temperature_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]) gt 1, nt_heated8)
              if nt_heated8 gt 0 then total_mass_heated_4 = total_mass_heated_4 + total(correction2d_8(i,j)*(density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]*scale_mass)
              if nt_heated8 gt 0 then total_vol_heated_4 = total_vol_heated_4 + total(correction2d_8(i,j)*(cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]*scale_vol)
              

              if nt_heated8 gt 0 then begin

                 if n_elements(T_amr_chauffe_n4) eq 1 then begin
                    T_amr_chauffe_n4 = (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    T_post_chauffe_n4 = (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    rho_amr_chauffe_n4 = (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    size_amr_chauffe_n4 = (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr100_amr_chauffe_n4 = (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr10_amr_chauffe_n4 = (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr1_amr_chauffe_n4 = (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr0_amr_chauffe_n4 = (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr100_post_chauffe_n4 = (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr10_post_chauffe_n4 = (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr1_post_chauffe_n4 = (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr0_post_chauffe_n4 = (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    Hneutre_chauffe_n4 = (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]

                 endif else begin
                    T_amr_chauffe_n4 = [T_amr_chauffe_n4, (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    T_post_chauffe_n4 = [T_post_chauffe_n4, (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    rho_amr_chauffe_n4 = [rho_amr_chauffe_n4, (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    size_amr_chauffe_n4 = [size_amr_chauffe_n4, (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr100_amr_chauffe_n4 = [sfr100_amr_chauffe_n4, (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr10_amr_chauffe_n4 = [sfr10_amr_chauffe_n4, (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr1_amr_chauffe_n4 = [sfr1_amr_chauffe_n4, (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr0_amr_chauffe_n4 = [sfr0_amr_chauffe_n4, (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr100_post_chauffe_n4 = [sfr100_post_chauffe_n4, (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr10_post_chauffe_n4 = [sfr10_post_chauffe_n4, (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr1_post_chauffe_n4 = [sfr1_post_chauffe_n4, (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr0_post_chauffe_n4 = [sfr0_post_chauffe_n4, (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    Hneutre_chauffe_n4 = [Hneutre_chauffe_n4, (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]

                 endelse


              endif
              
              cntr4 += 1
           endif
           
           if ri_1[ind] eq ri_1[ind+1] and ri_4[ind] ne ri_4[ind+1] then begin        ;si on loupe un bin de densite dans N=1, alors on prend le bin amr N=4
              t_41 = where(in_cell_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]] gt 1, nt_41) ; ne pas recompter les cellules deja comptees
              if nt_41 ne 0 then begin
                 
                 test_totrho_1 = test_totrho_1 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=1 
                 total_mass_1 = total_mass_1 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41]*scale_mass)
                 total_vol_1 = total_vol_1 + total(correction2d_4(i,j)*(cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41]*scale_vol)
                 
                 
                 t_ion41 = where((Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41] lt ion_crit, nt_ion41)
                 if nt_ion41 gt 0 then total_mass_ionized_1 = total_mass_ionized_1 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]*scale_mass)
                 if nt_ion41 gt 0 then total_vol_ionized_1 = total_vol_ionized_1 + total(correction2d_4(i,j)*(cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]*scale_vol)

                 if nt_ion41 gt 0 then begin

                    if n_elements(T_amr_ionise_n1) eq 1 then begin
                       T_amr_ionise_n1 = (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       T_post_ionise_n1 = (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       rho_amr_ionise_n1 = (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       size_amr_ionise_n1 = (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr100_amr_ionise_n1 = (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr10_amr_ionise_n1 = (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr1_amr_ionise_n1 = (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr0_amr_ionise_n1 = (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr100_post_ionise_n1 = (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr10_post_ionise_n1 = (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr1_post_ionise_n1 = (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       sfr0_post_ionise_n1 = (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]
                       Hneutre_ionise_n1 = (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]

                    endif else begin
                       T_amr_ionise_n1 = [T_amr_ionise_n1, (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       T_post_ionise_n1 = [T_post_ionise_n1, (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       rho_amr_ionise_n1 = [rho_amr_ionise_n1, (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       size_amr_ionise_n1 = [size_amr_ionise_n1, (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr100_amr_ionise_n1 = [sfr100_amr_ionise_n1, (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr10_amr_ionise_n1 = [sfr10_amr_ionise_n1, (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr1_amr_ionise_n1 = [sfr1_amr_ionise_n1, (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr0_amr_ionise_n1 = [sfr0_amr_ionise_n1, (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr100_post_ionise_n1 = [sfr100_post_ionise_n1, (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr10_post_ionise_n1 = [sfr10_post_ionise_n1, (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr1_post_ionise_n1 = [sfr1_post_ionise_n1, (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       sfr0_post_ionise_n1 = [sfr0_post_ionise_n1, (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]
                       Hneutre_ionise_n1 = [Hneutre_ionise_n1, (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_ion41]]]

                    endelse

                 endif
                 
                 
                 t_heated41 = where(round((temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41] - (temperature_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41]) gt 1, nt_heated41)
                 if nt_heated41 gt 0 then total_mass_heated_1 = total_mass_heated_1 + total(correction2d_4(i,j)*(density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]]*cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]*scale_mass)
                 if nt_heated41 gt 0 then total_vol_heated_1 = total_vol_heated_1 + total(correction2d_4(i,j)*(cell_size_amr_cube[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]*scale_vol)

                 if nt_heated41 gt 0 then begin


                    if n_elements(T_amr_chauffe_n1) eq 1 then begin
                       T_amr_chauffe_n1 = (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       T_post_chauffe_n1 = (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       rho_amr_chauffe_n1 = (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       size_amr_chauffe_n1 = (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr100_amr_chauffe_n1 = (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr10_amr_chauffe_n1 = (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr1_amr_chauffe_n1 = (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr0_amr_chauffe_n1 = (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr100_post_chauffe_n1 = (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr10_post_chauffe_n1 = (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr1_post_chauffe_n1 = (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       sfr0_post_chauffe_n1 = (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]
                       Hneutre_chauffe_n1 = (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]

                    endif else begin
                       T_amr_chauffe_n1 = [T_amr_chauffe_n1, (temperature_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       T_post_chauffe_n1 = [T_post_chauffe_n1, (temperature_post[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       rho_amr_chauffe_n1 = [rho_amr_chauffe_n1, (density_amr_with_all[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       size_amr_chauffe_n1 = [size_amr_chauffe_n1, (cell_size_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr100_amr_chauffe_n1 = [sfr100_amr_chauffe_n1, (SFR100_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr10_amr_chauffe_n1 = [sfr10_amr_chauffe_n1, (SFR10_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr1_amr_chauffe_n1 = [sfr1_amr_chauffe_n1, (SFR1_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr0_amr_chauffe_n1 = [sfr0_amr_chauffe_n1, (SFR0_amr[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr100_post_chauffe_n1 = [sfr100_post_chauffe_n1, (SFR100_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr10_post_chauffe_n1 = [sfr10_post_chauffe_n1, (SFR10_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr1_post_chauffe_n1 = [sfr1_post_chauffe_n1, (SFR1_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       sfr0_post_chauffe_n1 = [sfr0_post_chauffe_n1, (SFR0_hybrid[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]
                       Hneutre_chauffe_n1 = [Hneutre_chauffe_n1, (Hneutre[t_4[ri_4[ri_4[ind]:ri_4[ind+1]-1]]])[t_41[t_heated41]]]

                    endelse

                 endif
                 
                 cntr11 += 1
                 
              endif
           endif
           
           if ri_1[ind] eq ri_1[ind+1] and ri_4[ind] eq ri_4[ind+1] and ri_8[ind] ne ri_8[ind+1] then begin ;si on loupe un bin de densite dans N=1 et N=4, alors on prend le bin N=8. s'il n'existe pas, tant pis (tres probablement des bins de tres faible densite qui ne contribuent pas au sfr)
              test_totrho_1 = test_totrho_1 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]) ;dans ce bin-la, on prend la temperature d'un point situe un peu plus loin que N=1 ou 4
              total_mass_1 = total_mass_1 + total(correction2d_8(i,j)*density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*scale_mass)
              total_vol_1 = total_vol_1 + total(correction2d_8(i,j)*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*scale_vol) 
              
              
              if nt_ion8 gt 0 then total_mass_ionized_1 = total_mass_ionized_1 + total(correction2d_8(i,j)*(density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]*scale_mass)
              if nt_ion8 gt 0 then total_vol_ionized_1 = total_vol_ionized_1 + total(correction2d_8(i,j)*(cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]*scale_vol)

              if nt_ion8 gt 0 then begin

                 if n_elements(T_amr_ionise_n1) eq 1 then begin
                    T_amr_ionise_n1 = (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    T_post_ionise_n1 = (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    rho_amr_ionise_n1 = (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    size_amr_ionise_n1 = (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr100_amr_ionise_n1 = (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr10_amr_ionise_n1 = (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr1_amr_ionise_n1 = (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr0_amr_ionise_n1 = (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr100_post_ionise_n1 = (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr10_post_ionise_n1 = (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr1_post_ionise_n1 = (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    sfr0_post_ionise_n1 = (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]
                    Hneutre_ionise_n1 = (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]

                 endif else begin
                    T_amr_ionise_n1 = [T_amr_ionise_n1, (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    T_post_ionise_n1 = [T_post_ionise_n1, (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    rho_amr_ionise_n1 = [rho_amr_ionise_n1, (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    size_amr_ionise_n1 = [size_amr_ionise_n1, (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr100_amr_ionise_n1 = [sfr100_amr_ionise_n1, (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr10_amr_ionise_n1 = [sfr10_amr_ionise_n1, (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr1_amr_ionise_n1 = [sfr1_amr_ionise_n1, (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr0_amr_ionise_n1 = [sfr0_amr_ionise_n1, (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr100_post_ionise_n1 = [sfr100_post_ionise_n1, (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr10_post_ionise_n1 = [sfr10_post_ionise_n1, (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr1_post_ionise_n1 = [sfr1_post_ionise_n1, (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    sfr0_post_ionise_n1 = [sfr0_post_ionise_n1, (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]
                    Hneutre_ionise_n1 = [Hneutre_ionise_n1, (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_ion8]]

                 endelse

              endif

              
              if nt_heated8 gt 0 then total_mass_heated_1 = total_mass_heated_1 + total(correction2d_8(i,j)*(density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]]*cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]*scale_mass)
              if nt_heated8 gt 0 then total_vol_heated_1 = total_vol_heated_1 + total(correction2d_8(i,j)*(cell_size_amr_cube[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]*scale_vol)
              
              
              if nt_heated8 gt 0 then begin


                 if n_elements(T_amr_chauffe_n1) eq 1 eq -42 then begin
                    T_amr_chauffe_n1 = (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    T_post_chauffe_n1 = (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    rho_amr_chauffe_n1 = (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    size_amr_chauffe_n1 = (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr100_amr_chauffe_n1 = (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr10_amr_chauffe_n1 = (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr1_amr_chauffe_n1 = (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr0_amr_chauffe_n1 = (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr100_post_chauffe_n1 = (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr10_post_chauffe_n1 = (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr1_post_chauffe_n1 = (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    sfr0_post_chauffe_n1 = (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]
                    Hneutre_chauffe_n1 = (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]

                 endif else begin
                    T_amr_chauffe_n1 = [T_amr_chauffe_n1, (temperature_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    T_post_chauffe_n1 = [T_post_chauffe_n1, (temperature_post[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    rho_amr_chauffe_n1 = [rho_amr_chauffe_n1, (density_amr_with_all[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    size_amr_chauffe_n1 = [size_amr_chauffe_n1, (cell_size_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr100_amr_chauffe_n1 = [sfr100_amr_chauffe_n1, (SFR100_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr10_amr_chauffe_n1 = [sfr10_amr_chauffe_n1, (SFR10_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr1_amr_chauffe_n1 = [sfr1_amr_chauffe_n1, (SFR1_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr0_amr_chauffe_n1 = [sfr0_amr_chauffe_n1, (SFR0_amr[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr100_post_chauffe_n1 = [sfr100_post_chauffe_n1, (SFR100_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr10_post_chauffe_n1 = [sfr10_post_chauffe_n1, (SFR10_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr1_post_chauffe_n1 = [sfr1_post_chauffe_n1, (SFR1_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    sfr0_post_chauffe_n1 = [sfr0_post_chauffe_n1, (SFR0_hybrid[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]
                    Hneutre_chauffe_n1 = [Hneutre_chauffe_n1, (Hneutre[t_8[ri_8[ri_8[ind]:ri_8[ind+1]-1]]])[t_heated8]]

                 endelse

              endif
              cntr12 += 1
           endif
           
        endif 
     endfor


     print, fix(float(i+1)/n_elements(den_bins2d)*100), ' % proceeded.'
;stop
  endfor                        ;version boucle des lignes precedentes

  
  print, cntr11, cntr12, cntr4
  print, total(density_amr_with_all), test_totrho, test_totrho_1, test_totrho_4                          ;not physical
  print, total(density_amr_with_all*cell_size_amr_cube*scale_mass), total_mass, total_mass_1, total_mass_4 ;M_sun
  print, total_mass_ionized_1, total_mass_ionized_4                                                        ;M_sun
  print, total_mass_ionized_1/total_mass_1*100., total_mass_ionized_4/total_mass_4*100., ' %.'
  print, total_mass_heated_1, total_mass_heated_4 ;M_sun
  print, total_mass_heated_1/total_mass_1*100., total_mass_heated_4/total_mass_4*100., ' %.'

  print, total(cell_size_amr_cube*scale_vol), total_vol, total_vol_1, total_vol_4 ;kpc3
  print, total_vol_ionized_1, total_vol_ionized_4                                 ;kpc3
  print, total_vol_ionized_1/total_vol_1*100., total_vol_ionized_4/total_vol_4*100., ' %.'
  print, total_vol_heated_1, total_vol_heated_4 ;kpc3
  print, total_vol_heated_1/total_vol_1*100., total_vol_heated_4/total_vol_4*100., ' %.'

  print, nb_heated_1, nb_ionized_1 ;nb correspondent aux t = where(in_cell ge/eq 1 and temperature_post gt temperature_amr/Hneutre lt ion_crit)
  print, nb_heated_4, nb_ionized_4 
;stop

  openw, lun, nm+res+'corrected_total_mass_'+sim_with(iii)+'-'+sim_no(iii)+ndisk+nreservoir+'.txt', /get_lun
  printf, lun, 'Sum of density_with : ', total(density_amr_with_all), ' H/cc'
  printf, lun, 'Sum of corrected_density_with : ', test_totrho, ' H/cc'
  printf, lun, 'Sum of corrected_density_with1 : ', test_totrho_1, ' H/cc'
  printf, lun, 'Sum of corrected_density_with4 : ', test_totrho_4, ' H/cc' ;not physical
  printf, lun, 'Total mass_with : ', total(density_amr_with_all*cell_size_amr_cube*scale_mass), ' M_sun'
  printf, lun, 'Total corrected_mass_with : ', total_mass,   ' M_sun'
  printf, lun, 'Total corrected_mass_with1 : ', total_mass_1,  ' M_sun'
  printf, lun, 'Total corrected_mass_with4 : ', total_mass_4,  ' M_sun' ;M_sun
  printf, lun, 'Total corrected_ionized_mass_with1 : ', total_mass_ionized_1, ' M_sun'
  printf, lun, 'Total corrected_ionized_mass_with4 : ', total_mass_ionized_4, ' M_sun' ;M_sun
  printf, lun, 'Fraction of ionized mass (corrected_1) : ', total_mass_ionized_1/total_mass_1*100., ' %'
  printf, lun, 'Fraction of ionized mass (corrected_4) : ', total_mass_ionized_4/total_mass_4*100., ' %.'
  printf, lun, 'Total corrected_heated>1K_mass_with1 : ', total_mass_heated_1, ' M_sun'
  printf, lun, 'Total corrected_heated>1K_mass_with4 : ', total_mass_heated_4, ' M_sun' ;M_sun
  printf, lun, 'Fraction of heated>1K mass (corrected_1) : ', total_mass_heated_1/total_mass_1*100., ' %.' 
  printf, lun, 'Fraction of heated>1K mass (corrected_4) : ', total_mass_heated_4/total_mass_4*100., ' %.'
  printf, lun, 'Total volume_with of cube : ', total(cell_size_amr_cube*scale_vol), ' kpc^3'
  printf, lun, 'Total corrected_volume_with of cube : ', total_vol, ' kpc^3'
  printf, lun, 'Total corrected_volume_with1 of cube : ', total_vol_1, ' kpc^3'
  printf, lun, 'Total corrected_volume_with4 of cube : ', total_vol_4, ' kpc^3'
  printf, lun, 'Total corrected_ionized_volume_with1 of cube : ', total_vol_ionized_1, ' kpc^3'
  printf, lun, 'Total corrected_ionized_volume_with4 of cube : ', total_vol_ionized_4, ' kpc^3' 
  printf, lun, 'Fraction of ionized volume (corrected_1) : ', total_vol_ionized_1/total_vol_1*100., ' %.'
  printf, lun, 'Fraction of ionized volume (corrected_4) : ', total_vol_ionized_4/total_vol_4*100., ' %.'
  printf, lun, 'Total corrected_heated>1K_volume_with1 of cube : ', total_vol_heated_1, ' kpc^3'
  printf, lun, 'Total corrected_heated>1K_volume_with1 of cube : ', total_vol_heated_4, ' kpc^3'
  printf, lun, 'Fraction of heated>1K volume (corrected_1) : ', total_vol_heated_1/total_vol_1*100., ' %.'
  printf, lun, 'Fraction of heated>1K volume (corrected_4) : ', total_vol_heated_4/total_vol_4*100., ' %.'
  close, lun
  free_lun, lun


  print, avg(T_amr_chauffe_n4), minmax(T_amr_chauffe_n4)
  print, avg(T_post_chauffe_n4), minmax(T_post_chauffe_n4)
  print, avg(T_post_chauffe_n4-T_amr_chauffe_n4), minmax(T_post_chauffe_n4-T_amr_chauffe_n4)
  print, avg(rho_amr_chauffe_n4), minmax(rho_amr_chauffe_n4)
  print, avg(size_amr_chauffe_n4), minmax(size_amr_chauffe_n4)
  print, avg(sfr100_amr_chauffe_n4), minmax(sfr100_amr_chauffe_n4)
  print, avg(sfr10_amr_chauffe_n4), minmax(sfr10_amr_chauffe_n4)
  print, avg(sfr1_amr_chauffe_n4), minmax(sfr1_amr_chauffe_n4)
  print, avg(sfr0_amr_chauffe_n4), minmax(sfr0_amr_chauffe_n4)
  print, avg(sfr100_post_chauffe_n4), minmax(sfr100_post_chauffe_n4)
  print, avg(sfr10_post_chauffe_n4), minmax(sfr10_post_chauffe_n4)
  print, avg(sfr1_post_chauffe_n4), minmax(sfr1_post_chauffe_n4)
  print, avg(sfr0_post_chauffe_n4), minmax(sfr0_post_chauffe_n4)
  print, avg(Hneutre_chauffe_n4), minmax(Hneutre_chauffe_n4)
  print, avg(T_amr_chauffe_n1), minmax(T_amr_chauffe_n1)
  print, avg(T_post_chauffe_n1), minmax(T_post_chauffe_n1)
  print, avg(T_post_chauffe_n1-T_amr_chauffe_n1), minmax(T_post_chauffe_n1-T_amr_chauffe_n1)
  print, avg(rho_amr_chauffe_n1), minmax(rho_amr_chauffe_n1)
  print, avg(size_amr_chauffe_n1), minmax(size_amr_chauffe_n1)
  print, avg(sfr100_amr_chauffe_n1), minmax(sfr100_amr_chauffe_n1)
  print, avg(sfr10_amr_chauffe_n1), minmax(sfr10_amr_chauffe_n1)
  print, avg(sfr1_amr_chauffe_n1), minmax(sfr1_amr_chauffe_n1)
  print, avg(sfr0_amr_chauffe_n1), minmax(sfr0_amr_chauffe_n1)
  print, avg(sfr100_post_chauffe_n1), minmax(sfr100_post_chauffe_n1)
  print, avg(sfr10_post_chauffe_n1), minmax(sfr10_post_chauffe_n1)
  print, avg(sfr1_post_chauffe_n1), minmax(sfr1_post_chauffe_n1)
  print, avg(sfr0_post_chauffe_n1), minmax(sfr0_post_chauffe_n1)
  print, avg(Hneutre_chauffe_n1), minmax(Hneutre_chauffe_n1)


  print, avg(T_amr_ionise_n4), minmax(T_amr_ionise_n4)
  print, avg(T_post_ionise_n4), minmax(T_post_ionise_n4)
  print, avg(T_post_ionise_n4-T_amr_ionise_n4), minmax(T_post_ionise_n4-T_amr_ionise_n4)
  print, avg(rho_amr_ionise_n4), minmax(rho_amr_ionise_n4)
  print, avg(size_amr_ionise_n4), minmax(size_amr_ionise_n4)
  print, avg(sfr100_amr_ionise_n4), minmax(sfr100_amr_ionise_n4)
  print, avg(sfr10_amr_ionise_n4), minmax(sfr10_amr_ionise_n4)
  print, avg(sfr1_amr_ionise_n4), minmax(sfr1_amr_ionise_n4)
  print, avg(sfr0_amr_ionise_n4), minmax(sfr0_amr_ionise_n4)
  print, avg(sfr100_post_ionise_n4), minmax(sfr100_post_ionise_n4)
  print, avg(sfr10_post_ionise_n4), minmax(sfr10_post_ionise_n4)
  print, avg(sfr1_post_ionise_n4), minmax(sfr1_post_ionise_n4)
  print, avg(sfr0_post_ionise_n4), minmax(sfr0_post_ionise_n4)
  print, avg(Hneutre_ionise_n4), minmax(Hneutre_ionise_n4)
  print, avg(T_amr_ionise_n1), minmax(T_amr_ionise_n1)
  print, avg(T_post_ionise_n1), minmax(T_post_ionise_n1)
  print, avg(T_post_ionise_n1-T_amr_ionise_n1), minmax(T_post_ionise_n1-T_amr_ionise_n1)
  print, avg(rho_amr_ionise_n1), minmax(rho_amr_ionise_n1)
  print, avg(size_amr_ionise_n1), minmax(size_amr_ionise_n1)
  print, avg(sfr100_amr_ionise_n1), minmax(sfr100_amr_ionise_n1)
  print, avg(sfr10_amr_ionise_n1), minmax(sfr10_amr_ionise_n1)
  print, avg(sfr1_amr_ionise_n1), minmax(sfr1_amr_ionise_n1)
  print, avg(sfr0_amr_ionise_n1), minmax(sfr0_amr_ionise_n1)
  print, avg(sfr100_post_ionise_n1), minmax(sfr100_post_ionise_n1)
  print, avg(sfr10_post_ionise_n1), minmax(sfr10_post_ionise_n1)
  print, avg(sfr1_post_ionise_n1), minmax(sfr1_post_ionise_n1)
  print, avg(sfr0_post_ionise_n1), minmax(sfr0_post_ionise_n1)
  print, avg(Hneutre_ionise_n1), minmax(Hneutre_ionise_n1)


  openw, lun, nm+res+'heated_mass_n=4_'+sim_with(iii)+'-'+sim_no(iii)+ndisk+nreservoir+'.txt', /get_lun
  printf, lun, 'Parameter   Average   Minimum   Maximum'
  printf, lun, 'T_amr ', avg(T_amr_chauffe_n4), minmax(T_amr_chauffe_n4)
  printf, lun, 'T_post ', avg(T_post_chauffe_n4), minmax(T_post_chauffe_n4)
  printf, lun, 'T_post-T_amr ', avg(T_post_chauffe_n4-T_amr_chauffe_n4), minmax(T_post_chauffe_n4-T_amr_chauffe_n4)
  printf, lun, 'rho_amr ', avg(rho_amr_chauffe_n4), minmax(rho_amr_chauffe_n4)
  printf, lun, 'size_amr ', avg(size_amr_chauffe_n4), minmax(size_amr_chauffe_n4)
  printf, lun, 'SFR100_amr ', avg(sfr100_amr_chauffe_n4), minmax(sfr100_amr_chauffe_n4)
  printf, lun, 'SFR10_amr ', avg(sfr10_amr_chauffe_n4), minmax(sfr10_amr_chauffe_n4)
  printf, lun, 'SFR1_amr ', avg(sfr1_amr_chauffe_n4), minmax(sfr1_amr_chauffe_n4)
  printf, lun, 'SFR0_amr ', avg(sfr0_amr_chauffe_n4), minmax(sfr0_amr_chauffe_n4)
  printf, lun, 'SFR100_post ', avg(sfr100_post_chauffe_n4), minmax(sfr100_post_chauffe_n4)
  printf, lun, 'SFR10_post ', avg(sfr10_post_chauffe_n4), minmax(sfr10_post_chauffe_n4)
  printf, lun, 'SFR1_post ', avg(sfr1_post_chauffe_n4), minmax(sfr1_post_chauffe_n4)
  printf, lun, 'SFR0_post ', avg(sfr0_post_chauffe_n4), minmax(sfr0_post_chauffe_n4)
  printf, lun, 'Hneutre ', avg(Hneutre_chauffe_n4), minmax(Hneutre_chauffe_n4)
  close, lun
  free_lun, lun
  openw, lun, nm+res+'heated_mass_n=1_'+sim_with(iii)+'-'+sim_no(iii)+ndisk+nreservoir+'.txt', /get_lun
  printf, lun, 'Parameter   Average   Minimum   Maximum'
  printf, lun, 'T_amr ', avg(T_amr_chauffe_n1), minmax(T_amr_chauffe_n1)
  printf, lun, 'T_post ', avg(T_post_chauffe_n1), minmax(T_post_chauffe_n1)
  printf, lun, 'T_post-T_amr ', avg(T_post_chauffe_n1-T_amr_chauffe_n1), minmax(T_post_chauffe_n1-T_amr_chauffe_n1)
  printf, lun, 'rho_amr ', avg(rho_amr_chauffe_n1), minmax(rho_amr_chauffe_n1)
  printf, lun, 'size_amr ', avg(size_amr_chauffe_n1), minmax(size_amr_chauffe_n1)
  printf, lun, 'SFR100_amr ', avg(sfr100_amr_chauffe_n1), minmax(sfr100_amr_chauffe_n1)
  printf, lun, 'SFR10_amr ', avg(sfr10_amr_chauffe_n1), minmax(sfr10_amr_chauffe_n1)
  printf, lun, 'SFR1_amr ', avg(sfr1_amr_chauffe_n1), minmax(sfr1_amr_chauffe_n1)
  printf, lun, 'SFR0_amr ', avg(sfr0_amr_chauffe_n1), minmax(sfr0_amr_chauffe_n1)
  printf, lun, 'SFR100_post ', avg(sfr100_post_chauffe_n1), minmax(sfr100_post_chauffe_n1)
  printf, lun, 'SFR10_post ', avg(sfr10_post_chauffe_n1), minmax(sfr10_post_chauffe_n1)
  printf, lun, 'SFR1_post ', avg(sfr1_post_chauffe_n1), minmax(sfr1_post_chauffe_n1)
  printf, lun, 'SFR0_post ', avg(sfr0_post_chauffe_n1), minmax(sfr0_post_chauffe_n1)
  printf, lun, 'Hneutre ', avg(Hneutre_chauffe_n1), minmax(Hneutre_chauffe_n1)
  close, lun
  free_lun, lun

  openw, lun, nm+res+'ionized_mass_n=4_'+sim_with(iii)+'-'+sim_no(iii)+ndisk+nreservoir+'.txt', /get_lun
  printf, lun, 'Parameter   Average   Minimum   Maximum'
  printf, lun, 'T_amr ', avg(T_amr_ionise_n4), minmax(T_amr_ionise_n4)
  printf, lun, 'T_post ', avg(T_post_ionise_n4), minmax(T_post_ionise_n4)
  printf, lun, 'T_post-T_amr ', avg(T_post_ionise_n4-T_amr_ionise_n4), minmax(T_post_ionise_n4-T_amr_ionise_n4)
  printf, lun, 'rho_amr ', avg(rho_amr_ionise_n4), minmax(rho_amr_ionise_n4)
  printf, lun, 'size_amr ', avg(size_amr_ionise_n4), minmax(size_amr_ionise_n4)
  printf, lun, 'SFR100_amr ', avg(sfr100_amr_ionise_n4), minmax(sfr100_amr_ionise_n4)
  printf, lun, 'SFR10_amr ', avg(sfr10_amr_ionise_n4), minmax(sfr10_amr_ionise_n4)
  printf, lun, 'SFR1_amr ', avg(sfr1_amr_ionise_n4), minmax(sfr1_amr_ionise_n4)
  printf, lun, 'SFR0_amr ', avg(sfr0_amr_ionise_n4), minmax(sfr0_amr_ionise_n4)
  printf, lun, 'SFR100_post ', avg(sfr100_post_ionise_n4), minmax(sfr100_post_ionise_n4)
  printf, lun, 'SFR10_post ', avg(sfr10_post_ionise_n4), minmax(sfr10_post_ionise_n4)
  printf, lun, 'SFR1_post ', avg(sfr1_post_ionise_n4), minmax(sfr1_post_ionise_n4)
  printf, lun, 'SFR0_post ', avg(sfr0_post_ionise_n4), minmax(sfr0_post_ionise_n4)
  printf, lun, 'Hneutre ', avg(Hneutre_ionise_n4), minmax(Hneutre_ionise_n4)
  close, lun
  free_lun, lun
  openw, lun, nm+res+'ionized_mass_n=1_'+sim_with(iii)+'-'+sim_no(iii)+ndisk+nreservoir+'.txt', /get_lun
  printf, lun, 'Parameter   Average   Minimum   Maximum'
  printf, lun, 'T_amr ', avg(T_amr_ionise_n1), minmax(T_amr_ionise_n1)
  printf, lun, 'T_post ', avg(T_post_ionise_n1), minmax(T_post_ionise_n1)
  printf, lun, 'T_post-T_amr ', avg(T_post_ionise_n1-T_amr_ionise_n1), minmax(T_post_ionise_n1-T_amr_ionise_n1)
  printf, lun, 'rho_amr ', avg(rho_amr_ionise_n1), minmax(rho_amr_ionise_n1)
  printf, lun, 'size_amr ', avg(size_amr_ionise_n1), minmax(size_amr_ionise_n1)
  printf, lun, 'SFR100_amr ', avg(sfr100_amr_ionise_n1), minmax(sfr100_amr_ionise_n1)
  printf, lun, 'SFR10_amr ', avg(sfr10_amr_ionise_n1), minmax(sfr10_amr_ionise_n1)
  printf, lun, 'SFR1_amr ', avg(sfr1_amr_ionise_n1), minmax(sfr1_amr_ionise_n1)
  printf, lun, 'SFR0_amr ', avg(sfr0_amr_ionise_n1), minmax(sfr0_amr_ionise_n1)
  printf, lun, 'SFR100_post ', avg(sfr100_post_ionise_n1), minmax(sfr100_post_ionise_n1)
  printf, lun, 'SFR10_post ', avg(sfr10_post_ionise_n1), minmax(sfr10_post_ionise_n1)
  printf, lun, 'SFR1_post ', avg(sfr1_post_ionise_n1), minmax(sfr1_post_ionise_n1)
  printf, lun, 'SFR0_post ', avg(sfr0_post_ionise_n1), minmax(sfr0_post_ionise_n1)
  printf, lun, 'Hneutre ', avg(Hneutre_ionise_n1), minmax(Hneutre_ionise_n1)
  close, lun
  free_lun, lun


  window, 11
  plothist, alog10(Hneutre), bin=.1, Hneutre_bins, number_per_bin, /ylog, yr=[1d-1,1d8], min=-9      
  plothist, alog10(Hneutre[t_4]), bin=.1, /overplot, color=!gray
  if n_elements(Hneutre_ionise_n4) gt 1 then plothist, alog10(Hneutre_ionise_n4), bin=.1, /overplot, color=!red
  if n_elements(Hneutre_chauffe_n4) gt 1 then plothist, alog10(Hneutre_chauffe_n4), bin=.1, /overplot, color=!orange  

  window, 10            
  plothist, alog10(temperature_post[t_4]), bin=.1, tpost_bins, tpost_number_per_bin, /ylog, yr=[1d-1,1d8], min=-9
  plothist, alog10(temperature_amr_with_all[t_4]), bin=.1, /overplot, color=!green                                                                     
  if n_elements(t_amr_ionise_n4) gt 1 then plothist, alog10(t_amr_ionise_n4), bin=.1, /overplot, color=!red, line=1   
  if n_elements(t_post_ionise_n4) gt 1 then plothist, alog10(t_post_ionise_n4), bin=.1, /overplot, color=!red        
  if n_elements(t_amr_chauffe_n4) gt 1 then plothist, alog10(t_amr_chauffe_n4), bin=.1, /overplot, color=!orange, line=1
  if n_elements(t_post_chauffe_n4) gt 1 then plothist, alog10(t_post_chauffe_n4), bin=.1, /overplot, color=!orange

  stop

;stop
end
