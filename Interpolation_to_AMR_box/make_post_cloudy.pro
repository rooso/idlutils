pro make_post_cloudy

post_cloudy_parameters, s='00100', pa='all'
post_cloudy_parameters, s='00085', pa='all'
post_cloudy_parameters, s='00075w', pa='all'
post_cloudy_parameters, s='00075n', pa='all'

post_cloudy_parameters, s='00100', pa='all', f='10'
post_cloudy_parameters, s='00085', pa='all', f='10'
post_cloudy_parameters, s='00075w', pa='all', f='10'
post_cloudy_parameters, s='00075n', pa='all', f='10'

post_cloudy_parameters, s='00100', pa='all', f='100'
post_cloudy_parameters, s='00085', pa='all', f='100'
post_cloudy_parameters, s='00075w', pa='all', f='100'
post_cloudy_parameters, s='00075n', pa='all', f='100'

end
