pro post_cloudy_parameters, sim=sim, part=part, factor=factor, lmax=lmax, boxlen=boxlen
  
;boxlen in kpc !!!
  if not keyword_set(lmax) then lmax = 13
  if not keyword_set(boxlen) then boxlen = 50.
  
  res = ''
  
  if sim eq '00100' or sim eq '00075w' then fb_case = 'With'
  if sim eq '00085' or sim eq '00075n' then fb_case = 'No'
  
  if sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then fb_case = 'With'
  
  sm = sim
  
  if sim eq '00075w' or sim eq '00075n' then sm='00075'
  
  if lmax eq 12 then begin                                    ;/!\ dossier Done !
     dir = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/'; '/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/'
     dir_sink = dir+'output/'
     res = 'lores_'
  endif else begin
  dir = '/ccc/work/cont003/dsm/rooso/Tests_c13.02/'
  dir_sink = str_replace(dir, 'Tests_c13.02/', 'orianne_data/'+fb_case+'_AGN_feedback/')
endelse
  
  file_sink = dir_sink+'output_'+sm+'/sink_'+sm+'.out'
  readcol, file_sink,  var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  
  x_agn = x_center(0)
  y_agn = y_center(0)
  z_agn = z_center(0)
  
  print, x_agn, y_agn, z_agn
  
  
  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
     openw, out, dir+res+'post_cloudy_parameters_'+nm+sim+'_'+part+'.dat', /get_lun
     openw, out_ascii, dir+res+'ascii_parameters_'+nm+sim+'_'+part+'.dat', /get_lun
  endif else begin
     factor = ''
     nm = ''
     openw, out, dir+res+'post_cloudy_parameters_'+sim+'_'+part+'.dat', /get_lun
     openw, out_ascii, dir+res+'ascii_parameters_'+sim+'_'+part+'.dat', /get_lun
  endelse
  
  if part ne 'all' then begin
     files = findfile(dir+'LOPs'+sim+'/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part+'*.phyc',count=n_files)
     files = [files, findfile(dir+'Smoothed_sww/Done*/'+nm+res+'density_profile_'+sim+'_LOP*'+part+'*.phyc',count=n_files2)]
     files = [files, findfile(dir+'Rerun_sup24h/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part+'*.phyc',count=n_files3)]
     files_relaunched = findfile(dir+'Extend_trunc500/Done*/relaunch_'+nm+res+'density_profile_'+sim+'_LOP*'+part+'*.phyc',count=n_files_relaunched)
     n_files = n_files + n_files2 + n_files3
     print, "Il y a ", n_files, ' fichiers pour la simulation ', res+sim+part+nm,', dont ', n_files2, ' profils lisses, ', n_files3, ' profils sup 24h + ', n_files_relaunched, ' profils relances.'
     print, files(0)
     
     files = files[where(files ne '')]
     files_relaunched = files_relaunched[where(files_relaunched ne '')]
     part_file = strarr(n_files)+part
     
  endif else begin
     
     part2 = ['xz', 'zy', 'cx', 'cy', 'px', 'py', 'up', 'dn', 'cu', 'cd', 'dk']
     n_files = 0
     n_files2 = 0
     n_files3 = 0
     n_files_relaunched = 0
     nf = 0
     nf2 = 0
     nf3 = 0
     nf_relaunched = 0
     
     files = findfile(dir+'LOPs'+sim+'/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=nf)
     files = [files, findfile(dir+'Smoothed_sww/Done*/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=nf2)]
     files = [files, findfile(dir+'Rerun_sup24h/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=nf3)]
     files_relaunched = findfile(dir+'Extend_trunc500/Done*/relaunch_'+nm+res+'density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=nf_relaunched)
     part_file = strarr(nf+nf2+nf3)+part2(0)
     n_files = n_files + nf + nf2 + nf3
     n_files2 = n_files2 + nf2
     n_files3 = n_files3 + nf3
     n_files_relaunched = n_files_relaunched + nf_relaunched
     
     for k = 1, n_elements(part2)-1 do begin          
        files = [files, findfile(dir+'LOPs'+sim+'/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=nf)]    
        files = [files, findfile(dir+'Smoothed_sww/Done*/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=nf2)]
        files = [files, findfile(dir+'Rerun_sup24h/Done/'+nm+res+'density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=nf3)]
        files_relaunched = [files_relaunched, findfile(dir+'Extend_trunc500/Done*/relaunch_'+nm+res+'density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=nf_relaunched)]
        part_file = [part_file,strarr(nf+nf2+nf3)+part2(k)]
        
        n_files = n_files + nf + nf2 + nf3
        n_files2 = n_files2 + nf2
        n_files3 = n_files3 + nf3
        n_files_relaunched = n_files_relaunched + nf_relaunched
     endfor
     
     files = files[where(files ne '')]
     files_relaunched = files_relaunched[where(files_relaunched ne '')]
     
     print, "Il y a en tout ", n_files, ' fichiers pour la simulation ', res+sim+nm,', dont ', n_files2, ' profils lisses ', n_files3, ' profils sup 24h + ', n_files_relaunched, ' profils relances.'
;help, part_file
;print, unique(part_file,/sort,count)
;print, count     
;help, files
;help, files_relaunched
     
     
  endelse  
  
  flag = strarr(n_elements(files))
  lop_name = strarr(n_elements(files))
  help, flag
  
;On veut recuperer :
;depth, theta, phi et x,y,z des lop
;temperature, density, ionization,... post-cloudy + prolongees
  
  
  files_phyc = files
  
  files_con = files_phyc
  strreplace, files_con, '.phyc', '.con'
  files_fionH = files_phyc
  strreplace, files_fionH, '.phyc', '_H.el'
  files_fionO = files_phyc
  strreplace, files_fionO, '.phyc', '_O.el'
  files_in = files_phyc
  strreplace, files_in, '.phyc', '.in'
  files_em_em = files_phyc
  strreplace, files_em_em, '.phyc', '.em_emergent'
  files_em_int = files_phyc
  strreplace, files_em_int, '.phyc', '.em_intrinsic'
  
  files_ascii = files_phyc
  strreplace, files_ascii, '.phyc', '.ascii'
  if factor ne '' then strreplace, files_ascii, nm, ''
  
  check_number_of_files = 0
  
  for k = 0, n_elements(files)-1 do begin
     file_exists = file_test(files(k))
     if file_exists ne 0 then begin
        if files(k) ne '' then begin
           
           check_number_of_files = check_number_of_files + 1
           
           pos_lop = strpos(files(k),'_LOP')
           pos2 = strpos(files(k), '.phyc')
           lop_name(k) = strmid(files(k),pos_lop+1,pos2-pos_lop-1)
;print, lop_name(k), part_file(k)
           
           if strmatch(lop_name(k), '*'+part_file(k)+'*') ne 1 then stop
           
           flag(k) = 'ok'           
           if strmatch(files(k), '*sup24h*') eq 1 then flag(k) = '24h'
           if strmatch(files(k), '*Smoothed_sww*') eq 1 then flag(k) = 'smo'
           
;print, files(k)
           prolongation = ''
           if n_files_relaunched ne 0 then begin
              t = where(strmatch(files_relaunched, '*'+lop_name(k)+'*') eq 1, nt_rel)
              if nt_rel gt 0 then begin
;print, files_relaunched[t]
                 
                 prolongation = files_relaunched[t]
                 flag(k) = 'rel'
              endif
              if nt_rel gt 1 then stop
           endif
           
           ;phyc params : phyc(*,k)
           ; k = 0:depth_phyc, 1:temperature_phyc, 2:hden_phyc, 3:eden_phyc], 4:heating_phyc, 5:rad_acc_phyc, 6:fill_fact
           phyc = read_file_phyc(files_phyc(k))
           if prolongation ne '' then phyc = [phyc,read_file_phyc(prolongation)]


           ;ascii params : ascii(*,k)
           ; k = 0:sorted_depth, 1:sorted_density, 2:sorted_x_ascii, 3:sorted_y_ascii, 4:sorted_z_ascii, 5:sorted_temperature, 6:sorted_vx_ascii, 7:sorted_vy_ascii, 8:sorted_vz_ascii, 9:sorted_cell_size, 10:x_lops, 11:y_lops, 12:z_lops, 13:theta_lops, 14:phi_lops
           ascii = read_file_ascii(files_ascii(k)) ;ici, pas de prolongation (ligne originale !)
           
           ;H/O frac params : Xfrac(*,k)
           ; k = 0:depth_X, 1:Xneutre
           Hfrac = read_file_hfrac(files_fionH(k))
           Ofrac = read_file_ofrac(files_fionO(k))

          
           if prolongation ne '' then begin
              strreplace, prolongation, '.phyc', '_H.el'
              Hfrac = [Hfrac,read_file_hfrac(prolongation)]
              strreplace, prolongation, '_H.el', '_O.el'
              Ofrac = [Ofrac,read_file_ofrac(prolongation)]
           endif
           
;le nuage et l'AGN sont separes de ??? -> depend du profil de densite
;'radius inner'
           radius_inner = search_rinner(files_in(k),/silent)
           

;emissivities : em_X(*,k)
; k = 0:depth_em, 1:Lya_1216A, 2:Lyb_1026A, 3:Hb_4861A, 4:OIII_5007A, 5:Ha_6563A, 6:NII_6584A, 7:OI_6300A, 8:SII_6731A_6716A, 9:NeIII_3869A, 10:OII_3727A_multiplet, 11:NeV_3426A, 12:SiVII_2481m, 13:Brg_2166m, 14:SiVI_1963m, 15:AlIX_2040m, 16:CaVIII_2321m, 17:NeVI_7652m       
em_em = read_file_em(files_em_em(k),/emergent)
em_int = read_file_em(files_em_int(k),/intrinsic)
          

           
           if prolongation ne '' then begin
              strreplace, prolongation, '.in', '.em_emergent'
              em_em = [em_em, read_file_em(prolongation,/emergent)]
              strreplace, prolongation, '.em_emergent', '.em_intrinsic'
              em_int = [em_int, read_file_em(prolongation,/intrinsic)]
           endif

  if prolongation ne '' then begin
              profile_pro = read_file_in(prolongation)
              n = n_elements(profile_pro(*,0))

              phyc(-n,0) += radius_inner
              Hfrac(-n,0) += radius_inner
              Ofrac(-n,0) += radius_inner 
              em_em(-n,0) += radius_inner
              em_int(-n,0) += radius_inner              
           endif
           
           openr, f, files_ascii(k), /get_lun
           line1 = ''
           readf, f, line1
                                ;print, line1
           theta = double(strmid(line1,strpos(line1,':')+1,28))
                                ;print, 'theta_strmid = ', strmid(line1,strpos(line1,':')+1,28)
                                ;print, 'theta = ', theta
           phi = double(strmid(line1,strpos(line1,':')+29,28))
                                ;print, 'phi_strmid = ', strmid(line1,strpos(line1,':')+29,28)
                                ;print, 'phi = ', phi
           close, f
           free_lun, f
           
           if phi gt 2.*!dpi or theta gt 2.*!dpi then stop
           if phi lt 0. or theta lt 0. then stop

sorted_depth = ascii(*,0)           
sorted_density = ascii(*,1)
sorted_temperature = ascii(*,5)
x_lops = ascii(*,10)
y_lops = ascii(*,11)
z_lops = ascii(*,12)
theta_lops = ascii(*,13)
phi_lops = ascii(*,14)
depth_phyc = phyc(*,0)
hden_phyc = phyc(*,2)
temperature_phyc = phyc(*,1)
Hneutre = Hfrac(*,1)
Oneutre = Ofrac(*,1)
Hb_4861A = em_em(*,3)
OIII_5007A = em_em(*,4) 
Ha_6563A = em_em(*,5)
NII_6584A =em_em(*,6)

           
           taille_amr=n_elements(sorted_depth)
           taille_cloudy=n_elements(depth_phyc) ;inferieure a taille_amr
           
           temp_post_cloudy = fltarr(taille_cloudy)
           den_post_cloudy = fltarr(taille_cloudy)
           cell_size_post_cloudy = fltarr(taille_cloudy) ;valeurs post-cloudy non prolongees
           
;polytrope de jeans
;la temperature est arbitrairement elevee pour eviter une derive
;numerique (l'agitation doit toujours etre resolue)
;si densite > densite seuil
;T [K] = 0.041666 * sqrt(32pi) * e^2 [pc^2] * rho [H/cc]
;T [K] = 15.56 * rho [H/cc]
;ou e est la resolution spatiale 50kpc/2^(lmax=13) = 6.10 pc
           
;correction du polytrope de jeans
           alpha = (0.041666*sqrt(32*!dpi)*(boxlen*1d3/2^lmax)^2)
           rho_0 = 900./alpha   ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
           t_rho = where(sorted_density ge rho_0, nt_rho)
           if nt_rho gt 0 then begin
              T_polytrope = alpha*sorted_density[t_rho]
              t_temp = where(sorted_temperature[t_rho] le 2*T_polytrope, nt_temp)
              if nt_temp gt 0 then sorted_temperature[t_rho(t_temp)] = 900
           endif
           t_rho = where(hden_phyc ge rho_0, nt_rho)
           if nt_rho gt 0 then begin
              T_polytrope = alpha*hden_phyc[t_rho]
              t_temp = where(temperature_phyc[t_rho] le 2*T_polytrope, nt_temp)
              if nt_temp gt 0 then temperature_phyc[t_rho(t_temp)] = 900
           endif
           
;prolonger les parametres avec grille amr apres l'endroit ou cloudy
;s'est arrete avec des 0 pour pouvoir comparer au cube amr initial
           alpha = cos(phi)*sin(theta)
           beta = sin(phi)*sin(theta)
           gamma = cos(theta) 
           
           x = dblarr(n_elements(depth_phyc))
           y = dblarr(n_elements(depth_phyc))
           z = dblarr(n_elements(depth_phyc))
           
           for i = 0, n_elements(depth_phyc)-1 do begin
              x(i) = depth_phyc(i)*alpha + x_agn ;kpc
              y(i) = depth_phyc(i)*beta + y_agn  ;kpc
              z(i) = depth_phyc(i)*gamma + z_agn ;kpc
           endfor
           
;print, max(sorted_depth), max(depth_phyc)
           
           t = where(sorted_depth ge max(depth_phyc), nt) ;prolongation des lignes avec des 0 sur la grille amr
           if nt gt 0 then begin
              zeros = 0*sorted_depth[t]
              zeros1 = zeros + 1
              
              temp_all = [temperature_phyc,zeros] ;;;;;temperature cloudy prolongee
              den_all = [hden_phyc,sorted_density[t]]
              
              t_zero = where(den_all eq 0, nt_zero)
              if nt_zero gt 0 then begin
                 print, den_all[t_zero]
                 stop
              endif
              
              depth_all = [depth_phyc,sorted_depth[t]] 
              x_all = [x,x_lops[t]]
              y_all = [y,y_lops[t]]
              z_all = [z,z_lops[t]]
              theta_all = [fltarr(taille_cloudy)+theta,theta_lops[t]]
              phi_all = [fltarr(taille_cloudy)+phi,phi_lops[t]]
              
              
              Hneutre_all = [Hneutre,zeros+Hneutre(n_elements(depth_phyc)-1)]
              Oneutre_all = [Oneutre,zeros+Oneutre(n_elements(depth_phyc)-1)]
              
              
;help, Hneutre
;help, x
;help, Hneutre_all
;help, x_all
              
              Hb_4861A_all = [Hb_4861A,zeros]
              OIII_5007A_all = [OIII_5007A,zeros]
              Ha_6563A_all = [Ha_6563A,zeros]
              NII_6584A_all = [NII_6584A,zeros]
              
           endif else begin
              stop
           endelse
           
;help, depth_all
;help, theta_all
           
           for i = 0, n_elements(depth_all)-1 do begin
              printf, out, lop_name(k), x_all(i), y_all(i), z_all(i), theta_all(i), phi_all(i), depth_all(i), den_all(i), temp_all(i), $
                      Hneutre_all(i), Oneutre_all(i), Hb_4861A_all(i), OIII_5007A_all(i), Ha_6563A_all(i), NII_6584A_all(i), n_elements(depth_all)-1-i, '   '+flag(k), $
                      format='(A35,E,E,E,E,E,E,E,E,E,E,E,E,E,E,I,A10)'
              
;print, x_all(i), y_all(i), z_all(i), theta_all(i), phi_all(i), depth_all(i), den_all(i), temp_all(i), $
;Hneutre_all(i), Oneutre_all(i), Hb_4861A_all(i), OIII_5007A_all(i), Ha_6563A_all(i), NII_6584A_all(i), n_elements(depth_all)-1-i
;print, ''
           endfor
           
           for i = 0, n_elements(sorted_depth)-1 do begin
              
              printf, out_ascii, lop_name(k), sorted_depth(i), sorted_density(i), ascii(i,2), ascii(i,3), ascii(i,4), sorted_temperature(i), $
                      ascii(i,6), ascii(i,7), ascii(i,8), ascii(i,9), x_lops(i), y_lops(i), z_lops(i), theta, phi, $
                      n_elements(sorted_depth)-1-i, +'   '+flag(k), $
                      format='(A35,E,E,E,E,E,E,E,E,E,E,E,E,E,E,E,I,A10)'
           endfor
           
           if k mod 100L eq 0 then print, k*100L/n_elements(files), ' %'
        endif
     endif
  endfor
  
  close, out
  free_lun, out
  close, out_ascii
  free_lun, out_ascii
  
  print, check_number_of_files, ' fichers ont ete traites'
  
  
end
