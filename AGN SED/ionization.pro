pro ionization, ps=ps

loadct, 39
defplotcolors

;fmt = '(D,D,D,D,D,D)'

;;;;;;;;;;; routines Cloudy existantes;;;;;;;;;;;;

  ;;;;avec parametres par defaut;;;;;;;
    ;table power law
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_power_law_H.el", depth_H_pl, HI_pl, HII_pl, H2_pl
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_power_law_O.el", depth_O_pl, OI_pl, OII_pl, OIII_pl, OIV_pl, OV_pl, OVI_pl, OVII_pl, OVIII_pl, OIX_pl, O1_pl, O1e_pl, O1ee_pl
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;table agn : Ferland et al 2013
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_agn_H.el", depth_H_tagn, HI_tagn, HII_tagn, H2_tagn
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_agn_O.el", depth_O_tagn, OI_tagn, OII_tagn, OIII_tagn, OIV_tagn, OV_tagn, OVI_tagn, OVII_tagn, OVIII_tagn, OIX_tagn, O1_tagn, O1e_tagn, O1ee_tagn

    ;agn Kirk : (Kirk Korista et al., 1997a)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_kirk_H.el", depth_H_kirk, HI_kirk, HII_kirk, H2_kirk
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_kirk_O.el", depth_O_kirk, OI_kirk, OII_kirk, OIII_kirk, OIV_kirk, OV_kirk, OVI_kirk, OVII_kirk, OVIII_kirk, OIX_kirk, O1_kirk, O1e_kirk, O1ee_kirk

  ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson_H.el", depth_H_ferguson, HI_ferguson, HII_ferguson, H2_ferguson
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson_O.el", depth_O_ferguson, OI_ferguson, OII_ferguson, OIII_ferguson, OIV_ferguson, OV_ferguson, OVI_ferguson, OVII_ferguson, OVIII_ferguson, OIX_ferguson, O1_ferguson, O1e_ferguson, O1ee_ferguson

;;;;;;;;;;;;; scripts Cloudy crees par moi avec les parametres F97 ;;;;;;;;;;;;;;;
    ;Ferguson et al 1997 via Cloudy (nuage rempli uniformement)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_uniform_H.el", depth_H_unif, HI_unif, HII_unif, H2_unif
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_uniform_O.el", depth_O_unif, OI_unif, OII_unif, OIII_unif, OIV_unif, OV_unif, OVI_unif, OVII_unif, OVIII_unif, OIX_unif, O1_unif, O1e_unif, O1ee_unif

    ;Ferguson et al 1997 via Cloudy (nuage clumpy)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_clumpy_H.el", depth_H_clumpy, HI_clumpy, HII_clumpy, H2_clumpy
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_clumpy_O.el", depth_O_clumpy, OI_clumpy, OII_clumpy, OIII_clumpy, OIV_clumpy, OV_clumpy, OVI_clumpy, OVII_clumpy, OVIII_clumpy, OIX_clumpy, O1_clumpy, O1e_clumpy, O1ee_clumpy

;trace des graphes
if keyword_set(ps) then begin
;;----
   ; output PS plot
   outfile = '/Users/oroos/idl/pro/Cloudy/graph_ionization_clr_uniform.eps'   ;; filename
   old_dev=!D.NAME                   ;; save current device (to restore at the end)
   set_plot,/copy,'PS'                   ;; copy keyword to copy properties like color table definition
   ;; below: color for color plot, this example makes 7 inch x 10 inch
   ;; file at default resolution
   ;; encapsulated to make an .eps instead of a regular .ps (better for papers, articles, etc.)
   device,/color,/cm,ysize=10.,xsize=15.,file=outfile,/encapsulated
endif else begin
   set_plot, 'X'
   window, 0

endelse

loadct, 39
defplotcolors

;;;plot instructions
!p.multi=[0,2,2]
plot, depth_H_unif,  HI_unif, /xlog, /ylog, /ystyle, yrange=[1e-10,1], title='F97 (uniform)', xtitle='Depth (cm)', ytitle='Ioniziation fraction'
oplot, depth_H_unif, HII_unif, color=!red
oplot, depth_H_unif, H2_unif, color=!green
      ;unit cm


   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["HI", "HII", "H2"]
    psyms = [-3, -3, -3]
    colors = [0, !red, !green]
    linestyles = [0,0,0]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom

plot, depth_O_unif,  OI_unif, /xlog, /ylog, yrange=[1e-10,1], title='F97 (uniform)', xtitle='Depth (cm)', ytitle='Ioniziation fraction'
oplot, depth_O_unif, OII_unif, color=!red
oplot, depth_O_unif, OIII_unif, color=!green
;oplot, depth_O_unif, OIV_unif
;oplot, depth_O_unif, OV_unif
;oplot, depth_O_unif, OVI_unif
;oplot, depth_O_unif, OVII_unif
;oplot, depth_O_unif, OVIII_unif
;oplot, depth_O_unif, OIX_unif
;oplot, depth_O_unif, O1_unif
;oplot, depth_O_unif, O1e_unif
;oplot, depth_O_unif, O1ee_unif
      ;unit cm


   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["OI", "OII", "OIII"]
    psyms = [-3, -3, -3]
    colors = [0, !red, !green]
    linestyles = [0,0,0]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom

plot, depth_H_kirk,  HI_kirk, /xlog, /ylog, /ystyle, yrange=[1e-10,1], title='AGN Kirk', xtitle='Depth (cm)', ytitle='Ioniziation fraction'
oplot, depth_H_kirk, HII_kirk, color=!red
oplot, depth_H_kirk, H2_kirk, color=!green
      ;unit cm


   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["HI", "HII", "H2"]
    psyms = [-3, -3, -3]
    colors = [0, !red, !green]
    linestyles = [0,0,0]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom

plot, depth_O_kirk,  OI_kirk, /xlog, /ylog, yrange=[1e-10,1], title='AGN Kirk', xtitle='Depth (cm)', ytitle='Ioniziation fraction'
oplot, depth_O_kirk, OII_kirk, color=!red
oplot, depth_O_kirk, OIII_kirk, color=!green
;oplot, depth_O_kirk, OIV_kirk
;oplot, depth_O_kirk, OV_kirk
;oplot, depth_O_kirk, OVI_kirk
;oplot, depth_O_kirk, OVII_kirk
;oplot, depth_O_kirk, OVIII_kirk
;oplot, depth_O_kirk, OIX_kirk
;oplot, depth_O_kirk, O1_kirk
;oplot, depth_O_kirk, O1e_kirk
;oplot, depth_O_kirk, O1ee_kirk
      ;unit cm


   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["OI", "OII", "OIII"]
    psyms = [-3, -3, -3]
    colors = [0, !red, !green]
    linestyles = [0,0,0]

    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom

!p.multi=0



;;;plot instructions



if keyword_set(ps) then begin
   device,/close       ;; closes the device that made the ps
   set_plot,old_dev  ;; restore the previous device type (IDL default: set_plot,'X' for xterm window)

;; bonus lines that I use when I want the script to print that it wrote the file
   print,'Wrote file '+outfile
   print,'==========================================================='
   print,''

;;---
endif



end
