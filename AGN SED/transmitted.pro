pro transmitted, ps=ps

loadct, 39
defplotcolors

fmt = '(D,D,D,D,D,D)'

;;;;;;;;;;; routines Cloudy existantes;;;;;;;;;;;;

  ;;;;avec parametres par defaut;;;;;;;
    ;table power law
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_power_law.con", lambda_pl, nuFnu_pl, attenuated_pl, diffuse_pl, transmitted_pl, reflected_pl, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
    print, minmax(lambda_pl)

    ;table agn : Ferland et al 2013
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_agn.con", lambda_tagn, nuFnu_tagn, attenuated_tagn, diffuse_tagn, transmitted_tagn, reflected_tagn, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;agn Kirk : (Kirk Korista et al., 1997a)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_kirk.con", lambda_kirk, nuFnu_kirk, attenuated_kirk, diffuse_kirk, transmitted_kirk, reflected_kirk, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

  ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson.con", lambda_ferg, nuFnu_ferg, attenuated_ferg, diffuse_ferg, transmitted_ferg, reflected_ferg, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson_clumpy.con", lambda_ferg_clumpy, nuFnu_ferg_clumpy, attenuated_ferg_clumpy, diffuse_ferg_clumpy, transmitted_ferg_clumpy, reflected_ferg_clumpy, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

;;;;;;;;;;;;; scripts Cloudy crees par moi avec les parametres F97 ;;;;;;;;;;;;;;;
    ;Ferguson et al 1997 via Cloudy (nuage rempli uniformement)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_uniform.con", lambda_unif, nuFnu_unif, attenuated_unif, diffuse_unif, transmitted_unif, reflected_unif, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;Ferguson et al 1997 via Cloudy (nuage clumpy)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_clumpy.con", lambda_clumpy, nuFnu_clumpy, attenuated_clumpy, diffuse_clumpy, transmitted_clumpy, reflected_clumpy, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s


;trace des graphes
if keyword_set(ps) then begin
;;----
   ; output PS plot
   outfile = '/Users/oroos/idl/pro/Cloudy/graph_transmitted.eps'   ;; filename
   old_dev=!D.NAME                   ;; save current device (to restore at the end)
   set_plot,/copy,'PS'                   ;; copy keyword to copy properties like color table definition
   ;; below: color for color plot, this example makes 7 inch x 10 inch
   ;; file at default resolution
   ;; encapsulated to make an .eps instead of a regular .ps (better for papers, articles, etc.)
   device,/color,/cm,ysize=10.,xsize=15.,file=outfile,/encapsulated
endif else begin
   set_plot, 'X'
   window, 0

endelse

loadct, 39
defplotcolors

plot, lambda_pl, transmitted_pl,/xlog, line=2, /ylog, xtitle='Wavelength (micron)', ytitle='Net transmitted continuum (erg/cm2/s)' ;attenuated incident + diffuse continua
oplot, lambda_tagn, transmitted_tagn, line=3
oplot, lambda_kirk, transmitted_kirk, line=4, color=!green
oplot, lambda_ferg, transmitted_ferg, line=0, color=!dgreen
oplot, lambda_ferg_clumpy, transmitted_ferg_clumpy, line=2, color=!dblue
oplot, lambda_unif, transmitted_unif, color=!blue;, /xlog, /ylog, yr=[1e-8,1e1], xr=[1e-2,1e6]
oplot, lambda_clumpy, transmitted_clumpy, color=!red, line=2
      ;units micron

   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["Table power law", "Table AGN", "AGN Kirk", "AGN with F97","AGN with F97 clumpy", "F97 (uniform)", "F97 (clumpy)" ]
    psyms = [-3, -3, -3, -3, -3, -3, -3]
    colors = [0, 0, !green, !dgreen, !dblue, !blue, !red]
    linestyles = [2, 3, 4, 0, 2, 0, 2]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /right


if keyword_set(ps) then begin
   device,/close       ;; closes the device that made the ps
   set_plot,old_dev  ;; restore the previous device type (IDL default: set_plot,'X' for xterm window)

;; bonus lines that I use when I want the script to print that it wrote the file
   print,'Wrote file '+outfile
   print,'==========================================================='
   print,''

;;---
endif

;2e graphe

if keyword_set(ps) then begin
;;----
   ; output PS plot
   outfile = '/Users/oroos/idl/pro/Cloudy/graph_reflected.eps'   ;; filename
   old_dev=!D.NAME                   ;; save current device (to restore at the end)
   set_plot,/copy,'PS'                   ;; copy keyword to copy properties like color table definition
   ;; below: color for color plot, this example makes 7 inch x 10 inch
   ;; file at default resolution
   ;; encapsulated to make an .eps instead of a regular .ps (better for papers, articles, etc.)
   device,/color,/cm,ysize=10.,xsize=15.,file=outfile,/encapsulated
endif else begin window, 1
endelse

loadct, 39
defplotcolors
;window, 1
;set_plot, 'X'
plot, lambda_pl, reflected_pl,/xlog, line=2, yr=[1e-8,1e3], xr=[1e-4,1e5], xstyle=1, ystyle=1, /ylog, xtitle='Wavelength (micron)', ytitle='Reflected continuum (erg/cm2/s)'
oplot, lambda_tagn, reflected_tagn, line=3
oplot, lambda_kirk, reflected_kirk, line=4, color=!green
oplot, lambda_ferg, reflected_ferg, line=0, color=!dgreen
oplot, lambda_ferg_clumpy, reflected_ferg_clumpy, line=2, color=!dblue
oplot, lambda_unif, reflected_unif, color=!blue
oplot, lambda_clumpy, reflected_clumpy, color=!red, line=2
      ;units micron

   ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["Table power law", "Table AGN", "AGN Kirk", "AGN with F97","AGN with F97 clumpy", "F97 (uniform)", "F97 (clumpy)" ]
    psyms = [-3, -3, -3, -3, -3, -3, -3]
    colors = [0, 0, !green, !dgreen, !dblue, !blue, !red]
    linestyles = [2, 3, 4, 0, 2, 0, 2]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /right


if keyword_set(ps) then begin
   device,/close       ;; closes the device that made the ps
   set_plot,old_dev  ;; restore the previous device type (IDL default: set_plot,'X' for xterm window)

;; bonus lines that I use when I want the script to print that it wrote the file
   print,'Wrote file '+outfile
   print,'==========================================================='
   print,''
endif
;;---

end
