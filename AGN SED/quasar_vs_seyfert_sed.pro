pro quasar_vs_seyfert_sed, ps=ps


readcol,  "/Users/oroos/Post-stage/SED_AGN/realistic_AGN.con", lambda, nuFnu, attenuated, diffuse, transmitted, reflected

readcol,  "/Users/oroos/Post-stage/SED_AGN/realistic_quasarAGN.con", lambda_qu, nuFnu_qu, attenuated_qu, diffuse_qu, transmitted_qu, reflected_qu

READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
    ;nu : units Hz
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
t1_bas = where(nu1 le 1e11)
t2_bas = where(nu2 le 1e12)

t1_milieu = where(nu1 gt 1e11 and nu1 le 1e16)
t2_milieu = where(nu2 gt 1e12 and nu2 le 1e16)

t1_haut = where(nu1 gt 1e16)
t2_haut = where(nu2 gt 1e16)

;SED individuelles de Sy1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC7469_Sy1.dat", nu7469, Fnu7469
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC3783_Sy1.dat", nu3783, Fnu3783
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC1566_Sy1.dat", nu1566, Fnu1566
    ;nu : units Hz
    ;Fnu : units Jy = 1e-23 erg/cm2/s/Hz
                                        ;donnees  ;ce que l'on veut tracer  
;rest-frame correction with redshifts : nu_obs = nu_rest-frame/(1 + z)
z3783 = 0.00973
z1566 = 0.005017
z7469 = 0.0163

;nu_rest-frame = nu_obs*(1+z)
nu7469 = nu7469*(1+z7469)
nu3783 = nu3783*(1+z3783)
nu1566 = nu1566*(1+z1566)
Fnu7469 = Fnu7469/(1+z7469)
Fnu3783 = Fnu3783/(1+z3783)
Fnu1566 = Fnu1566/(1+z1566)

nuFnu7469 = nu7469*Fnu7469/mean(nu7469*Fnu7469)
nuFnu3783 = nu3783*Fnu3783/mean(nu3783*Fnu3783)
nuFnu1566 = nu1566*Fnu1566/mean(nu1566*Fnu1566) ;normalization
shift = 8.5


READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioQuiet.dat", logNu_quiet, logNuLnu_quiet
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioLoud.dat", logNu_loud, logNuLnu_loud

correction_quasar=36.5

t_bas_ququiet = where(logNu_quiet ge 9.75 and logNu_quiet le 10.25,nt1)
t_milieu_ququiet = where(logNu_quiet ge 12.5 and logNu_quiet le 15.5,nt2)
t_haut_ququiet = where(logNu_quiet ge 16.6 and logNu_quiet le 18.3,nt3)
t_bas_quloud = where(logNu_loud ge 9.75 and logNu_loud le 10.25,nt4)
t_milieu_quloud = where(logNu_loud ge 12.5 and logNu_loud le 15.5,nt5)
t_haut_quloud = where(logNu_loud ge 16.6 and logNu_loud le 18.3,nt6)

t = where(abs(alog10(3e8/10d^lognu_loud*1e6) + 2.9) lt 0.01,nt)
if nt gt 0 then begin
print, 'ok loud', (alog10(3e8/10d^lognu_loud*1e6))[t], logNuLnu_loud[t]-correction_quasar
mean_loud =  logNuLnu_loud[t(0)]-correction_quasar
endif
t = where(abs(alog10(3e8/10d^lognu_loud*1e6) + 2.9) lt 0.01,nt)
if nt gt 0 then begin
print, 'ok quiet', (alog10(3e8/10d^lognu_quiet*1e6))[t], logNuLnu_quiet[t]-correction_quasar
endif

if ps eq 0 then thick = 1 else thick = 10

if ps eq 0 then window,0, ypos=425
if ps eq 1 then begin
ps_start, 'sy_vs_quasar_sed.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20, /helvetica, /bold
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1
     !y.charsize=1
!x.thick=40
!y.thick=40
     !p.thick=4
endif

plot, alog10(lambda), alog10(nuFnu+transmitted), xtitle='Wavelength ['+textoidl('\mu')+'m]', ytitle='Seyfert and QSO SEDs [erg cm'+textoidl('^{-2}')+' s'+textoidl('^{-1}')+']', xr=[-8,6], yr=[2,12], /xstyle, /ystyle, thick=2, /nodata
oplot, alog10(lambda_qu), alog10(nuFnu_qu+transmitted_qu), color=!red, thick=2*thick
oplot, alog10(1d6*6.63d-34*3.0d8/(13.6*1.6d-19))*[1,1], [2,12], thick=4*thick, color=!blue
xyouts, -6, 11, 'Ionizing photons', charthick=2*thick, color=!blue;, charsize=2
xyouts, -7, 4, 'Sy', charthick=2*thick
xyouts, -7, 3.4, 'QSO', color=!red, charthick=2*thick
xyouts, -5, 4, 'Sy 1 obs.', color=!dgray, charthick=2*thick
xyouts, -5, 3.5, 'RL-QSO obs.', color=!orange, charthick=2*thick
xyouts, -7, 2.7, 'L'+textoidl('_{bol}')+' = '+textoidl('10^{44.5}')+' erg s'+textoidl('^{-1}'), charthick=2*thick

files_quasar_indiv = findfile('/Users/oroos/Post-stage/Observational_SEDs/Quasars_SEDs_for_median/Q*.asc.txt', count=nfiles_quasar)
for kk = 0, nfiles_quasar-1 do begin
readcol, files_quasar_indiv(kk), log_nu0, log_nu1, log_nu2, log_nuFnu, /silent

t_bas_mean = where(alog10(3e8/10d^log_nu0*1e6) le 2.1,nt1)
t_haut_mean = where(alog10(3e8/10d^log_nu0*1e6) ge 4.2 and alog10(3e8/10d^log_nu0*1e6) le 5,nt3)
;if nt1 gt 0 then oplot, alog10(3e8/10d^log_nu0[t_bas_mean]*1e6), log_nuFnu[t_bas_mean]-11.666654+mean_loud, psym=sym(1), color=!dgray
;if nt3 gt 0 then oplot, alog10(3e8/10d^log_nu0[t_haut_mean]*1e6), log_nuFnu[t_haut_mean]-11.666654+mean_loud, psym=sym(1), color=!dgray ;;diviser par la moyenne et multiplier par la moyenne attendue 
endfor

oplot, alog10(lambda), alog10(nuFnu+transmitted), thick=2*thick
oplot, alog10(lambda_qu), alog10(nuFnu_qu+transmitted_qu), color=!red, thick=2*thick

;oplot, alog10(3e8/10d^lognu_quiet[t_bas_ququiet]*1e6), (logNuLnu_quiet[t_bas_ququiet]-correction_quasar), color=!blue, thick=2*thick, syms=thick/2.
;oplot, alog10(3e8/10d^lognu_quiet[t_milieu_ququiet]*1e6), (logNuLnu_quiet[t_milieu_ququiet]-correction_quasar), color=!blue, thick=2*thick, syms=thick/2.
;oplot, alog10(3e8/10d^lognu_quiet[t_haut_ququiet]*1e6), (logNuLnu_quiet[t_haut_ququiet]-correction_quasar), color=!blue, thick=2*thick, syms=thick/2.
oplot, alog10(3e8/10d^lognu_loud[t_bas_quloud]*1e6), (logNuLnu_loud[t_bas_quloud]-correction_quasar), color=!orange, thick=2*thick, psym=1, syms=thick/2.
oplot, alog10(3e8/10d^lognu_loud[t_milieu_quloud]*1e6), (logNuLnu_loud[t_milieu_quloud]-correction_quasar), color=!orange, thick=2*thick, psym=1, syms=thick/2.
oplot, alog10(3e8/10d^lognu_loud[t_haut_quloud]*1e6), (logNuLnu_loud[t_haut_quloud]-correction_quasar), color=!orange, thick=2*thick, psym=1, syms=thick/2.

oplot, alog10(3e8/nu1[t1_bas]*1e6), alog10(nuFnu1[t1_bas])+shift, color=!dgray, thick=2*thick, psym=1, syms=thick/2.
oplot, alog10(3e8/nu1[t1_haut]*1e6), alog10(nuFnu1[t1_haut])+shift, color=!dgray, thick=2*thick, psym=1, syms=thick/2.
oplot, alog10(3e8/nu1[t1_milieu]*1e6), alog10(nuFnu1[t1_milieu])+shift, color=!dgray, thick=2*thick, psym=1, syms=thick/2.

if ps eq 1 then ps_end, /png
if ps ne 0 then spawn, 'open sy_vs_quasar_sed.eps'
stop
end
