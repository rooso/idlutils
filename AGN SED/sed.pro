pro sed, ps=ps

 ;table power law
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_power_law.con", lambda_pl, nuFnu_pl, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
    print, minmax(lambda_pl)

    ;table agn : Ferland et al 2013
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_agn.con", lambda_tagn, nuFnu_tagn, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;agn Kirk : (Kirk Korista et al., 1997a)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_kirk.con", lambda_kirk, nuFnu_kirk, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

 ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson.con", lambda_ferg, nuFnu_ferg, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

  ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson_clumpy.con", lambda_ferg_clumpy, nuFnu_ferg_clumpy, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

;SED observationnelles moyennes pour AGN de types Seyfert 1 et Seyfert 2
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
    ;nu : units Hz
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

t1_bas = where(nu1 le 1e16)
t2_bas = where(nu2 le 1e16)

t1_haut = where(nu1 gt 1e16)
t2_haut = where(nu2 gt 1e16)

if ps eq 0 then begin 
window, 0
chars=0.9
thick=1
endif
if ps eq 1 then begin 
ps_start, 'sed.eps', /encapsulated, /cm, xsize=20, ysize=15
thick=5
!p.charsize=4
!p.charthick=2
chars=2
endif
defplotcolors
!p.background=!white & !p.color=!black

plot, [1e-9,1e6], [1e-9, 1e2], /xstyle, /ystyle, /xlog, /nodata, /ylog, xtitle='Wavelength (micron)', ytitle='SED (nu*Fnu) (arbitrary units)', title="SEDs for various AGN models and observations", thick=thick
oplot, lambda_pl, nuFnu_pl, line=0, color=!magenta, thick=thick
oplot, lambda_tagn, nuFnu_tagn, line=0, color=!orange, thick=thick
oplot, lambda_kirk, nuFnu_kirk, line=0, color=!purple, thick=thick
oplot, lambda_ferg, nuFnu_ferg, line=0, color=!blue, thick=thick
;oplot, lambda_ferg, nuFnu_ferg_clumpy, color=!cyan, line=2, thick=thick
oplot, 3e8/nu1[t1_bas]*1e6, nuFnu1[t1_bas], color=!red, thick=thick
oplot, 3e8/nu1[t1_haut]*1e6, nuFnu1[t1_haut], color=!red, thick=thick
oplot, 3e8/nu2[t2_bas]*1e6, nuFnu2[t2_bas], color=!green, line=0, thick=thick
oplot, 3e8/nu2[t2_haut]*1e6, nuFnu2[t2_haut], color=!green, line=0, thick=thick
oplot, lambda_tagn, nuFnu_tagn, line=0, color=!orange, thick=thick


 ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["Table power law", "Table AGN", "AGN Kirk", "AGN with F97", "Mean observational Sy1", "Mean observational Sy2" ]
    psyms = -3
    colors = [!magenta, !orange, !purple, !blue, !red, !green]
    linestyles = [0,0,0,0,0,0]
    txt=0
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, textcolors=txt,  /bottom, chars=chars

if ps eq 1 then ps_end, /png


end
