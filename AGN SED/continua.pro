pro continua, ps=ps

defplotcolors

fmt = '(D,D)'

;;;;;;;;;;; routines Cloudy existantes;;;;;;;;;;;;

  ;;;;avec parametres par defaut;;;;;;;
    ;table power law
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_power_law.con", lambda_pl, nuFnu_pl, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
    print, minmax(lambda_pl)

    ;table agn : Ferland et al 2013
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/table_agn.con", lambda_tagn, nuFnu_tagn, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;agn Kirk : (Kirk Korista et al., 1997a)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_kirk.con", lambda_kirk, nuFnu_kirk, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

  ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson.con", lambda_ferg, nuFnu_ferg, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

  ;;;avec parametres de F97;;;;;
    ;agn avec parametres de Ferguson et al 1997
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/agn_ferguson_clumpy.con", lambda_ferg_clumpy, nuFnu_ferg_clumpy, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

;;;;;;;;;;;;; scripts Cloudy crees par moi avec les parametres F97 ;;;;;;;;;;;;;;;
    ;Ferguson et al 1997 via Cloudy (nuage rempli uniformement)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_uniform.con", lambda_unif, nuFnu_unif, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

    ;Ferguson et al 1997 via Cloudy (nuage clumpy)
    READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/clr_clumpy.con", lambda_clumpy, nuFnu_clumpy, format=fmt
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s


;SED observationnelles moyennes pour AGN de types Seyfert 1 et Seyfert 2
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
    ;nu : units Hz
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

t1_bas = where(nu1 le 1e16)
t2_bas = where(nu2 le 1e16)

t1_haut = where(nu1 gt 1e16)
t2_haut = where(nu2 gt 1e16)

;Mean Quasars SEDs
;Richards et al 2006
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/type1_Quasar_SED.txt", logNu, logL_all, err_L_all, logBlue, logRed, logOptlum, logOptdim, logIRlum, logIRdim

nu = 10^logNu

;L_all_qu = 10^logL_all
;print, L_all_qu
logNuFnu = logNu + logL_all - alog10(4*!pi) - 2*20.97;log nuFnu = log (nu*L_all/4*pi*r0^2) avec log r0 = 20.97
nuFnu = (10^logNuFnu)/1e20*10^2.5

;Median quasar SEDs :
;Elvis et al 1994
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioQuiet.dat", logNu_quiet, logNuLnu_quiet
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioLoud.dat", logNu_loud, logNuLnu_loud

t_bas_ququiet = where(logNu_quiet ge 9.8 and logNu_quiet le 10.25,nt1)
t_milieu_ququiet = where(logNu_quiet ge 12.5 and logNu_quiet le 15.5,nt2)
t_haut_ququiet = where(logNu_quiet ge 16.4 and logNu_quiet le 18,nt3)
t_bas_quloud = where(logNu_loud ge 9.8 and logNu_loud le 10.25,nt4)
t_milieu_quloud = where(logNu_loud ge 12.5 and logNu_loud le 15.5,nt5)
t_haut_quloud = where(logNu_loud ge 16.4 and logNu_loud le 18,nt6)




;spectre AGN+BLR+dust 'realiste'
readcol, '/Users/oroos/Post-stage/SED_AGN/realistic_agn.con', lambda_dustBLR, nuFnu_dustBLR, attenuated_dustBLR, diffuse_dustBLR, transmitted_dustBLR, reflected_dustBLR, format=fmt
;lambda : units micron !
;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

;;;;;;Caclul IDL : spectre theorique de Ferguson & al 1997
Emin=13.6 ;eV
Emax=100*1e3 ;eV
h = 6.62e-34 ;cte de Planck
eV = 1.6e-19 ;joules
c = 3.0e8 ;m/s

nu_min = Emin*eV/h ;Hz
nu_max = Emax*eV/h ;Hz

print, nu_min, nu_max

nu_th = nu_min + (nu_max - nu_min)*dindgen(100)/100.0 ;Hz
;print, nu_th


;partie rayons X
Fnu_th_X = 1.0/nu_th
;print, Fnu_th_X

;partie UV
kT=68.6*eV
Fnu_th_UV = nu_th^(-0.3)*exp(-h*nu_th/kT)
;on cherche kT pour que nuFnu soit 'pique' a 48 eV : kT= 68.6 eV

;lien entre les deux
alpha_ox = -1.2
A_UV = 1e-20
A_X = 3e-1;double(A_UV * nu_min^(1+alpha_ox)/nu_max^(0.3+alpha_ox)*exp(-Emin/kT) )
print, 'A_UV = ', A_UV, ' A_X = ', A_X

Fnu_th = A_UV*Fnu_th_UV + A_X*Fnu_th_X
;print, 'Fnu_UV : ', Fnu_th_UV, ' Fnu_X ; ', Fnu_th_X, 'Fnu = ', Fnu_th

print, 'min, max de nu :', minmax(nu_th)
;print, nu_th*Fnu_th


;trace des graphes
if ps eq 0 then window, 0
if ps eq 1 then ps_start, 'continua.eps', /encapsulated

defplotcolors
!p.background=!white & !p.color=!black

plot, lambda_pl, nuFnu_pl,/xlog, /nodata, /ylog, xtitle='Wavelength (micron)', ytitle='Incident continuum (erg/cm2/s)',  yrange=[1e-11, 1e2], xrange=[1e-10,1e20]
oplot, lambda_pl, nuFnu_pl, line=0, color=!dred
oplot, lambda_tagn, nuFnu_tagn*1e2, line=3
oplot, lambda_kirk, nuFnu_kirk*1e1, line=4, color=!green
oplot, lambda_ferg, nuFnu_ferg*1e1, line=0, color=!dgreen
oplot, lambda_ferg, nuFnu_ferg_clumpy*1e1, color=!dblue, line=2
;oplot, lambda_unif, nuFnu_unif/1e2, color=!blue
;oplot, lambda_clumpy, nuFnu_clumpy/1e2, color=!red, line=2
oplot, 3e8/nu1[t1_bas]*1e6, nuFnu1[t1_bas]/1e4, color=!pink
oplot, 3e8/nu1[t1_haut]*1e6, nuFnu1[t1_haut]/1e4, color=!pink
oplot, 3e8/nu2[t2_bas]*1e6, nuFnu2[t2_bas]/1e4, color=!pink, line=2
oplot, 3e8/nu2[t2_haut]*1e6, nuFnu2[t2_haut]/1e4, color=!pink, line=2
oplot, 3e8/nu*1e6, nuFnu/1e7, color=!blue

oplot, 3e8/10d^lognu_quiet[t_bas_ququiet]*1e6, 10d^(logNuLnu_quiet[t_bas_ququiet]-42), color=!cyan
oplot, 3e8/10d^lognu_quiet[t_milieu_ququiet]*1e6, 10d^(logNuLnu_quiet[t_milieu_ququiet]-42), color=!cyan
oplot, 3e8/10d^lognu_quiet[t_haut_ququiet]*1e6, 10d^(logNuLnu_quiet[t_haut_ququiet]-42), color=!cyan
oplot, 3e8/10d^lognu_loud[t_bas_quloud]*1e6, 10d^(logNuLnu_loud[t_bas_quloud]-42), color=!magenta
oplot, 3e8/10d^lognu_loud[t_milieu_quloud]*1e6, 10d^(logNuLnu_loud[t_milieu_quloud]-42), color=!magenta
oplot, 3e8/10d^lognu_loud[t_haut_quloud]*1e6, 10d^(logNuLnu_loud[t_haut_quloud]-42), color=!magenta


;print, minmax(logNuLnu_quiet), minmax(logNuLnu_loud)
;print, minmax(logLambda_quiet), minmax(logLambda_loud)

oplot, lambda_dustBLR, (nuFnu_dustBLR+diffuse_dustBLR)/1e9, color=!purple
;oplot, c/nu_th*1e6, A_UV*nu_th*Fnu_th_UV, /ylog ,/xlog
;oplot, c/nu_th*1e6, A_X*nu_th*Fnu_th_X
;oplot, c/nu_th*1e6, nu_th*Fnu_th, line=2, thick=3, color=!green;, /ylog ,/xlog
      ;units micron
;-----> ici, le spectre theorique est completement aplati par
;l'echelle utilisee. pour sa forme, voir tracer_continuum.pro

 ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["Table AGN", "AGN Kirk", "AGN with F97","AGN with F97 clumpy", "Observed Sy1", "Observed Sy2", "Observed Quasars 1", "Realistic (nH=1e9)" ]
    psyms = [-3, -3, -3, -3, -3, -3, -3, -3]
    colors = [0, !green, !dgreen, !dblue, !pink, !pink, !blue, !purple]
    linestyles = [3, 4, 0, 2, 0, 2, 0, 0]
    txt=0

; items = ["Table power law", "Table AGN", "AGN Kirk", "AGN with F97", "F97 (uniform)", "F97 (clumpy)", "Observed Sy1", "Observed Sy2", "Observed Quasars 1", "Realistic (nH=1e9)" ]
;    psyms = [-3, -3, -3, -3, -3, -3, -3, -3, -3, -3]
;    colors = [!dred, 0, !green, !dgreen, !blue, !red, !pink, !pink, !blue, !purple]
;    linestyles = [0, 3, 4, 5, 0, 2, 0, 2, 0, 0]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, textcolors=txt,  /right, chars=0.9

if ps eq 1 then ps_end, /png

end
