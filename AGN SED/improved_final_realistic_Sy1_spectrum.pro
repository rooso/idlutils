pro improved_final_realistic_Sy1_spectrum, factor=factor, ps=ps

defplotcolors
!x.margin=[8,2]   
!y.margin=[4,1]

   if ps eq 0 then thick=2 else thick=25
   if ps eq 0 then lin=1 else lin=0.2

if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse


;solar abundances, no grains
;solar abundances + blackbody (should be a greybody ---> dust)
readcol,  "/Users/oroos/Post-stage/SED_AGN/"+nm+"realistic_AGN.con", lambda, nuFnu, attenuated, diffuse, transmitted, reflected

readcol,  "/Users/oroos/Post-stage/SED_AGN/"+nm+"realistic_AGN_transmitted.txt", E_transmitted_final, nuFnu_transmitted_final          ;E in Ryd
;transmitted = diffuse* + attenuated         ;* % of diffuse emission
;that is transmitted in the outward direction
lambda_transmitted_final = 6.63e-34*3.0e8*1e6/(E_transmitted_final*2.18e-18);h*c*1e6/(E(Ryd)*2.18e-18) ;microns

;calcul du flux de photons ionisants :
nu_transmitted_final = 3.0d8/(lambda_transmitted_final*1d-6) ;Hz
Fnu_transmitted_final = nuFnu_transmitted_final/nu_transmitted_final ;erg/cm2/s

;ionisant = E >= 13.6 eV <=> nu >= 3.28d15 Hz
nu_transmitted_final_ion = nu_transmitted_final[where(nu_transmitted_final ge 3.28d15 and finite(nu_transmitted_final) eq 1,nt_ion)]
Fnu_transmitted_final_ion = Fnu_transmitted_final[where(nu_transmitted_final ge 3.28d15 and finite(nu_transmitted_final) eq 1,nt_ion)]

print, nt_ion

Flux_ionisant = int_tabulated(nu_transmitted_final_ion,Fnu_transmitted_final_ion,/double,/sort) ;erg/cm2
print, flux_ionisant

;stop

;nuFnu = 0.000000003*nuFnu
;attenuated = 0.000000003*attenuated
;diffuse = 0.000000003*diffuse
;transmitted = 0.000000003*transmitted
;reflected = 0.000000003*reflected
;nuFnu_transmitted_final = 0.000000003*nuFnu_transmitted_final


;SED observationnelles moyennes pour AGN de types Sy 1 et Seyfert 2
;Prieto et al 2010
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
    ;nu : units Hz
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
t1_bas = where(nu1 le 1e11)
t2_bas = where(nu2 le 1e12)

t1_milieu = where(nu1 gt 1e11 and nu1 le 1e16)
t2_milieu = where(nu2 gt 1e12 and nu2 le 1e16)

t1_haut = where(nu1 gt 1e16)
t2_haut = where(nu2 gt 1e16)

;SED individuelles de Sy1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC7469_Sy1.dat", nu7469, Fnu7469
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC3783_Sy1.dat", nu3783, Fnu3783
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC1566_Sy1.dat", nu1566, Fnu1566
    ;nu : units Hz
    ;Fnu : units Jy = 1e-23 erg/cm2/s/Hz
                                        ;donnees  ;ce que l'on veut tracer  
;rest-frame correction with redshifts : nu_obs = nu_rest-frame/(1 + z)
z3783 = 0.00973
z1566 = 0.005017
z7469 = 0.0163

;nu_rest-frame = nu_obs*(1+z)
nu7469 = nu7469*(1+z7469)
nu3783 = nu3783*(1+z3783)
nu1566 = nu1566*(1+z1566)
Fnu7469 = Fnu7469/(1+z7469)
Fnu3783 = Fnu3783/(1+z3783)
Fnu1566 = Fnu1566/(1+z1566)

nuFnu7469 = nu7469*Fnu7469/mean(nu7469*Fnu7469)
nuFnu3783 = nu3783*Fnu3783/mean(nu3783*Fnu3783)
nuFnu1566 = nu1566*Fnu1566/mean(nu1566*Fnu1566) ;normalization


;theoretical greybody
Tdust = 300 ;K

h = 6.63e-34 ;kg m2 / s
k = 1.38e-23 ;J/K

nu_bb_theoretical = double(3e8/(lambda*1e-6))
nu_0 = max(nu_bb_theoretical)
alpha = .93

nuFnu_bb_theoretical = nu_bb_theoretical*(nu_bb_theoretical^3)/(exp(h*nu_bb_theoretical/(k*Tdust)) - 1.0)*2.0*h/3e8/3e8

nuFnu_GB_theoretical = nuFnu_bb_theoretical * (nu_0/nu_bb_theoretical)^alpha

window, 1
plot, nu_bb_theoretical, nuFnu_GB_theoretical/1e10, /xlog, /ylog

;Sy1 : galaxy seen face-on (normal to the galactic plane)
;incident + diffusion by dust + attenuation by clumps but not by dust (radiation comes from sublimation zone)
;realistic_Sy1 = a_Sy1*nuFnu_nodust_alphaox +
;b_Sy1*nuFnu_transmitted_bb


if ps eq 0 then window, 20, ypos=425
if ps eq 1 then begin
ps_start, nm+'final_realistic_Sy1_spectrum.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20, /helvetica, /bold
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1
     !y.charsize=1
     !p.thick=4
endif
defplotcolors
           
!p.background=!white & !p.color=!black     ;;;, title="Realistic+BB Sy1 spectrum"
plot, alog10(lambda), alog10(nuFnu), /nodata, xtitle='log Wavelength ['+textoidl('\mu')+'m]', ytitle='Seyfert SED : log '+textoidl('\nuF_\nu')+' [erg cm'+textoidl('^{-2}')+' s'+textoidl('^{-1}')+']', xr=[-8,6], yr=[2,12], /xstyle, /ystyle, thick=thick
;
;
;final realistic Sy1 spectrum
oplot, alog10(lambda), alog10(nuFnu + transmitted), color=!green, thick=thick
oplot, alog10(lambda_transmitted_final), alog10(nuFnu + nuFnu_transmitted_final), color=!dgreen, thick=thick
;
shift = 8.5
;
;;;DISPERSION DES DONNEES OBSERVATIONNELLES
oband, alog10(3e8/nu1[t1_bas]*1e6), alog10(nuFnu1[t1_bas])+shift-0.8, alog10(nuFnu1[t1_bas])+shift+0.4, color=!dmagenta, /line_fill, border=!dmagenta, thick=thick
oband, alog10(3e8/nu1[t1_haut]*1e6), alog10(nuFnu1[t1_haut])+shift-0.7, alog10(nuFnu1[t1_haut])+shift+0.4, color=!dmagenta, /line_fill, border=!dmagenta, thick=thick
oband, alog10(3e8/nu1[t1_milieu]*1e6), alog10(nuFnu1[t1_milieu])+shift-0.7, alog10(nuFnu1[t1_milieu])+shift+0.3, color=!dmagenta, /line_fill, border=!dmagenta, thick=thick
;;;SED INCIDENTE (==SED de l'AGN + BB)
;oplot, alog10(lambda), alog10(nuFnu), line=2, thick=thick
;;;SED TRANSMISSE (== SED de la BLR)
;oplot, alog10(lambda), alog10(transmitted), color=!orange, line=3, thick=thick
;;
;;
;;final realistic Sy1 spectrum : remettre sur le devant de la figure ?
oplot, alog10(lambda), alog10(nuFnu + transmitted), color=!green, thick=thick
oplot, alog10(lambda_transmitted_final), alog10(nuFnu + nuFnu_transmitted_final), color=!dgreen, thick=thick
;;
;;
;;
;;;;SED Sy1 MOYENNE
oplot, alog10(3e8/nu1[t1_bas]*1e6), alog10(nuFnu1[t1_bas])+shift, color=!red, thick=thick
oplot, alog10(3e8/nu1[t1_haut]*1e6), alog10(nuFnu1[t1_haut])+shift, color=!red, thick=thick
oplot, alog10(3e8/nu1[t1_milieu]*1e6), alog10(nuFnu1[t1_milieu])+shift, color=!red, thick=thick
;;;SEDs Sy1 INDIVIDUELLES
plotsym, 0, 0.5, /fill
;oplot, alog10(3e8/nu7469*1e6), alog10(nuFnu7469)+shift, color=!dpink, psym=8, thick=thick, syms=thick/2.
;oplot, alog10(3e8/nu3783*1e6), alog10(nuFnu3783)+shift, color=!blue, psym=8, thick=thick, syms=thick/2.
;oplot, alog10(3e8/nu1566*1e6), alog10(nuFnu1566)+shift, color=!dgreen, psym=8, thick=thick, syms=thick/2.

;;;SED d'un CORPS GRIS a T
;;;oplot, alog10(3e8/nu_bb_theoretical*1e6), alog10(nuFnu_GB_theoretical/1e10)+shift, color=!cyan, thick=thick

defplotcolors
!p.background=!white & !p.color=!black
 ; Create the legend with NASA Astronomy routine AL_LEGEND.
 ;   items = [ "NGC 7469", "NGC 3783", "NGC 1566", "Mean Sy1 SED", "Realistic Sy1 SED", "Incident 'AGN+BB' SED", "Transmitted 'AGN+BB' SED", "Greybody"]
 ;   psyms = [ 8, 8, 8, -3, -3, -3, -3, -3]
 ;   colors = [!dpink, !blue, !dgreen, !red, !green, 0, !orange, !cyan]
 ;   linestyles = [0, 0, 0, 0, 0, 2, 3, 0]
    
    items = ['Model of Sy1 SED','Mean obs. Sy1 SED',  'Data dispersion']
    lines= 0 
    colors = [!green,!red,!dmagenta]
    
    ; Add the legend.
    AL_Legend, items, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, background_color=!white, linsize=lin, line=lines


if ps eq 1 then ps_end, /png
;if ps ne 0 then spawn, 'open '+nm+'final_realistic_Sy1_spectrum.eps'


stop

end
