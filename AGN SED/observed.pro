pro observed, ps=ps

defplotcolors


;SED observationnelles moyennes pour AGN de types Seyfert 1 et Seyfert
;2
;Prieto et al 2010
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
    ;nu : units Hz
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s

t1_bas = where(nu1 le 1e16)
t2_bas = where(nu2 le 1e16)

t1_haut = where(nu1 gt 1e16)
t2_haut = where(nu2 gt 1e16)

;Mean Quasars SEDs
;Richards et al 2006
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/type1_Quasar_SED.txt", logNu, logL_all, err_L_all, logBlue, logRed, logOptlum, logOptdim, logIRlum, logIRdim

nu = 10^logNu
logLambda=8*alog10(3) + 6 - logNu ;log lambda (microns) = log (3e8/nu*1e6)
;L_all_qu = 10^logL_all
;print, L_all_qu
logNuFnu = logNu + logL_all - alog10(4*!pi) - 2*20.97;log nuFnu = log (nu*L_all/4*pi*r0^2) avec log r0 = 20.97
nuFnu = (10^logNuFnu)/1e20*10^2.5

;Elvis et al 1994  (median)
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioQuiet.dat", logNu_RQ, logNuLnu_RQ
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioLoud.dat", logNu_RL, logNuLnu_RL
;nu : Hz, nuLnu : erg/s
nu_RQ = 10^logNu_RQ
nu_RL = 10^logNu_RL
logLambda_RQ = 8*alog10(3) + 6 - logNu_RQ
logLambda_RL = 8*alog10(3) + 6 - logNu_RL
logNuFnu_RQ = logNuLnu_RQ  - alog10(4*!pi) - 2*20.97
logNuFnu_RL = logNuLnu_RL  - alog10(4*!pi) - 2*20.97
nuFnu_RQ = (10^logNuFnu_RQ)/100
nuFnu_RL = (10^logNuFnu_RL)/100

t_bas_RQ = where(logNu_RQ le 11 and logNu_RQ gt 9.5) ;tronque car part dans les choux
t_bas_RL = where(logNu_RL le 11)
t_milieu_RQ = where(logNu_RQ gt 11 and logNu_RQ le 16)
t_milieu_RL = where(logNu_RL gt 11 and logNu_RL le 16)
t_haut_RQ = where(logNu_RQ gt 16 and logNu_RQ le 18) ;idem
t_haut_RL = where(logNu_RL gt 16)

;Rq : il semble que les trous de E94 soient artificiels car IL Y A des
;donnees PARTOUT

;trace des graphes
if ps eq 0 then begin 
window, 0
chars=0.9
thick=1
endif
if ps eq 1 then begin 
ps_start, 'observed.eps', /encapsulated, /cm, xsize=20, ysize=15
chars=2
thick=5
!p.charsize=4
!p.charthick=2
endif
defplotcolors

;!p.multi=[0,2,1]
!x.margin=[10,3]
plot, 3e8/nu1[t1_bas]*1e6, nuFnu1[t1_bas], /nodata, /xlog, /ylog, title='Mean observational SEDs', xtitle='Wavelength (micron)', ytitle='SED (nu*Fnu) (arbitrary units)', xr=[1e-8,1e6], yr=[1e-9,1e2], /xstyle, /ystyle, thick=thick
oplot, 3e8/nu1[t1_bas]*1e6, nuFnu1[t1_bas], color=!red, thick=thick
oplot, 3e8/nu1[t1_haut]*1e6, nuFnu1[t1_haut], color=!red, thick=thick
oplot, 3e8/nu2[t2_bas]*1e6, nuFnu2[t2_bas], color=!green, thick=thick
oplot, 3e8/nu2[t2_haut]*1e6, nuFnu2[t2_haut], color=!green, thick=thick
oplot, 3e8/nu*1e6, nuFnu, color=!blue, thick=thick
oplot, 3e8/nu_RQ[t_bas_RQ]*1e6, nuFnu_RQ[t_bas_RQ], color=!purple, thick=thick
oplot, 3e8/nu_RQ[t_milieu_RQ]*1e6, nuFnu_RQ[t_milieu_RQ], color=!purple, thick=thick
oplot, 3e8/nu_RQ[t_haut_RQ]*1e6, nuFnu_RQ[t_haut_RQ], color=!purple, thick=thick
oplot, 3e8/nu_RL[t_bas_RL]*1e6, nuFnu_RL[t_bas_RL], color=!cyan, thick=thick
oplot, 3e8/nu_RL[t_milieu_RL]*1e6, nuFnu_RL[t_milieu_RL], color=!cyan, thick=thick
oplot, 3e8/nu_RL[t_haut_RL]*1e6, nuFnu_RL[t_haut_RL], color=!cyan, thick=thick



 ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = ["Seyfert 1 P10", "Seyfert 2 P10", "Quasar R06","Radio Quiet Quasar E94", "Radio Loud Quasar E94" ]
    psyms = [-3, -3, -3, -3, -3]
    colors = [!red, !green, !blue, !purple, !cyan]
    linestyles = [0, 0, 0, 0, 0]
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcol=!p.color, chars=chars


;plot, logLambda, lognuFnu, title='Observed SEDs', xtitle='log (Wavelength (micron))', ytitle='log nu*Fnu', xr=[-9,4], yr=[10,18.5]
;oplot, logLambda, lognuFnu, color=!blue
;oplot, logLambda_RQ[t_bas_RQ], lognuFnu_RQ[t_bas_RQ]+15, color=!purple
;oplot, logLambda_RQ[t_milieu_RQ], lognuFnu_RQ[t_milieu_RQ]+15, color=!purple
;oplot, logLambda_RQ[t_haut_RQ], lognuFnu_RQ[t_haut_RQ]+15, color=!purple
;oplot, logLambda_RL[t_bas_RL], lognuFnu_RL[t_bas_RL]+15, color=!cyan
;oplot, logLambda_RL[t_milieu_RL], lognuFnu_RL[t_milieu_RL]+15, color=!cyan
;oplot, logLambda_RL[t_haut_RL], lognuFnu_RL[t_haut_RL]+15, color=!cyan
      ;units micron (log)

 ; Create the legend with NASA Astronomy routine AL_LEGEND.
  ;  items = ["Quasar R06", "Radio Quiet Quasar E94", "Radio Loud Quasar E94" ]
  ;  psyms = [-3,-3,-3]
  ;  colors = [!blue,!purple,!cyan]
  ;  linestyles = [0,0,0]
    
    ; Add the legend.
 ;   AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom

;!p.multi=0

if ps eq 1 then ps_end, /png

end
