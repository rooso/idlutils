pro bpt_initial, factor=factor, ps=ps

defplotcolors

if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse

;;;rapports de raies de la SED du l'AGN Sy1 modelise et qui
;;;sert de source pour calculer le transfert radiatif avec cloudy

file_emergent = '/Users/oroos/Post-stage/SED_AGN/'+nm+'realistic_AGN.em_emergent'
file_intrinsic =  '/Users/oroos/Post-stage/SED_AGN/'+nm+'realistic_AGN.em_intrinsic'

  readcol, file_emergent, depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
  readcol, file_intrinsic, depth_int, Lya_1216A_int, Lyb_1026A_int, Hb_4861A_int, OIII_5007A_int, Ha_6563A_int, NII_6584A_int, OI_6300A_int, SII_6731A_6716A_int, NeIII_3869A_int, OII_3727A_multiplet_int, NeV_3426A_int, SiVII_2481m_int, Brg_2166m_int, SiVI_1963m_int, AlIX_2040m_int, CaVIII_2321m_int, NeVI_7652m_int, /silent
;depth : cm
;emissivities

radius_inner = 10d^16.79 ;cm

  depth_em = depth_em + radius_inner ;cm
  depth_int = depth_int + radius_inner ;cm

;print, depth_em
;stop

resolution = 50/2.^13/3.24e-22

!p.multi=0

demarcation_x = findgen(100)/100*3.5-2.5
demarcation_y1 = 0.61/(demarcation_x-0.47)+1.19 ;Kewley 2001
demarcation_y2 = 0.61/(demarcation_x-0.05)+1.3  ;Kaufmann 2003

t1 = where(demarcation_y1 lt 1.1)
t2 = where(demarcation_y2 lt 1.1)

;;;bpt emergent
if ps eq 0 then window, 0, xsize=450, ysize=400
if ps eq 1 then ps_start, 'initial_bpt_emergent_emissivites.eps', /encapsulated;, /cm, xsize=15, ysize=10

plot, [-2.5,1], [-1.5,1.4], /nodata, title='BPT diagram : Emergent emissivities', xtitle='log [NII]/Ha', ytitle='log [OIII]/Hb', /xstyle, /ystyle, /isotropic
defplotcolors
djs_xyouts, 0.4, -1.4, 'AGN', chars=2, thick=10
djs_xyouts, -1.7, -1.4, 'HII region', chars=2, thick=10
oplot, alog10(NII_6584A/Ha_6563A), alog10(OIII_5007A/Hb_4861A);, psym=3, color=100*k
oplot, demarcation_x[t1], demarcation_y1[t1], color=!blue, line=2
oplot, demarcation_x[t2], demarcation_y2[t2], color=!blue

if ps eq 1 then ps_end, /png


;;;bpt intrinseque
if ps eq 0 then window, 1, xsize=450, ysize=400
if ps eq 1 then ps_start, 'initial_bpt_intrinsic_emissivites.eps', /encapsulated;, /cm, xsize=15, ysize=10

plot, [-2.5,1], [-1.5,1.4], /nodata, title='BPT diagram : Intrinsic emissivities', xtitle='log [NII]/Ha', ytitle='log [OIII]/Hb', /xstyle, /ystyle, /isotropic
defplotcolors
djs_xyouts, 0.4, -1.4, 'AGN', chars=2, thick=10
djs_xyouts, -1.7, -1.4, 'HII region', chars=2, thick=10
oplot, alog10(NII_6584A/Ha_6563A), alog10(OIII_5007A/Hb_4861A), psym=3;, color=100*k
oplot, demarcation_x[t1], demarcation_y1[t1], color=!blue, line=2
oplot, demarcation_x[t2], demarcation_y2[t2], color=!blue

if ps eq 1 then ps_end, /png


;;;rapports en fonction de la profondeur

;;;emergent
if ps eq 0 then window, 2, xsize=450, ysize=400
if ps eq 1 then ps_start, 'initial_ratio_emergent.eps', /encapsulated;, /cm, xsize=15, ysize=10

     plot, [3e16,1e18], [0.,0.45], /ystyle, /xlog, /nodata, title='Diagnostic ratios', xtitle='Depth in cm', ytitle='log Emergent emissivity ratio', /xstyle
     oplot, [resolution,resolution], [-2.5,1.5], color=!cyan
     ;BPT ratios
     oplot, depth_em, alog10(OIII_5007A/Hb_4861A), color=!black
     oplot, depth_em, alog10(NII_6584A/Ha_6563A), color=!red
; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["log [OIII]/Hb", "log [NII]/Ha" ]
     psyms = [1, 1]
     colors = [!black, !red]
     linestyles = [0,0]
                               ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /right, textcolor=0, outline_color=0, /clear
if ps eq 1 then ps_end, /png

;;;;intrinsic
if ps eq 0 then window, 3, xsize=450, ysize=400
if ps eq 1 then ps_start, 'initial_ratio_intrinsic.eps', /encapsulated;, /cm, xsize=15, ysize=10

     plot, [3e16,1e18],  [0.,0.45], /ystyle, /xlog, /nodata, title='Diagnostic ratios', xtitle='Depth in cm', ytitle='log intrinsic emissivity ratio', /xstyle
     oplot, [resolution,resolution], [-2.5,1.5], color=!cyan
     ;BPT ratios
     oplot, depth_int, alog10(OIII_5007A_int/Hb_4861A_int), color=!black
     oplot, depth_int, alog10(NII_6584A_int/Ha_6563A_int), color=!red
; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["log [OIII]/Hb", "log [NII]/Ha" ]
     psyms = [1, 1]
     colors = [!black, !red]
     linestyles = [0,0]
                               ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /right, textcolor=0, outline_color=0, /clear
if ps eq 1 then ps_end, /png


end

