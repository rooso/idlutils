pro components, ps=ps

defplotcolors
!p.background=!white & !p.color=!black
!p.charsize=1.5
;!p.charthick=2

!x.margin=[8,2]   
!y.margin=[4,1]

   if ps eq 0 then thick=2 else thick=25
   if ps eq 0 then lin=1 else lin=0.17

   items = ['     AGN', '  90 K BB', ' 200 K BB', ' 300 K BB', ' 900 K BB', '1400 K BB', '2900 K BB', 'Total (x1, x10, x100)' ]
   colors = [!red,    !green,     !orange,    !blue,   !purple,   !cyan,      !tan,       !black] 
   linestyles = 0

readcol,  "/Users/oroos/Post-stage/SED_AGN/realistic_AGN.con", lambda_tot, nuFnu_tot, attenuated_tot, diffuse_tot, transmitted_tot, reflected_tot, /silent
readcol,  "/Users/oroos/Post-stage/SED_AGN/x10_realistic_AGN.con", lambda_tot_x10, nuFnu_tot_x10, attenuated_tot_x10, diffuse_tot_x10, transmitted_tot_x10, reflected_tot_x10, /silent
readcol,  "/Users/oroos/Post-stage/SED_AGN/x100_realistic_AGN.con", lambda_tot_x100, nuFnu_tot_x100, attenuated_tot_x100, diffuse_tot_x100, transmitted_tot_x100, reflected_tot_x100, /silent


BB = planck(lambda_tot,200,/microns)
t = where(lambda_tot lt 1)
BB[t] = 0


 if ps ne 0 then begin

if ps eq 1 then name='incident'
if ps eq 2 then name='transmitted'
if ps eq 3 then name='incident+transmitted'

        ps_start, 'SED_with_components_'+name+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20, /helvetica, /bold
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1
     !y.charsize=1
!x.thick=40
!y.thick=40
     !p.thick=4
  endif


if ps eq 0 then window, 0, ypos=425
if ps eq 1 or ps eq 0 then begin
 plot, [1e-8,1e6], [1e2,1e12], /nodata, xtitle='Wavelength ['+textoidl('\mu')+'m]', ytitle="Incident SED [erg cm"+textoidl('^{-2}')+" s"+textoidl('^{-1}')+"]", /ylog, /xlog, /noerase, /xstyle, /ystyle
oplot, lambda_tot, nuFnu_tot, color=!gray
oplot, lambda_tot_x10, nuFnu_tot_x10, color=!gray
oplot, lambda_tot_x100, nuFnu_tot_x100, color=!gray
                                ; Add the legend.
     AL_Legend, items, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, background_color=!white, linsize=lin, line=linestyles
p0 = !p & x0 = !x & y0 = !y
endif

if ps eq 0 then window, 1
if ps eq 2 or ps eq 0 then begin
 plot, [1e-8,1e6], [1e2,1e12], /nodata, xtitle='Wavelength ['+textoidl('\mu')+'m]', ytitle="Transmitted SED [erg cm"+textoidl('^{-2}')+" s"+textoidl('^{-1}')+"]", /ylog, /xlog, /noerase, /xstyle, /ystyle
oplot, lambda_tot, transmitted_tot, color=!gray
oplot, lambda_tot_x10, transmitted_tot_x10, color=!gray
oplot, lambda_tot_x100, transmitted_tot_x100, color=!gray
p1 = !p & x1 = !x & y1 = !y
endif

if ps eq 0 then window, 2, ypos=425
if ps eq 3 or ps eq 0 then begin
 plot, [1e-8,1e6], [1e2,1e12], /nodata, xtitle='Wavelength ['+textoidl('\mu')+'m]', ytitle="Incident + Transmitted SED [erg cm"+textoidl('^{-2}')+" s"+textoidl('^{-1}')+"]", /ylog, /xlog, /noerase, /xstyle, /ystyle
oplot, lambda_tot, nuFnu_tot+transmitted_tot, color=!gray
oplot, lambda_tot_x10, nuFnu_tot_x10+transmitted_tot_x10, color=!gray
oplot, lambda_tot_x100, nuFnu_tot_x100+transmitted_tot_x100, color=!gray
                                ; Add the legend.
     AL_Legend, items, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, thick=thick, background_color=!white, linsize=lin, line=linestyles
p2 = !p & x2 = !x & y2 = !y
endif

files = findfile('/Users/oroos/Post-stage/SED_AGN/Components/*.con', count=nf)

; print, files

print, nf

colors = [!red, !dred, !brown, !blue, !green, !orange, !purple, !cyan, !tan, !black, !black, !black]
help, colors

for k = 0, nf-1 do begin
;if k ne 5 then begin
readcol, files(k), lambda, incident, attenuated, diffuse, transmitted, reflected, /silent

if n_elements(lambda) gt 1 and colors(k) ne !dred and colors(k) ne !brown then begin ;ignorer les BB pas calcules et les composantes AGN x10 et x100
if ps eq 0 then wset, 0
if ps eq 1 or ps eq 0 then begin
!p = p0  & !x = x0  & !y = y0
oplot, lambda, incident, color=colors(k), thick=thick
p0 = !p & x0 = !x & y0 = !y
endif

if ps eq 0 then wset, 1
if ps eq 2 or ps eq 0 then begin
!p = p1  & !x = x1  & !y = y1
oplot, lambda, transmitted, color=colors(k), thick=thick
p1 = !p & x1 = !x & y1 = !y
endif

if ps eq 0 then wset, 2
if ps eq 3 or ps eq 0 then begin
!p = p2  & !x = x2  & !y = y2
oplot, lambda, incident+transmitted, color=colors(k), thick=thick
p2 = !p & x2 = !x & y2 = !y
endif

print, files(k)
;print, 'pause'
;read, go_on
endif 
;endif
endfor

if ps ne 0 then ps_end, /png

stop
print, 'Fin ?'
read, go_on

end
