pro final_realistic_quasar_spectrum, factor=factor, ps=ps

defplotcolors

if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse


;solar abundances, no grains
;solar abundances + blackbody (should be a greybody ---> dust)
readcol,  "/Users/oroos/Post-stage/SED_AGN/"+nm+"realistic_quasarAGN.con", lambda, nuFnu, attenuated, diffuse, transmitted, reflected

readcol,  "/Users/oroos/Post-stage/SED_AGN/"+nm+"realistic_quasarAGN_transmitted.txt", E_transmitted_final, nuFnu_transmitted_final          ;E in Ryd
;transmitted = diffuse* + attenuated         ;* % of diffuse emission
;that is transmitted in the outward direction
lambda_transmitted_final = 6.63e-34*3.0e8*1e6/(E_transmitted_final*2.18e-18);h*c*1e6/(E(Ryd)*2.18e-18)


;calcul du flux de photons ionisants :
nu_transmitted_final = 3.0d8/(lambda_transmitted_final*1d-6) ;Hz
Fnu_transmitted_final = nuFnu_transmitted_final/nu_transmitted_final ;erg/cm2/s

;ionisant = E >= 13.6 eV <=> nu >= 3.28d15 Hz
nu_transmitted_final_ion = nu_transmitted_final[where(nu_transmitted_final ge 3.28d15 and finite(nu_transmitted_final) eq 1,nt_ion)]
Fnu_transmitted_final_ion = Fnu_transmitted_final[where(nu_transmitted_final ge 3.28d15 and finite(nu_transmitted_final) eq 1,nt_ion)]

print, nt_ion

Flux_ionisant = int_tabulated(nu_transmitted_final_ion,Fnu_transmitted_final_ion,/double,/sort) ;erg/cm2
print, flux_ionisant

;stop

nuFnu = 0.00000003/2.5*nuFnu
attenuated = 0.00000003/2.5*attenuated
diffuse = 0.00000003/2.5*diffuse
transmitted = 0.00000003/2.5*transmitted
reflected = 0.00000003/2.5*reflected
nuFnu_transmitted_final = 0.00000003/2.5*nuFnu_transmitted_final

print, minmax(nuFnu)

;SED observationnelle mediane pour des AGN de type Quasar Radio Loudet Radio Quiet
;Elvis et al 1994
;Median quasar SEDs :
;Elvis et al 1994
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioQuiet.dat", logNu_quiet, logNuLnu_quiet
READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/E94_MEDIAN_RadioLoud.dat", logNu_loud, logNuLnu_loud

t_bas_ququiet = where(logNu_quiet ge 9.75 and logNu_quiet le 10.25,nt1)
t_milieu_ququiet = where(logNu_quiet ge 12.5 and logNu_quiet le 15.5,nt2)
t_haut_ququiet = where(logNu_quiet ge 16.6 and logNu_quiet le 18.3,nt3)
t_bas_quloud = where(logNu_loud ge 9.75 and logNu_loud le 10.25,nt4)
t_milieu_quloud = where(logNu_loud ge 12.5 and logNu_loud le 15.5,nt5)
t_haut_quloud = where(logNu_loud ge 16.6 and logNu_loud le 18.3,nt6)

print, minmax(lognu_quiet)

;SED observationnelles moyennes pour AGN de types Sy 1 et Seyfert 2
;Prieto et al 2010
;READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy1.dat.txt", nu1, nuFnu1
;READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/average_sy2.dat.txt", nu2, nuFnu2
;    ;nu : units Hz
;    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s
;t1_bas = where(nu1 le 1e11)
;t2_bas = where(nu2 le 1e12)
;
;t1_milieu = where(nu1 gt 1e11 and nu1 le 1e16)
;t2_milieu = where(nu2 gt 1e12 and nu2 le 1e16)
;
;t1_haut = where(nu1 gt 1e16)
;t2_haut = where(nu2 gt 1e16)
;
;;SED individuelles de Sy1
;READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC7469_Sy1.dat", nu7469, Fnu7469
;READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC3783_Sy1.dat", nu3783, Fnu3783
;READCOL, "/Users/oroos/Cloudy/c13.00/Mydata/Observational SEDs/NGC1566_Sy1.dat", nu1566, Fnu1566
;    ;nu : units Hz
;    ;Fnu : units Jy = 1e-23 erg/cm2/s/Hz
;                                        ;donnees  ;ce que l'on veut tracer  
;;rest-frame correction with redshifts : nu_obs = nu_rest-frame/(1 + z)
;z3783 = 0.00973
;z1566 = 0.005017
;z7469 = 0.0163
;
;;nu_rest-frame = nu_obs*(1+z)
;nu7469 = nu7469*(1+z7469)
;nu3783 = nu3783*(1+z3783)
;nu1566 = nu1566*(1+z1566)
;Fnu7469 = Fnu7469/(1+z7469)
;Fnu3783 = Fnu3783/(1+z3783)
;Fnu1566 = Fnu1566/(1+z1566)
;
;nuFnu7469 = nu7469*Fnu7469/mean(nu7469*Fnu7469)
;nuFnu3783 = nu3783*Fnu3783/mean(nu3783*Fnu3783)
;nuFnu1566 = nu1566*Fnu1566/mean(nu1566*Fnu1566) ;normalization
;

;theoretical greybody
Tdust = 300 ;K

h = 6.63e-34 ;kg m2 / s
k = 1.38e-23 ;J/K

nu_bb_theoretical = double(3e8/(lambda*1e-6))
nu_0 = max(nu_bb_theoretical)
alpha = .93

nuFnu_bb_theoretical = nu_bb_theoretical*(nu_bb_theoretical^3)/(exp(h*nu_bb_theoretical/(k*Tdust)) - 1.0)*2.0*h/3e8/3e8

nuFnu_GB_theoretical = nuFnu_bb_theoretical * (nu_0/nu_bb_theoretical)^alpha

window, 1
plot, nu_bb_theoretical, nuFnu_GB_theoretical/1e10, /xlog, /ylog

;Sy1 : galaxy seen face-on (normal to the galactic plane)
;incident + diffusion by dust + attenuation by clumps but not by dust (radiation comes from sublimation zone)
;realistic_Sy1 = a_Sy1*nuFnu_nodust_alphaox +
;b_Sy1*nuFnu_transmitted_bb


if ps eq 0 then begin 
window, 20
thicks=1
chars=0.9
endif
if ps eq 1 then begin
ps_start, nm+'final_realistic_quasar_spectrum.eps', /encapsulated, /color, /decomposed, /cm, xsize=20, ysize=15
thicks=10
chars=2
     !p.font=0
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1.5
     !y.charsize=1.5
     !p.thick=6
endif
defplotcolors
           
!p.background=!white & !p.color=!black     ;;;, title="Realistic+BB Sy1 spectrum"
plot, alog10(lambda), alog10(nuFnu), /nodata, xtitle='log Wavelength (micron)', ytitle='log nu*Fnu (arbitrary units)', xr=[-9,6], yr=[-6,3], /xstyle, /ystyle, thick=thicks
;
;
;final realistic quasar spectrum
oplot, alog10(lambda), alog10(nuFnu + transmitted), color=!green, thick=thicks
oplot, alog10(lambda_transmitted_final), alog10(nuFnu + nuFnu_transmitted_final), color=!dgreen, thick=thicks

;mean (or median ??? E94 is not clear) observation quasar SEDs :
oplot, alog10(3e8/10d^lognu_quiet[t_bas_ququiet]*1e6), (logNuLnu_quiet[t_bas_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_quiet[t_milieu_ququiet]*1e6), (logNuLnu_quiet[t_milieu_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_quiet[t_haut_ququiet]*1e6), (logNuLnu_quiet[t_haut_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_loud[t_bas_quloud]*1e6), (logNuLnu_loud[t_bas_quloud]-44.5), color=!magenta
oplot, alog10(3e8/10d^lognu_loud[t_milieu_quloud]*1e6), (logNuLnu_loud[t_milieu_quloud]-44.5), color=!magenta
oplot, alog10(3e8/10d^lognu_loud[t_haut_quloud]*1e6), (logNuLnu_loud[t_haut_quloud]-44.5), color=!magenta
;
t = where(abs(alog10(3e8/10d^lognu_loud*1e6) + 2.9) lt 0.01,nt)
if nt gt 0 then begin
print, 'ok loud', (alog10(3e8/10d^lognu_loud*1e6))[t], logNuLnu_loud[t]-44.5
mean_loud =  logNuLnu_loud[t(0)]-44.5
endif
t = where(abs(alog10(3e8/10d^lognu_loud*1e6) + 2.9) lt 0.01,nt)
if nt gt 0 then begin
print, 'ok quiet', (alog10(3e8/10d^lognu_quiet*1e6))[t], logNuLnu_quiet[t]-44.5
endif



print, minmax(alog10(lambda)), minmax(alog10(3e8/10d^lognu_quiet*1e6))
print, minmax(alog10(nuFnu + transmitted)), minmax(logNuLnu_loud-45)

;SEDs individuelles de quasars
files_quasar_indiv = findfile('/Users/oroos/Post-stage/Observational_SEDs/Quasars_SEDs_for_median/Q*.asc.txt', count=nfiles_quasar)
for kk = 0, nfiles_quasar-1 do begin
readcol, files_quasar_indiv(kk), log_nu0, log_nu1, log_nu2, log_nuFnu, /silent
;print, files_quasar_indiv(kk)
;print, log_nuFnu
;stop
;help, log_nu0
t_bas_mean = where(alog10(3e8/10d^log_nu0*1e6) le 2.1,nt1)
t_haut_mean = where(alog10(3e8/10d^log_nu0*1e6) ge 4.2 and alog10(3e8/10d^log_nu0*1e6) le 5,nt3)
if nt1 gt 0 then oplot, alog10(3e8/10d^log_nu0[t_bas_mean]*1e6), log_nuFnu[t_bas_mean]-11.666654+mean_loud, psym=sym(1), color=!dgray
if nt3 gt 0 then oplot, alog10(3e8/10d^log_nu0[t_haut_mean]*1e6), log_nuFnu[t_haut_mean]-11.666654+mean_loud, psym=sym(1), color=!dgray ;;diviser par la moyenne et multiplier par la moyenne attendue 
                                ;pour bien placer le graphe sur les
                                ;autres
;print, minmax(log_nuFnu-11.666654+mean_loud)
;t = where(abs(alog10(3e8/10d^log_nu0*1e6) + 2.9) lt 0.01,nt)
;if nt gt 0 then begin
;print, 'ok', (alog10(3e8/10d^log_nu0*1e6))[t], log_nuFnu[t], kk
;if kk eq 0 then all_log_nuFnu290 = log_nuFnu[t] else  all_log_nuFnu290 = [all_log_nuFnu290, log_nuFnu[t]] 
;endif
;help, log_nuFnu
;stop
endfor
;print, alog10(mean(10d^all_log_nuFnu290)), alog10(median(10d^all_log_nuFnu290))
;       11.666654                            11.420000

;
;;;DISPERSION DES DONNEES OBSERVATIONNELLES
;oband, alog10(3e8/nu1[t1_bas]*1e6), alog10(nuFnu1[t1_bas])-0.8, alog10(nuFnu1[t1_bas])+0.4, color=!dmagenta, /line_fill, border=!dmagenta, thick=thicks
;oband, alog10(3e8/nu1[t1_haut]*1e6), alog10(nuFnu1[t1_haut])-0.7, alog10(nuFnu1[t1_haut])+0.4, color=!dmagenta, /line_fill, border=!dmagenta, thick=thicks
;oband, alog10(3e8/nu1[t1_milieu]*1e6), alog10(nuFnu1[t1_milieu])-0.7, alog10(nuFnu1[t1_milieu])+0.3, color=!dmagenta, /line_fill, border=!dmagenta, thick=thicks
;;;SED INCIDENTE (==SED de l'AGN + BB)
;oplot, alog10(lambda), alog10(nuFnu), line=2, thick=thicks
;;;SED TRANSMISSE (== SED de la BLR)
;oplot, alog10(lambda), alog10(transmitted), color=!orange, line=3, thick=thicks

;remettre devant la figure ?
;final realistic quasar spectrum
oplot, alog10(lambda), alog10(nuFnu + transmitted), color=!green, thick=thicks
oplot, alog10(lambda_transmitted_final), alog10(nuFnu + nuFnu_transmitted_final), color=!dgreen, thick=thicks

;mean (or median ??? E94 is not clear) observation quasar SEDs :
oplot, alog10(3e8/10d^lognu_quiet[t_bas_ququiet]*1e6), (logNuLnu_quiet[t_bas_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_quiet[t_milieu_ququiet]*1e6), (logNuLnu_quiet[t_milieu_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_quiet[t_haut_ququiet]*1e6), (logNuLnu_quiet[t_haut_ququiet]-44.5), color=!blue
oplot, alog10(3e8/10d^lognu_loud[t_bas_quloud]*1e6), (logNuLnu_loud[t_bas_quloud]-44.5), color=!magenta
oplot, alog10(3e8/10d^lognu_loud[t_milieu_quloud]*1e6), (logNuLnu_loud[t_milieu_quloud]-44.5), color=!magenta
oplot, alog10(3e8/10d^lognu_loud[t_haut_quloud]*1e6), (logNuLnu_loud[t_haut_quloud]-44.5), color=!magenta


defplotcolors
!p.background=!white & !p.color=!black
 ; Create the legend with NASA Astronomy routine AL_LEGEND.
    items = [ "Model of quasar SED in CLOUDY", "Mean RL quasar SED", "Mean RQ quasar SED", "Individual quasar SEDs"]
    psyms = [ -3,-3,-3,sym(1)]
    colors = [!dgreen,!magenta,!blue,!dgray]
    linestyles = 0
    
    ;items = ['Model of Sy1 SED in CLOUDY','Mean of observed Sy1 SED',  'Data dispersion']
    ;psyms = [-3,-3,-3]
    ;colors = [!green,!red,!dmagenta]

     txt = 0
    
    
    ; Add the legend.
    AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, textcolors=txt, /bottom, chars=chars, outline=0, linsize=0.1

if ps eq 1 then ps_end, /png

if ps ne 0 then spawn, 'open '+nm+'final_realistic_quasar_spectrum.eps'


end
