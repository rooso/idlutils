pro psd
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ce programme affiche les PSD calculees par RDRAMSES avec la commande ;
;~/bin/rdramses -inp output_00210 -lmax 13 -psd                       ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


  dir = '/Users/oroos/Post-stage/RDRAMSES/'

  readcol, dir+'psd_00075_l13_rho.psd', wavenumber075, power075
  readcol, dir+'psd_00100_l13_rho.psd', wavenumber100, power100  
  readcol, dir+'psd_00150_l13_rho.psd', wavenumber150, power150
  readcol, dir+'psd_00170_l13_rho.psd', wavenumber170, power170 
  readcol, dir+'psd_00210_l13_rho.psd', wavenumber210, power210

  readcol, dir+'psd_00178_l12_rho.psd', wavenumber178_lores, power178_lores ;temps le plus proche de 00150_hires
  readcol, dir+'psd_00150_l13_read_at_l12_rho.psd', wavenumber150_read_at_lores, power150_read_at_lores

  window,1
  plot, wavenumber100,power100, /xlog, /ylog, xr=[1e-5,2e-1], yr=[1e-8,1e2], xtitle='k [pc^-1]', ytitle='Power', /xstyle, /ystyle                                    
  oplot, wavenumber075, power075, color=!blue                      
  oplot, wavenumber150, power150, color=!green
  oplot, wavenumber170, power170, color=!purple
  oplot, wavenumber210, power210, color=!red    
  oplot, wavenumber178_lores, power178_lores, psym=1
  oplot, wavenumber150_read_at_lores, power150_read_at_lores, psym=1, color=!green

  txt = ['075','100','150','170','210', '178_lores', '150_read_at_lores']
  al_legend, txt, colors=[!blue,!black,!green,!purple,!red, !black,!green], /bottom, lines=0, textc=!black, /box
end
