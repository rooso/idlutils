pro pdf_amr, sim=sim, ps=ps, what_to_plot=what_to_plot

correct_ps = ps

if not keyword_set(what_to_plot) then what_to_plot = 'all'

if what_to_plot eq 'all' then nm_file = 'mass_sfr_' else nm_file = what_to_plot+'_' 

  kpc_to_m = 1./(3.24d-20)
  hcc_to_kgm3 = 1.67d-21

  sim_with = ['00075w', '00100', '00108', '00150', '00170', '00210']
  sim_no   = ['00075n', '00085', '00090', '00120', '00130', '00148']
  
  for sim_ii = 0,  n_elements(sim_with)-1 do if strmatch(sim, sim_with(sim_ii)) eq 1 then break
  
  
  print, 'Restauration du fichier AMR '+sim_with(sim_ii)
;readcol, '/Users/oroos/Post-stage/orianne_data/'+fb_case+'_AGN_feedback/gas_part_'+sim+'.ascii.lmax'+lmax, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr 
;save, /variables, file='amr_'+sim+'_all.sav'
  restore, '/Users/oroos/Post-stage/LOPs'+sim_with(sim_ii)+'/amr_'+sim_with(sim_ii)+'_all.sav'
  print, 'Fin de la restauration du fichier AMR '+sim_with(sim_ii)
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(50.d3/2^13)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_amr ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_amr[t_rho]
     t_temp = where(temperature_amr[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_amr[t_rho(t_temp)] = 900
  endif
  
ps = correct_ps

;;;
cell_size_amr_cube = double(cell_size_amr)^3

if ps eq 0 then window, 1
 plothist, alog10(density_amr), den_bins, number_per_den_bin, bin=0.3d, /ylog, xr=[-8,7], yr=[1,1e8], /ystyle, min=-7d, omin=mino, omax=maxo, /l64, /xstyle, /save, xtitle='Density [H/cc]', ytitle='Number of cells per density bin', reverse_indices=rev_in_den
oplot, [0,0], [1,1e8], thick=4, color=!gray
oplot, [1,1], [1,1e8], thick=4, color=!gray
oplot, [2,2], [1,1e8], thick=4, color=!gray
 plothist, alog10(density_amr), den_bins, number_per_den_bin, bin=0.3d, min=-7d, omin=mino, omax=maxo, /l64, /overplot

mass_fraction_per_den_bin =  number_per_den_bin*0.0
sfr_fraction_per_den_bin =  number_per_den_bin*0.0


for kk = 0, n_elements(den_bins)-1 do begin
if rev_in_den[kk] ne rev_in_den[kk+1] then begin
mass_fraction_per_den_bin(kk) = total(density_amr[rev_in_den[rev_in_den[kk]:rev_in_den[kk+1]-1]]*cell_size_amr_cube[rev_in_den[rev_in_den[kk]:rev_in_den[kk+1]-1]])/total(density_amr*cell_size_amr_cube)


t_sfr = where(temperature_amr[rev_in_den[rev_in_den[kk]:rev_in_den[kk+1]-1]] lt 1d4,nt_sfr)
if nt_sfr gt 0 then sfr_fraction_per_den_bin(kk) = total(((density_amr[rev_in_den[rev_in_den[kk]:rev_in_den[kk+1]-1]])[t_sfr])^1.5*(cell_size_amr_cube[rev_in_den[rev_in_den[kk]:rev_in_den[kk+1]-1]])[t_sfr])/total((density_amr[where(temperature_amr lt 1d4)])^1.5*cell_size_amr_cube[where(temperature_amr lt 1d4)])
endif
endfor

if ps eq 0 then thick = 2 else thick = 40
if ps eq 0 then lin = 0.5 else lin = 0.1

!x.margin=[9,1]
!y.margin=[3,0]

 if ps eq 1 then begin                   ;NB : tres similaire pour tous les snapshots
        ps_start, 'amr_'+nm_file+'pdf_'+sim+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=25, ysize=18, /helvetica, /bold
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1
     !y.charsize=1
     !p.thick=4
!x.thick=30
!y.thick=30
  endif

if what_to_plot eq 'sfr' then add = 'of SFR '
if what_to_plot eq 'mass' then add = 'of mass '
if what_to_plot eq 'all' then add = ''

if what_to_plot eq 'sfr' then yr = [1d-6,1] else yr = [1d-11,1]
if what_to_plot eq 'sfr' then xr = [-2,6] else xr = [-8,7]

if ps eq 0 then window, 3
 plot, den_bins, mass_fraction_per_den_bin, /ylog, xr=xr, yr=yr, /ystyle, /xstyle, xtitle='Density [cm'+textoidl('^{-3}')+']', ytitle='Fraction '+add+'per bin', psym=10, /nodata, /isotropic
oplot, [0,0], [1d-11,1], thick=2*thick, color=!gray
oplot, [1,1], [1d-11,1], thick=2*thick, color=!gray
oplot, [2,2], [1d-11,1], thick=2*thick, color=!gray
if what_to_plot eq 'all' or what_to_plot eq 'mass' then oplot, [den_bins(0),den_bins,den_bins(-1)], [0,mass_fraction_per_den_bin,0], psym=10, thick=thick
if what_to_plot eq 'all' or what_to_plot eq 'sfr'  then oplot, [den_bins(0),den_bins,den_bins(-1)], [0,sfr_fraction_per_den_bin,0], psym=10, color=!green, thick=thick
     items = ["Mass fraction", "  SFR fraction"]
     colors = [!black,!green]
     linestyle = [0,0]
                                ; Add the legend.
if what_to_plot eq 'all' then AL_Legend, items, Color=colors, /top, /left, textcolor=!black, outline_color=!black, /clear, thick=thick, linsize=lin, back=!white, line=linestyle



     if ps eq 1 then ps_end, /png
     if ps eq 1 then print, 'Conversion en png...'
stop


end
