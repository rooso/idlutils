;
; Make a movie from Ramses output maps.  These can be generated
; on-the-fly in Ramses with the movie_module, or they can be made
; using amr2map in post-processing.  We basically just convert the
; fits image files to jpg.
;
; 
; scale_bar -- length of scale bar in kpc.  Default is no scale bar
; no_time -- do not print out the sim time in the corner of each image
; filenum_offset -- start the naming of output jpg files with this
;    number.  Default is 0.
; skip_done -- skip over files that appear to be done already based
;    on the existence of the output jpg file.  BE CAREFUL WITH THIS!
;    The existing output files may not correspond to any new
;    FITS images.
; no_bh -- do not plot location of BH particles
; do_1st_only -- only make the first image, don't run ffmpeg.
;                This allows you to check what the image will look
;                like before running a large batch of files (temp.eps)
; colortable -- a number specifying a color table to use for the
;               image.
; velocity_flip -- used to convert a z-velocity to outflow velocity.
;                  Pixels below the image mid-plane will be multiplied
;                  by -1. 
;
; range -- set the byte scale range of the simulation image.  Default
;          value is for a log image of gas density for a 6-pc
;          simulation w/ AGN.  Some useful values:
;          [4,7] -- Temperature in edge-on view to show AGN winds
;          [0,4] -- Velocity in edge-on view to show AGN winds
;
PRO ramses_movie, filesearch_string, files=files, ffmpeg=ffmpeg,$
                  zoom = zoom, range=range, scale_bar=scale_bar, $
                  smooth=smooth, filenum_offset=filenum_offset,$
                  acc_file=acc_file, no_time=no_time, skip_done=skip_done,$
                  no_bh = no_bh, do_1st_only=do_1st_only,$
                  colortable=colortable, velocity_flip=velocity_flip,$
                  colorbar=colorbar, label_colorbar=label_colorbar,$
                  tar_jpg=tar_jpg

IF N_ELEMENTS(filesearch_string) EQ 0 AND N_ELEMENTS(files) EQ 0 THEN BEGIN
   PRINT,"ramses_movie, filesearch_string, files=files, ffmpeg=ffmpeg,$"
   PRINT,"   zoom=zoom, range=range, scale_bar=scale_bar,$"
   PRINT,"   smooth=smooth, filenum_offset=filenum_offset, $"
   PRINT,"   acc_file=acc_file, no_time=no_time, skip_done=skip_done,$"
   PRINT,"   no_bh=no_bh, do_1st_only=do_1st_only"
   PRINT,"   colortable=colortable, velocity_flip=velocity_flip,$"
   PRINT,"   colorbar=colorbar, label_colorbar=label_colorbar"
   PRINT,"   tar_jpg=tar_jpg"
   RETURN
ENDIF

IF NOT KEYWORD_SET(files) THEN BEGIN
   files = FILE_SEARCH(filesearch_string)
;stop
ENDIF

nfiles = N_ELEMENTS(files)
IF nfiles LT 1 THEN BEGIN
   MESSAGE,/info, "No files w/ search string", filesearch_string
   RETURN
ENDIF

IF NOT KEYWORD_SET(filenum_offset) THEN filenum_offset = 0

set_plot, 'ps'
FOR i=0, nfiles-1 DO BEGIN
; Read the image file and replot it in postscript format.
; The main reason to do this is to change the image scaling (make it
; logarithmic), and to plot the BH
   PRINT, "File " + STRN(i,LEN=5) + " of " + STRN(nfiles,LEN=5)
   PRINT, files[i]

   if KEYWORD_SET(skip_done) THEN BEGIN
      filenum = i + filenum_offset
      jpg_filename = 'pic' + STRN(filenum,LEN=5, PADCHAR='0') + '.jpg'
      if FILE_TEST(jpg_filename) THEN BEGIN
         PRINT, "Skipping..."
         continue
      ENDIF
   ENDIF

   im = readfits(files[i], hdr,/silent)
   logim = ALOG10(im)

   IF KEYWORD_SET(velocity_flip) THEN BEGIN
; multiply bottom half of image by -1
      ny_pix = N_ELEMENTS(im[0,*])
      im[*,0:ny_pix/2] *= -1.0
; set inflowing values to "zero"
      w_inflow = WHERE(im LT 0.0,ninflow)
      im[w_inflow] = 1d-10
      logim = ALOG10(im)
;;      logim = im  ;; use linear scaling
   ENDIF

;hprint, hdr
;stop

   ; get box info from header
   Lbox = sxpar(hdr, 'BOXLEN')
   xmin = sxpar(hdr, 'XMIN')
   xmax = sxpar(hdr, 'XMAX')
   ymin = sxpar(hdr, 'YMIN')
   ymax = sxpar(hdr, 'YMAX')
   xlen = (xmax - xmin) * Lbox
   ylen = (ymax - ymin) * Lbox
   time = sxpar(hdr, 'TIME')
   idim = sxpar(hdr, 'IDIM')
   jdim = sxpar(hdr, 'JDIM')

   ; Unit here will generally be in kpc, and the center of the image
   ; will be the point [0,0] 
   imgxrange = [-1.0 * xlen / 2.0, xlen / 2.0]
   imgyrange = [-1.0 * ylen / 2.0, ylen / 2.0]
   IF NOT KEYWORD_SET(range) THEN range = [-4,1.5]

; count how many sink particles
   w_xsink = WHERE(STRMATCH(hdr, 'XSINK*'), nsink)
; loop over sink particles, extracting their positions from the header
 if nsink ne 0 then begin
   pos_sink = FLTARR(3)
   xsink = FLTARR(nsink)
   ysink = FLTARR(nsink)
   FOR isink = 0, nsink - 1 DO BEGIN
      pos_sink[0] = sxpar(hdr, 'XSINK'+STRN(isink+1, LEN=3, PADCHAR='0'))
      pos_sink[1] = sxpar(hdr, 'YSINK'+STRN(isink+1, LEN=3, PADCHAR='0'))
      pos_sink[2] = sxpar(hdr, 'ZSINK'+STRN(isink+1, LEN=3, PADCHAR='0'))

; Figure out which projection this is, and convert the sink position
; to a position in the image.
      xsink[isink] = pos_sink[idim-1]
      ysink[isink] = pos_sink[jdim-1]
   ENDFOR
endif
   
   ; create a temporary postscript image
   device, filename = 'temp.eps', encaps = 1, /color, bits_per_pixel=8,$
           xsize = 7, ysize=7, /inches, xoffset=0.5, yoffset=0.5
   !p.font = 0
   t = 5
   charthick=t
   xthick=t
   ythick=xthick
   blankticks= REPLICATE(' ', 30)
   xmargin = [1,1]
   ymargin = [1,1]

   IF KEYWORD_SET(zoom) THEN BEGIN
                                ; don't allow zooming out
      IF zoom GT 1 THEN BEGIN
         npix_orig = N_ELEMENTS(logim[*,0])
         npix_new = npix_orig / zoom
         ipix_first = (npix_orig / 2) - (npix_new / 2)
         ipix_last = (npix_orig / 2) + (npix_new / 2)
         zoom_im = logim[ipix_first:ipix_last, ipix_first:ipix_last]
         logim = zoom_im
         imgxrange = imgxrange / zoom
         imgyrange = imgyrange / zoom
         xlen /= zoom
         ylen /= zoom
      ENDIF ELSE MESSAGE,/info, "No zooming out... zoom="+ STRN(zoom)
   ENDIF

   IF KEYWORD_SET(smooth) THEN logim = SMOOTH(logim, smooth,/nan,/edge_tru)
;stop
   IF KEYWORD_SET(colortable) THEN loadct, colortable,/silent

   plotimage, logim, range= range, $
              imgxrange=imgxrange, imgyrange=imgyrange,$
              xtitle=xtitle, ytitle=ytitle, title=title, $
              xthick=xthick,ythick=ythick, charthick=charthick, $
              xmargin=xmargin, ymargin=ymargin, bottom=bottom, ncolors=ncolors,$
              xtickname=blankticks, ytickname=blankticks
   
   IF KEYWORD_SET(colortable) THEN loadct, 0, /silent

if nsink ne 0 then begin
;----------- Plot the sink/BH particle position
   IF NOT KEYWORD_SET(no_bh) THEN BEGIN
      FOR isink = 0, nsink-1 DO BEGIN
;         print, xsink[isink], ysink[isink]
;   stop
; (MEAN(xmin,xmax)*Lbox - Lbox/2.0) is the offset, in kpc, that the
; center of this IMAGE is from the center of SIMULATION BOX
         xsink[isink] = xsink[isink] - $
                        Lbox / 2.0 - (MEAN([xmin,xmax])*Lbox - Lbox/2.0)
         ysink[isink] = ysink[isink] - $
                        Lbox / 2.0 - (MEAN([ymin,ymax])*Lbox - Lbox/2.0)
         sinksym = 1
         sink_color = 50
         sinkthick=5
         sinksize=1.6           ;  1.4
         sinksym_normal = 1
         
; plot a colored symbol on top of a larger white one so that it will
; stand out
;;       loadct, 0, /silent
;;       sinksym = sym(12)
;;       oplot, [xsink], [ysink], psym = sinksym, $
;;              thick=sinkthick*1.4, symsize=sinksize*1.1,color=254
         IF sinksym_normal GT 0 THEN BEGIN
            loadct, 40,/silent
            sinkthick=8
;            stop
            oplot, [xsink[isink]], [ysink[isink]], psym = sinksym, $
                   thick=sinkthick, symsize=sinksize,color=sink_color
            loadct,0, /silent
         ENDIF ELSE BEGIN
;;       loadct, 40,/silent
            loadct, 40,/silent
            sinkthick = 2
            jmg_plotsym, 9, /fill, thick=sinkthick
            oplot, [xsink[isink]], [ysink[isink]], psym = 8, $
                   thick=sinkthick, symsize=sinksize,color=sink_color
            jmg_plotsym, 9, thick=sinkthick
            loadct, 0,/silent
            oplot, [xsink[isink]], [ysink[isink]], psym = 8, $
                   thick=sinkthick, symsize=sinksize,color=254
         ENDELSE
         
      ENDFOR ; loop over sink particles
   ENDIF ; is no_bh set?
endif

;----------- Plot a scale bar if requested
   IF KEYWORD_SET(scale_bar) THEN BEGIN
      x_scale = REPLICATE(imgxrange[0] + 0.1*xlen, 2)
      x_scale[1] = x_scale[0] + scale_bar
      y_scale = REPLICATE(imgyrange[0] + 0.05*ylen, 2)
      oplot, x_scale, y_scale, thick=8, color=254
      
                                ; plot a label for the scale bar
      scale_label = STRN(FIX(scale_bar)) + " kpc"
      xyouts, MEAN(x_scale), y_scale[0] + 0.02*ylen, $
              align=0.5, scale_label, /data, charsize=1.2, color=254
      
;   print, xmin, ymin
;   stop
   ENDIF


;---------- Plot the simulation time in the corner of the image
   IF NOT KEYWORD_SET(no_time) THEN BEGIN
      x_time = 0.1
      y_time = 0.9

      xyouts, x_time, y_time, /norm, STRN(time, form='(f8.1)', LEN=8) + ' Myr',$
              color=254, align=0.5
   ENDIF

;---------- Plot a color bar
   IF KEYWORD_SET(colorbar) THEN BEGIN
;;    cbar_x = [2.7,3.5]           ; in data coordinates
;;    cbar_y = [-3.0, 2.5]

;;     ; convert to normalized coords
;;     cx = convert_plot_coords(cbar_x, 'x')
;;     cy = convert_plot_coords(cbar_y, 'y')

; in normalized coords
      IF KEYWORD_SET(colortable) THEN loadct, colortable, /silent
      cbar_pos = [0.94, 0.1, 0.99, 0.9]
      
      value_range = range
      fmt = '(F4.1)'
;;      divisions = 5
      fsc_colorbar, ncolors = ncolors,$
                    maxrange= MAX(value_range), minrange = MIN(value_range), $
                    charsize=1, /vertical, format = fmt,$
                    position = cbar_pos, /data, bottom=bottom,$
                    divisions = divisions, color=254
      
; go back to black-and-white
      loadct, 0, /silent
      
; print a label for the color bar
      IF KEYWORD_SET(label_colorbar) THEN BEGIN
         label_coord = [0.97, 0.93]
         
         xyouts, label_coord[0], label_coord[1], $
                 align=1.0, label_colorbar,/norm, charsize=1,$
                 color = 254
      ENDIF

   ENDIF ; colorbar

;---------- Plot a plot of accretion rate if available
   IF KEYWORD_SET(acc_file) THEN BEGIN
      acc = read_accretion(acc_file)
      t= 5
      xtit='Time (Myrs)'
      ytit='Bondi (Msun/yr)'
      plot_pos = [0.25,0.8, 0.85, 0.98]
      
; clear the plot area
      dx = 0.1
      dy = 0.065
      polyfill, [plot_pos[0]-dx, plot_pos[0]-dx, plot_pos[2]+dx/5, plot_pos[2]+dx/5],$
                [plot_pos[1]-dy, plot_pos[3]+dy, plot_pos[3]+dy, plot_pos[1]-dy],$
                color=254, /norm

; make the main accretion rate plot
      mybondi = SMOOTH(acc.bondi,40)
      plot, acc.time, SMOOTH(acc.bondi,40), /ylog, /noerase, $
            position = plot_pos, $
            xthick=t, ythick=t, thick=t,$
            xtitle = xtit, ytitle=ytit

; overplot a big point on the line at this timestep
      ; find the point on the timeline corresponding to this image
      junk = MIN(ABS(acc.time - time), w_min)
      mycolor = 50
      loadct, 40,/silent
      plotsym , 0, /fill
      mypsym = 8
      oplot, [acc[w_min[0]].time], [mybondi[w_min[0]]], psym=mypsym, $
             symsize=symsize, color=mycolor

; also overplot a colored line up to this point
      oplot, acc[0:w_min[0]].time, mybondi[0:w_min[0]], thick=t+2,$
             color=mycolor
      loadct, 0,/silent
   ENDIF

;###################
   device, /close

;stop

   ; convert postscript file into jpg for movie-making
   filenum = i + filenum_offset
   jpg_filename = 'pic' + STRN(filenum,LEN=5, PADCHAR='0') + '.jpg'
   spawn, "convert -density 300 temp.eps "+  jpg_filename

   ; Quit the procedure after the first image is made, if desired
   IF KEYWORD_SET(do_1st_only) THEN RETURN

ENDFOR ; loop over all the files


set_plot, 'x'

IF KEYWORD_SET(ffmpeg) THEN BEGIN
   SPAWN, '/ccc/cont003/home/dsm/rooso/bin/ffmpeg -y -f image2 -i pic%05d.jpg output.mpg'
ENDIF

IF KEYWORD_SET(tar_jpg) THEN BEGIN
   SPAWN, 'tar cvf jpg.tar pic*jpg'
   SPAWN, 'gzip jpg.tar'
ENDIF

END
