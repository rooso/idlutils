PRO outflow_vel_movie, _extra=extra

  ramses_movie, 'img*v*zz*gz', /colorbar, $
                label_color=textoidl('Log(v_{out})'), range=[0,4.], $
                colortable=0, /ffmpeg, /velocity_flip, $
                scale=10.0, _extra=extra
END
