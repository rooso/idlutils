;
; Use a particle list to plot the star-formation history of a simulation
;
; step -- if the particle file was written with every Nth particle
;         output, this is "N" 
; time_bins -- an output of this function
;
;
FUNCTION star_formation_hist, part_file, step, plot=plot,$
                              time_bins=time_bins

  IF N_ELEMENTS(step) NE 1 THEN $
     MESSAGE, "usage: a=star_formation_hist(part_file, step, /plot,time_bins=tb)"

;mass_unit = 1d9    ; Msun
;time_unit = 14.9d6 ; yrs
  mass_unit = 1.0               ; units are already handled by part2map
  time_unit = 1.0 

; read particle data: mass and time of formation
  readfast, part_file, data
  help, data
  mass = REFORM(data[6,*]) * mass_unit
;tform = REFORM(data[8,*]) * time_unit
  tform = REFORM(data[7,*]) * time_unit

  help, mass
  help, tform

  print, minmax(mass), minmax(tform)

  w = WHERE( tform LT 800,c)
  if c gt 0 then begin
     tform=tform[w]
     mass=mass[w]
  endif else begin
     stop
  endelse
;stop

; correct masses for the fact that some particles were skipped
  mass = step * mass

  t_range = MINMAX(tform)
; dt = 1.0 * 1d6   ; yrs
  dt = 0.1                      ; Myrs
  n_t_bins = FIX((t_range[1] - t_range[0]) / dt)
  print, t_range, n_t_bins

  time_bins = DBLARR(n_t_bins)
  sf_hist =  DBLARR(n_t_bins)

  FOR i=0, n_t_bins - 1 DO BEGIN
     binmin = i * dt + t_range[0]
     binmax = binmin + dt
     time_bins[i] = binmax

     w_bin = WHERE( tform LT binmax AND tform GE binmin, n_bin)
     IF n_bin GT 0 THEN BEGIN
        mass_formed = TOTAL(mass[w_bin])

                                ; sfr averaged over this time interval
        sfr_avg = mass_formed / (dt*1d6)
        sf_hist[i] = sfr_avg

        print, time_bins[i], sf_hist[i]
     ENDIF

  ENDFOR

  ;print, time_bins
;stop

  IF KEYWORD_SET(plot) THEN BEGIN
     window, 1
     plot, time_bins, sf_hist,$
           xtit='time (Myrs)', ytit='SFR (Msun/yr)'

  ENDIF

  print, "total mass formed: ", total(mass)
;stop
  RETURN, sf_hist
END
