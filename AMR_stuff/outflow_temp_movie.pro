PRO outflow_temp_movie, _extra = extra

  ramses_movie, 'img*T*gz', /colorbar, $
                label_color='Log(T/K)', range=[4,7.5], $
                colortable=3,/ffmpeg,$
                scale=10.0, _extra = extra

END
