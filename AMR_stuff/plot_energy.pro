pro plot_energy

;;verifier que le nouveau code avec les SNe cinetiques + thermiques
;;fonctionne bien

dir = '/Users/oroos/Post-stage/SIMUS/Tests_thermal+kinetic/'

;;; verif Etot thermal 
readcol, dir+'Etot_thermal_checkthermal', tot, en, ofn, gas, therm, fb, col, Etot_thermal_checkthermal, f='(A,A,A,A,A,A,A,D)'
print, 'Etot_thermal_checkthermal'
readcol, dir+'Etot_thermal_thermalonly', tot, en, ofn, gas, therm, fb, col, Etot_thermal_thermalonly, f='(A,A,A,A,A,A,A,D)'  
print, 'Etot_thermal_thermalonly'        
readcol, dir+'Etot_thermal_kintherm', tot, en, ofn, gas, therm, fb, col, Etot_thermal_kintherm, f='(A,A,A,A,A,A,A,D)'
print, 'Etot_thermal_kintherm'                 

window, 3      
cgplot, Etot_thermal_checkthermal, col=!green, /xlog, xr=[1,1e8], yr=[0,9e6], title='Total Thermal Energy'   
oplot, Etot_thermal_thermalonly, col=!blue             
oplot, Etot_thermal_kintherm, col=!red  
xyouts, 2, 7.5e6, 'f_ek = 0.1'
;xyouts, 2, 2e6, 'Comportement aleatoire similaire = OK ?'          

window, 5      
cgplot, Etot_thermal_checkthermal, col=!green, /xlog, /ylog, xr=[1,1e8], yr=[1e-5,1e7], title='Total Thermal Energy', psym=3
oplot, Etot_thermal_thermalonly, col=!blue, psym=3             
oplot, Etot_thermal_kintherm, col=!red, psym=3 
xyouts, 2, 4e6, 'f_ek = 0.1'
;xyouts, 2, 5e1, 'Comportement aleatoire similaire = OK ?'


;;; verif Etot kinetic 
readcol, dir+'Etot_kinetic_checkkinetic', tot, en, ofn, gas, kin, fb, col, Etot_kinetic_checkkinetic, f='(A,A,A,A,A,A,A,D)'
print, 'Etot_kinetic_checkkinetic'
readcol, dir+'Etot_kinetic_kineticonly', tot, en, ofn, gas, kin, fb, col, Etot_kinetic_kineticonly, f='(A,A,A,A,A,A,A,D)'  
print, 'Etot_kinetic_kineticonly'        
readcol, dir+'Etot_kinetic_kintherm', tot, en, ofn, gas, kin, fb, col, Etot_kinetic_kintherm, f='(A,A,A,A,A,A,A,D)'
print, 'Etot_kinetic_kintherm'                 

window, 7      
cgplot, Etot_kinetic_checkkinetic, col=!green, /xlog,  xr=[1,1e6], title='Total Kinetic Energy'   
oplot, Etot_kinetic_kineticonly, col=!blue             
oplot, Etot_kinetic_kintherm, col=!red  
xyouts, 2, 7.5e6, 'f_ek = 0.1'
xyouts, 2, 2e6, 'Comportement aleatoire similaire = OK ?'          

window, 9      
cgplot, Etot_kinetic_checkkinetic, col=!green, /xlog, /ylog, xr=[1,1e6], yr=[1e1,1e7], title='Total Kinetic Energy', psym=3
oplot, Etot_kinetic_kineticonly, col=!blue, psym=3             
oplot, Etot_kinetic_kintherm, col=!red, psym=3 
xyouts, 2, 4e6, 'f_ek = 0.1'
xyouts, 2, 5e1, 'Comportement aleatoire similaire = OK ?'



;;;;;;;;;;;;;;;;;;;;;;;;;verifier la masse ejectee (thermique)
;;NB : il n'y a pas de variable 'mejecta' pour le fb cinetique
readcol, dir+'mejecta_thermal_checkthermal', mej, therm, col, mejecta_thermal_checkthermal, f='(A,A,A,D)'
print, 'mejecta_thermal_checkthermal'
readcol, dir+'mejecta_thermal_thermalonly', mej, therm, col, mejecta_thermal_thermalonly, f='(A,A,A,D)'    
print, 'mejecta_thermal_thermalonly'      
readcol, dir+'mejecta_thermal_kintherm', mej, therm, col, mejecta_thermal_kintherm, f='(A,A,A,D)'
print, 'mejecta_thermal_kintherm'                 

print, minmax(mejecta_thermal_checkthermal)
print, minmax(mejecta_thermal_thermalonly)
print, minmax(mejecta_thermal_kintherm)               

window, 11      
cgplot, mejecta_thermal_checkthermal, col=!green, thick=4, /xlog, /ylog, xr=[1e0,2e3], yr=[1e-7,1e-6], title='Ejected mass (thermal FB)'   
oplot, mejecta_thermal_thermalonly, col=!blue             
oplot, mejecta_thermal_kintherm, col=!red  
xyouts, 1, 2e-7, 'when Kin+Therm, m is ejected in Kin part. -> 0 is OK.'
xyouts, 1, 1.5e-7, 'else, seems ok too bec. both are ~ equal ????'


;;;;;;;;;;;;verifier la densite de masse (thermique)
readcol, dir+'mass_density_thermal_checkthermal', mass, den, in, the, cell, therm, fb, col, mass_density_thermal_checkthermal, f='(A,A,A,A,A,A,A,A,D)'
print, 'mass_density_thermal_checkthermal'
readcol, dir+'mass_density_thermal_thermalonly', mass, den, in, the, cell, therm, fb, col, mass_density_thermal_thermalonly, f='(A,A,A,A,A,A,A,A,D)'  
print, 'mass_density_thermal_thermalonly'        
readcol, dir+'mass_density_thermal_kintherm', mass, den, in, the, cell, therm, fb, col, mass_density_thermal_kintherm, f='(A,A,A,A,A,A,A,A,D)'
print, 'mass_density_thermal_kintherm'                 

window, 13      
cgplot, mass_density_thermal_checkthermal, col=!green, /xlog,  xr=[1,1e8], title='Mass density (thermal FB)'   
oplot, mass_density_thermal_thermalonly, col=!blue             
oplot, mass_density_thermal_kintherm, col=!red  
xyouts, 2, 2.8e5, 'f_ek = 0.1'
;xyouts, 2, 5e4, 'Comportement aleatoire similaire = OK ?'

window, 15      
cgplot,mass_density_thermal_checkthermal, col=!green, /xlog,  xr=[1,1e8], /ylog, yr=[1e-5,3e5], title='Mass density (thermal FB)', psym=3
oplot, mass_density_thermal_thermalonly, col=!blue, psym=3             
oplot, mass_density_thermal_kintherm, col=!red, psym=3 
xyouts, 2, 5e5, 'f_ek = 0.1'
;xyouts, 2, 2e0, 'Comportement aleatoire similaire = OK ?

;;;;;;;;;;;;verifier la densite de masse (cinetique)
readcol, dir+'mass_density_kinetic_checkkinetic', mass, den, in, the, cell, kin, fb, col, mass_density_kinetic_checkkinetic, f='(A,A,A,A,A,A,A,A,D)'
print, 'mass_density_kinetic_checkkinetic'
readcol, dir+'mass_density_kinetic_kineticonly', mass, den, in, the, cell, kin, fb, col, mass_density_kinetic_kineticonly, f='(A,A,A,A,A,A,A,A,D)'
print, 'mass_density_kinetic_kineticonly'          
readcol, dir+'mass_density_kinetic_kintherm', mass, den, in, the, cell, kin, fb, col, mass_density_kinetic_kintherm, f='(A,A,A,A,A,A,A,A,D)'
print, 'mass_density_kinetic_kintherm'                 

window, 17      
cgplot, mass_density_kinetic_checkkinetic, col=!green, /xlog,  xr=[1,1e6], title='Mass density (kinetic FB)'   
oplot, mass_density_kinetic_kineticonly, col=!blue             
oplot, mass_density_kinetic_kintherm, col=!red  
xyouts, 2, 2.8e5, 'f_ek = 0.1'
xyouts, 2, 5e4, 'Comportement aleatoire similaire = OK ?'

window, 19      
cgplot,mass_density_kinetic_checkkinetic, col=!green, /xlog, /ylog, xr=[1,1e6], yr=[1e0,3e5], title='Mass density (kinetic FB)', psym=3
oplot, mass_density_kinetic_kineticonly, col=!blue, psym=3             
oplot, mass_density_kinetic_kintherm, col=!red, psym=3 
xyouts, 2, 5e5, 'f_ek = 0.1'
xyouts, 2, 2e0, 'Comportement aleatoire similaire = OK ?'


stop
end


;;;verif de ESN :

;;ESN thermal = ESN tot * (1 - f_ek)
;readcol, dir+'ESN_thermal_checkthermal', nom, thth, col, ESN_thermal_checkthermal, format='(A,A,A,D)' ;code initial
;readcol, dir+'ESN_thermal_thermalonly', nom, thth, col, ESN_thermal_thermalonly, format='(A,A,A,D)'   ;code modifie, thermique seul
;readcol, dir+'ESN_thermal_kintherm', nom, thth, col, ESN_thermal_kintherm, format='(A,A,A,D)'         ;code modifie, thermique+cinetique

;window, 3
;cgplot, ESN_thermal_checkthermal, color=!green, thick=4, yr=[400,500], title='ESN Thermal Energy'
;oplot, ESN_thermal_thermalonly, color=!blue        
;oplot, ESN_thermal_kintherm, color=!red    
;xyouts, 0.2e5, 490, 'f_ek = 0.1 pour K+Th, 0 sinon'   
;al_legend, ['Thermal only', 'Kinetic + thermal', 'Check thermal', 'Check kinetic'], color=[!blue,!red,!green,!cyan], /right, line=[0,0,0,1]
;xyouts, 0.2e5, 420, 'E (K+Th) = (1 - f_ek) x E (Thermal only). OK.'

;print, minmax(ESN_thermal_checkthermal)
;print, minmax(ESN_thermal_thermalonly)  
;print, minmax(ESN_thermal_kintherm)

;;ESN kinetic = ESN tot * f_ek
;readcol, dir+'ESN_kinetic_checkkinetic', nom, thth, col, ESN_kinetic_checkkinetic, format='(A,A,A,D)' ;code initial
;readcol, dir+'ESN_kinetic_kineticonly', nom, thth, col, ESN_kinetic_kineticonly, format='(A,A,A,D)'   ;code modifie, thermique seul
;readcol, dir+'ESN_kinetic_kintherm', nom, thth, col, ESN_kinetic_kintherm, format='(A,A,A,D)'         ;code modifie, thermique+cinetique

;window, 5
;cgplot, ESN_kinetic_checkkinetic, color=!green, thick=4, yr=[400,500], title='ESN Kinetic Energy'
;oplot, ESN_kinetic_kineticonly, color=!blue        
;oplot, ESN_kinetic_kintherm, color=!red    
;xyouts, 50, 490, 'f_ek = 0.1'
;al_legend, ['Kinetic only', 'Kinetic + thermal', 'Check kinetic'], color=[!blue,!red,!green], /right, line=0, text=!black, out=!black
;xyouts, 50, 420, '1 ESN/CPU + la meme partout == pas utilise ... ??'

;print, minmax(ESN_kinetic_checkkinetic)
;print, minmax(ESN_kinetic_kineticonly)  
;print, minmax(ESN_kinetic_kintherm)
