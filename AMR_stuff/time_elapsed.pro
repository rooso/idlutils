pro time_elapsed, file=file

openr, lun, file, /get_lun

start = 0
WHILE ~ EOF(lun) DO BEGIN
line=''
readf, lun, line
if start eq 0 then time_elapsed = double(strmid(line,strpos(line,'p:')+2, strlen(line)-strpos(line,'p:')-2)) else time_elapsed = [time_elapsed,double(strmid(line,strpos(line,'p:')+2, strlen(line)-strpos(line,'p:')-2))]
if start eq 0 then start = 1
ENDWHILE

close, lun
free_lun, lun

abscissa = indgen(n_elements(time_elapsed)+1)

window, 0
plot, abscissa, time_elapsed, title=file, xtitle='Coarse time step', ytitle='Time elapsed (s)'

end
