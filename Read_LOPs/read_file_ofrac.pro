function read_file_ofrac, file, cm=cm, kpc=kpc
  
  if not keyword_set(kpc) then kpc = 1
  if not keyword_set(cm) then cm = 0
  
  if cm eq 1 then kpc = 0
  
  if strmatch(file, '*.in') eq 1 then begin
     file_in = file
     file_O = file_in
     strreplace, file_O, '.in', '_O.el'
  endif else begin
     if strmatch(file, '*_O.el') eq 1 then begin
        file_O = file
        file_in = file_O
        strreplace, file_in, '_O.el', '.in'
     endif else begin
        print, 'Format accepte : *_O.el ou *.in'
     endelse
  endelse
  
  READCOL, file_O, depth_O, OI, OII, OIII, OIV, OV, OVI, OVII, OVIII, OIX, O1, O1e, O1ee, /silent, format='(D,D,D,D,D,D,D,D,D,D,D,D,D)'
 ;units :              cm, log numbers
           
 Oneutre = OI
           
  
  r_inner = search_rinner(file_in,/silent)
  depth_O = depth_O + 10d^r_inner          ;cm
  if kpc eq 1 then depth_O *= 3.24d-22 ;kpc
  
  return, [[depth_O],[Oneutre]]
  
end
