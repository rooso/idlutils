function read_file_em, file, cm=cm, kpc=kpc, intrinsic=intrinsic, emergent=emergent
  
  if not keyword_set(kpc) then kpc = 1
  if not keyword_set(cm) then cm = 0

if keyword_set(intrinsic) then suffix = '_intrinsic'
if keyword_set(emergent)  then suffix = '_emergent'
  
  if cm eq 1 then kpc = 0
  
  if strmatch(file, '*.in') eq 1 then begin
     file_in = file
     file_em = file_in
     strreplace, file_em, '.in', '.em'+suffix
  endif else begin
     if strmatch(file, '*.em_*') eq 1 then begin
        file_em = file
        file_in = file_em
        strreplace, file_in, '.em'+suffix, '.in'
     endif else begin
        print, 'Format accepte : *.em_emergent/intrinsic ou *.in'
     endelse
  endelse
  
  readcol, file_em, depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D)'
  
  r_inner = search_rinner(file_in,/silent)
  depth_em = depth_em + 10d^r_inner          ;cm
  if kpc eq 1 then depth_em *= 3.24d-22 ;kpc
  
  return, [[depth_em],[Lya_1216A],[Lyb_1026A],[Hb_4861A],[OIII_5007A],[Ha_6563A],[NII_6584A],[OI_6300A],[SII_6731A_6716A],[NeIII_3869A],[OII_3727A_multiplet],[NeV_3426A],[SiVII_2481m],[Brg_2166m],[SiVI_1963m],[AlIX_2040m],[CaVIII_2321m],[NeVI_7652m]]
  
end
