function read_file_phyc, file, kpc=kpc, cm=cm

if not keyword_set(kpc) then kpc = 1
if not keyword_set(cm) then cm = 0

if cm eq 1 then kpc = 0

  if strmatch(file, '*.in') eq 1 then begin
     file_in = file
     file_phyc = file_in
     strreplace, file_phyc, '.in', '.phyc'
  endif else begin
     if strmatch(file, '*.phyc') eq 1 then begin
        file_phyc = file
        file_in = file_phyc
        strreplace, file_in, '.phyc', '.in'
     endif else begin
        print, 'Format accepte : *.phyc ou *.in'
     endelse
  endelse
  
  r_inner = search_rinner(file_in,/silent)
  
  
  readcol, file_phyc, depth_phyc, temperature_phyc, hden_phyc, eden_phyc, heating_phyc, rad_acc_phyc, fill_fact, format='(D,D,D,D,D,D,D)',/silent
                                ;units :                cm,           K, cm-3, cm-3, erg/cm3/s, number
  if strmatch(file, '*transp*') eq 0 then begin 
     hden_phyc = hden_phyc*fill_fact
     eden_phyc = eden_phyc*fill_fact
  endif else begin
     t_transp = where(hden_phyc - 1d-6 gt 5e-6)
     hden_phyc[t_transp] = hden_phyc[t_transp]*fill_fact[t_transp]
     eden_phyc[t_transp] = eden_phyc[t_transp]*fill_fact[t_transp]
  endelse
  
  depth_phyc = depth_phyc + 10d^r_inner   ;cm
  if kpc eq 1 then depth_phyc *= 3.24d-22 ;kpc
  
  return, [[depth_phyc], [temperature_phyc], [hden_phyc], [eden_phyc], [heating_phyc], [rad_acc_phyc], [fill_fact]]
  
end
