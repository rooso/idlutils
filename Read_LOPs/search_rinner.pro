function search_rinner, file, silent=silent, res=res

;#####################################################
;renvoie la valeur du rayon interieur cloudy en log cm
;#####################################################

if not keyword_set(silent) then silent = 1
if not keyword_set(res) then res = ''

 if strmatch(file, '*x1*') eq 1 then nm = 'x' else nm = ''
openr, fin, file, /get_lun
           line1=''
           readf, fin, line1
           line2=''
           readf, fin, line2
           line3=''
           readf, fin, line3
            ;line4=''
             ; readf, fin, line4
           if nm ne '' or res ne '' then begin
              line5=''
              readf, fin, line5
           endif
           line_radius = ''
           readf, fin, line_radius
           
;print, line1
;print, line2
;print, line3
;print, line4
; if nm ne '' then print, line5
;print, line_radius
           
           inner = strpos(line_radius, 'inner')
           outer = strpos(line_radius, 'outer')
           len = outer - inner
           radius_inner = double(strmid(line_radius,inner+5,len-5))
                            

while radius_inner eq 0 do begin
;print, 'Hein ? Probleme ?'
line_radius = ''
  readf, fin, line_radius

 inner = strpos(line_radius, 'inner')
  outer = strpos(line_radius, 'outer')
  len = outer - inner
  radius_inner = double(strmid(line_radius,inner+5,len-5))
;print, line_radius
;print, 'rayon', radius_inner

;if radius_inner ne 0 then print, 'OK'
endwhile    

         close, fin
           free_lun, fin       

   if silent eq 0 then print, 'rayon', radius_inner

           if radius_inner eq 0 then stop
return, radius_inner

end
