function read_file_hfrac, file, cm=cm, kpc=kpc
  
  if not keyword_set(kpc) then kpc = 1
  if not keyword_set(cm) then cm = 0
  
  if cm eq 1 then kpc = 0
  
  if strmatch(file, '*.in') eq 1 then begin
     file_in = file
     file_H = file_in
     strreplace, file_H, '.in', '_H.el'
  endif else begin
     if strmatch(file, '*_H.el') eq 1 then begin
        file_H = file
        file_in = file_H
        strreplace, file_in, '_H.el', '.in'
     endif else begin
        print, 'Format accepte : *_H.el ou *.in'
     endelse
  endelse
  
  READCOL, file_H, depth_H, HI, HII, H2, /silent, format='(D,D,D,D)'
  Hneutre = HI + H2
  
  r_inner = search_rinner(file_in,/silent)
  depth_H = depth_H + 10d^r_inner          ;cm
  if kpc eq 1 then depth_H *= 3.24d-22 ;kpc
  
  return, [[depth_H],[Hneutre]]
  
end
