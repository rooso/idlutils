function read_file_ascii, file, kpc=kpc, cm=cm, boxlen=boxlen, lmax=lmax

if not keyword_set(lmax) then lmax = 13
if not keyword_set(boxlen) then boxlen = 50. ;kpc

if not keyword_set(kpc) then kpc = 1
if not keyword_set(cm) then cm = 0

if cm eq 1 then kpc = 0

if strmatch(file, '*.in') eq 1 then begin
   file_in = file
   file_ascii = file_in
   strreplace, file_ascii, '.in', '.ascii'
endif else begin
   if strmatch(file, '*.ascii') eq 1 then begin
      file_ascii = file
   endif else begin
      print, 'Format accepte : *.ascii ou *.in'
   endelse
endelse

if strmatch(file_ascii, '*x10_*') eq 1 then strreplace, file_ascii, 'x10_', ''
if strmatch(file_ascii, '*x100_*') eq 1 then strreplace, file_ascii, 'x100_', ''


readcol, file_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, /silent, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D)'

t_sort = sort(depth_ascii)
sorted_depth = depth_ascii[t_sort]
sorted_density = density_ascii[t_sort]
sorted_x_ascii = x_ascii[t_sort]
sorted_y_ascii = y_ascii[t_sort]
sorted_z_ascii = z_ascii[t_sort]
sorted_temperature = temperature_ascii[t_sort]
sorted_vx_ascii = vx_ascii[t_sort]
sorted_vy_ascii = vy_ascii[t_sort]
sorted_vz_ascii = vz_ascii[t_sort]
sorted_cell_size = cell_size_ascii[t_sort]
x_lops = x_lop_ascii[t_sort]
y_lops = y_lop_ascii[t_sort]
z_lops = z_lop_ascii[t_sort]
theta_lops = theta_lop_ascii[t_sort]
phi_lops = phi_lop_ascii[t_sort]

count=0
for i = 0, (size(sorted_depth))[1]-1 do begin
   for j = 0, (size(sorted_depth))[1]-1 do begin   
      if (sorted_depth(i) eq sorted_depth(j) && j ne i) then begin
         count++
         print, sorted_depth(i), sorted_density(i), sorted_density(j)
      endif
   endfor
endfor

if count gt 0 then begin
   print, 'Il y a ', count, ' problemes'
   stop
endif

if cm eq 1 then sorted_depth /= 3.24d-22

;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(boxlen*1d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(sorted_density ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*sorted_density[t_rho]
     t_temp = where(sorted_temperature[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then sorted_temperature[t_rho(t_temp)] = 900
     ;print, nt_temp
  endif

return, [[sorted_depth],[sorted_density],[sorted_x_ascii],[sorted_y_ascii],[sorted_z_ascii],[sorted_temperature],[sorted_vx_ascii],[sorted_vy_ascii],[sorted_vz_ascii],[sorted_cell_size],[x_lops],[y_lops],[z_lops],[theta_lops],[phi_lops]] 

end
