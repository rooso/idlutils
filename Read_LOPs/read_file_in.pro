function read_file_in, file, log=log, cm=cm, kpc=kpc, silent=silent
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;    cette fonction lit les fichiers *.in (scripts CLOUDY)    ;;;
;;;    et retourne la profondeur et la densite du profil        ;;; 
;;;                    (res(*,0) et res(*,1))                   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if not keyword_set(silent) then silent = 1
  if not keyword_set(log) then log = 0
  if not keyword_set(cm) then cm = 0
  if not keyword_set(kpc) then kpc = 1 ;par defaut, sortir des kpc en echelle lineaire
  if cm eq 1 then kpc = 0
  
  readcol, file, col_continue, depth_den, density, format='(A8,D,D)', /silent
                                ;units : none, log cm, log H/cc
  cmp = strcmp(col_continue, 'continue')
  t = where( cmp eq 1 and depth_den gt -35, nt)
  col_continue = col_continue[t]
  depth_den = depth_den[t]
  density = density[t]
  if kpc eq 1 then depth_den += alog10(3.24e-22)                            ;log kpc
  if strmatch(file, '*transp*') eq 0 then begin 
     density += alog10(1./5.)   ;correction du filling_factor
  endif else begin
     t_transp = where(density ne -6)
     density[t_transp] += alog10(1./5.)
  endelse
  if silent eq 0 then print, minmax(density)
  
  if log eq 0 then begin
     depth_den = 10d^depth_den
     density = 10d^density
  endif
  
;print, [[depth_den],[density]]
  return, [[depth_den],[density]]
  
end
