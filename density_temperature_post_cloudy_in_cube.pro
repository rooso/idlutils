pro density_temperature_post_cloudy_in_cube, center=center, show_sf=show_sf, only_sf=only_sf, show_den=show_den, sfr_threshold=sfr_threshold, factor=factor, ps=ps, sim=sim, boxlen=boxlen, lmax=lmax, n_criterium=n_criterium
;print, only_sf
;stop  


;;;;;; demande du referee : HR = heating rate = pas bien. --->
;;;;;; relative temperature change (RTC)


  if not keyword_Set(sim) then sim = '00100'                     ;'00075w'/'00100'/'00108'/'00150'/'00170'/'00210'
  ps = keyword_Set(ps)                                           ;0/1
  if not keyword_set(sfr_threshold) then sfr_threshold = 'sfr10' ;sfr100/sfr10/sfr1/sfr0
  if not keyword_set(n_criterium) then n_criterium = 0           ; n=0 : all / n=4 / n=1 
  if not keyword_Set(lmax) then lmax = 13
  if not keyword_Set(boxlen) then boxlen = 50 ;kpc
  if not keyword_set(center) then center = 0
  
  if keyword_set(factor) then begin ;''/'10'/'100'
     if factor ne '' then begin
        nm = 'x'+factor+'_'
        L_AGN = factor+' x L'+textoidl('_{AGN}')
     endif
  endif else begin
     nm = ''
     factor = ''
     L_AGN = 'L'+textoidl('_{AGN}')
  endelse
  
;print, factor, nm
  entered_factor = factor
  entered_nm = nm
  
  if n_criterium eq 0 then nm_n = 'N=all_'
  if n_criterium eq 1 then nm_n = 'N=1_'
  if n_criterium eq 4 then nm_n = 'N=4_'
  
  if center eq 1 then nm_center = 'center_' else nm_center = ''
  
  if ps eq 1 then begin
     !p.font=0
     !p.charsize=4
     !p.charthick=2
     !p.thick=4
     !x.thick=20
     !y.thick=20
  endif 
  
  entered_ps = ps
  
  sim_with = ['00075w', '00100', '00108', '00150', '00170', '00210']
  sim_no   = ['00075n', '00085', '00090', '00120', '00130', '00148']
  
  for i = 0,  n_elements(sim_with)-1 do if strmatch(sim, sim_with(i)) eq 1 then break
  
  print, 'Restauration du fichier AMR '+sim_with(i)
;readcol, '/Users/oroos/Post-stage/orianne_data/'+fb_case+'_AGN_feedback/gas_part_'+sim+'.ascii.lmax'+lmax, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr 
;save, /variables, file='amr_'+sim+'_all.sav'
  restore, '/Users/oroos/Post-stage/LOPs'+sim_with(i)+'/amr_'+sim_with(i)+'_all.sav'
  print, 'Fin de la restauration du fichier AMR '+sim_with(i)
  
  
  factor = entered_factor
  nm = entered_nm
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(boxlen*1d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_amr ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_amr[t_rho]
     t_temp = where(temperature_amr[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_amr[t_rho(t_temp)] = 900
  endif
  
  
;/!\/!\/!\/!\/!\/!\ SEULE LA TEMPERATURE (ET DONC LE SFR,
;calcul'e ensuite) A ETE CORRIGEE POUR LE POLYTROPE DE JEANS
;L'IONISATION N'EST PAS CORRIGEE ! mais pas besoin de corriger
;l'ionisation puisque cloudy ne se base pas sur l temperature.............
  
  print, 'Restauration du fichier POST-CLOUDY '+nm+sim_with(i)
  restore, '/Volumes/TRANSCEND/'+nm+'cloudy_near_'+sim_with(i)+'_all.sav'
;variables :
; x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A5,A35)'
  print, 'Fin de la restauration du fichier POST-CLOUDY '+nm+sim_with(i)
  
  factor = entered_factor
  nm = entered_nm
  
;print, factor, nm
;stop
  
  if sfr_threshold eq 'sfr100' then begin
     SFR_amr_with_all = SFR100_amr
     SFR_hybrid_all = SFR100_hybrid
     SFR_cloudy_all = SFR100_cloudy
  endif
  if sfr_threshold eq 'sfr10' then begin
     SFR_amr_with_all = SFR10_amr
     SFR_hybrid_all = SFR10_hybrid
     SFR_cloudy_all = SFR10_cloudy
  endif
  if sfr_threshold eq 'sfr1' then begin
     SFR_amr_with_all = SFR1_amr
     SFR_hybrid_all = SFR1_hybrid
     SFR_cloudy_all = SFR1_cloudy
  endif
  if sfr_threshold eq 'sfr0' then begin
     SFR_amr_with_all = SFR0_amr
     SFR_hybrid_all = SFR0_hybrid
     SFR_cloudy_all = SFR0_cloudy
  endif
  in_cell_all = in_cell
  
  density_amr_with_all = density_amr
  temperature_amr_with_all = temperature_amr
  
  hcc_to_kgm3=1.67d-21
  kpc_to_m=1./3.24d-20
  scale_mass = hcc_to_kgm3*kpc_to_m^3/2.0d30
  
  mass = density_amr_with_all*cell_size_amr^3*scale_mass ;M_sun
  print, 'M_tot=', total(mass)  
  
  
;print, 'Restauration du fichier AMR sans FB '+sim_no(i)
;;readcol, '/Users/oroos/Post-stage/LOPs'+sim_no(i)+'/initial_individual_sfr_'+sim_no(i)+'_all.dat', SFR100_amr_no, SFR10_amr_no, SFR1_amr_no, SFR0_amr_no, format='(D,D,D,D)'
;;save,  SFR100_amr_no, SFR10_amr_no, SFR1_amr_no, SFR0_amr_no, file='/Users/oroos/Post-stage/LOPs'+sim_no(i)+'/initial_individual_sfr_'+sim_no(i)+'_all_dat.sav'
;restore, '/Users/oroos/Post-stage/LOPs'+sim_no(i)+'/initial_individual_sfr_'+sim_no(i)+'_all_dat.sav'
;if sfr_threshold eq 'sfr100' then SFR_amr_no_all = SFR100_amr_no
;if sfr_threshold eq 'sfr10' then SFR_amr_no_all = SFR10_amr_no
;if sfr_threshold eq 'sfr1' then SFR_amr_no_all = SFR1_amr_no
;if sfr_threshold eq 'sfr0' then SFR_amr_no_all = SFR0_amr_no
;print, 'Fin de la restauration du fichier AMR sans FB '+sim_no(i)
  
;23 millions de points c'est trop de toute facon donc on ne s'embete
;pas a corriger par rapport aux 'Point cloudy le plus proche
;dans 1 ou 4 x la taille de la cellule' et on n'affiche que ces
;points-la
  
  ps = entered_ps
  
  
  t_4 = where(in_cell_all ge 1, nt_4)
  t_1 = where(in_cell_all eq 1, nt_1)
  
  if n_criterium eq 1 then t_N = t_1
  if n_criterium eq 4 then t_N = t_4
  if n_criterium eq 0 then t_N = where(in_cell_all ge 0) ;tout 
  taille = n_elements(density_lop[t_N])
  
  if center eq 1 then begin
     
     if sim eq '00100' or sim eq '00075w' $
        or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
     if sim eq '00085' or sim eq '00075n' $
        or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then name = 'No'
     
     sm = sim
     
     if sim eq '00075w' or sim eq '00075n' then sm='00075'
     
     readcol, "/Users/oroos/Post-stage/orianne_data/"+name+"_AGN_feedback/output_"+sm+"/sink_"+sm+".out", rien, rien2, x_c, y_c, z_c, /silent
     
     thickness = 4d             ;kpc
     rad_center = 2d
     
     t_disk = where(abs(z_amr[t_N] - replicate(z_c,taille)) le thickness/2. and sqrt((y_amr[t_N]-replicate(y_c,taille))^2+(x_amr[t_N]-replicate(x_c,taille))^2) le rad_center, nt_disk) ;kpc
     
     taille = n_elements(t_disk)
     
     if nt_disk eq 0 then stop
  endif else begin
     t_disk =  where(in_cell_all[t_N] ge 0) ;si on ne restreint pas au disque, on fait un indice qui prend tout
  endelse
  
  param1 = Hneutre_lop          ;/!\ non corrigee pour le polytrope de jeans
  rg1=[-13,0]
  cols1 = find_colors(param1[t_N(t_disk)], rg1(0), rg1(1)) ;cette fonction prend le log et cherche les couleurs dans la gamme donnee
  
  param2 = abs(temperature_post-temperature_amr_with_all)/temperature_amr_with_all ;corrigee pour le polytrope de jeans
  rg2=[-2,7.5]
  cols2 = find_colors(param2[t_N(t_disk)], rg2(0), rg2(1)) 
  
  sort_cols = 1
  
  if sort_cols eq 1 then begin
     t_sort_cols1=sort(cols1)   ;NB : LES COULEURS SONT TRIEES ! IL Y A LES AUTRES DESSOUS (trop de points, sinon on ne voit rien)
     cols1=reverse(cols1[t_sort_cols1])
     param1=reverse(param1[t_N(t_disk(t_sort_cols1))])
     density_amr_with_all1 = reverse(density_amr_with_all[t_N(t_disk(t_sort_cols1))])
     temperature_amr_with_all1 = reverse(temperature_amr_with_all[t_N(t_disk(t_sort_cols1))])
     density_lop1 = reverse(density_lop[t_N(t_disk(t_sort_cols1))])
     temperature_post1 = reverse(temperature_post[t_N(t_disk(t_sort_cols1))])
     mass1 = reverse(mass[t_N(t_disk(t_sort_cols1))])
     total_mass1 = total(mass1)
     
     t_sort_cols2=sort(cols2)
     cols2=(cols2[t_sort_cols2])
     param2=(param2[t_N(t_disk(t_sort_cols2))])
     density_amr_with_all2 = (density_amr_with_all[t_N(t_disk(t_sort_cols2))])
     temperature_amr_with_all2 = (temperature_amr_with_all[t_N(t_disk(t_sort_cols2))])
     density_lop2 = (density_lop[t_N(t_disk(t_sort_cols2))])
     temperature_post2 = (temperature_post[t_N(t_disk(t_sort_cols2))])
     mass2 = (mass[t_N(t_disk(t_sort_cols2))])
     total_mass2 = total(mass2)
  endif else begin
     param1=reverse(param1[t_N(t_disk)])
     density_amr_with_all1 = reverse(density_amr_with_all[t_N(t_disk)])
     temperature_amr_with_all1 = reverse(temperature_amr_with_all[t_N(t_disk)])
     density_lop1 = reverse(density_lop[t_N(t_disk)])
     temperature_post1 = reverse(temperature_post[t_N(t_disk)])
     mass1 = reverse(mass[t_N(t_disk)])
     total_mass1 = total(mass1)
     
     param2=(param2[t_N(t_disk)])
     density_amr_with_all2 = (density_amr_with_all[t_N(t_disk)])
     temperature_amr_with_all2 = (temperature_amr_with_all[t_N(t_disk)])
     density_lop2 = (density_lop[t_N(t_disk)])
     temperature_post2 = (temperature_post[t_N(t_disk)])
     mass2 = (mass[t_N(t_disk)])
     total_mass2 = total(mass2)
  endelse
  
  print, 'Mass of the subsample : ', total_mass1 ;, total_mass2
  
  t_H_13 = where(param1 le 1d-13, nt_H_13)
  t_H_13_10 = where(param1 gt 1d-13 and param1 le 1d-10, nt_H_13_10)
  t_H_10_5 = where(param1 gt 1d-10 and param1 le 1d-5, nt_H_10_5)
  t_H_5_4 = where(param1 gt 1d-5 and param1 le 1d-4, nt_H_5_4)
  t_H_4_3 = where(param1 gt 1d-4 and param1 le 1d-3, nt_H_4_3)
  t_H_3_2 = where(param1 gt 1d-3 and param1 le 1d-2, nt_H_3_2)
  t_H_2_1 = where(param1 gt 1d-2 and param1 le 1d-1, nt_H_2_1)
  t_H_1_0 = where(param1 gt 1d-1 and param1 le 1d0, nt_H_1_0)
  t_H_0 = where(param1 gt 1d0, nt_H_0)
  print, 'log Hneutre :'
  print, '               -13,       -10,         -5,         -4,        -3,         -2,         -1,         0'
  print, nt_H_13, nt_H_13_10, nt_H_10_5, nt_H_5_4, nt_H_4_3, nt_H_3_2, nt_H_2_1, nt_H_1_0, nt_H_0
  print, 1.*nt_H_13/n_elements(param1), 1.*nt_H_13_10/n_elements(param1), 1.*nt_H_10_5/n_elements(param1), 1.*nt_H_5_4/n_elements(param1), 1.*nt_H_4_3/n_elements(param1), 1.*nt_H_3_2/n_elements(param1), 1.*nt_H_2_1/n_elements(param1), 1.*nt_H_1_0/n_elements(param1), 1.*nt_H_0/n_elements(param1)
  
  print, total(mass1[t_H_13])*nt_H_13/nt_H_13, total(mass1[t_H_13_10])*nt_H_13_10/nt_H_13_10, total(mass1[t_H_10_5])*nt_H_10_5/nt_H_10_5, total(mass1[t_H_5_4])*nt_H_5_4/nt_H_5_4, total(mass1[t_H_4_3])*nt_H_4_3/nt_H_4_3, total(mass1[t_H_3_2])*nt_H_3_2/nt_H_3_2, total(mass1[t_H_2_1])*nt_H_2_1/nt_H_2_1, total(mass1[t_H_1_0])*nt_H_1_0/nt_H_1_0, total(mass1[t_H_0])*nt_H_0/nt_H_0 ;*nt/nt pour eviter les erreurs quand n=0 (t=-1 : dernier element du vecteur)
  print, total(mass1[t_H_13])/total_mass1*nt_H_13/nt_H_13, total(mass1[t_H_13_10])/total_mass1*nt_H_13_10/nt_H_13_10, total(mass1[t_H_10_5])/total_mass1*nt_H_10_5/nt_H_10_5, total(mass1[t_H_5_4])/total_mass1*nt_H_5_4/nt_H_5_4, total(mass1[t_H_4_3])/total_mass1*nt_H_4_3/nt_H_4_3, total(mass1[t_H_3_2])/total_mass1*nt_H_3_2/nt_H_3_2, total(mass1[t_H_2_1])/total_mass1*nt_H_2_1/nt_H_2_1, total(mass1[t_H_1_0])/total_mass1*nt_H_1_0/nt_H_1_0, total(mass1[t_H_0])/total_mass1*nt_H_0/nt_H_0
  
  
  t_HR_2 = where(param2 le 1d-2, nt_HR_2)
  t_HR_2_1 = where(param2 le 1d-1 and param2 gt 1d-2, nt_HR_2_1)
  t_HR_1_0 = where(param2 le 1d0 and param2 gt 1d-1, nt_HR_1_0)
  t_HR_0_p1 = where(param2 le 1d1 and param2 gt 1d0, nt_HR_0_p1)
  t_HR_p1_p2 = where(param2 le 1d2 and param2 gt 1d1, nt_HR_p1_p2)
  t_HR_p2_p3 = where(param2 le 1d3 and param2 gt 1d2, nt_HR_p2_p3)
  t_HR_p3_p4 = where(param2 le 1d4 and param2 gt 1d3, nt_HR_p3_p4)
  t_HR_p4_p5 = where(param2 le 1d5 and param2 gt 1d4, nt_HR_p4_p5)
  t_HR_p5_p6 = where(param2 le 1d6 and param2 gt 1d5, nt_HR_p5_p6)
  t_HR_p6 = where(param2 gt 1d6, nt_HR_p6)
  print, 'log Heating Rate :'
  print, '              -2,        -1,          0,          1,          2,          3,          4,          5,          6'   
  print, nt_HR_2, nt_HR_2_1, nt_HR_1_0, nt_HR_0_p1, nt_HR_p1_p2, nt_HR_p2_p3, nt_HR_p3_p4, nt_HR_p4_p5, nt_HR_p5_p6, nt_HR_p6
  print, 1.*nt_HR_2/n_elements(param2), 1.*nt_HR_2_1/n_elements(param2), 1.*nt_HR_1_0/n_elements(param2), 1.*nt_HR_0_p1/n_elements(param2), 1.*nt_HR_p1_p2/n_elements(param2), 1.*nt_HR_p2_p3/n_elements(param2), 1.*nt_HR_p3_p4/n_elements(param2), 1.*nt_HR_p4_p5/n_elements(param2), 1.*nt_HR_p5_p6/n_elements(param2), 1.*nt_HR_p6/n_elements(param2)
  
  print, total(mass2[t_HR_2])*nt_HR_2/nt_HR_2, total(mass2[t_HR_2_1])*nt_HR_2_1/nt_HR_2_1, total(mass2[t_HR_1_0])*nt_HR_1_0/nt_HR_1_0, total(mass2[t_HR_0_p1])*nt_HR_0_p1/nt_HR_0_p1, total(mass2[t_HR_p1_p2])*nt_HR_p1_p2/nt_HR_p1_p2, total(mass2[t_HR_p2_p3])*nt_HR_p2_p3/nt_HR_p2_p3, total(mass2[t_HR_p3_p4])*nt_HR_p3_p4/nt_HR_p3_p4, total(mass2[t_HR_p4_p5])*nt_HR_p4_p5/nt_HR_p4_p5, total(mass2[t_HR_p5_p6])*nt_HR_p5_p6/nt_HR_p5_p6, total(mass2[t_HR_p6])*nt_HR_p6/nt_HR_p6
  
  
  print, total(mass2[t_HR_2])/total_mass2*nt_HR_2/nt_HR_2, total(mass2[t_HR_2_1])/total_mass2*nt_HR_2_1/nt_HR_2_1, total(mass2[t_HR_1_0])/total_mass2*nt_HR_1_0/nt_HR_1_0, total(mass2[t_HR_0_p1])/total_mass2*nt_HR_0_p1/nt_HR_0_p1, total(mass2[t_HR_p1_p2])/total_mass2*nt_HR_p1_p2/nt_HR_p1_p2, total(mass2[t_HR_p2_p3])/total_mass2*nt_HR_p2_p3/nt_HR_p2_p3, total(mass2[t_HR_p3_p4])/total_mass2*nt_HR_p3_p4/nt_HR_p3_p4, total(mass2[t_HR_p4_p5])/total_mass2*nt_HR_p4_p5/nt_HR_p4_p5, total(mass2[t_HR_p5_p6])/total_mass2*nt_HR_p5_p6/nt_HR_p5_p6, total(mass2[t_HR_p6])/total_mass2*nt_HR_p6/nt_HR_p6
  
;construction du diagramme densite-temperature
  !y.margin=[4,4]
     lim_temp=1d4
     if sfr_threshold eq 'sfr100' then lim_den=100
     if sfr_threshold eq 'sfr10' then lim_den=10
     if sfr_threshold eq 'sfr1' then lim_den=1
     if sfr_threshold eq 'sfr0' then lim_den=1d-10
     
     if ps eq 0 then ps_thick=1 else ps_thick=4
  yes = 0
  if yes eq 1 then begin
     if ps eq 0 then window, 1, xsize=1000, ysize=300
     if ps eq 1 then ps_start, nm+'den_tempamr_diagram_'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=30, ysize=12, /helvetica, /bold
     
     if ps eq 0 then symb = 3 else symb=sym(1)
     device, /decompose 
     !p.multi=[0,2,1]
     cgloadct, 39, /silent, /reverse
     plot, [1e-7,1e6], [1e1,1e10], /nodata, xtitle='Density [cm'+textoidl('^{-3}')+']', ytitle='Initial Temperature [K]', /ylog, /xlog, /xstyle ,/ystyle, /isotropic
     device, decompose=0
     for iiii = 0, taille-1 do plots, density_amr_with_all1(iiii), temperature_amr_with_all1(iiii), color=cols1(iiii), psym=symb, thick=1,syms=0.25
     device, /decompose
     oplot, [lim_den,lim_den],[1e0,lim_temp], color=!red, thick=4*ps_thick, line=0
     oplot, [lim_den,1d6], [lim_temp, lim_temp], color=!red, thick=4*ps_thick, line=0
     cgColorbar, range=rg1, ncol=255, color=!p.color, /fit, tloc='bottom', title='log Neutral fraction of hydrogen'
     
     cgloadct, 39, /silent
     plot, [1e-7,1e6], [1e1,1e10], /nodata, xtitle='Density [cm'+textoidl('^{-3}')+']', ytitle='Initial Temperature [K]', /ylog, /xlog, /xstyle ,/ystyle, /isotropic
     device, decompose=0
     for iiii = 0, taille-1 do plots, density_amr_with_all2(iiii), temperature_amr_with_all2(iiii), color=cols2(iiii), psym=symb, thick=1,syms=0.25
     device, /decompose
     oplot, [lim_den,lim_den],[1e0,lim_temp], color=!red, thick=4*ps_thick, line=0
     oplot, [lim_den,1d6], [lim_temp, lim_temp], color=!red, thick=4*ps_thick, line=0
     cgColorbar, range=rg2, ncol=255, color=!p.color, /fit, tloc='bottom', title='log Heating rate'
     
     if ps eq 1 then print, 'Conversion en png...'
     if ps eq 1 then ps_end, /png
  endif
;---------------------------------------------------------------------------------------------------
  ok = 0
  if ok eq 1 then begin
     if ps eq 0 then window, 3, xsize=1000, ysize=300
     if ps eq 1 then ps_start, nm+'den_temppost_diagram_'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=30, ysize=12, /helvetica, /bold
     
     device, /decompose
     !p.multi=[0,2,1]
     cgloadct, 39, /silent, /reverse
     plot, [1e-7,1e6], [1e1,1e10], /nodata, xtitle='Density [cm'+textoidl('^{-3}')+']', ytitle='Final temperature [K]', /ylog, /xlog, /xstyle ,/ystyle, /isotropic
     device, decompose=0
     for iiii = 0, taille-1 do plots, density_lop1(iiii), temperature_post1(iiii), color=cols1(iiii), psym=symb, thick=1,syms=0.25
     device, /decompose
     oplot, [lim_den,lim_den],[1e0,lim_temp], color=!red, thick=4*ps_thick, line=0
     oplot, [lim_den,1d6], [lim_temp, lim_temp], color=!red, thick=4*ps_thick, line=0
     cgColorbar, range=rg1, ncol=255, color=!p.color, /fit, tloc='bottom', title='log Neutral fraction of hydrogen'
     
     cgloadct, 39, /silent
     plot, [1e-7,1e6], [1e1,1e10], /nodata, xtitle='Density [cm'+textoidl('^{-3}')+']', ytitle='Final temperature [K]', /ylog, /xlog, /xstyle ,/ystyle, /isotropic
     device, decompose=0
     for iiii = 0, taille-1 do plots, density_lop2(iiii), temperature_post2(iiii), color=cols2(iiii), psym=symb, thick=1,syms=0.25
     device, /decompose
     oplot, [lim_den,lim_den],[1e0,lim_temp], color=!red, thick=4*ps_thick, line=0
     oplot, [lim_den,1d6], [lim_temp, lim_temp], color=!red, thick=4*ps_thick, line=0
     cgColorbar, range=rg2, ncol=255, color=!p.color, /fit, tloc='bottom', title='log Heating rate'
     !y.margin=[2,2]
     
     if ps eq 1 then print, 'Conversion en png...'
     if ps eq 1 then ps_end, /png
  endif
  
  bins2d = [0.3d0,0.3d0,0.3d0]
  mx=[3.5d0,10d0,6d0]
  mn=[-2d0,1d0,-7d0]
  


  histo_HR_T=hist_nd(transpose([[alog10(param2)],[alog10(temperature_amr_with_all2)]]),bins2d[0:1], min=[mn(0),mn(1)], max=[mx(0),mx(1)], rev=ri_T)
  histo_HR_den=hist_nd(transpose([[alog10(param2)],[alog10(density_amr_with_all2)]]),bins2d[0:2:2], min=[mn(0),mn(2)], max=[mx(0),mx(2)], rev=ri_den)
  
  offset = 0
  nbin   = long((mx-mn)/bins2d+1)
  
  HR_bins2d = mn(0) + (LINDGEN(nbin(0))+offset)*bins2d(0)
  T_bins2d = mn(1) + (LINDGEN(nbin(1))+offset)*bins2d(1)
  den_bins2d = mn(2) + (LINDGEN(nbin(2))+offset)*bins2d(2)
  
  
;pour chaque histo, on veut la somme des masses des cellules de chaque
;bin.
  mass_histo_HR_T = histo_HR_T*0d0
  mass_histo_HR_den = histo_HR_den*0d0
  
  t = where(alog10(temperature_amr_with_all2) lt T_bins2d(0) and alog10(param2) lt HR_bins2d(0),nt)
  if nt gt 0 then mass_histo_HR_T(0,0) += total(mass2[t])
  t = where(alog10(density_amr_with_all2) lt den_bins2d(0) and alog10(param2) lt HR_bins2d(0),nt)
  if nt gt 0 then mass_histo_HR_den(0,0) += total(mass2[t])
  
  
  for iii_loop=0,n_elements(HR_bins2d)-2 do begin
     for j=0,n_elements(T_bins2d)-2 do begin
        
        if iii_loop eq 0 and j ne 0 then begin
           t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) lt HR_bins2d(0),nt)
           if nt gt 0 then mass_histo_HR_T(0,j) += total(mass2[t])
        endif
        if j eq 0 and iii_loop ne 0 then begin
           t = where(alog10(temperature_amr_with_all2) lt T_bins2d(0) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_T(iii_loop,j) += total(mass2[t])
        endif
        
        t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
        if nt gt 0 then mass_histo_HR_T(iii_loop,j) += total(mass2[t])
        
        if iii_loop eq n_elements(HR_bins2d)-2 and j ne n_elements(T_bins2d)-2 then begin
           t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_T(iii_loop,j) += total(mass2[t])
        endif
        
        if j eq n_elements(T_bins2d)-2 and iii_loop ne n_elements(HR_bins2d)-2 then begin
           t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_T(iii_loop,j) += total(mass2[t])
        endif
     endfor
     
     for j=0,n_elements(den_bins2d)-2 do begin
        if iii_loop eq 0 and j ne 0 then begin
           t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) lt HR_bins2d(0),nt)
           if nt gt 0 then mass_histo_HR_den(0,j) += total(mass2[t])
        endif
        if j eq 0 and iii_loop ne 0 then begin
           t = where(alog10(density_amr_with_all2) lt den_bins2d(0) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_den(iii_loop,j) += total(mass2[t])
        endif
        
        t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
        if nt gt 0 then mass_histo_HR_den(iii_loop,j) += total(mass2[t])
        
        if iii_loop eq n_elements(HR_bins2d)-2 and j ne n_elements(den_bins2d)-2 then begin
           t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_den(iii_loop,j) += total(mass2[t])
        endif
        
        if j eq n_elements(den_bins2d)-2 and iii_loop ne n_elements(HR_bins2d)-2 then begin
           t = where(alog10(density_amr_with_all2) ge den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_histo_HR_den(iii_loop,j) += total(mass2[t])
        endif
     endfor
  endfor    
  
  t = where(alog10(temperature_amr_with_all2) ge T_bins2d(-1) and alog10(param2) ge HR_bins2d(-1),nt)
  if nt gt 0 then mass_histo_HR_T(-1,-1) += total(mass2[t])
  t = where(alog10(density_amr_with_all2) ge den_bins2d(-1) and alog10(param2) ge HR_bins2d(-1),nt)
  if nt gt 0 then mass_histo_HR_den(-1,-1) += total(mass2[t])
  
  
  log_histo_HR_T=alog10(histo_HR_T)
  log_histo_HR_den=alog10(histo_HR_den)
  
  log_mass_histo_HR_T=alog10(mass_histo_HR_T)
  log_mass_histo_HR_den=alog10(mass_histo_HR_den)
  
  print, minmax(log_mass_histo_HR_T[where(finite(log_mass_histo_HR_T) eq 1)])
  print, minmax(log_mass_histo_HR_den[where(finite(log_mass_histo_HR_den) eq 1)])
  
  mx_mass = float(round(max([log_mass_histo_HR_T[where(finite(log_mass_histo_HR_T) eq 1)],log_mass_histo_HR_den[where(finite(log_mass_histo_HR_den) eq 1)]])*10)/10.)+0.1
  mn_mass = float(round(min([log_mass_histo_HR_T[where(finite(log_mass_histo_HR_T) eq 1)],log_mass_histo_HR_den[where(finite(log_mass_histo_HR_den) eq 1)]])*10)/10.)-0.1

;print, mx_mass, mn_mass
;help, log_mass_histo_HR_T, mass_histo_HR_T
;print, total(mass_histo_HR_T)
;print, minmax(mass_histo_HR_T)
;help, temperature_amr_with_all2, density_amr_with_all2
;help, mass2
;stop
  
sfr_thr = fix(strmid(sfr_threshold,3,strlen(sfr_threshold)))

  t_sf = where(temperature_amr_with_all2 le 1d4 and density_amr_with_all2 ge sfr_thr, nt_sf)
  if nt_sf gt 0 then begin
     
     param2 = param2[t_sf]
     temperature_amr_with_all2 = temperature_amr_with_all2[t_sf]
     density_amr_with_all2 = density_amr_with_all2[t_sf]
     mass2 = mass2[t_sf]
     
     sf_histo_HR_T=hist_nd(transpose([[alog10(param2)],[alog10(temperature_amr_with_all2)]]),bins2d[0:1], min=[-5,2], max=[mx(0),mx(1)], rev=ri_sf_T)
     sf_histo_HR_den=hist_nd(transpose([[alog10(param2)],[alog10(density_amr_with_all2)]]),bins2d[0:2:2], min=[-5,-7], max=[mx(0),mx(2)], rev=ri_sf_den)
     
     mass_sf_histo_HR_T = sf_histo_HR_T*0d0
     mass_sf_histo_HR_den = sf_histo_HR_den*0d0
     
;;;
     t = where(alog10(temperature_amr_with_all2) lt T_bins2d(0) and alog10(param2) lt HR_bins2d(0),nt)
     if nt gt 0 then mass_sf_histo_HR_T(0,0) += total(mass2[t])
     t = where(alog10(density_amr_with_all2) lt den_bins2d(0) and alog10(param2) lt HR_bins2d(0),nt)
     if nt gt 0 then mass_sf_histo_HR_den(0,0) += total(mass2[t])
     
     
     for iii_loop=0,n_elements(HR_bins2d)-2 do begin
        for j=0,n_elements(T_bins2d)-2 do begin
           
           if iii_loop eq 0 and j ne 0 then begin
              t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) lt HR_bins2d(0),nt)
              if nt gt 0 then mass_sf_histo_HR_T(0,j) += total(mass2[t])
           endif
           if j eq 0 and iii_loop ne 0 then begin
              t = where(alog10(temperature_amr_with_all2) lt T_bins2d(0) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_T(iii_loop,j) += total(mass2[t])
           endif
           
           t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_sf_histo_HR_T(iii_loop,j) += total(mass2[t])
           
           if iii_loop eq n_elements(HR_bins2d)-2 and j ne n_elements(T_bins2d)-2 then begin
              t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j) and  alog10(temperature_amr_with_all2) lt T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_T(iii_loop,j) += total(mass2[t])
           endif
           
           if j eq n_elements(T_bins2d)-2 and iii_loop ne n_elements(HR_bins2d)-2 then begin
              t = where(alog10(temperature_amr_with_all2) ge T_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_T(iii_loop,j) += total(mass2[t])
           endif
        endfor
        
        for j=0,n_elements(den_bins2d)-2 do begin
           if iii_loop eq 0 and j ne 0 then begin
              t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) lt HR_bins2d(0),nt)
              if nt gt 0 then mass_sf_histo_HR_den(0,j) += total(mass2[t])
           endif
           if j eq 0 and iii_loop ne 0 then begin
              t = where(alog10(density_amr_with_all2) lt den_bins2d(0) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_den(iii_loop,j) += total(mass2[t])
           endif
           
           t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
           if nt gt 0 then mass_sf_histo_HR_den(iii_loop,j) += total(mass2[t])
           
           if iii_loop eq n_elements(HR_bins2d)-2 and j ne n_elements(den_bins2d)-2 then begin
              t = where(alog10(density_amr_with_all2) ge den_bins2d(j) and  alog10(density_amr_with_all2) lt den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_den(iii_loop,j) += total(mass2[t])
           endif
           
           if j eq n_elements(den_bins2d)-2 and iii_loop ne n_elements(HR_bins2d)-2 then begin
              t = where(alog10(density_amr_with_all2) ge den_bins2d(j+1) and alog10(param2) ge HR_bins2d(iii_loop) and alog10(param2) lt HR_bins2d(iii_loop+1),nt)
              if nt gt 0 then mass_sf_histo_HR_den(iii_loop,j) += total(mass2[t])
           endif
        endfor
     endfor    
     
     t = where(alog10(temperature_amr_with_all2) ge T_bins2d(-1) and alog10(param2) ge HR_bins2d(-1),nt)
     if nt gt 0 then mass_sf_histo_HR_T(-1,-1) += total(mass2[t])
     t = where(alog10(density_amr_with_all2) ge den_bins2d(-1) and alog10(param2) ge HR_bins2d(-1),nt)
     if nt gt 0 then mass_sf_histo_HR_den(-1,-1) += total(mass2[t])
;;;
     
     log_sf_histo_HR_T=alog10(sf_histo_HR_T)
     log_sf_histo_HR_den=alog10(sf_histo_HR_den)
     
     log_mass_sf_histo_HR_T=alog10(mass_sf_histo_HR_T)
     log_mass_sf_histo_HR_den=alog10(mass_sf_histo_HR_den)
  endif

;print, mx_mass, mn_mass
;help, log_mass_histo_HR_T, mass_histo_HR_T
;print, total(mass_histo_HR_T)
;print, minmax(mass_histo_HR_T)
;help, temperature_amr_with_all2, density_amr_with_all2
;help, mass2
;stop

  xs=500
  ys=500

  if not keyword_set(show_den) then show_den = 0
  if not keyword_Set(show_sf) then show_sf = 0     

  if only_sf eq 1 then show_sf = 0
  if show_sf eq 1 and show_den eq 1 then ys=850
  if only_sf eq 0 then sf_name = '' else sf_name='sf_gas_'

;problemes d'affichage dans le fichier eps pour les den...

  if ps eq 1 then begin
     if show_den eq 0 and show_sf eq 0 then ps_start, nm+'HR_tempamr_diagram_'+sf_name+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=16, ysize=14, /helvetica, /bold
     if show_den eq 1 and show_sf eq 1 then ps_start, nm+'HR_den_tempamr_diagram_sf+non_sf_gas'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=25, ysize=25, /helvetica, /bold
     if show_den eq 0 and show_sf eq 1 then ps_start, nm+'HR_tempamr_diagram_sf+non_sf_gas'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=16, ysize=14, /helvetica, /bold
     if show_den eq 1 and show_sf eq 0 then ps_start, nm+'HR_den_tempamr_diagram_'+sf_name+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.eps', /encapsulated, /color, /cm, xsize=16, ysize=25, /helvetica, /bold
  endif
  if ps eq 0 then window, 5, ys=ys, xs=ys
  if show_sf eq 1 and show_den eq 1 then !p.multi=[0,2,2] 
  if show_sf eq 1 and show_den eq 0 then !p.multi=[0,2,1]
  if show_sf eq 0 and show_den eq 1 then !p.multi=[0,1,2]
  if only_sf eq 1 then !p.multi=0
  ;if show_sf eq 1 or show_den eq 1 then !y.omargin=[0,4]
  !x.margin=[0,0]
  !x.omargin=[6,1]
  !p.color=!black
  max_T = mx(1)
;device, decompose=0
  cgloadct,39, /reverse
  device, decompose=1
  chars=4


 if nt_sf gt 0 then begin

        mx_mass_sf = float(round(max([log_mass_sf_histo_HR_T[where(finite(log_mass_sf_histo_HR_T) eq 1)],log_mass_sf_histo_HR_den[where(finite(log_mass_sf_histo_HR_den) eq 1)]])*10)/10.)+0.1
        mn_mass_sf = float(round(min([log_mass_sf_histo_HR_T[where(finite(log_mass_sf_histo_HR_T) eq 1)],log_mass_sf_histo_HR_den[where(finite(log_mass_sf_histo_HR_den) eq 1)]])*10)/10.)-0.1


print, mn_mass, mx_mass
print, mn_mass_sf, mx_mass_sf
;stop
endif

     if show_den eq 1 then begin
        xtitle = ''
        xtickn = replicate(' ', 15)
     endif else begin
        xtitle = 'log Rel. temperature change';'log '+textoidl('\DeltaT/T_i')
        xtickn = replicate('', 15)
     endelse

  if show_sf eq 0 then begin
     if only_sf eq 0 then begin
        plotimage, log_mass_histo_HR_T, range=[mn_mass,mx_mass], imgxr=[mn(0),mx(0)], imgyr=[mn(1),mx(1)], xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='log Initial temperature [K]', ncolors=256, bottom=0, xtickn=xtickn, /xstyle, /isotropic, chars=3
     cgColorbar, range=[mn_mass,mx_mass], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'
     endif else begin
        if nt_sf gt 0 then begin
           plotimage, log_mass_sf_histo_HR_T, range=[mn_mass_sf,mx_mass_sf], imgxr=[mn(0),mx(0)], imgyr=[mn(1),mx(1)], yr=[1,max_T], xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='log Initial temperature [K]', ncolors=256, bottom=0, /xstyle, xtickn=xtickn, /isotropic, chars=3
     cgColorbar, range=[mn_mass_sf,mx_mass_sf], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'

        endif
     endelse
     oplot, [-3,HR_bins2d], 4-alog10(10d^[-3,HR_bins2d]+1), color=!gray, thick=8*ps_thick
  endif else begin

     plotimage, log_mass_histo_HR_T, range=[mn_mass,mx_mass], imgxr=[mn(0),mx(0)], imgyr=[mn(1),mx(1)], xr=[mn(0)-1,mx(0)],  xtitle=xtitle, ytitle='log Initial temperature [K]', ncolors=256, bottom=0, xtickn=xtickn, /xstyle, /isotropic, chars=3
     cgColorbar, range=[mn_mass,mx_mass], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'
     oplot, [-3,HR_bins2d], 4-alog10(10d^[-3,HR_bins2d]+1), color=!gray, thick=8*ps_thick

     if nt_sf gt 0 then begin
        cgText, 0.20, 0.775, ALIGNMENT=0.5, CHARSIZE=chars*.9, /NORMAL, 'All gas', color=!black
        plotimage, log_mass_sf_histo_HR_T, range=[mn_mass_sf,mx_mass_sf], imgxr=[mn(0),mx(0)], imgyr=[mn(1),mx(1)], yr=[1,max_T], xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='', ncolors=256, bottom=0, /xstyle, ytickn=[replicate(' ',15)], xtickn=xtickn, /isotropic, chars=3
        oplot, [-3,HR_bins2d], 4-alog10(10d^[-3,HR_bins2d]+1), color=!gray, thick=8*ps_thick
        cgColorbar, range=[mn_mass_sf,mx_mass_sf], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']' 
         cgText, 0.75, 0.775, ALIGNMENT=0.5, CHARSIZE=chars*.9, /NORMAL, 'Star-forming gas', color=!black
         cgText, 0.75, 0.725, ALIGNMENT=0.5, CHARSIZE=chars*.9, /NORMAL, '(before RT)', color=!black
     endif
  endelse
  cgText, 0.42, 0.77, ALIGNMENT=0.5, CHARSIZE=0.9*chars, /NORMAL, L_AGN, color=!black

  if show_den eq 1 then begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if show_sf eq 0 then begin
     if only_sf eq 0 then begin
        plotimage, log_mass_histo_HR_den, range=[mn_mass,mx_mass], imgxr=[mn(0),mx(0)], imgyr=[mn(2),mx(2)], xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='log Gas density [cm'+textoidl('^{-3}')+']', ncolors=256, bottom=0, /xstyle, /isotropic, chars=3
     cgColorbar, range=[mn_mass,mx_mass], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'

     endif else begin
        if nt_sf gt 0 then begin
           plotimage, log_mass_sf_histo_HR_den, range=[mn_mass_sf,mx_mass_sf], imgxr=[mn(0),mx(0)], imgyr=[mn(2),mx(2)],  xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='log Gas density [cm'+textoidl('^{-3}')+']', ncolors=256, bottom=0, /xstyle, /isotropic, chars=3
     cgColorbar, range=[mn_mass_sf,mx_mass_sf], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'

        endif
     endelse
  endif else begin

     plotimage, log_mass_histo_HR_den, range=[mn_mass,mx_mass], imgxr=[mn(0),mx(0)], imgyr=[mn(2),mx(2)], xtitle=xtitle, ytitle='log Gas density [cm'+textoidl('^{-3}')+']', ncolors=256, bottom=0, xr=[mn(0)-1,mx(0)], /xstyle, /isotropic, chars=3
     cgColorbar, range=[mn_mass,mx_mass], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']'

     if nt_sf gt 0 then begin
        plotimage, log_mass_sf_histo_HR_den, range=[mn_mass_sf,mx_mass_sf], imgxr=[mn(0),mx(0)], imgyr=[mn(2),mx(2)], xr=[mn(0)-1,mx(0)], xtitle=xtitle, ytitle='', ncolors=256, bottom=0, /xstyle, ytickn=[replicate(' ',15)], /isotropic, chars=3
        cgColorbar, range=[mn_mass_sf,mx_mass_sf], ncol=256, color=!p.color, /fit, tloc='bottom', title='log Mass per bin [M'+textoidl('_\odot')+']' 
     endif
  endelse
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


endif



  if ps eq 1 then begin
     ps_end, /png

     if show_den eq 0 and show_sf eq 0 then spawn, 'open '+nm+'HR_tempamr_diagram_'+sf_name+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.png'
     if show_den eq 1 and show_sf eq 1 then spawn, 'open '+nm+'HR_den_tempamr_diagram_sf+non_sf_gas'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.png'
     if show_den eq 0 and show_sf eq 1 then spawn, 'open '+nm+'HR_tempamr_diagram_sf+non_sf_gas'+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.png'
     if show_den eq 1 and show_sf eq 0 then spawn, 'open '+nm+'HR_den_tempamr_diagram_'+sf_name+sfr_threshold+'Hcc_'+nm_center+nm_n+sim_with(i)+'.png'
  endif
  
  print, total(mass_histo_hr_t), total(mass_sf_histo_hr_t)
  print, total(mass_histo_hr_den), total(mass_sf_histo_hr_den)
  stop
end
