;compare_luminosities_interp, s='00100', pl='dk', z=82, /interp, w='all_den_temp_sfr', ps=1, thr='10'

function trace, param, pos1_cloudy, pos2_cloudy, w0, title, signe, colorbar, pos1_agn, pos2_agn, pos1_amr, pos2_amr, param_init, flag, strct, cb_pos, lmax, ps
;##############################################################
;cette fonction trace les lops sur les cartes avec les couleurs
;correspondant au parametre d'entree
;##############################################################
  taille = n_elements(param)

  if n_elements(cb_pos) ne 1 then fit=0 else fit=1    

  lab_col = !black 

  change_pos = 0
  if signe eq 'sfr_one' then begin
     change_pos = 1
     signe = 'sfr'
  endif
  
  if signe eq 'density' then begin
     rg = [-7,6]
     ndiv = 13
     if fit eq 0 then ndiv=7
  endif
  
  if signe eq 'temperature' then begin
     rg = [2,10]
     rg = reverse(rg)
     ndiv = 8
     if fit eq 0 then ndiv=4
  endif

  if signe eq 'heating_rate' then begin
     rg = [-2,7.5]              ;[-12.5,7.5] --> vraie gamme de chauffage mais pas significatif si inferieur a 1 % = 10^-2
     rg = reverse(rg)
     ndiv = 20
     print, 'Min/max :', minmax(alog10(param[where(param ne 0)]))
     if fit eq 0 then ndiv=10
     lab_col = !white
     signe = 'temperature'
  endif

  if signe eq 'delta_T' then begin
     rg = [-5,10]               ;vraie gamme aussi mais on ne peut pas faire de coupure en %age comme avec le chauffage
                                ;NB : avec les deux "vraies gammes de valeurs", leux cartes HR et dT sont tres similaires
                                ;---> garder celle-la pour se rappeler comment etait l'autre avant le "seuil de pertinence"
     rg = reverse(rg)
     ndiv = 15
     print, 'Min/max :', minmax(alog10(param[where(param ne 0)]))
     if fit eq 0 then ndiv=7
     lab_col = !white
     signe = 'temperature'
  endif
  
  if signe eq 'sfr' then begin
                                ;rg = [-20,0]
     rg = [-18,-5]
     ndiv = 13
     if fit eq 0 then ndiv=7
  endif
  
  if signe eq 'Hfrac' then begin
     rg = [-13, 0]
     ndiv = 13
     lab_col = !white
     if fit eq 0 then ndiv=7
  endif
  
  if signe eq 'Ofrac' then begin
     rg = [-15, 0]
     ndiv=15
     if fit eq 0 then ndiv=8
  endif
  
  if signe eq 'emissivity' then begin
     rg = [-8,-1]
     ndiv = 5
  endif

  ndiv=0

;print, min(alog10(param)), rg(0)
;if min(alog10(param)) lt rg(0) and min(param) ne 0. then begin
;print, 'borne inferieure non coloree !'
;stop
;endif
;if max(alog10(param)) gt rg(1) and max(param) ne 0. then begin
;print, 'borne superieure non coloree !'
;stop
;endif
  
  
  if signe eq 'Ofrac' or signe eq 'Hfrac' then signe = 'moins'
  

  if ps eq 0 then begin
     wset, w0
     chars = 0.7
  endif else begin
     chars = 2
  endelse
  !p = strct.p & !x = strct.x & !y = strct.y
  
  
  colors = cgScaleVector(Findgen(taille), rg(0), rg(1))
                                ;print, colors
  cols = Value_Locate(colors, alog10(param))
  
;print, min(cols), max(cols)
;;print, min(param), max(param)
  
  if min(cols) eq -1 then color_min = 0.
  if min(cols) ge 0 then color_min = float(min(cols))/(taille-1)*255.
  
  color_max = float(max(cols))/(taille-1)*255.
;print, color_min, color_max
  
  if color_min ne color_max then cols = Byte(Round(cgScaleVector(cols, color_min, color_max)))
  if color_min eq color_max then cols = Byte(Round(cols))
  
;print,  'Doivent etre entre 0 et 255 :', minmax(cols)
  
;defplotcolors
;device, decompose=0
  t = where(alog10(param) le min(rg), nt)
  if signe eq 'temperature' then first_color = 255 else first_color = 0
  if nt gt 0 then cols[t]= first_color
  
  device,decompose=0
  cgloadct, 39, /silent, /reverse
  
  if n_elements(unique(flag,/sort)) ne 1 then stop  


  if strmatch(flag(0), '*ok*') eq 1 then lines=0 ;continu
  if strmatch(flag(0), '*smo*') eq 1 then lines=2 ;- - - pointilles
  if strmatch(flag(0), '*24h*') eq 1 then lines=4 ;- ... - ... - tirets + pointilles
  if strmatch(flag(0), '*rel*') eq 1 then lines=1 ;... pointilles

  psym = cols*0.0 - 3
  if ps eq 0 then thicks = cols*0.0+1 else thicks = cols*0.0+10
  
;points relies
  imin = 0
  for i = 0, taille-2 do begin
     if cols(i) ne cols(i+1) or i eq (size(param))[1]-2 then begin
        oplot, [pos1_cloudy(imin), pos1_cloudy(i+1)], [pos2_cloudy(imin), pos2_cloudy(i+1)], color=cols(i), thick=thicks(i), lines=lines ;, psym=psym(i), syms=thicks(i)
        imin = i
     endif 
  endfor

;point non relies
;for i = 0, taille-1 do begin
;        oplot, [1,1]*pos1_cloudy(i), [1,1]*pos2_cloudy(i), color=cols(i), thick=thicks(i), psym=3
;endfor


;if compact ne 0 then begin

  lab=print_label(lab_col, change_pos, ps)

;stop
;endif  
  

;cette partie avait du sens quand les lops etaient prolongees dans
;cette fonction. maintenant elles sont prolongees directement dans le *.dat
;  depth_amr = sqrt((pos1_amr-pos1_agn)^2 + (pos2_amr-pos2_agn)^2)
;  depth_cloudy = sqrt((pos1_cloudy-pos1_agn)^2 + (pos2_cloudy-pos2_agn)^2)
;;print, 'PFDEURS', max(depth_amr), max(depth_cloudy)
;  
;  if (size(param_init))[1] ne (size(pos1_amr))[1] then existe = 0
;  if (size(param_init))[1] eq (size(pos1_amr))[1] then existe = 1
;  
;;if min(alog10(param_init)) lt rg(0) and min(param_init) ne 0. then begin
;;print, 'borne inferieure non coloree (init) !'
;;stop
;;endif
;;if max(alog10(param_init)) gt rg(1)  and max(param_init) ne 0. then begin
;;print, 'borne superieure non coloree (init) !'
;;stop
;;endif
;  
;  t = where(depth_amr gt max(depth_cloudy), nt)
;  if nt gt 0 then param_init = param_init[t]
;  psym = param_init*0.0 - 3
;  if ps eq 0 then thicks =  param_init*0.0+1 else thicks =  param_init*0.0+5
;  
;  colors = cgScaleVector(Findgen((size(param_init))[1]), rg(0), rg(1))
;                                ;print, colors
;  cols_init = Value_Locate(colors, alog10(param_init))
;  
;;print, 'Paremetre initial :', min(param_init), max(param_init)
;;print, min(cols_init), max(cols_init)
;  
;  
;  if min(cols_init) eq -1 then color_min = 0.
;  if min(cols_init) ge 0 then color_min = float(min(cols_init))/((size(param_init))[1]-1)*255.
;  
;  color_max = float(max(cols_init))/((size(param_init))[1]-1)*255.
;;print, color_min, color_max
;  
;  if color_min ne color_max then cols_init = Byte(Round(cgScaleVector(cols_init, color_min, color_max)))
;  if color_min eq color_max then cols_init = Byte(Round(cols_init))
;  
;;print,  'Doivent etre entre 0 et 255 :', minmax(cols_init)
;  
;;defplotcolors
;;device, decompose=0
;;t = where(param_init eq 0.0, nt)
;;if nt gt 0 then cols_init[t]=!magenta
;  
;  non = 1
;  defplotcolors
;  if non eq 0 then begin
;;les LOP sont prolongees apres la fin cloudy (T<4000K)
;     if max(depth_cloudy) le depth_amr((size(pos1_amr))[1]-1) then begin ;si la ligne de visee n'est pas entiere
;        
;        defplotcolors
;                                ;si le parametre initial n'existe pas, prolonger en blanc pointille
;        if existe eq 0 and signe ne 'moins' then oplot, [pos1_cloudy((size(pos1_cloudy))[1]-1), pos1_amr((size(pos1_amr))[1]-1)], $
;           [pos2_cloudy((size(pos1_cloudy))[1]-1), pos2_amr((size(pos1_amr))[1]-1)], color=!white, line=2 ;, color=cols((size(cols))[1]-1)
;        
;        device, decompose=0
;        cgloadct, 39, /silent, /reverse
;if signe eq 'temperature' then cgloadct, 39, /silent
;                                ;si le parametre initial n'existe pas mais si c'est l'ionisation
;                                ;on sait que c'est neutre --> noir
;        if existe eq 0 and signe eq 'moins' then oplot, [pos1_cloudy((size(pos1_cloudy))[1]-1), pos1_amr((size(pos1_amr))[1]-1)], $
;           [pos2_cloudy((size(pos1_cloudy))[1]-1), pos2_amr((size(pos1_amr))[1]-1)], color=255
;        
;                                ;MAIS si Te n'est pas le critere d'arret, c'est le rayon exterieur, qui est un peu plus petit que la longueur AMR de la LOP
;                                ;---> dans ces cas-la, prolonger avec la derniere couleur CLOUDY
;        if existe eq 0 and signe eq 'moins' and max(depth_cloudy) ge 0.8*depth_amr((size(pos1_amr))[1]-1) then oplot, $
;           [pos1_cloudy((size(pos1_cloudy))[1]-1), pos1_amr((size(pos1_amr))[1]-1)], $
;           [pos2_cloudy((size(pos1_cloudy))[1]-1), pos2_amr((size(pos1_amr))[1]-1)], color=cols((size(cols))[1]-1)          
;        
;                                ;si le parametre initial existe, alors prolonger avec les couleurs correspondantes
;                                ;(devenu inutile car parametre prolonge avant l'entree dans la fonction)
;                                ;if existe eq 1 then begin
;                                ;    imin = 0
;                                ;    for i = 0, (size(param_init))[1]-2 do begin
;                                ;       if cols_init(i) ne cols_init(i+1) or i eq (size(param_init))[1]-2 then begin
;                                ;          oplot, [pos1_amr(imin), pos1_amr(i+1)], [pos2_amr(imin), pos2_amr(i+1)], color=cols_init(i), psym=psym(i), thick=thicks(i), syms=thicks(i)
;                                ;          imin = i
;                                ;       endif 
;                                ;    endfor
;                                ;endif
;     endif
;endif
  resol = 50d/2d^lmax           ;BOXLEN !!!
  res=affiche_resol(pos1_agn, pos2_agn, resol)

  
  
  if colorbar eq 1 then begin
     device, decompose=0
     cgloadct, 39, /silent, /reverse
     if ps eq 0 then begin
        chars = 2
        textt = 1
     endif else begin
        chars = 5
        textt = 5
     endelse

     if signe eq 'temperature' then begin
        rg = reverse(rg)
        rev = 1
     endif else begin
        rev = 0
     endelse

     if fit eq 0 then begin
        if ps eq 0 then begin
           chars = 2.5
           textt = 1
        endif else begin
           chars = 5.5
           textt = 5
        endelse
     endif

                                ;left, bottom, right, top
     if fit eq 1 then cgColorbar, range=rg, ncol=255, divisions=ndiv, color=!p.color, /vertical, /fit, title=title, reverse=rev, /right, chars=chars, textthick=textt else $
        cgColorbar, range=rg, ncol=255, divisions=ndiv, color=!p.color, /vertical, title=title, reverse=rev, /right, chars=chars, textthick=textt, position=cb_pos
     
     
     device, /decompose
  endif
  
  return, 0
end


function print_whole_lops, pos1, pos2, w0
;#############################################################
;cette fonction trace les lignes en blanc avant de les colorer
;#############################################################
  wset, w0
  defplotcolors
  oplot, pos1, pos2, color=!white, line=2
  return, 0
end

function read_LOPs, x_all, y_all, z_all, theta_all, phi_all, depth_all, den_all, temp_all, $
                    Hneutre_all, Oneutre_all, Hb_4861A_all, OIII_5007A_all, Ha_6563A_all, NII_6584A_all, x_kpc, y_kpc, z_kpc, $
                    depth_ascii, density_ascii, temperature_ascii, cell_size_ascii, $
                    x_ascii, y_ascii, z_ascii, vx_ascii, vy_ascii, vz_ascii, $
                    x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, flag, lop_name, $
                    w0, sim, plane, colorbar, value, strct, nm, header, sfr_threshold, offset,  temp_choice, ps

;############################################################################
;cette fonction decide quels parametres afficher et calcule les sfr et colden
;et re-echantillonne pour pouvoir comparer la grille amr et la grille cloudy
;############################################################################  

  verbose = 0
  cb_pos = 0

  lmax = sxpar(header, 'lmax')
  
  if value eq 'den_temp_sfr' then begin
     Tinit = 1
     Tfin = 1
     Em_em = 0
     Int_em = 0
     Hfrac = 1
     Ofrac = 0
                                ;sfr_avant = 1 & rrhosfr_avant = 0
                                ;sfr_apres = 1 & rrhosfr_apres = 0
     sfr_avant = 0 & rrhosfr_avant = 1
     sfr_apres = 0 & rrhosfr_apres = 1
     densite = 1
  endif

  if value eq 'all_den_temp_sfr' then begin
     if nm eq '' then Tinit = 1 else Tinit = 0
     Tfin = 1
     Em_em = 0
     Int_em = 0
     Hfrac = 1
     Ofrac = 0
                                ;if if nm eq '' then sfr_avant = 1 else sfr_avant = 0
                                ;rrhosfr_avant = 0
                                ;sfr_apres = 1 & rrhosfr_apres = 0
     if nm eq '' then rrhosfr_avant = 1 else rrhosfr_avant = 0
     sfr_avant = 0 
     sfr_apres = 0 & rrhosfr_apres = 1
     if nm eq '' then densite = 1 else densite = 0
  endif

  if value eq 'compare_lum' then begin
     if nm eq '' then Tinit = 1 else Tinit = 0
     Tfin = 1
     Em_em = 0
     Int_em = 0
     Hfrac = 0
     Ofrac = 0
                                ;if if nm eq '' then sfr_avant = 1 else sfr_avant = 0
                                ;rrhosfr_avant = 0
                                ;sfr_apres = 1 & rrhosfr_apres = 0
     if nm eq '' then rrhosfr_avant = 1 else rrhosfr_avant = 0
     sfr_avant = 0 
     sfr_apres = 0 & rrhosfr_apres = 1
     densite = 0
  endif

  if value eq 'compare_lum2' then begin
     Tinit = 0
     Tfin = 0
     Em_em = 0
     Int_em = 0
     Hfrac = 0
     Ofrac = 0
                                ;if if nm eq '' then sfr_avant = 1 else sfr_avant = 0
                                ;rrhosfr_avant = 0
                                ;sfr_apres = 1 & rrhosfr_apres = 0
     if nm eq '' then rrhosfr_avant = 1 else rrhosfr_avant = 0
     sfr_avant = 0 
     sfr_apres = 0 & rrhosfr_apres = 0
     densite = 0
  endif
  
  if value eq 'den_em' then begin
     Tinit = 0
     Tfin = 0
     Em_em = 1
     Int_em = 1
     Hfrac = 0
     Ofrac = 1
     sfr_avant = 0 & rrhosfr_avant = 0
     sfr_apres = 0 & rrhosfr_apres = 0
     densite = 1
  endif
  
  if value eq 'colden' then begin
     Tinit = 0
     Tfin = 0
     Em_em = 0
     Int_em = 0
     Hfrac = 0
     Ofrac = 0
     sfr_avant = 0 & rrhosfr_avant = 0
     sfr_apres = 0 & rrhosfr_apres = 0
     densite = 0
  endif
  

  if plane eq 'xz' then begin
     pos1 = x_all - offset
     pos2 = z_all - offset
     pos1_agn = x_kpc - offset
     pos2_agn = z_kpc - offset
     pos1_ascii = x_ascii - offset
     pos2_ascii = z_ascii - offset
  endif
  if plane eq 'yz' or plane eq 'zy' then begin
     pos1 = z_all - offset
     pos2 = y_all - offset
     pos1_agn = z_kpc - offset
     pos2_agn = y_kpc - offset
     pos1_ascii = z_ascii - offset
     pos2_ascii = y_ascii - offset
     plane = 'zy'
  endif 
  if plane eq 'dk' then begin
     pos1 = x_all - offset
     pos2 = y_all - offset
     pos1_agn = x_kpc - offset
     pos2_agn = y_kpc - offset
     pos1_ascii = x_ascii - offset
     pos2_ascii = y_ascii - offset
  endif
  
  pos1_all = pos1
  pos2_all = pos2
  
;/!\ temp_all = [temp_phyc,zeros] ----> prendre le max / temp_amr !!!
  
  taille_cloudy = n_elements(temp_all[where(Hb_4861A_all ne 0 and OIII_5007A_all ne 0)])
  
  den_resample = fltarr(taille_cloudy)
  temp_resample = fltarr(taille_cloudy)
  cell_size_resample = fltarr(taille_cloudy)
  
  temp_post_cloudy = fltarr(taille_cloudy)
  den_post_cloudy = fltarr(taille_cloudy)
  
  taille_amr = n_elements(density_ascii)
  
  den_init = fltarr(taille_cloudy)
  temp_init = fltarr(taille_cloudy)

;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(50.d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_ascii ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_ascii[t_rho]
     t_temp = where(temperature_ascii[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_ascii[t_rho(t_temp)] = 900
  endif
  
  for i = 0, taille_cloudy-1 do begin ;;;re-echantillonnage de cell_size selon histogramme car variable discrete
     
     if depth_all(i) le (depth_ascii(0)+depth_ascii(1))/2. then begin
        cell_size_resample(i) = cell_size_ascii(0)
     endif
     for j = 1, taille_amr-2 do begin
        if depth_all(i) gt (depth_ascii(j)+depth_ascii(j-1))/2. and depth_all(i) le (depth_ascii(j)+depth_ascii(j+1))/2. then begin
           cell_size_resample(i) = cell_size_ascii(j)
        endif
     endfor
     if depth_all(i) gt (depth_ascii(taille_amr-2)+depth_ascii(taille_amr-1))/2. then begin
        cell_size_resample(i) = cell_size_ascii(taille_amr-2)
     endif
  endfor
  for i = 0, taille_cloudy-1 do begin
     if cell_size_resample(i) eq 0 then cell_size_resample(i) = cell_size_resample(i-1)
  endfor
  
  
  t = where(Hb_4861A_all ne 0 and OIII_5007A_all ne 0, nt)

        ;;;interpoler comme cloudy
  xx = alog10(depth_ascii[1:n_elements(depth_ascii)-1]) ;depth_amr
  
  yy = alog10(density_ascii[1:n_elements(depth_ascii)-1])   ;density_amr
  den_resample = 10d^(interpol(yy,xx,alog10(depth_all[t]))) ;density resampled onto Cloudy depth variable
  
  yy2 = alog10(temperature_ascii[1:n_elements(depth_ascii)-1]) ;temperature_amr
  temp_resample = 10d^(interpol(yy2,xx,alog10(depth_all[t])))  ;temperature resampled onto Cloudy depth variable
  
  t = where(temp_resample eq 0, nt)
  if nt gt 0 then begin
     print, 'nb de zeros', nt
     stop
  endif
  
  t=where(finite(temp_resample) eq 0, nt)
  if nt gt 0 then begin
     print, 'probleme d echantillonnage !'
     stop
  endif
  
                                ;help, temp_resample
                                ;help, den_resample
  t = where(Hb_4861A_all ne 0 and OIII_5007A_all ne 0, nt) ;savoir ou sont les points cloudy avant prolongation
                                ;help, temp_all[t]
  
  if verbose eq 1 then print, 'Cell size min : ', min(cell_size_ascii)
;print, 'cell_size AMR : ', cell_size_ascii, ' cell_size resampled :', cell_size_resample
  

;temp_all : temperatures cloudy (grille cloudy) puis 0 (grille amr) jusqu'a la fin de la ligne amr
;temp_resample : temperature amr re-echantillonnee sur la grille cloudy (pas prolongee sur la grille amr)


;prendre le maximum entre la valeur initiale et la valeur calculee par
;cloudy (temperature d'equilibre)
  for i = 0, taille_cloudy-1 do begin
     temp_post_cloudy(i) = max([temp_resample(i),temp_all(i)])
     if max([temp_resample(i),temp_all(i)]) eq temp_all(i) then den_post_cloudy(i) = den_all(i)
     if max([temp_resample(i),temp_all(i)]) eq temp_resample(i) then den_post_cloudy(i) = den_resample(i)
     
;print, temp_resample(i), temp_all(i), max([temp_resample(i),temp_all(i)])
  endfor
  

;print, depth_all
;print, '?'
  
  t = where(Hb_4861A_all eq 0 and OIII_5007A_all eq 0, nt)
;print, nt
;print, depth_all[t]
  if nt gt 0 then begin
     max_depth = depth_all(t(0)-1) 

;print, max_depth
     t2 = where(depth_ascii gt max_depth)

;;;sur-echantillonner la partie amr restante pour avoir un espacement
;;;similaire a la grille cloudy et avoir un sfr comparable (eviter le
;;;changement vert/bleu a l'interface grille cloudy/grille amr
;;;sur la carte du sfr)
;cette interface est genante car elle n'est pas au meme endroit
;pour les differentes luminosites et donc la carte de sfr initial
;semble changer

     dpth = congrid([1.0001*depth_all[t(0)-1],depth_ascii[t2]],11*(n_elements(depth_ascii)+1),/interp)
     dn = congrid([density_ascii[t2(0)],density_ascii[t2]],11*(n_elements(depth_ascii)+1),/interp)
     tpr =  congrid([temperature_ascii[t2(0)],temperature_ascii[t2]],11*(n_elements(depth_ascii)+1),/interp)

     dpth = dpth(uniq(dpth))    ;se debarrasser des profondeurs en double a la fin
     dn = dn(uniq(dpth))
     tpr = tpr(uniq(dpth))

;print, depth_all

     depth_all = [depth_all[0:t(0)-1],dpth]
     den_all = [den_all[0:t(0)-1],dn]
     Hneutre_all = [ Hneutre_all[0:t(0)-1], fltarr(n_elements(dpth))+1]
     Oneutre_all = [ Oneutre_all[0:t(0)-1], fltarr(n_elements(dpth))+1]
     Hb_4861A_all = [ Hb_4861A_all[0:t(0)-1], 0*dpth]
     OIII_5007A_all = [ OIII_5007A_all[0:t(0)-1], 0*dpth]
     Ha_6563A_all = [Ha_6563A_all[0:t(0)-1], 0*dpth]
     NII_6584A_all = [ NII_6584A_all[0:t(0)-1], 0*dpth]

     alpha = cos(phi_all(0))*sin(theta_all(0))
     beta = sin(phi_all(0))*sin(theta_all(0))
     gamma = cos(theta_all(0)) 

                                ;x = depth*alpha + x_agn ;kpc
                                ;y = depth*beta + y_agn  ;kpc
                                ;z = depth*gamma + z_agn ;kpc

     if plane eq 'xz' then begin
        pos1_all = [pos1_all[0:t(0)-1],dpth*alpha+pos1_agn] ;x
        pos2_all = [pos2_all[0:t(0)-1],dpth*gamma+pos2_agn] ;z
     endif
     if plane eq 'zy' then begin
        pos1_all = [pos1_all[0:t(0)-1],dpth*gamma+pos1_agn] ;z
        pos2_all = [pos2_all[0:t(0)-1],dpth*beta+pos2_agn]  ;y
     endif
     if plane eq 'dk' then begin
        pos1_all = [pos1_all[0:t(0)-1],dpth*alpha+pos1_agn] ;x
        pos2_all = [pos2_all[0:t(0)-1],dpth*beta+pos2_agn]  ;y
     endif
     pos1 = pos1_all
     pos2 = pos2_all

     temp_post_cloudy = [temp_post_cloudy, tpr]
     den_post_cloudy = [den_post_cloudy,dn]
     temp_init = [temp_resample, tpr]
     den_init = [den_resample,dn]
  endif else begin
     temp_init = temp_resample
     den_init = den_resample
  endelse
;temp_post_cloudy : maximum entre T_cloudy et T_amr (grille
;cloudy) puis T_amr jusqu'a la fin de la ligne amr

;stop

;polytrope de jeans
;la temperature est arbitrairement elevee pour eviter une derive
;numerique (l'agitation doit toujours etre resolue)
;si densite > densite seuil
;T [K] = 0.041666 * sqrt(32pi) * e^2 [pc^2] * rho [H/cc]
;T [K] = 15.56 * rho [H/cc]
;ou e est la resolution spatiale 50kpc/2^(lmax=13) = 6.10 pc
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(50.d3/2^lmax)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(den_all ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*den_all[t_rho]
     t_temp = where(temp_all[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temp_all[t_rho(t_temp)] = 900

     t_temp = where(temp_init[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temp_init[t_rho(t_temp)] = 900

     t_temp = where(temp_post_cloudy[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temp_post_cloudy[t_rho(t_temp)] = 900
  endif
  
;prolonger les parametres

  
;calcul des densites de colonne et des masses totales
  colden_post_cloudy = fltarr(n_elements(den_all))
  mass_post_cloudy = fltarr(n_elements(den_all))
  
  colden_amr = fltarr(n_elements(density_ascii))
  mass_amr = fltarr(n_elements(density_ascii))
  
  colden_resample = fltarr(n_elements(den_init))
  mass_resample = fltarr(n_elements(den_init))
  
  m_H = 1.67d-27                ;kg
  Msun = 2.0d30                 ;kg
  kpc = 1.0/3.24d-22            ;cm
  scale = m_H / Msun * kpc^3    ;= 1.67e-27/2.0e30/(3.24e-22)^3 = 2.45e7 Msun/cm3
;scale = 2.45d7
  
  
  depth_centered = depth_ascii*0
  depth_centered(0) = depth_ascii(0)
  for k = 1, (size(depth_ascii))[1]-1 do begin
     depth_centered(k) = (depth_ascii(k)+depth_ascii(k-1))/2.
  endfor
  depth_centered_lop = depth_all*0
  depth_centered_lop(0) = depth_all(0)
  for k = 1, (size(depth_all))[1]-1 do begin
     depth_centered_lop(k) = (depth_all(k)+depth_all(k-1))/2.
  endfor

  
;if depth_ascii(0) ge min(depth_all) then begin
  colden_amr(0) = density_ascii(0)*depth_centered(0)/3.24d-22
  mass_amr(0) = scale*density_ascii(0)*depth_centered(0)^3 ;;;*(cell_size(0))^2
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_centered(0),depth_centered(0)], [total(mass_amr(0)),total(mass_amr(0))], psym=1, color=!orange
;print, 'masse amr 0' , mass_amr(0), density_ascii(0), depth_ascii(0), cell_size(0)
;endif
  for i = 1, (size(depth_ascii))[1]-1 do begin
;if depth_ascii(i) ge min(depth_all) then begin
     colden_amr(i) = density_ascii(i)*(depth_centered(i)-depth_centered(i-1))/3.24d-22
     mass_amr(i) = scale*density_ascii(i)*(depth_centered(i)-depth_centered(i-1))^3 ;;;*(cell_size(i))^2 ;Msun
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_centered(i),depth_centered(i)], [total(mass_amr[0:i]),total(mass_amr[0:i])], psym=1, color=!black
;endif
  endfor
;;;prendre en compte que le milieu de la carte est ignore par cloudy
;;;il ne faut donc pas le compter dans le calcul de pourcentage de
;;;masse chauffee ou non car si cloudy avait pris ces points en
;;;compte, il les aurait surement chauffes
  t_milieu = where(depth_centered lt min(depth_centered_lop), nt_milieu)
  colden_amr_total = total(colden_amr) 
  mass_amr_total = total(mass_amr)
  if nt_milieu gt 0 then colden_amr_total = colden_amr_total - total(colden_amr[t_milieu]) ;;colden au milieu de la carte
  if nt_milieu gt 0 then mass_amr_total = mass_amr_total - total(mass_amr[t_milieu])       ;;masse au milieu de la carte
  
;;;re-echantillonnage de depth_centered pour avoir les 2 autres
;;;dimensions dans la grille cloudy 

  xx = alog10(depth_ascii[1:n_elements(depth_ascii)-1])    ;depth_amr
  yy = alog10(depth_centered[1:n_elements(depth_ascii)-1]) ;density_amr
  depth_centered_resample = 10d^interpol(yy, xx, alog10(depth_all))
  t=where(finite(depth_centered_resample) eq 0, nt)
  if nt gt 0 then stop
  
;help, depth_centered_resample
;help, den_all
;help, den_all[where(Hb_4861A_all ne 0 and OIII_5007A_all ne 0, nt)]
  
  t = where(Hb_4861A_all eq 0)
  max_depth = depth_all(t(0)-1)
;print, max_depth, depth_all(t(0)) 
;print, Hb_4861A_all(t(0)-1), Hb_4861A_all(t(0)) 
;t = where(depth_ascii gt max_depth, nt)
;print, depth_ascii[t], nt
  t = where(depth_centered gt max_depth, nt)
;print, depth_centered[t], nt
  
                                ;depth_centered_resample = [depth_centered_resample,depth_centered[t]]
  
;help, depth_centered_resample
;help, depth_centered
;help, depth_all
;help, depth_centered_lop
;help, depth_ascii
  
;;;valeurs AMR re-echantillonnes sur grille cloudy + prolongation des
;;;lignes apres (mais pas avant, ie au milieu de la carte)
  colden_resample(0) = den_init(0)*depth_centered_resample(0)/3.24d-22 
  mass_resample(0) = scale*den_init(0)*depth_centered_resample(0)*(depth_centered_resample(0))^2 ;;;*(cell_size_post_cloudy(0))^2 
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_centered_resample(0),depth_centered_resample(0)], [total(mass_resample[0:0]),total(mass_resample[0:0])], color=!blue
;print, 'masse resample 0' , mass_resample(0), den_init(0), depth_all(0), cell_size_post_cloudy(0)
  for i = 1, (size(depth_all))[1]-1 do begin
     colden_resample(i) = den_init(i)*(depth_centered_resample(i)-depth_centered_resample(i-1))/3.24d-22
     mass_resample(i) = scale*den_init(i)*(depth_centered_resample(i)-depth_centered_resample(i-1))*(depth_centered_resample(i)-depth_centered_resample(i-1))^2 ;;;*(cell_size_post_cloudy(i))^2 ;Msun
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_all(i),depth_all(i)], [total(mass_resample[0:i]),total(mass_resample[0:i])], color=!blue
  endfor
  colden_resample_total = total(colden_resample) 
  mass_resample_total = total(mass_resample)
  
;;;valeurs post cloudy, ie max de AMR resampled et de CLOUDY
  colden_post_cloudy(0) = den_post_cloudy(0)*depth_all(0)/3.24d-22 
  mass_post_cloudy(0) = scale*den_post_cloudy(0)*depth_centered_lop(0)*(depth_centered_resample(0))^2 ;;;*(cell_size_post_cloudy(0))^2 
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_centered_lop(0),depth_centered_lop(0)], [total(mass_post_cloudy[0:0]),total(mass_post_cloudy[0:0])], psym=3, color=!red
  for i = 1, (size(depth_all))[1]-1 do begin
     colden_post_cloudy(i) = den_post_cloudy(i)*(depth_centered_lop(i)-depth_centered_lop(i-1))/3.24d-22
     mass_post_cloudy(i) = scale*den_post_cloudy(i)*(depth_centered_lop(i)-depth_centered_lop(i-1))*(depth_centered_resample(i)-depth_centered_resample(i-1))^2 ;;;*(cell_size_post_cloudy(i))^2 ;Msun
                                ;if ps eq 0 and value ne 'colden' then oplot, [depth_centered_lop(i),depth_centered_lop(i)], [total(mass_post_cloudy[0:i]),total(mass_post_cloudy[0:i])], psym=3, color=!red
  endfor
  colden_post_cloudy_total = total(colden_post_cloudy)
  mass_post_cloudy_total = total(mass_post_cloudy)  ;;;; /!\ DEJA en masses solaires, sinon trop grand pour etre calcule
  
  colden_chauffe_total = 0
  mass_chauffe_total = 0
  t = where(temp_post_cloudy gt temp_resample, nt) ;quelle fraction du temps Tf est-elle plus grande que Ti ?
  if nt gt 0 then begin
     den_chauffe = den_post_cloudy[t]
     depth_chauffe = depth_all[t]
     colden_chauffe = colden_post_cloudy[t]
     mass_chauffe = mass_post_cloudy[t]
     colden_chauffe_total = total(colden_chauffe)
     mass_chauffe_total = total(mass_chauffe)
  endif
  
                                ;help, pos1_all
                                ;help, temp_init
                                ;help, temp_post_cloudy
  
;tracer les lop en blanc avant de les colorer ?
;lops=print_whole_lops(pos1, pos2, 0)
  
;building gas temperature map (PRE-CLOUDY resampled)
  if Tinit eq 1   then begin
     if verbose eq 1 then print, 'Min/max de la temperature du gas pre-CLOUDY : ', minmax(temp_init)
     strct_ti = {p:strct.pti, x:strct.xti, y:strct.yti}

     if  value eq 'all_den_temp_sfr' and nm eq '' then cb_pos = [0.197,0.33,0.23,0.64]
     affiche = trace(temp_init, pos1_all, pos2_all, 0, 'log Initial Temperature [K]', 'temperature', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_ti, cb_pos, lmax, ps)
     
;temperature AMR avant re-echantillonnage :
;affiche = trace(temperature_ascii, pos1, pos2, 0, 'log Gas temperature in K', 'temperature', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_ti, cb_pos, lmax, ps)
  endif
  
  if Tfin eq 1   then begin
     if verbose eq 1 then print, "Min/max de la temperature post-cloudy : ", minmax(temp_post_cloudy)
     strct_tf = {p:strct.ptf, x:strct.xtf, y:strct.ytf}
     if value eq 'compare_lum' or value eq 'all_den_temp_sfr' and nm ne '' then begin
        if nm eq 'x10_' then strct_tf = {p:strct.ptfx10, x:strct.xtfx10, y:strct.ytfx10}
        if nm eq 'x100_' then strct_tf = {p:strct.ptfx100, x:strct.xtfx100, y:strct.ytfx100}
     endif

     prev_colorbar = colorbar
     if value eq 'all_den_temp_sfr' and (nm eq '' or nm eq 'x10_') then colorbar = 0
     if value eq 'all_den_temp_sfr' and nm eq 'x100_' then cb_pos = [0.894,0.33,0.93,0.64]

     if temp_choice eq 'temperature' then begin
        affiche = trace(temp_post_cloudy, pos1_all, pos2_all, 0, 'log Final Temperature [K]', 'temperature', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_tf,  cb_pos, lmax, ps)
     endif else begin
        if temp_choice eq 'heating_rate' then begin
           
           affiche = trace(abs(temp_post_cloudy-temp_init)/temp_init, pos1_all, pos2_all, 0, 'log Rel. temperature change', 'heating_rate', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_tf,  cb_pos, lmax, ps)
           
        endif else begin
           if temp_choice eq 'delta_T' then begin
              
              affiche = trace(abs(temp_post_cloudy-temp_init), pos1_all, pos2_all, 0, 'log '+textoidl('\Delta')+'T [K]', 'delta_T', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_tf,  cb_pos, lmax, ps)
              
           endif
        endelse
     endelse
;help, temp_init
;help, temp_post_cloudy

     colorbar = prev_colorbar
  endif

  
;print, 'pause'
;read, go_on
  
;wset, 27
;plot, density_ascii, temp_amr, psym=3, color=!blue, /ylog, /xlog, /noerase, xr=[1e-10,1e10], yr=[1,1e10], /xstyle, /ystyle
;oplot, density_all_post_cloudy, temp_post_cloudy, psym=3, color=!red
;if nt gt 0 then oplot, density_ascii[t], temp_amr[t], psym=3, color=!green
  
;;;;;;;;;;;;;;;;;cartes du SFR dans chaque point de chaque LOP,
;;;;;;;;;;;;;;;;;avant/apres
;SFR par unite de volume : rhoSFR en Msun/yr/pc3
;rhoSFR/LOPpt = efficacite*coeff*rho**n_SK si rho > rho_seuil et T > Tseuil
;n_SK : exposant de Schmidt-Kennicutt. = 1.5
  
;1 H/cc = 1 x m_H x 1e6 kg/m3
;       = 1.67e-19 kg/m3
;G = 6.67e-11 m3/kg/s2
  n=1.5
  epsilon=0.01
  G = 6.67d-11
  hcc_to_kgm3 = 1.67d-21
  alpha = sqrt(32.*G/(3.*!dpi))
  kgsm3_to_Msunyrpc3 = 1./(2.17d-27)
  kpc_to_m = 1./(3.24d-20)
  scale_sfr_max = 0.2455        ;=1.67d-27/2d30*(1./3024d-22)^3*1d-9
  
;;valeurs trop grande pour idl...
  scale = 0.475                 ;pour avoir le SFR en Msun/yr
;=alpha*(H/cc_to_kg/m3)^1.5*(kpc_to_m)^3*(kg/s_to_Msun/yr)
;=sqrt(32*6.67e-11/(3*pi))*(1.67e-27/1e-6)^1.5*(1/3.24e-20)^3*365*24*3600/2e30
  
;rhoSFR avant : valeurs AMR
;rhoSFR_avant = density_ascii*0.0
;t_sfr = where((density_ascii ge sfr_threshold) and (temperature_ascii le 1e4), nt_sfr)
;if nt_sfr gt 0 then rhoSFR_avant[t_sfr] = epsilon*alpha*(density_ascii[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3
;if SFR_avant eq 1 and lop eq 1 then begin
;t = where(rhoSFR_avant ne 0, nt)
;if verbose ne 0 and nt gt 0 then print, "Min/max rhoSFR pre-cloudy : ", minmax(rhoSFR_avant[t])
;if verbose ne 0 and nt le 0 then print, 'Pas de SFR pre-cloudy'
;strct_sfri = {p:strct.psfri, x:strct.xsfri, y:strct.ysfri}
;affiche = trace(rhoSFR_avant, pos1_all, pos2_all, 0, 'log
;'+textoidl('\rho_{SFR_i}')+' [M'+sunsymbol()+'
;yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (>
;'+strn(sfr_threshold)+' cm'+textoidl('^{-3}')+')', 'sfr',
;colorbar, pos1_agn, pos2_agn, pos1_as;cii, pos2_ascii,0, flag,
;strct_sfri, cb_pos, lmax, ps)
;endif

  SFR_tot_avant_amr = density_ascii*0.0
  for i = 0, (size(depth_ascii))[1]-1 do begin
     if density_ascii(i) ge sfr_threshold and temperature_ascii(i) le 1d4 then begin
        if i eq 0 then begin
           volume = depth_centered(0)*depth_centered(0)^2
           SFR_tot_avant_amr(0) = epsilon*(density_ascii(0))^n*volume*scale ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*density_ascii(0)*volume
           if SFR_tot_avant_amr(0) gt SFR_max then SFR_tot_avant_amr(0) = SFR_max
        endif
        if i ne 0 then begin
           volume = (depth_centered(i)-depth_centered(i-1))*((depth_centered(i)-depth_centered(i-1))^2)
           SFR_tot_avant_amr(i) = epsilon*(density_ascii(i))^n*volume*scale ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*density_ascii(i)*volume
           if SFR_tot_avant_amr(i) gt SFR_max then SFR_tot_avant_amr(i) = SFR_max
        endif
                                ; print, 'avant amr', sfr_tot_avant_amr(i), sfr_max, density_ascii(i), temperature_ascii(i), sfr_threshold, nm
     endif

  endfor
  t_milieu = where(depth_centered lt min(depth_centered_lop), nt_milieu)
  SFRavant_amr_total = total(SFR_tot_avant_amr) 
  if nt_milieu gt 0 then SFRavant_amr_total = SFRavant_amr_total - total(SFR_tot_avant_amr[t_milieu])
  
;help, depth_centered      
;help, sfr_tot_avant_amr
;help, x_ascii
                                ;rhoSFR avant : valeurs AMR resampled
  rhoSFR_avant = den_init*0.0
  t_sfr = where((den_init ge sfr_threshold) and (temp_init le 1e4), nt_sfr)
  if nt_sfr gt 0 then rhoSFR_avant[t_sfr] = epsilon*alpha*(den_init[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3*1d-3 ;Ms/yr/kpc3
  if rrhoSFR_avant eq 1   then begin
     t = where(rhoSFR_avant ne 0, nt)
     if verbose ne 0 and nt gt 0 then print, "Min/max rhoSFR pre-cloudy : ", minmax(rhoSFR_avant[t])
     if verbose ne 0 and nt le 0 then print, 'Pas de SFR pre-cloudy'

     if value eq 'compare_lum2'   then begin
        strct_sfri = {p:strct.pone, x:strct.xone, y:strct.yone} 
        signe_sfr = 'sfr_one'
     endif else begin
        strct_sfri = {p:strct.psfri, x:strct.xsfri, y:strct.ysfri}
        signe_sfr = 'sfr'
     endelse

     if  value eq 'all_den_temp_sfr' and nm eq '' then cb_pos = [0.197,0.01,0.23,0.32]

     affiche = trace(rhoSFR_avant, pos1_all, pos2_all, 0, 'log Initial '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']', signe_sfr, colorbar, pos1_agn, pos2_agn, pos1_ascii, pos2_ascii,0, flag, strct_sfri, cb_pos, lmax, ps)
  endif

  SFR_tot_avant = den_init*0.0
  for i = 0, (size(depth_all))[1]-1 do begin
     if den_init(i) ge sfr_threshold and temp_init(i) le 1d4 then begin
        if i eq 0 then begin
           volume = (depth_centered_resample(0)*depth_centered_resample(0)^2)
           SFR_tot_avant(0) = epsilon*(den_init(0))^n*volume*scale ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*den_init(0)*volume
           if SFR_tot_avant(0) gt SFR_max then SFR_tot_avant(0) = SFR_max
        endif
        if i ne 0 then begin
           volume = ((depth_centered_resample(i)-depth_centered_resample(i-1))*(depth_centered_resample(i)-depth_centered_resample(i-1))^2) ;;==cell_size resampled
           SFR_tot_avant(i) = epsilon*(den_init(i))^n*volume*scale                                                                          ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*den_init(i)*volume
           if SFR_tot_avant(i) gt SFR_max then SFR_tot_avant(i) = SFR_max
        endif
;print, 'avant resample', sfr_tot_avant(i), sfr_max, den_init(i), temp_init(i), sfr_threshold, nm
;stop

     endif
  endfor
  SFRavant_total = total(SFR_tot_avant)

;help, depth_centered_resample
;help, sfr_tot_avant
  
;SFR avant : valeurs AMR resampled
;t_sfr = where((den_init ge 10) and (temp_init le 1d4), nt_sfr)
;if nt_sfr gt 0 then rhoSFR_avant[t_sfr] =
;epsilon*alpha*(den_init[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3*1d-3 ; Ms/yr/kpc3
;  if SFR_avant eq 1   then begin ;amr
;     t = where(SFR_tot_avant_amr ne 0, nt)
;     if nt gt 0 then print, "Min/max SFR pre-cloudy : ", minmax(SFR_tot_avant_amr[t])
;     if nt le 0 then print, 'Pas de SFR pre-cloudy'
;     strct_sfri = {p:strct.psfri, x:strct.xsfri, y:strct.ysfri}
;     affiche = trace(SFR_tot_avant_amr, pos1_ascii,
;pos2_ascii, 0, 'log Initial SFR [M'+sunsymbol()+'
;yr'+textoidl('^{-1}')+'] (> '+strn(sfr_threshold)+'
;cm'+textoidl('^{-3}')+')', 'sfr', colorbar, pos1_agn, pos2_agn,
;pos1_ascii, pos2_ascii,0, flag, strct_sfri, cb_pos, lmax, ps)
; endif
  if SFR_avant eq 1   then begin ;amr resampled
     t = where(SFR_tot_avant ne 0, nt)
     if verbose eq 1 and nt gt 0 then print, "Min/max SFR pre-cloudy : ", minmax(SFR_tot_avant[t])
     if verbose eq 1 and nt le 0 then print, 'Pas de SFR pre-cloudy'
     strct_sfri = {p:strct.psfri, x:strct.xsfri, y:strct.ysfri}
     if  value eq 'all_den_temp_sfr' and nm eq '' then cb_pos = [0.197,0.01,0.23,0.32]
     affiche = trace(SFR_tot_avant, pos1_all, pos2_all, 0, 'log Initial SFR [M'+sunsymbol()+' yr'+textoidl('^{-1}')+'] (> '+strn(sfr_threshold)+' cm'+textoidl('^{-3}')+')', 'sfr', colorbar, pos1_agn, pos2_agn, pos1_ascii, pos2_ascii,0, flag, strct_sfri, cb_pos, lmax, ps)
     if value eq 'compare_lum2' then begin
        strct_sfri = {p:strct.pone, x:strct.xone, y:strct.yone}
        affiche = trace(SFR_tot_avant, pos1_all, pos2_all, 10, 'log Initial SFR [M'+sunsymbol()+' yr'+textoidl('^{-1}')+'] (> '+strn(sfr_threshold)+' cm'+textoidl('^{-3}')+')', 'sfr', colorbar, pos1_agn, pos2_agn, pos1_ascii, pos2_ascii,0, flag, strct_sfri, cb_pos,  lmax, ps)
     endif
  endif

  track_sfr_pos1 = -42
  track_sfr_pos2 = -42

  
;rhoSFR apres
  rhoSFR_apres = den_all*0.0
  t_sfr = where((den_all ge sfr_threshold) and (temp_post_cloudy le 1e4), nt_sfr)
  if nt_sfr gt 0 then rhoSFR_apres[t_sfr] = epsilon*alpha*(den_all[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3*1d-3 ;Ms/yr/kpc3

  t = where(rhoSFR_apres ne 0, nt)
;[0,0,0,1,1,2,1,1,0,0,1,0]
;       .       .     .
;left_contour_sfr = rhoSFR_apres*0
;right_contour_sfr = rhoSFR_apres*0
  if nt gt 0 then begin
     left_contour_sfr = rhoSFR_apres[t-1] ;on veut trouver les contours des regions de formation stellaire
     right_contour_sfr = rhoSFR_apres[t+1]
     track_sfr_pos1 = pos1_all[t]
     track_sfr_pos2 = pos2_all[t]
     
     t1 = where(left_contour_sfr eq 0 or right_contour_sfr eq 0, nt1)
     if nt1 gt 0 then track_sfr_pos1 = track_sfr_pos1[t1] else  track_sfr_pos1 = -42
     t2 = where(left_contour_sfr eq 0 or right_contour_sfr eq 0, nt2)
     if nt2 gt 0 then track_sfr_pos2 = track_sfr_pos2[t2] else  track_sfr_pos2 = -42
     
;help, track_sfr_pos1
;help, track_sfr_pos2
  endif

  if rrhoSFR_apres eq 1   then begin
     t = where(rhoSFR_apres ne 0, nt)
     if verbose ne 0 and nt gt 0 then print, "Min/max rhoSFR post-cloudy : ", minmax(rhoSFR_apres[t])
     if verbose ne 0 and nt le 0 then print, 'Pas de SFR post-cloudy'
     
     strct_sfrf = {p:strct.psfrf, x:strct.xsfrf, y:strct.ysfrf}
     if value eq 'compare_lum' or value eq 'all_den_temp_sfr' and nm ne '' then begin
        if nm eq 'x10_' then strct_sfrf = {p:strct.psfrfx10, x:strct.xsfrfx10, y:strct.ysfrfx10}
        if nm eq 'x100_' then strct_sfrf = {p:strct.psfrfx100, x:strct.xsfrfx100, y:strct.ysfrfx100}
     endif

     prev_colorbar = colorbar
     if value eq 'all_den_temp_sfr' and (nm eq '' or nm eq 'x10_') then colorbar = 0
     if  value eq 'all_den_temp_sfr' and nm eq 'x100_' then cb_pos = [0.894,0.01,0.93,0.32]
     affiche = trace(rhoSFR_apres, pos1_all, pos2_all, 0, 'log Final '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']', 'sfr', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_sfrf, cb_pos, lmax, ps)
     colorbar = prev_colorbar                                       
  endif

  
;print, 'min/max log rhoSFR_avant (ne 0)', minmax(alog10(rhoSFR_avant[where(rhoSFR_avant ne 0)]))
;print, 'min/max log rhoSFR_apres (ne 0)', minmax(alog10(rhoSFR_apres[where(rhoSFR_apres ne 0)]))

  if value eq 'compare_lum2' then begin
     if track_sfr_pos1(0) ne -42 and track_sfr_pos2(0) ne -42 then begin
        !p = strct.pone & !x = strct.xone & !y =strct.yone
        col_sfr = !black 
        if nm eq 'x10_' then col_sfr = !brown
        if nm eq 'x100_' then col_sfr = !red
        symb = sym(1)
        if nm eq 'x10_' then symb = sym(1)
        if nm eq 'x100_' then symb = sym(1)
        oplot, track_sfr_pos1, track_sfr_pos2, psym=symb, color=col_sfr
     endif
  endif
  
  SFR_tot_apres = den_all*0.0
  for i = 0, (size(den_post_cloudy))[1]-1 do begin
     if den_all(i) ge sfr_threshold and temp_post_cloudy(i) le 1d4 then begin
        if i eq 0 then begin
           volume = (depth_centered_lop(0)*depth_centered_resample(0)^2)
           SFR_tot_apres(0) = epsilon*(den_post_cloudy(0))^n*volume*scale ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*den_post_cloudy(0)*volume
           if SFR_tot_apres(0) gt SFR_max then SFR_tot_apres(0) = SFR_max
        endif
        if i ne 0 then begin
           volume = ((depth_centered_lop(i)-depth_centered_lop(i-1))*(depth_centered_resample(i)-depth_centered_resample(i-1))^2)
           SFR_tot_apres(i) = epsilon*(den_post_cloudy(i))^n*volume*scale ;Msun/yr
           SFR_max = 0.3*9.9*scale_sfr_max*den_post_cloudy(i)*volume
           if SFR_tot_apres(i) gt SFR_max then SFR_tot_apres(i) = SFR_max
        endif
     endif
  endfor
  SFRapres_total = total(SFR_tot_apres)



;window, 5
;!p.multi=0
;plot, depth_ascii, density_ascii, /xlog, /ylog, xr=[1e-3,50], yr=[1e-6,1e6]
;oplot, depth_all, den_all, color=!red
;oplot, depth_all, den_init, color=!blue


;window, 6
;!p.multi=0
;plot, depth_ascii, temperature_ascii, /xlog, /ylog, xr=[1e-3,50], yr=[1e2,1e8] ;amr
;oplot, depth_all, temp_all, color=!magenta ;cloudy
;oplot, depth_all, temp_init, color=!blue ;amr resampled
;oplot, depth_all, temp_post_cloudy, color=!red, psym=-3 ;max (T_cloudy,T_amr_resampled)

;help, depth_all
;help, temp_all
;help, temp_post_cloudy
;help, temp_init

;print, abs(temp_post_cloudy-temp_all)/temp_post_cloudy*100

;stop
  
;rhoSFR_apres
;rhoSFR_apres = den_all*0.0
;print, hden, density
;print, ','
;print, temp_post_cloudy
;t_sfr = where(den_post_cloudy ge 10 and temp_post_cloudy le 1d4, nt_sfr)
;if nt_sfr gt 0 then rhoSFR_apres[t_sfr] = epsilon*alpha*(den_post_cloudy[t_sfr]*hcc_to_kgm3)^n*kgsm3_to_Msunyrpc3
;SFRapres_total = total(rhoSFR_apres)
  if SFR_apres eq 1   then begin
     t = where(SFR_tot_apres ne 0, nt)
     if verbose eq 1 and nt gt 0 then print, "Min/max SFR post-cloudy : ", minmax(SFR_tot_apres[t])
     if verbose eq 1 and nt le 0 then print, 'Pas de SFR post-cloudy'
     strct_sfrf = {p:strct.psfrf, x:strct.xsfrf, y:strct.ysfrf}
     if value eq 'compare_lum' or value eq 'all_den_temp_sfr' and nm ne '' then begin
        if nm eq 'x10_' then strct_sfrf = {p:strct.psfrfx10, x:strct.xsfrfx10, y:strct.ysfrfx10}
        if nm eq 'x100_' then strct_sfrf = {p:strct.psfrfx100, x:strct.xsfrfx100, y:strct.ysfrfx100}
     endif

     prev_colorbar = colorbar
     if value eq 'all_den_temp_sfr' and (nm eq '' or nm eq 'x10_') then colorbar = 0
     if  value eq 'all_den_temp_sfr' and nm eq 'x100_' then cb_pos = [0.894,0.01,0.93,0.32]
     affiche = trace(SFR_tot_apres, pos1_all, pos2_all, 0, 'log Final SFR [M'+sunsymbol()+' yr'+textoidl('^{-1}')+'] (> '+strn(sfr_threshold)+' cm'+textoidl('^{-3}')+')', 'sfr', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_sfrf, cb_pos, lmax, ps)
     colorbar = prev_colorbar
  endif
  

;carte de densite AMR resampled
  if densite eq 1   then begin
     strct_den = {p:strct.pden, x:strct.xden, y:strct.yden}
     if  value eq 'all_den_temp_sfr' and nm eq '' then cb_pos = [0.197,0.65,0.23,0.96]
     if strmatch(value, '*den_temp_sfr') eq 1 or value eq 'den_em' then affiche = trace(den_init, pos1_all, pos2_all, 0, 'log Gas density [cm'+textoidl('^{-3}')+']', 'density', colorbar, pos1_agn, pos2_agn, pos1, pos2,0, flag, strct_den, cb_pos, lmax, ps)
     
;;;valeurs originales AMR avant resample :
;if value eq 'den_temp_sfr' then affiche = trace(density_ascii, pos1,
;pos2, 0, 'log Gas density [H/cc]', 'density', colorbar, pos1_agn,
;pos2_agn, pos1, pos2,0, flag, strct_den, cb_pos,  lmax, ps)
;if value eq 'den_em' then affiche = trace(density_ascii, pos1, pos2,
;1, 'log Gas density [H/cc]', 'density', colorbar, pos1_agn, pos2_agn,
;pos1, pos2,0, flag, strct_den, cb_pos,  lmax, ps)
  endif
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  if Hfrac eq 1   then begin
     if verbose eq 1 then print, "Min/max de Hneutre : ", minmax(Hneutre_all)
     strct_hfr = {p:strct.phfr, x:strct.xhfr, y:strct.yhfr}
     if value eq 'compare_lum' or value eq 'all_den_temp_sfr' and nm ne '' then begin
        if nm eq 'x10_' then strct_hfr = {p:strct.phfrx10, x:strct.xhfrx10, y:strct.yhfrx10}
        if nm eq 'x100_' then strct_hfr = {p:strct.phfrx100, x:strct.xhfrx100, y:strct.yhfrx100}
     endif

     prev_colorbar = colorbar
     if value eq 'all_den_temp_sfr' and (nm eq '' or nm eq 'x10_') then colorbar = 0
                                ;left, bottom, right, top
     if  value eq 'all_den_temp_sfr' and nm eq 'x100_' then cb_pos = [0.894,0.65,0.93,0.96]

     affiche = trace(Hneutre_all, pos1_all, pos2_all, 0, 'log Fraction of neutral H', 'Hfrac', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_hfr, cb_pos, lmax, ps)
     colorbar = prev_colorbar
  endif
;on s'interesse au critere 'Hionise > 0.9 == region ionisee' ==
;'Hneutre < 0.1 == region ionisee'
;---> TOUT CE QUI N EST PAS NOIR-BORDEAUX FONCE EST IONISE. 
;BLANC == 0.0 % de neutre (pour H, il n'y a pas de valeurs
;vraiment a 0 mais pour O, il y en a)
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;cartes avec O
  if Ofrac eq 1   then begin
     if verbose eq 1 then print, "Min/max de Oneutre : ", minmax(Oneutre_all)
     strct_ofr = {p:strct.pofr, x:strct.xofr, y:strct.yofr}
     affiche = trace(Oneutre_all, pos1_all, pos2_all, 0, 'log Fraction of neutral O', 'Ofrac', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_ofr, cb_pos, lmax, ps)
  endif
;nb : le critere neutre/ionise dans plot_phyc.pro est pour H --> 90 % de H ionise
  
  
;building OIII emissivity map (emergent/intrinsic)
  fill_fact = 1./5.
  
;The first column is the depth [cm] into the cloud. The remaining
;columns give the volume emissivity [erg/cm3/s] for each line. 
;The intensity is for a fully filled volume so the saved intensity 
;should be ***multiplied by the filling factor to compare with
;observations of a clumpy medium.***
  
;cartes de l'emissivite emergente 
;cartes de OIII/Hbeta et NII/Halpha
  if Em_em eq 1   then begin  
     strct_emo = {p:strct.pemo, x:strct.xemo, y:strct.yemo}
     if verbose eq 1 then print, 'Min/max de OIII em em', minmax(OIII_5007A_all*fill_fact)  
     affiche = trace(10d^(OIII_5007A_all*fill_fact), pos1_all, pos2_all, 0, 'Emergent emissivity of OIII5007', 'emissivity', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_emo, cb_pos, lmax, ps)
     strct_emn = {p:strct.pemn, x:strct.xemn, y:strct.yemn}
     if verbose eq 1 then print, 'Min/max de NII em em', minmax(NII_6584A_all*fill_fact)  
     affiche = trace(10d^(NII_6584A_all*fill_fact), pos1_all, pos2_all, 0, 'Emergent emissivity of NII6584'  , 'emissivity', colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_emn, cb_pos, lmax, ps)
  endif
  
  
;/!\ ne sont pas dans post_cloudy_parameters.dat /!\
;cartes de l'emissivite intrinseque
;if Int_em eq 1   then begin
;   print, 'Min/max de OIII em int ', minmax(OIII_5007A_int*fill_fact)
;   strct_ino = {p:strct.pino, x:strct.xino, y:strct.yino}
;   affiche = trace(10d^(OIII_5007A_int*fill_fact), pos1_cloudy,
;   pos2_cloudy, 1, 'Intrinsic emissivity of OIII5007', 'emissivity',
;   colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_ino, cb_pos, lmax, ps)
;   print, 'Min/max de NII int', minmax(NII_6584A_int*fill_fact) 
;   strct_inn = {p:strct.pinn, x:strct.xinn, y:strct.yinn} 
;   affiche = trace(10d^(NII_6584A_int*fill_fact), pos1_cloudy,
;   pos2_cloudy, 1, 'Intrinsic emissivity of NII6584', 'emissivity',
;   colorbar, pos1_agn, pos2_agn, pos1, pos2, 0, flag, strct_inn, cb_pos, lmax, ps)
;endif


  t = where(Hb_4861A_all eq 0 and OIII_5007A_all eq 0, nt)
;print, nt
;print, depth_all[t]
  if nt gt 0 then begin
     max_depth = depth_all(t(0)-1) 

;if (depth_all(t(0)) - max_depth) gt 50d/2d^lmax then begin
;print, 'ECART TROP GRAND pour la LOP ', lop_name(0)

     print, 'ECART pour la LOP ', nm+lop_name(0), ' : ', (depth_all(t(0)) - max_depth)
;stop
;endif
  endif


  if value eq 'colden' then begin
     return, [colden_amr_total, mass_amr_total, colden_resample_total, mass_resample_total, colden_post_cloudy_total, mass_post_cloudy_total, colden_chauffe_total, mass_chauffe_total, SFRavant_total, SFRapres_total, SFRavant_amr_total]
  endif else begin
     return, transpose([[pos1_all],[pos2_all],[den_init],[temp_init],[temp_post_cloudy],[rhoSFR_avant],[SFR_tot_avant],[rhoSFR_Apres],[SFR_tot_apres],[Hneutre_all],[Oneutre_all],[OIII_5007A_all],[NII_6584A_all]])
  endelse

  
end

pro compare_luminosities_interp, sim=sim, plane=plane, zoom=zoom, factor=factor, threshold=threshold, ps=ps, what_to_plot=what_to_plot, quasar=quasar, contour=contour, interp=interp, temp_choice=temp_choice, lmax_name=lmax_name, boxlen=boxlen
;#############################################################################################
;ce programme remplace 2D_maps.pro
; il permet de faire les memes cartes mais sans passer par tous les
; fichiers cloudy --> utilise juste les post_cloudy_parameters.dat et les ascii_parameters.dat
;+ carte interpolee pour faire disparaitre les lop et les trous
;#############################################################################################


;----------------------------------------------------------------------------------------------PARAMETRES
                                ;temp_choice = 'delta_T' ; 'delta_T' ; 'heating_rate' ; 'temperature'

  colden = 0
  offset = 0                    ;offset = 25
  compact = 1                   ;compact = 0
                                ;WHAT_TO_PLOT =   'compare_lum2' ;'compare_lum' ;'den_temp_sfr' ;'den_em'       ;'colden' ;
;---------------------------------------------------------------------------------------------------------
  
  if compact ne 0 then nm_compact = '_comp' else nm_compact = ''
  
  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
  endif else begin
     nm = ''
  endelse
  if not keyword_set(contour) then contour = -42
  if not keyword_set(lmax_name) then lmax_name='13'
  if not keyword_set(boxlen) then boxlen = 50 ;kpc
  if not keyword_set(threshold) then threshold = 10
  if not keyword_set(plane) then plane = 'dk'
  if not keyword_set(what_to_plot) then what_to_plot = 'all_den_temp_sfr'
  if not keyword_set(temp_choice) then temp_choice = 'heating_rate'
  if not keyword_set(zoom) then zoom = 82
  if not keyword_set(sim) then sim = '00100'
  ps = keyword_set(ps)
  if keyword_set(quasar) then begin
     qsr = 'quasar_'
     if quasar eq 0 or quasar eq '' then qsr=''
  endif else begin
     qsr = ''
  endelse
  if not keyword_set(interp) then interp = 1

  if interp eq 1 then interpf = '_interp2' else interpf = ''
  zoom = strcompress(string(fix(zoom)),/remove_all)
  if WHAT_TO_PLOT eq 'compare_lum' or WHAT_TO_PLOT eq 'compare_lum2' then nm = 'compare_lum_'

  if sim eq '00100' or sim eq '00075w' $
     or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
  if sim eq '00085' or sim eq '00075n' then name = 'No'

  if lmax_name eq '12' then begin
     directory = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/'
     directory_sink = directory + 'output/'
     res = 'lores_'
  endif else begin
     directory = '/Users/oroos/Post-stage/'
     directory_sink = '/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/'
     res = ''
  endelse
  
  if plane eq 'xz' then dir = 'y'
  if plane eq 'zy' then dir = 'x'
  if plane eq 'dk' then dir = 'z'
  
  den_map=readfits(directory_sink+'map_'+sim+'_rho_central_'+dir+'_thinslice_lmax'+lmax_name+'.fits.gz', header) 
  
;xz : abscisse x, ordonnee z
;zy : abscisse z, ordonnee y
  taille_pixel = sxpar(header, 'cdelt1')
  if taille_pixel ne sxpar(header, 'cdelt2') then stop
  x_kpc=sxpar(header, 'xsink')
  y_kpc=sxpar(header, 'ysink')
  z_kpc=sxpar(header, 'zsink')
  
  lmax=sxpar(header, 'lmax')
  lmax = strcompress(string(fix(lmax)),/remove_all)
  
  n_pixel = sxpar(header, 'naxis1')
  if n_pixel ne sxpar(header, 'naxis2') then stop
  
  
  if plane eq 'xz' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = z_kpc - offset
  endif
  if plane eq 'yz' or plane eq 'zy' then begin
     pos1_agn = z_kpc - offset
     pos2_agn = y_kpc - offset
     plane = 'zy'
  endif 
  if plane eq 'dk' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = y_kpc - offset
  endif 

  if temp_choice eq 'temperature' then tmp_choice = '' else tmp_choice = temp_choice + '_'  

;ouverture de l'arriere-plan :
  if ps ne 0 then begin
     if WHAT_TO_PLOT eq 'den_temp_sfr' then ps_start, nm+qsr+res+tmp_choice+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=29, ysize=31, /helvetica, /bold
     if WHAT_TO_PLOT eq 'compare_lum' then ps_start, nm+qsr+res+tmp_choice+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=28, ysize=35, /helvetica, /bold
     if WHAT_TO_PLOT eq 'compare_lum2' then ps_start, nm+qsr+res+tmp_choice+'comparison_of_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=28, ysize=20, /helvetica, /bold   
     if WHAT_TO_PLOT eq 'all_den_temp_sfr' then ps_start, nm+qsr+res+tmp_choice+'all_den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=45, ysize=28.45, /helvetica, /bold
     
     if WHAT_TO_PLOT eq 'den_em' then begin
        ps_start, nm+qsr+res+'den_em'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=50, ysize=30, /helvetica, /bold
        threshold = 0
     endif
     !p.font=0
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1.5
     !y.charsize=1.5
     !x.thick=30
     !y.thick=30
     !p.thick=6
  endif
  if WHAT_TO_PLOT eq 'colden' then begin
     win=0
     colden = 1
     threshold = 0
  endif else begin
     
     win = create_windows(den_map, x_kpc, y_kpc, z_kpc, sim, plane, zoom, 0, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, ps)
     
  endelse


  if nm eq 'compare_lum_' or WHAT_TO_PLOT eq 'all_den_temp_sfr' then nm = ['', 'x10_', 'x100_']  
  for jj = 0, n_elements(nm)-1 do begin
     colorbar = 1

     print, 'Snapshot : ', nm(jj)+sim+plane

;ouverture des fichiers de lop pour le remplissage
     print, 'ouverture fichier post'
     file_name_post = directory+'/LOPs'+sim+'/'+qsr+res+'post_cloudy_parameters_'+nm(jj)+sim+'_all.dat'
                                ;print, file_name_post
     
     dat_file_info = file_info(file_name_post)
     sav_file_info = file_info(directory+'LOPs'+sim+'/'+qsr+res+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav')
                                ;print, directory+'LOPs'+sim+'/'+qsr+res+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'


     if dat_file_info.mtime gt sav_file_info.mtime then begin
        readcol, file_name_post, name_lop, x_all, y_all, z_all, theta_all, phi_all, depth_all, den_all, temp_all, $
                 Hneutre_all, Oneutre_all, Hb_4861A_all, OIII_5007A_all, Ha_6563A_all, NII_6584A_all, remaining_lop_points, flag, $
                 format='(A35,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A3)'
        save, name_lop, x_all, y_all, z_all, theta_all, phi_all, depth_all, den_all, temp_all, $
              Hneutre_all, Oneutre_all, Hb_4861A_all, OIII_5007A_all, Ha_6563A_all, NII_6584A_all, remaining_lop_points, flag, $
              file=directory+'LOPs'+sim+'/'+qsr+res+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endif else begin
        restore, directory+'LOPs'+sim+'/'+qsr+res+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endelse
     print, 'fin lecture fichier post'
     
     print, 'ouverture fichier ascii'
     file_name_ascii = directory+'LOPs'+sim+'/'+qsr+res+'ascii_parameters_'+nm(jj)+sim+'_all.dat'
                                ;print, file_name_ascii
     
     dat_file_info = file_info(file_name_ascii)
     sav_file_info = file_info(directory+'LOPs'+sim+'/'+qsr+res+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav')
                                ;print, directory+'LOPs'+sim+'/'+qsr+res+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     if dat_file_info.mtime gt sav_file_info.mtime then begin
        readcol, file_name_ascii, name_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, $
                 cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, remaining_lop_points_ascii, flag_ascii, $
                 format='(A35,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A3)'
        save, name_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, $
              cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, remaining_lop_points_ascii, flag_ascii, file=directory+'LOPs'+sim+'/'+qsr+res+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endif else begin
        restore, directory+'LOPs'+sim+'/'+qsr+res+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endelse
     print, 'fin lecture fichier ascii'


     if plane eq 'xz' then t = where(strmatch(name_lop, '*xz*') eq 1 or strmatch(name_lop, '*px*') eq 1 or strmatch(name_lop, '*cx*') eq 1, nt)
     if plane eq 'zy' then t = where(strmatch(name_lop, '*zy*') eq 1 or strmatch(name_lop, '*py*') eq 1 or strmatch(name_lop, '*cy*') eq 1, nt)
     if plane eq 'dk' then t = where(strmatch(name_lop, '*dk*') eq 1, nt) ; or strmatch(name_lop, '*px*') eq 1 or strmatch(name_lop, '*py*') eq 1, nt)
     
     print, nt, ' points trouves pour la simulation ', qsr+res+nm(jj)+sim+plane
     
     if nt gt 0 then begin      ;optimiser ca 
        x_all = x_all[t]
        y_all = y_all[t]
        z_all = z_all[t]
        theta_all = theta_all[t] 
        phi_all = phi_all[t]
        depth_all = depth_all[t]
        den_all = den_all[t]
        temp_all = temp_all[t]
        Hneutre_all = Hneutre_all[t]
        Oneutre_all = Oneutre_all[t]
        Hb_4861A_all = Hb_4861A_all[t]
        OIII_5007A_all = OIII_5007A_all[t]
        Ha_6563A_all = Ha_6563A_all[t]
        NII_6584A_all = NII_6584A_all[t]
        remaining_lop_points = remaining_lop_points[t] 
        flag = flag[t]
        name_lop = name_lop[t]
     endif
     
     if plane eq 'xz' then t = where(strmatch(name_ascii, '*xz*') eq 1 or strmatch(name_ascii, '*px*') eq 1 or strmatch(name_ascii, '*cx*') eq 1, nt)
     if plane eq 'zy' then t = where(strmatch(name_ascii, '*zy*') eq 1 or strmatch(name_ascii, '*py*') eq 1 or strmatch(name_ascii, '*cy*') eq 1, nt)
     if plane eq 'dk' then t = where(strmatch(name_ascii, '*dk*') eq 1 , nt) ;or strmatch(name_ascii, '*px*') eq 1 or strmatch(name_ascii, '*py*') eq 1, nt)
     if nt gt 0 then begin
        depth_ascii = depth_ascii[t]
        density_ascii = density_ascii[t]
        temperature_ascii  = temperature_ascii[t] 
        remaining_lop_points_ascii = remaining_lop_points_ascii[t] 
        x_ascii = x_ascii[t]
        y_ascii = y_ascii[t]
        z_ascii = z_ascii[t]
        vx_ascii = vx_ascii[t]
        vy_ascii = vy_ascii[t]
        vz_ascii = vz_ascii[t]
        cell_size_ascii = cell_size_ascii[t] 
        x_lop_ascii = x_lop_ascii[t]
        y_lop_ascii = y_lop_ascii[t]
        z_lop_ascii = z_lop_ascii[t]
        theta_lop_ascii =  theta_lop_ascii[t]
        phi_lop_ascii = phi_lop_ascii[t]
        flag_ascii = flag_ascii[t]
        name_ascii = name_ascii[t]
     endif
     
     
     if colden eq 1 then begin
        get_lun, col_den
        openw, col_den, directory+'LOPs'+sim+'/'+nm(jj)+qsr+res+'total_column_densities_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'.txt'
        totl = 0.
     endif
     
;;;probleme : il faut separer les lop pour les tracer...
     t_other = where(remaining_lop_points eq 0, nt_other)
     print, nt_other, ' LOPs differentes pour la simulation ', qsr+res+sim+plane
     
     t_other_ascii = where(remaining_lop_points_ascii eq 0, nt_other_ascii)
     print, nt_other_ascii, ' LOPs ascii differentes pour la simulation ', qsr+res+sim+plane
     
     if nt_other ne nt_other_ascii then begin
        print, "Attention, le nombre de LOP avant et apres CLOUDY n'est pas le meme... PROBLEME ???"
        stop
     endif

;##########################################################################################; 
;                                                                                          ;
;################################ DEBUT DE LA BOUCLE ######################################;
;                                                                                          ;
;##########################################################################################;

     for i = 0, nt_other-1 do begin ;15 do begin;
        
        if i eq 0 then begin    ;optimiser ca
           x_all_1 = x_all(0:t_other(0))
           y_all_1 = y_all(0:t_other(0))
           z_all_1 = z_all(0:t_other(0))
           theta_all_1 = theta_all(0:t_other(0)) 
           phi_all_1 = phi_all(0:t_other(0))
           depth_all_1 = depth_all(0:t_other(0))
           den_all_1 = den_all(0:t_other(0))
           temp_all_1 = temp_all(0:t_other(0))
           Hneutre_all_1 = Hneutre_all(0:t_other(0))
           Oneutre_all_1 = Oneutre_all(0:t_other(0))
           Hb_4861A_all_1 = Hb_4861A_all(0:t_other(0))
           OIII_5007A_all_1 = OIII_5007A_all(0:t_other(0))
           Ha_6563A_all_1 = Ha_6563A_all(0:t_other(0))
           NII_6584A_all_1 = NII_6584A_all(0:t_other(0))
           remaining_lop_points_1 = remaining_lop_points(0:t_other(0))
           flag_1 = flag(0:t_other(0))
           name_lop_1 = name_lop(0:t_other(0))
           
           depth_ascii_1 = depth_ascii(0:t_other_ascii(0))
           density_ascii_1 = density_ascii(0:t_other_ascii(0))
           temperature_ascii_1 = temperature_ascii(0:t_other_ascii(0))
           x_ascii_1 = x_ascii(0:t_other_ascii(0))
           y_ascii_1 = y_ascii(0:t_other_ascii(0))
           z_ascii_1 = z_ascii(0:t_other_ascii(0))
           vx_ascii_1 = vx_ascii(0:t_other_ascii(0))
           vy_ascii_1 = vy_ascii(0:t_other_ascii(0))
           vz_ascii_1 = vz_ascii(0:t_other_ascii(0))
           cell_size_ascii_1 = cell_size_ascii(0:t_other_ascii(0)) 
           x_lop_ascii_1 = x_lop_ascii(0:t_other_ascii(0))
           y_lop_ascii_1 = y_lop_ascii(0:t_other_ascii(0))
           z_lop_ascii_1 = z_lop_ascii(0:t_other_ascii(0))
           theta_lop_ascii_1 =  theta_lop_ascii(0:t_other_ascii(0))
           phi_lop_ascii_1 = phi_lop_ascii(0:t_other_ascii(0))
           remaining_lop_points_ascii_1 = remaining_lop_points_ascii(0:t_other_ascii(0))
           flag_ascii_1 = flag_ascii(0:t_other_ascii(0))
           name_ascii_1 = name_ascii(0:t_other_ascii(0))
           
        endif else begin        ;et ca
           x_all_1 = x_all(t_other(i-1)+1:t_other(i))
           y_all_1 = y_all(t_other(i-1)+1:t_other(i))
           z_all_1 = z_all(t_other(i-1)+1:t_other(i))
           theta_all_1 = theta_all(t_other(i-1)+1:t_other(i)) 
           phi_all_1 = phi_all(t_other(i-1)+1:t_other(i))
           depth_all_1 = depth_all(t_other(i-1)+1:t_other(i))
           den_all_1 = den_all(t_other(i-1)+1:t_other(i))
           temp_all_1 = temp_all(t_other(i-1)+1:t_other(i))
           Hneutre_all_1 = Hneutre_all(t_other(i-1)+1:t_other(i))
           Oneutre_all_1 = Oneutre_all(t_other(i-1)+1:t_other(i))
           Hb_4861A_all_1 = Hb_4861A_all(t_other(i-1)+1:t_other(i))
           OIII_5007A_all_1 = OIII_5007A_all(t_other(i-1)+1:t_other(i))
           Ha_6563A_all_1 = Ha_6563A_all(t_other(i-1)+1:t_other(i))
           NII_6584A_all_1 = NII_6584A_all(t_other(i-1)+1:t_other(i))
           remaining_lop_points_1 = remaining_lop_points(t_other(i-1)+1:t_other(i))
           flag_1 = flag(t_other(i-1)+1:t_other(i))
           name_lop_1 = name_lop(t_other(i-1)+1:t_other(i))

           depth_ascii_1 = depth_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           density_ascii_1 = density_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           temperature_ascii_1 = temperature_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           x_ascii_1 = x_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           y_ascii_1 = y_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           z_ascii_1 = z_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           vx_ascii_1 = vx_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           vy_ascii_1 = vy_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           vz_ascii_1 = vz_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           cell_size_ascii_1 = cell_size_ascii(t_other_ascii(i-1)+1:t_other_ascii(i)) 
           x_lop_ascii_1 = x_lop_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           y_lop_ascii_1 = y_lop_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           z_lop_ascii_1 = z_lop_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           theta_lop_ascii_1 =  theta_lop_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           phi_lop_ascii_1 = phi_lop_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           remaining_lop_points_ascii_1 = remaining_lop_points_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           flag_ascii_1 = flag_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
           name_ascii_1 = name_ascii(t_other_ascii(i-1)+1:t_other_ascii(i))
        endelse

        if unique(name_lop_1,/sort) ne unique(name_ascii_1,/sort) then stop
        
;print, remaining_lop_points_1, remaining_lop_points_ascii_1  
        
        if colden eq 1 then begin  
;                     0               1              2                     3                    4                         5                         6                7
;;;return, [colden_amr_total, mass_amr_total, colden_resample_total,
;;;mass_resample_total, colden_post_cloudy_total,
;;;mass_post_cloudy_total, colden_chauffe_total, mass_chauffe_total,
;;;SFRavant_total, SFRapres_total, SFRavant_amr_total]
;               8              9         10
           
           
           totl = totl + read_LOPs(x_all_1, y_all_1, z_all_1, theta_all_1, phi_all_1, depth_all_1, den_all_1, temp_all_1, $
                                   Hneutre_all_1, Oneutre_all_1, Hb_4861A_all_1, OIII_5007A_all_1, Ha_6563A_all_1, NII_6584A_all_1, x_kpc, y_kpc, z_kpc, $
                                   depth_ascii_1, density_ascii_1, temperature_ascii_1, cell_size_ascii_1, $
                                   x_ascii_1, y_ascii_1, z_ascii_1, vx_ascii_1, vy_ascii_1, vz_ascii_1, $
                                   x_lop_ascii_1, y_lop_ascii_1, z_lop_ascii_1, theta_lop_ascii_1, phi_lop_ascii_1, flag_1, name_lop_1, $
                                   0, sim, plane, colorbar, WHAT_TO_PLOT, 0, nm(jj), header, threshold, offset,  temp_choice, ps)
        endif else begin
           ionfrac_map = read_LOPs(x_all_1, y_all_1, z_all_1, theta_all_1, phi_all_1, depth_all_1, den_all_1, temp_all_1, $
                                   Hneutre_all_1, Oneutre_all_1, Hb_4861A_all_1, OIII_5007A_all_1, Ha_6563A_all_1, NII_6584A_all_1, x_kpc, y_kpc, z_kpc, $
                                   depth_ascii_1, density_ascii_1, temperature_ascii_1, cell_size_ascii_1, $
                                   x_ascii_1, y_ascii_1, z_ascii_1, vx_ascii_1, vy_ascii_1, vz_ascii_1, $
                                   x_lop_ascii_1, y_lop_ascii_1, z_lop_ascii_1, theta_lop_ascii_1, phi_lop_ascii_1, flag_1, name_lop_1, $
                                   0, sim, plane, colorbar, WHAT_TO_PLOT, win, nm(jj), header, threshold, offset,  temp_choice, ps)
           
           if i eq 0 then all_params = ionfrac_map else all_params = [[all_params],[ionfrac_map]]
           
        endelse
        
        colorbar = 0
        
     endfor
     
     if  WHAT_TO_PLOT eq 'den_temp_sfr' and contour(0) ne -42   then begin
        
        if offset ne 0 then begin
           if zoom eq 0 then map = den_map[0:n_pixel-1,0:n_pixel-1] else map = den_map[fix(n_pixel/2.)-zoom:fix(n_pixel/2.)+zoom,fix(n_pixel/2.)-zoom:fix(n_pixel/2.)+zoom]
        endif else begin
           if zoom eq 0 then map = den_map else map = den_map[fix((pos1_agn-7.5)/taille_pixel)-zoom:fix((pos1_agn-7.5)/taille_pixel)+zoom,fix((pos2_agn-7.5)/taille_pixel)-zoom:fix((pos2_agn-7.5)/taille_pixel)+zoom] ;attention a ca : ne fonctionne par pour d'autres zooms/tailles de boite AMR
        endelse
        
        
        if ps eq 0 then wset, 0
        if ps ne 0 then c_thick = 15 else c_thick = 2
;pden:p1,xden:x1,yden:y1,phfr:p2,xhfr:x2,yhfr:y2,pti:p3,xti:x3,yti:y3,ptf:p4,xtf:x4,ytf:y4,psfri:p5,xsfri:x5,ysfri:y5,psfrf:p6,xsfrf:x6,ysfrf:y6
        !p = win.pden & !x = win.xden & !y = win.yden
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.phfr & !x = win.xhfr & !y = win.yhfr
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.pti & !x = win.xti & !y = win.yti
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.ptf & !x = win.xtf & !y = win.ytf
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfri & !x = win.xsfri & !y = win.ysfri
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfrf & !x = win.xsfrf & !y = win.ysfrf
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        
     endif
     
;------------------------------------------------------------------------
;interpolation
     
     if interp eq 1 then begin
;zoom=82 <=> zoom * 2 * resol (6pc) = 1 kpc
;pour 1 kpc on veut des pixels d'environ 2*resol --> 82 pixels
        
        resol= boxlen/2^lmax    ;kpc
        eps = 2d*resol          ;kpc
        div = 10.
        
        
        if zoom lt 1000 then map = dblarr(zoom,zoom) else map = dblarr(round(zoom/div),round(zoom/div))
        if zoom ge 1000 then      eps = eps*div
        
        
        if jj eq 0 then log_maps = interp_maps(all_params, jj, pos1_agn, pos2_agn, zoom, taille_pixel, what_to_plot, eps, map, temp_choice) else $
           log_maps = CREATE_STRUCT(log_maps, interp_maps(all_params, jj, pos1_agn, pos2_agn, zoom, taille_pixel, what_to_plot, eps, map, temp_choice)) 
        
        if what_to_plot eq 'den_em' or what_to_plot eq 'den_temp_sfr' or ps eq 0 then begin
           xs = 800
           ys=xs
           if what_to_plot eq 'den_em' then xs = 1200
           if what_to_plot eq 'all_den_temp_sfr' then xs = 1000
           if what_to_plot eq 'all_den_temp_sfr' then ys = 0.196/0.31*xs
           if what_to_plot eq 'compare_lum2' then ys = 500
           if what_to_plot eq 'compare_lum2' then xs = 700
           if ps eq 0 then window, 1, xsize=xs, ysize=ys, ypos=245, xpos=0
           
;;;if jj eq 0 then window, 1, xsize = xs, ysize = ys, ypos=50 else
;;;wset, 1
           
           if ps ne 0 then begin
              !p.font=0
              !p.charsize=5 
              !p.charthick=2
              !x.charsize=1.5
              !y.charsize=1.5
              !p.thick=6
           endif
           plot = interp_windows(log_maps, x_kpc, y_kpc, z_kpc, sim, plane, zoom, 1, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, interp, contour, temp_choice, ps)
        endif
;------------------------------------------------------------------------
        
     endif
     
;NOTE :
;les valeurs "avant" amr et amr resampled ne sont pas egales quand le volume est pris en compte dans le calcul (masse, sfr) car l'echantillonnage est different
;l'important est de toujours comparer deux valeurs qui ont le meme echantillonnage
;---> valeurs amr resampled et valeurs post-cloudy
;apres, on peut comparer les rapports entre eux car l'echantillonnage se simplifie
     
     if colden eq 1 then begin
        print, 'AMR values : '
        print, 'Total column density in cm-2 : ', totl(0)
        print, 'Total mass in Msun : ', totl(1)
        print, ''
        print, 'AMR resampled values : '
        print, 'Total column density in cm-2 : ', totl(2)
        print, 'Total mass in Msun : ', totl(3)
        print, ''
        print, 'Post-Cloudy values (max Cloudy/AMR resampled) : '
        print, 'Total column density in cm-2 : ', totl(4)
        print, 'Total mass in Msun : ', totl(5)
        print, ''
        print, 'Column density of heated gas in cm-2 : ', totl(6)
        print, 'colden_heated(post-CLOUDY)/colden_total(post-CLOUDY) = ', totl(6)/totl(4)
        print, 'colden_heated(post-CLOUDY)/colden_total(AMR resampled) = ', totl(6)/totl(2)
        print, ''
        print, 'Mass of heated gas in Msun : ', totl(7)
        print, 'M_heated(post-CLOUDY)/M_total(post-CLOUDY) = ', totl(7)/totl(5)
        print, 'M_heated(post-CLOUDY)/M_total(AMR resampled) = ', totl(7)/totl(3)
        print, ''
        print, 'Initial SFR (AMR) in Msun/yr : ', totl(10) ;NB : sont completement differents a cause de l'echantillonnage
        print, 'Initial SFR (AMR resampled) in Msun/yr : ', totl(8)
        print, 'Final SFR (post-cloudy) in Msun/yr : ', totl(9)
        print, 'SFR ratio final(post-cloudy)/initial(AMR) : ', totl(9)/totl(10)
        print, 'SFR ratio final(post-cloudy)/initial(AMR resampled) : ', totl(9)/totl(8)
        
        printf, col_den, 'AMR values : '
        printf, col_den, 'Total column density in cm-2 : ', totl(0)
        printf, col_den, 'Total mass in Msun : ', totl(1)
        printf, col_den, ''
        printf, col_den, 'AMR resampled values : '
        printf, col_den, 'Total column density in cm-2 : ', totl(2)
        printf, col_den, 'Total mass in Msun : ', totl(3)
        printf, col_den, ''
        printf, col_den, 'Post-Cloudy values (max Cloudy/AMR resampled) : '
        printf, col_den, 'Total column density in cm-2 : ', totl(4)
        printf, col_den, 'Total mass in Msun : ', totl(5)
        printf, col_den, ''
        printf, col_den, 'Column density of heated gas in cm-2 : ', totl(6)
        printf, col_den, 'colden_heated(post-CLOUDY)/colden_total(post-CLOUDY) = ', totl(6)/totl(4)
        printf, col_den, 'colden_heated(post-CLOUDY)/colden_total(AMR resampled) = ', totl(6)/totl(2)
        printf, col_den, ''
        printf, col_den, 'Mass of heated gas in Msun : ', totl(7)
        printf, col_den, 'M_heated(post-CLOUDY)/M_total(post-CLOUDY) = ', totl(7)/totl(5)
        printf, col_den, 'M_heated(post-CLOUDY)/M_total(AMR resampled) = ', totl(7)/totl(3)
        printf, col_den, ''
        printf, col_den, 'Initial SFR (AMR) in Msun/yr : ', totl(10)
        printf, col_den, 'Initial SFR (AMR resampled) in Msun/yr : ', totl(8)
        printf, col_den, 'Final SFR (post-cloudy) in Msun/yr : ', totl(9)
        printf, col_den, 'SFR ratio final(post-cloudy)/initial(AMR) : ', totl(9)/totl(10)
        printf, col_den, 'SFR ratio final(post-cloudy)/initial(AMR resampled) : ', totl(9)/totl(8)
        
        close, col_den
        free_lun, col_den
     endif 

     if ps eq 0 then wset, 0
  endfor

  !p.multi = 0
  !X.MARGIN=[10,3] & !Y.MARGIN=[4,2] 
  !X.OMARGIN=0 & !Y.OMARGIN=0

  if  WHAT_TO_PLOT eq 'compare_lum'   then begin
     
     if offset ne 0 then begin
        if zoom eq 0 then map = den_map[0:n_pixel-1,0:n_pixel-1] else map = den_map[fix(n_pixel/2.)-zoom:fix(n_pixel/2.)+zoom,fix(n_pixel/2.)-zoom:fix(n_pixel/2.)+zoom]
     endif else begin
        if zoom eq 0 then map = den_map else map = den_map[fix((pos1_agn-7.5)/taille_pixel)-zoom:fix((pos1_agn-7.5)/taille_pixel)+zoom,fix((pos2_agn-7.5)/taille_pixel)-zoom:fix((pos2_agn-7.5)/taille_pixel)+zoom] ;attention ici
     endelse
     if ps eq 0 then wset, 0
     
     lvls = alog10(contour)     ;lvls = indgen(14)-7 ;
     
     if ps eq 0 then wset, 0
     if ps ne 0 then c_thick = 15 else c_thick = 2
;['ti', 'sfri', 'tf', 'sfrf', 'tfx10', 'sfrfx10', 'tfx100',
;'sfrfx100'] 
     if contour ne -42 then begin
        !p = win.pti & !x = win.xti & !y = win.yti
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfri & !x = win.xsfri & !y = win.ysfri
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.ptf & !x = win.xtf & !y = win.ytf
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfrf & !x = win.xsfrf & !y = win.ysfrf
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        
        !p = win.ptfx10 & !x = win.xtfx10 & !y = win.ytfx10
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfrfx10 & !x = win.xsfrfx10 & !y = win.ysfrfx10
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        
        !p = win.ptfx100 & !x = win.xtfx100 & !y = win.ytfx100
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
        !p = win.psfrfx100 & !x = win.xsfrfx100 & !y = win.ysfrfx100
        cgcontour, alog10(map), /overplot, /onimage, levels=lvls, c_thick=c_thick
     endif   
  endif

  if interp eq 1 then begin

     if what_to_plot eq 'all_den_temp_sfr' then begin
        xs = 800
        ys=xs
        if what_to_plot eq 'all_den_temp_sfr' then xs = 1000
        if what_to_plot eq 'all_den_temp_sfr' then ys = 0.196/0.31*xs
        if ps eq 0 then window, 1, xsize=xs, ysize=ys, ypos=245, xpos=0
        
;;;if jj eq 0 then window, 1, xsize = xs, ysize = ys, ypos=50 else
;;;wset, 1
        
        if ps ne 0 then begin
           !p.font=0
           !p.charsize=5 
           !p.charthick=2
           !x.charsize=1.5
           !y.charsize=1.5
           !p.thick=6
        endif
        plot = interp_windows(log_maps, x_kpc, y_kpc, z_kpc, sim, plane, zoom, 1, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, interp, contour, temp_choice, ps)
     endif


     if what_to_plot eq 'compare_lum' or what_to_plot eq 'compare_lum2' then begin
        xs = 800
        ys=xs
        if what_to_plot eq 'compare_lum2' then ys = 500
        if what_to_plot eq 'compare_lum2' then xs = 700
        if ps eq 0 then window, 1, xsize = xs, ysize = ys, ypos=50
;;;if jj eq 0 then window, 1, xsize = xs, ysize = ys, ypos=50 else
;;;wset, 1
        
        
        if ps ne 0 then begin
           !p.font=0
           !p.charsize=5 
           !p.charthick=2
           !x.charsize=1.5
           !y.charsize=1.5
           !p.thick=6
           !x.thick=40
           !y.thick=40
        endif
        plot = interp_windows(log_maps, x_kpc, y_kpc, z_kpc, sim, plane, zoom, 1, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, interp, contour, temp_choice, ps)
     endif
     
     if  WHAT_TO_PLOT eq 'compare_lum2' then begin
        if ps eq 0 then wset, 1
        
        lvls = min(log_maps.rhosfrf[where(finite(log_maps.rhosfrf) ne 0)])
        lvls_x10 = min(log_maps.rhosfrf_x10[where(finite(log_maps.rhosfrf_x10) ne 0)])
        lvls_x100 = min(log_maps.rhosfrf_x100[where(finite(log_maps.rhosfrf_x100) ne 0)])
        
        print, 'Val min du log_rhoSFR non nul : (I,x1,x10,x100) ',  min(log_maps.rhosfri[where(finite(log_maps.rhosfri) ne 0)]), lvls, lvls_x10, lvls_x100
        
        
        if ps ne 0 then c_thick = 30 else c_thick = 4
;['sfri','sfrf','sfrfx10','sfrfx100'] 
        !p = plot.pone_interp & !x = plot.xone_interp & !y = plot.yone_interp
        
        dummy_value=10
        t = where(finite(log_maps.rhosfrf) eq 0)
        ctr_map = log_maps.rhosfrf
        ctr_map[t] = dummy_value
        t = where(finite(log_maps.rhosfrf_x10) eq 0)
        ctr_map_x10 = log_maps.rhosfrf_x10
        ctr_map_x10[t] = dummy_value
        t = where(finite(log_maps.rhosfrf_x100) eq 0)
        ctr_map_x100 = log_maps.rhosfrf_x100
        ctr_map_x100[t] = dummy_value
        
        
        cgcontour, ctr_map, /overplot, /onimage, levels=dummy_value, c_thick=4*c_thick, color=255, c_label=''   ;c_annotation='Seyfert'
        cgcontour, ctr_map_x10, /overplot, /onimage, levels=dummy_value, c_thick=2*c_thick, color=240, c_label='' ;c_annotation='wQuasar'
        cgcontour, ctr_map_x100, /overplot, /onimage, levels=dummy_value, c_thick=c_thick, color=1, c_label=''    ;c_annotation='sQuasar'
        
        check_contours = 0
        if ps ne 0 then check_contours = 0
        if check_contours eq 1 then begin
           cgloadct,39, /reverse, /silent
           
           window, 4, ypos=425
           plotimage, log_maps.rhosfrf, range=[-18,-5], ncolors=256, bottom=0
           cgcontour, ctr_map, /overplot, /onimage, levels=dummy_value, c_thick=c_thick, color=255, c_label='' ;c_annotation='Seyfert'
           
           window, 5
           plotimage, log_maps.rhosfrf_x10, range=[-18,-5], ncolors=256, bottom=0
           cgcontour, ctr_map_x10, /overplot, /onimage, levels=dummy_value, c_thick=c_thick, color=240, c_label='' ;c_annotation='Seyfert'
           
           window, 6, ypos=425
           plotimage, log_maps.rhosfrf_x100, range=[-18,-5], ncolors=256, bottom=0
           cgcontour, ctr_map_x100, /overplot, /onimage, levels=dummy_value, c_thick=c_thick, color=1, c_label='' ;c_annotation='Seyfert'
           
           
           
        endif   
     endif
  endif


  if ps ne 0 then begin
     if interp eq 0 then begin
        print, 'Conversion en png...'
        ps_end, /png
     endif else begin
        print, 'Decoupage du fichier eps et conversion en png...' 
        ps_end
        
        if what_to_plot eq 'compare_lum' or what_to_plot eq 'compare_lum2' then nm='compare_lum_' 
        if WHAT_TO_PLOT eq 'den_temp_sfr' then fname = nm+qsr+res+tmp_choice+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps'
        if WHAT_TO_PLOT eq 'compare_lum'  then fname = nm+qsr+res+tmp_choice+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps'
        if WHAT_TO_PLOT eq 'compare_lum2' then fname = nm+qsr+res+tmp_choice+'comparison_of_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps'
        if WHAT_TO_PLOT eq 'den_em'       then fname = nm+qsr+res+'den_em'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps'
        if WHAT_TO_PLOT eq 'all_den_temp_sfr' then fname = nm+qsr+res+tmp_choice+'all_den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+interpf+'.eps'
        
        spawn, 'psselect -p1 -q '+fname+' '+repstr(fname,'_interp2','') ;psselect -p1 -q file.eps page1.eps
        spawn, 'psselect -p2 -q '+fname+' '+repstr(fname,'_interp2','_interp')
        spawn, 'convert '+repstr(fname,'_interp2','')+' '+repstr(repstr(fname,'_interp2',''),'eps','png') ;convert eps into png
        spawn, 'convert '+repstr(fname,'_interp2','_interp')+' '+repstr(repstr(fname,'_interp2','_interp'),'eps','png')
        spawn, 'rm '+fname
     endelse
  endif

;stop
;--------------------------------------------------------------
  
  print, 'Fin du programme. Tout est OK.'
  
end
