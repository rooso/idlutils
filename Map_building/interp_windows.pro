function interp_windows, log_maps, x_kpc, y_kpc, z_kpc, sim, plane, zoom, w0, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, interp, contour, temp_choice, ps
;#######################################################################;
;cette fonction cree la fenetre ou seront tracees les cartes interpolees;
;#######################################################################;

if interp ne 1 then stop 

  if plane eq 'xz' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = z_kpc - offset
  endif
  if plane eq 'yz' or plane eq 'zy' then begin
     pos1_agn = z_kpc - offset
     pos2_agn = y_kpc - offset
     plane = 'zy'
  endif
  if plane eq 'dk' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = y_kpc - offset
  endif
  
  if offset eq 0 then imgxr = [7.5,42.5] else imgxr = [-17.5,17.5]
  imgyr = imgxr
  
  if zoom eq 0 then begin
  if offset eq 0 then imgxr = [7.5,42.5] else imgxr = [-17.5,17.5]
     imgyr = imgxr
  endif else begin
     if offset eq 0 then imgxr = [pos1_agn-zoom*taille_pixel,pos1_agn+zoom*taille_pixel] else imgxr = [-zoom*taille_pixel,zoom*taille_pixel]
     if offset eq 0 then imgyr = [pos2_agn-zoom*taille_pixel,pos2_agn+zoom*taille_pixel] else imgyr = [-zoom*taille_pixel,zoom*taille_pixel]
  endelse
  
;NB :
;zoom de l'image : (offset = 0)
;zoom=0 -> image = (0.85-0.15)*50 kpc = 35 kpc de large
;sinon :
; image = 2 * zoom * taille_pixel = 2 * zoom * 50 kpc / 2^13 = zoom * 12.2 pc de large.
;soit : zoom = 50 -> largeur = 610 pc
;       zoom = 100 -> largeur = 1.22 kpc 
  
  
  if ps eq 0 then wset, w0
erase
  
  zoomn = strcompress(string(fix(zoom)),/remove_all)
  xt = strmid(plane, 0, 1)+' axis [kpc]'
  yt = strmid(plane, 1, 1)+' axis [kpc]'
  if plane eq 'dk' then begin
     xt = 'x axis [kpc]'
     yt = 'y axis [kpc]'
  endif
  
  xtickn = ''
  ytickn = '' ;affiche les valeurs sur les graduations
  !x.omargin = 1
  !y.omargin = 1

  if compact ne 0 then begin
     xt = ' '
     yt = ' '
     blank = replicate(' ',10)
     titles = blank ;supprime les annotations sur les graduations
     xtickn = blank
     ytickn = blank
     !x.margin = 0
     !y.margin = 0
  endif

 if WHAT_TO_PLOT eq 'compare_lum' then nm_strct = ['ti', 'sfri', 'tf', 'sfrf', 'tfx10', 'sfrfx10', 'tfx100', 'sfrfx100'] 
 if WHAT_TO_PLOT eq 'den_em' then nm_strct = ['den','ofr','emo','emn']
 if WHAT_TO_PLOT eq 'den_temp_sfr' then nm_strct = ['den', 'hfr', 'ti', 'tf', 'sfri', 'sfrf']
 if WHAT_TO_PLOT eq 'all_den_temp_sfr' then nm_strct = ['den', 'hfr', 'ti', 'tf', 'sfri', 'sfrf', 'hfrx10', 'tfx10', 'sfrfx10', 'hfrx100', 'tfx100', 'sfrfx100']

if WHAT_TO_PLOT eq 'compare_lum2' then nm_strct = ['one']
 
 if ps eq 0 then begin
        chars = 2
        textt = 1
     endif else begin
        chars = 5
        textt = 5
     endelse 
 
 k_max = n_elements(nm_strct)

 !p.multi = [0,2,k_max/2]
if WHAT_TO_PLOT eq 'compare_lum2' then begin
!p.multi=0
!X.MARGIN=[0,20]
endif

 resol=taille_pixel
 titles=strarr(k_max)

 if temp_choice eq 'temperature' then begin
    tmp_rg = [10,2]
    tmp_title = 'Final temperature [K]' 
    tmp_col = !black
 endif else begin
    if temp_choice eq 'heating_rate' then begin
       tmp_rg = [7.5,-2];[7.5,-12.5]
       tmp_title = 'Rel. temperature change';'Heating rate'
    tmp_col = !white
    endif else begin
       if temp_choice eq 'delta_T' then begin
          tmp_rg = [10,-5]
          tmp_title = textoidl('\Delta')+'T [K]'
              tmp_col = !white
       endif
    endelse
 endelse
 tmp_rg2 = reverse(tmp_rg) 
 
 
;afficher les titres ?
 affiche_titre = 0
 if affiche_titre eq 1 then begin
    if WHAT_TO_PLOT eq 'den_temp_sfr' then titles = [sim+" : log Gas density in cm"+textoidl('^{-3}'),sim+" : log fraction of neutral H",sim+" : log Initial temperature in K",sim+" : log "+tmp_title,sim+" : log Initial SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')+" (> "+strn(threshold, length=6)+" cm"+textoidl('^{-3}')+')',sim+" : log Final SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')+" (> "+strn(threshold, length=6)+" cm"+textoidl('^{-3}')]
    
;titles = [sim+" : log Gas density in cm'+textoidl('^{-3}')",sim+" : log fraction of
;neutral O",sim+" : Emergent emissivity of OIII",sim+" : Intrinsic
;emissivity of OIII",sim+" : Emergent emissivity of NII",sim+" :
;Intrinsic emissivity of NII"]
    if WHAT_TO_PLOT eq 'den_em' then titles = [sim+" : log Gas density in cm"+textoidl('^{-3}'),sim+" : log fraction of neutral O",sim+" : Emergent emissivity of OIII",sim+" : Emergent emissivity of NII"]
    if WHAT_TO_PLOT eq 'compare_lum' then titles = [sim+" : log Initial temperature in K", sim+" : log Initial SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log "+tmp_title+"_x1", sim+" : log Final_x1 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log "+tmp_title+"_x1", sim+" : log Final_x10 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log "+tmp_title+"_x1", sim+" : log Final_x100 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')]
    
    if WHAT_TO_PLOT eq 'one' then titles=[sim+' : log '+textoidl('\rho_{SFR}')+' in M'+textoidl('_\odot')+' yr'+textoidl('^{-1}')+'']
 endif
 
 tags=tag_names(log_maps)
;print, minmax(log_maps.(where(strcmp(tags,tags(0)) eq 1)))
 
 k_max=min([k_max,n_elements(tags)])
 
 if WHAT_TO_PLOT eq 'den_temp_sfr' then begin
    btitle=['log Gas density [cm'+textoidl('^{-3}')+']','log Fraction of neutral H','log Initial temperature [K]','log '+tmp_title,'log '+tmp_title,'log '+tmp_title,'log '+textoidl('\rho_{SFR_i}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (> '+strn(threshold)+' cm'+textoidl('^{-3}')+')','log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (> '+strn(threshold)+' cm'+textoidl('^{-3}')+')','log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (> '+strn(threshold)+' cm'+textoidl('^{-3}')+')','log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (> '+strn(threshold)+' cm'+textoidl('^{-3}')+')']
    rg =[[-7,6],[-13,0],[-13,0],[-13,0],[10,2],[tmp_rg],[tmp_rg],[tmp_rg],[-18,-5],[-18,-5],[-18,-5],[-18,-5]]
    rg2=[[-7,6],[-13,0],[-13,0],[-13,0],[2,10],[tmp_rg2],[tmp_rg2],[tmp_rg2],[-18,-5],[-18,-5],[-18,-5],[-18,-5]]
    contour_map = log_maps.den
    lab_Col = [!black,!white,!white,!white,!black,tmp_col,tmp_col,tmp_col,!black,!black,!black,!black]
    bh_Col  = [255,255,255,255,255,255,255,255,0,0,0,0]
    lvls = alog10(contour)
    change_pos = 0
    rev=[0,0,0,0,1,1,1,1,0,0,0,0]
    nbottom=[0,0,0,0,0,0,0,0,60,60,60,60]
 endif
 
 if WHAT_TO_PLOT eq 'all_den_temp_sfr' then begin
    btitle=['log Gas density [cm'+textoidl('^{-3}')+']','log Fraction of neutral H','log Initial temperature [K]','log '+tmp_title,'log Initial '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']','log Final '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']','log Fraction of neutral H','log '+tmp_title,'log Final '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']','log Fraction of neutral H','log '+tmp_title,'log Final '+textoidl('\rho_{SFR}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']']
    
    rg =[[-7,6],[-13,0],[10,2],[tmp_rg],[-18,-5],[-18,-5],[-13,0],[tmp_rg],[-18,-5],[-13,0],[tmp_rg],[-18,-5]]
    rg2=[[-7,6],[-13,0],[2,10],[tmp_rg2],[-18,-5],[-18,-5],[-13,0],[tmp_rg2],[-18,-5],[-13,0],[tmp_rg2],[-18,-5]]    
    
    contour_map = log_maps.den
    lab_Col = [!black,!white,!black,tmp_col,!black,!black,!white,tmp_col,!black,!white,tmp_col,!black]
    bh_Col  = [255,255,255,255,0,0,255,255,0,255,255,0]
    lvls = alog10(contour)
    change_pos = 0
    rev=[0,0,1,1,0,0,0,1,0,0,1,0]
    nbottom=[0,0,0,0,60,60,0,0,60,0,0,60]
    titles=['Before RT', 'L'+textoidl('_{AGN}'), '', '', '', '', '10 x L'+textoidl('_{AGN}'), '', '', '100 x L'+textoidl('_{AGN}'), '', '']
;den, hfr, hfrx10, hfr100, ti, tf, tfx10, tfx100, sfri, sfrf, sfrx10, sfrfx100
    left = [0.01,0.295,0.01,0.295,0.01,0.295,0.497,0.497,0.497,0.698,0.698,0.698]
    bottom = [0.65,0.65,0.33,0.33,0.01,0.01,0.65,0.33,0.01,0.65,0.33,0.01]
    right = [0.197,0.492,0.197,0.492,0.197,0.492,0.693,0.693,0.693,0.894,0.894,0.894]
    top = [0.96,0.96,0.64,0.64,0.32,0.32,0.96,0.64,0.32,0.96,0.64,0.32]
    
    left_bar = [0.197,0,0.197,0,0.197,0,0,0,0,0.894,0.894,0.894]
    bottom_bar = [0.65,0,0.33,0,0.01,0,0,0,0,0.65,0.33,0.01]
    right_bar = [0.23,0,0.23,0,0.23,0,0,0,0,0.93,0.93,0.93]
    top_bar = [0.96,0,0.64,0,0.32,0,0,0,0,0.96,0.64,0.32]
    plot_color=[255,0,0,0,0,0,0,0,0,0,0,0]
    
    chars_label = 2*chars
    
    if ps eq 0 then begin
       chars = 2.5
       textt = 1
    endif else begin
       chars = 5.5
       textt = 5
    endelse
    
 endif
 
;help, log_maps
;stop
 
 
 if WHAT_TO_PLOT eq 'den_em' then begin
    btitle=['log Gas density [cm'+textoidl('^{-3}')+']','log Fraction of neutral O', 'Emergent emissivity of OIII5007', 'Emergent emissivity of NII6584']
    rg =[[-7,6],[-15,0], [-8,-1], [-8,-1]]
    rg2=[[-7,6],[-15,0], [-8,-1], [-8,-1]]
    contour_map = log_maps.den
    lab_Col = [!black,!white,!black,!black]
    bh_Col  = [255,255,255,255]
    lvls = alog10(contour)
    change_pos = 0
    rev=[0,0,0,0]
    nbottom=[0,0,0,0]
 endif
 
 if WHAT_TO_PLOT eq 'compare_lum' then begin
    btitle = ['log Initial temperature [K]', 'log '+textoidl('\rho_{SFR_i}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']','log '+tmp_title, 'log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+'/kpc'+textoidl('^3')+']','log '+tmp_title, 'log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']','log '+tmp_title, 'log '+textoidl('\rho_{SFR_f}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+']']
    rg =[[10,2],[-18,-5],[tmp_rg],[-18,-5],[tmp_rg],[-18,-5],[tmp_rg],[-18,-5]]
    rg2=[[2,10],[-18,-5],[tmp_rg2],[-18,-5],[tmp_rg2],[-18,-5],[tmp_rg2],[-18,-5]]
    contour_map = log_maps.rhosfri
    lab_Col = [!black,!black,tmp_col,!black,tmp_col,!black,tmp_col,!black]
    bh_Col  = [255,0,255,0,255,0,255,0]
    lvls = alog10(contour)
    change_pos = 0
    rev=[1,0,1,0,1,0,1,0]
    nbottom=[0,60,0,60,0,60,0,60]
 endif
 
 if WHAT_TO_PLOT eq 'compare_lum2' then begin
    btitle=['log '+textoidl('\rho_{SFR_i}')+' [M'+sunsymbol()+' yr'+textoidl('^{-1}')+' kpc'+textoidl('^{-3}')+'] (> '+strn(threshold)+' cm'+textoidl('^{-3}')+')']
    rg = [-18,-5]
    rg2 = rg
    lab_col = !black
    bh_col = 0
    change_pos = 1
    rev = 0
    nbottom = 60
    k_max = 1
 endif
 
 if ps ne 0 then c_thick = 15 else c_thick = 2
 if ps eq 0 then thick = 4 else thick=40
 
 ndiv = 0                       ; ndiv = abs(rg2(1,k)-rg2(0,k))
 
 if ps eq 0 then syms = 4 else syms=10
 
 for k = 0, k_max-1 do begin
    cgloadct,39, /silent, /reverse
    
    if WHAT_TO_PLOT eq 'all_den_temp_sfr' then  begin
       !p.color=plot_color(k)
       plotimage,log_maps.(where(strcmp(tags,tags(k)) eq 1)), range=rg(*,k), imgxrange=imgxr, imgyrange=imgyr, title=titles[k], xtitle=xt, ytitle=yt, /save, xtickn=xtickn, ytickn=ytickn, ncolors=256, bottom=0, position=[left(k),bottom(k),right(k),top(k)], /noerase, chars=chars_label, textt=textt
    endif else begin
       plotimage,log_maps.(where(strcmp(tags,tags(k)) eq 1)), range=rg(*,k), imgxrange=imgxr, imgyrange=imgyr, title=titles[k], xtitle=xt, ytitle=yt, /isotropic, /save, xtickn=xtickn, ytickn=ytickn, ncolors=256, bottom=0
    endelse
    device,decompose=0
    loadct, 0, /silent
    plots, pos1_agn, pos2_agn, psym=1, color=bh_col(k), thick=thick, syms=syms
                                ;res=affiche_resol(pos1_agn, pos2_agn, resol)
    lab=print_label(lab_col(k), change_pos, ps)
    device, /decompose
    if contour ne -42 then cgcontour, contour_map, /overplot, /onimage, levels=lvls, c_thick=c_thick
    cgloadct,39, /silent, /reverse
    if WHAT_TO_PLOT ne 'all_den_temp_sfr' then begin
       cgColorbar, range=rg2(*,k), divisions=ndiv, color=!p.color, /vertical, /fit, title=btitle(k), /right, chars=1.5*chars, textt=textt, rev=rev(k)
    endif else begin
       if k eq 0 or k eq 2 or k eq 4 or k ge 9 then cgColorbar, range=rg2(*,k), divisions=ndiv, color=255, /vertical, title=btitle(k), /right, chars=chars, textt=textt, rev=rev(k), pos=[left_bar(k),bottom_bar(k),right_bar(k),top_bar(k)]
       
    endelse
    
    if k eq 0 then begin
       strct = CREATE_STRUCT('p'+nm_strct[0]+'_interp', !p, 'x'+nm_strct[0]+'_interp', !x, 'y'+nm_strct[0]+'_interp', !y)
    endif else begin
       strct = CREATE_STRUCT(strct,'p'+nm_strct[k]+'_interp', !p, 'x'+nm_strct[k]+'_interp', !x, 'y'+nm_strct[k]+'_interp', !y)
    endelse
 endfor
 
;stop
 return, strct
end

