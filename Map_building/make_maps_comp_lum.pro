pro make_maps_comp_lum, zoom=zoom

compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f='', thr=100, ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f='', thr=10,  ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f='', thr=1,   ps=1, w='compare_lum', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f='', thr=0,   ps=1, w='compare_lum', contour=-42, /interp

end
