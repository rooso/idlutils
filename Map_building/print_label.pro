function print_label, lab_col, change_pos, ps

;print, !x.crange[0], !x.crange[1]

x05 = (!x.crange[1] - !x.crange[0])*0.05 + !x.crange[0]
y05 = (!y.crange[1] - !y.crange[0])*0.05 + !y.crange[0]
lab = (!x.crange[1] - !x.crange[0])*0.10
;print, lab*1000, round(lab*1000)

shift = (!y.crange[1] - !y.crange[0])*0.05
shift2 = shift


if round(lab*1000) lt 1000 then label = strn(round(lab*1000),length=3)+' pc' else label = strn(round(lab),length=3)+' kpc'
if round(lab*1000) ge 1000 then shift2 = 1.8*shift2


if change_pos eq 1 then begin
shift2 = +0.5*shift/3.
shift = shift/2.
endif


if ps eq 0 then thicks = 1 else thicks = 4

  defplotcolors
  ;;;;xyouts, x05, y05+shift, label, color=!white, charthick=!p.charthick+2
  xyouts, x05, y05+shift, label, color=lab_col, charthick=thicks
  oplot, [x05+shift2,x05+shift2+lab], [y05,y05], color=lab_col, thick=thicks
  if ps ne 0 then begin
     njj=100
     for jj = 0, njj do begin
        oplot, [x05+shift2,x05+shift2+lab], [y05-float(jj)/njj*shift/5,y05-float(jj)/njj*shift/5], color=lab_col, thick=thicks
        oplot, [x05+shift2,x05+shift2+lab], [y05+float(jj)/njj*shift/5,y05+float(jj)/njj*shift/5], color=lab_col, thick=thicks
     endfor
  endif

;print, x05, y05
return, 0

end
