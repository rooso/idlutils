pro make_maps

twod_maps, s='00075w',  pl='xz', z=50, f='', ps=1
twod_maps, s='00075n',  pl='xz', z=50, f='', ps=1
twod_maps, s='00075w',  pl='xz', z=50, f='10', ps=1
twod_maps, s='00075n',  pl='xz', z=50, f='10', ps=1
twod_maps, s='00075w',  pl='xz', z=50, f='100', ps=1
twod_maps, s='00075n',  pl='xz', z=50, f='100', ps=1


;;;
twod_maps, s='00075w',  pl='xz', z=0, f='', ps=1
twod_maps, s='00075n',  pl='xz', z=0, f='', ps=1
twod_maps, s='00075w',  pl='xz', z=0, f='10', ps=1
twod_maps, s='00075n',  pl='xz', z=0, f='10', ps=1
twod_maps, s='00075w',  pl='xz', z=0, f='100', ps=1
twod_maps, s='00075n',  pl='xz', z=0, f='100', ps=1

;;;;;;;;;;;
twod_maps, s='00075w',  pl='xz', z=100, f='', ps=1
twod_maps, s='00075n',  pl='xz', z=100, f='', ps=1
twod_maps, s='00075w',  pl='xz', z=100, f='10', ps=1
twod_maps, s='00075n',  pl='xz', z=100, f='10', ps=1
twod_maps, s='00075w',  pl='xz', z=100, f='100', ps=1
twod_maps, s='00075n',  pl='xz', z=100, f='100', ps=1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
twod_maps, s='00085',  pl='xz', z=50, f='', ps=1
twod_maps, s='00100',  pl='xz', z=50, f='', ps=1
twod_maps, s='00085',  pl='xz', z=0, f='', ps=1
twod_maps, s='00100',  pl='xz', z=0, f='', ps=1
twod_maps, s='00085',  pl='xz', z=100, f='', ps=1
twod_maps, s='00100',  pl='xz', z=100, f='', ps=1
twod_maps, s='00085',  pl='xz', z=50, f='10', ps=1
twod_maps, s='00100',  pl='xz', z=50, f='10', ps=1
twod_maps, s='00085',  pl='xz', z=0, f='10', ps=1
twod_maps, s='00100',  pl='xz', z=0, f='10', ps=1
twod_maps, s='00085',  pl='xz', z=100, f='10', ps=1
twod_maps, s='00100',  pl='xz', z=100, f='10', ps=1
twod_maps, s='00085',  pl='xz', z=50, f='100', ps=1
twod_maps, s='00100',  pl='xz', z=50, f='100', ps=1
twod_maps, s='00085',  pl='xz', z=0, f='100', ps=1
twod_maps, s='00100',  pl='xz', z=0, f='100', ps=1
twod_maps, s='00085',  pl='xz', z=100, f='100', ps=1
twod_maps, s='00100',  pl='xz', z=100, f='100', ps=1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
twod_maps, s='00075w',  pl='zy', z=50, f='', ps=1
twod_maps, s='00075n',  pl='zy', z=50, f='', ps=1
twod_maps, s='00085',  pl='zy', z=50, f='', ps=1
twod_maps, s='00100',  pl='zy', z=50, f='', ps=1
twod_maps, s='00085',  pl='zy', z=0, f='', ps=1
twod_maps, s='00100',  pl='zy', z=0, f='', ps=1
twod_maps, s='00075w',  pl='zy', z=0, f='', ps=1
twod_maps, s='00075n',  pl='zy', z=0, f='', ps=1
twod_maps, s='00085',  pl='zy', z=100, f='', ps=1
twod_maps, s='00100',  pl='zy', z=100, f='', ps=1
twod_maps, s='00075w',  pl='zy', z=100, f='', ps=1
twod_maps, s='00075n',  pl='zy', z=100, f='', ps=1
twod_maps, s='00085',  pl='zy', z=50, f='10', ps=1
twod_maps, s='00100',  pl='zy', z=50, f='10', ps=1
twod_maps, s='00075w',  pl='zy', z=50, f='10', ps=1
twod_maps, s='00075n',  pl='zy', z=50, f='10', ps=1
twod_maps, s='00085',  pl='zy', z=0, f='10', ps=1
twod_maps, s='00100',  pl='zy', z=0, f='10', ps=1
twod_maps, s='00075w',  pl='zy', z=0, f='10', ps=1
twod_maps, s='00075n',  pl='zy', z=0, f='10', ps=1
twod_maps, s='00085',  pl='zy', z=100, f='10', ps=1
twod_maps, s='00100',  pl='zy', z=100, f='10', ps=1
twod_maps, s='00075w',  pl='zy', z=100, f='10', ps=1
twod_maps, s='00075n',  pl='zy', z=100, f='10', ps=1
twod_maps, s='00085',  pl='zy', z=50, f='100', ps=1
twod_maps, s='00100',  pl='zy', z=50, f='100', ps=1
twod_maps, s='00075w',  pl='zy', z=50, f='100', ps=1
twod_maps, s='00075n',  pl='zy', z=50, f='100', ps=1
twod_maps, s='00085',  pl='zy', z=0, f='100', ps=1
twod_maps, s='00100',  pl='zy', z=0, f='100', ps=1
twod_maps, s='00075w',  pl='zy', z=0, f='100', ps=1
twod_maps, s='00075n',  pl='zy', z=0, f='100', ps=1
twod_maps, s='00085',  pl='zy', z=100, f='100', ps=1
twod_maps, s='00100',  pl='zy', z=100, f='100', ps=1
twod_maps, s='00075w',  pl='zy', z=100, f='100', ps=1
twod_maps, s='00075n',  pl='zy', z=100, f='100', ps=1


end
