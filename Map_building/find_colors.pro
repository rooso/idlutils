function find_colors, param, rg1, rg2
;####################################################################################################
;# Cette fonction retourne les couleurs correspondant au parametre d'entree avec les bornes donnees #
;####################################################################################################

  rg=[rg1,rg2]
  taille = n_elements(param)
  colors = cgScaleVector(Findgen(taille), rg(0), rg(1))
                                ;print, colors
  cols = Value_Locate(colors, alog10(param))
  
;print, min(cols), max(cols)
;;print, min(param), max(param)
  
  if min(cols) eq -1 then color_min = 0.
  if min(cols) ge 0 then color_min = float(min(cols))/(taille-1)*255.
  
  color_max = float(max(cols))/(taille-1)*255.
;print, color_min, color_max
  if color_min ne color_max then cols = Byte(Round(cgScaleVector(cols, color_min, color_max)))
  if color_min eq color_max then cols = Byte(Round(cols))
  
;print,  'Doivent etre entre 0 et 255 :', minmax(cols)
  return, cols
  
end 
