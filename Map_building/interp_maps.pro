function interp_maps, all_params, jj, pos1_agn, pos2_agn, zoom, taille_pixel, what_to_plot, eps, map, temp_choice

  nb = map
  
  ni = (size(map))[1]
  nj = (size(map))[2]
  
  fill_fact = 1./5. ;pas bien ca... a coder en variable
  print, minmax(all_params(0,*))
  
  t = where(all_params(0,*) gt pos1_agn-zoom*taille_pixel and all_params(0,*) le pos1_agn+zoom*taille_pixel and all_params(1,*) gt pos2_agn-zoom*taille_pixel and all_params(1,*) le pos2_agn+zoom*taille_pixel)
  
  x=(all_params(0,*))[t]
  y=(all_params(1,*))[t]
  den_init=(all_params(2,*))[t]
  temp_init=(all_params(3,*))[t]
  temp_post_cloudy=(all_params(4,*))[t]
  rhoSFR_avant=(all_params(5,*))[t]
  SFR_tot_avant=(all_params(6,*))[t]
  rhoSFR_Apres=(all_params(7,*))[t]
  SFR_tot_apres=(all_params(8,*))[t]
  Hneutre_all=(all_params(9,*))[t]
  Oneutre_all=(all_params(10,*))[t]
  OIII_5007A_all=(all_params(11,*))[t]
  NII_6584A_all=(all_params(12,*))[t]
  
  print, minmax(x)
  
  x = x - min(x)
  y = y - min(y)
  
  print, minmax(x)
  
  map_den = map
  map_ti = map
  map_tf = map
  map_rhosfri = map
  map_rhosfrf = map
  map_Hneutre = map
  map_Oneutre = map
  map_OIII = map
  map_NII = map
  
  print, 'Debut du mappage...'
  for i=0, ni-1 do begin
     print, float(i+1)/ni*100, ' % ... ', systime()
     for j=0, nj-1 do begin 
        t = where(floor(x/eps) eq i and floor(y/eps) eq j,nt)
        if nt gt 0 then begin
           map_den(i,j) = total(den_init[t])
           map_ti(i,j) = total(temp_init[t])
           if temp_choice eq 'temperature' then  map_tf(i,j) = total(temp_post_cloudy[t])
           if temp_choice eq 'heating_rate' then map_tf(i,j) = total(abs(temp_post_cloudy[t]-temp_init[t])/temp_init[t])
           if temp_choice eq 'delta_T' then      map_tf(i,j) = total(abs(temp_post_cloudy[t]-temp_init[t]))
           map_rhosfri(i,j) = total(rhoSFR_avant[t])
           map_rhosfrf(i,j) = total(rhoSFR_apres[t])
           map_Hneutre(i,j) = total(Hneutre_all[t])
           map_Oneutre(i,j) = total(Oneutre_all[t])
           map_OIII(i,j) = total(OIII_5007A_all[t])
           map_NII(i,j) = total(NII_6584A_all[t])
        endif
        nb(i,j) = nt
                                ;if nt gt 0 then print, i,j, nt,
                                ;den_init[t]
     endfor
  endfor
  
  t=where(nb ne 0)
  map_den[t] = map_den[t]/nb[t]
  map_ti[t] = map_ti[t]/nb[t]
  map_tf[t] = map_tf[t]/nb[t]
  map_rhosfri[t] = map_rhosfri[t]/nb[t]
  map_rhosfrf[t] = map_rhosfrf[t]/nb[t]
  map_Hneutre[t] = map_Hneutre[t]/nb[t]
  map_Oneutre[t] = map_Oneutre[t]/nb[t]
  map_OIII[t] = map_OIII[t]/nb[t]
  map_NII[t] = map_NII[t]/nb[t]
  
  
  log_map_den = alog10(map_Den)
  log_map_ti = alog10(map_ti)
  log_map_tf = alog10(map_tf)
  log_map_rhosfri = alog10(map_rhosfri)
  log_map_rhosfrf = alog10(map_rhosfrf)
  log_map_Hneutre = alog10(map_Hneutre)
  log_map_Oneutre = alog10(map_Oneutre)
  log_map_OIII = alog10(map_OIII)
  log_map_NII = alog10(map_NII)
  t = where(nb eq 0,nt0)
  if nt0 gt 0 then begin
     log_map_den[t] = -42
     log_map_ti[t] = -42
     log_map_tf[t] = -42
     log_map_rhosfri[t] = -42
     log_map_rhosfrf[t] = -42
     log_map_Hneutre[t] = -42
     log_map_Oneutre[t] = -42
     log_map_OIII[t] = -42
     log_map_NII[t] = -42
  endif
  
  cntr=0
  while nt0 ne 0 do begin
     if jj eq 0 then print, 'Iteration #',cntr+1 
     if jj eq 1 then print, 'Iteration_x10 #',cntr+1 
     if jj eq 2 then print, 'Iteration_x100 #',cntr+1 
     
     interp_den = my_interp(map_den,nb)
     interp_ti = my_interp(map_ti,nb)
     interp_tf = my_interp(map_tf,nb)
     interp_rhosfri = my_interp(map_rhosfri,nb)
     interp_rhosfrf = my_interp(map_rhosfrf,nb)
     interp_Hneutre = my_interp(map_Hneutre,nb)
     interp_Oneutre = my_interp(map_Oneutre,nb)
     interp_OIII = my_interp(map_OIII,nb)
     interp_NII = my_interp(map_NII,nb)
     
     nb=interp_den.nb
     
     map_den=interp_den.map
     map_ti=interp_ti.map
     map_tf=interp_tf.map
     map_rhosfri=interp_rhosfri.map
     map_rhosfrf=interp_rhosfrf.map
     map_Hneutre=interp_Hneutre.map
     map_Oneutre=interp_Oneutre.map
     map_OIII=interp_OIII.map
     map_NII=interp_NII.map
     
     t = where(nb eq 0, nt0)
     cntr+=1
     
  endwhile
  log_map_den = alog10(map_Den)
  log_map_ti = alog10(map_ti)
  log_map_tf = alog10(map_tf)
  log_map_rhosfri = alog10(map_rhosfri)
  log_map_rhosfrf = alog10(map_rhosfrf)
  log_map_Hneutre = alog10(map_Hneutre)
  log_map_Oneutre = alog10(map_Oneutre)
  log_map_OIII = alog10(map_OIII)
  log_map_NII = alog10(map_NII)
  
  if what_to_plot eq 'den_temp_sfr' then log_maps = {den:log_map_den, Hneutre:log_map_Hneutre, ti:log_map_ti, tf:log_map_tf, rhosfri:log_map_rhosfri, rhosfrf:log_map_rhosfrf}
  if what_to_plot eq 'den_em' then log_maps = {den:log_map_den, Oneutre:log_map_Oneutre, oiii:map_OIII*fill_fact, nii:map_NII*fill_fact}
  if what_to_plot eq 'compare_lum' then begin
     if jj eq 0 then begin
        log_maps = CREATE_STRUCT('ti', log_map_ti, 'rhosfri', log_map_rhosfri, 'tf', log_map_tf, 'rhosfrf', log_map_rhosfrf) 
     endif else begin
        
        if jj eq 1 then nm='_x10'
        if jj eq 2 then nm='_x100'
        
        log_maps = CREATE_STRUCT('tf'+nm, log_map_tf, 'rhosfrf'+nm, log_map_rhosfrf)
     endelse
  endif
  if what_to_plot eq 'all_den_temp_sfr' then begin
     if jj eq 0 then begin
        log_maps = CREATE_STRUCT('den', log_map_den, 'Hneutre', log_map_Hneutre, 'ti', log_map_ti, 'tf', log_map_tf, 'rhosfri', log_map_rhosfri, 'rhosfrf', log_map_rhosfrf) 
     endif else begin
        
        if jj eq 1 then nm='_x10'
        if jj eq 2 then nm='_x100'
        
        log_maps = CREATE_STRUCT('Hneutre'+nm, log_map_Hneutre, 'tf'+nm, log_map_tf, 'rhosfrf'+nm, log_map_rhosfrf)
     endelse
  endif
  if what_to_plot eq 'compare_lum2' then begin
     if jj eq 0 then begin
        log_maps = CREATE_STRUCT('rhosfri', log_map_rhosfri,  'rhosfrf', log_map_rhosfrf) 
     endif else begin
        
        if jj eq 1 then nm='_x10'
        if jj eq 2 then nm='_x100'
        
        log_maps = CREATE_STRUCT('rhosfrf'+nm, log_map_rhosfrf)
     endelse
  endif
  
                                ;help, log_maps
  
  
  return, log_maps
  
end

