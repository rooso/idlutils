pro make_maps_lores

threshold='10'
temp_choice='heating_rate'

  compare_luminosities_interp, sim='00178', pl='dk', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00178', pl='xz', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00119', pl='dk', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00119', pl='xz', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00197', pl='dk', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00197', pl='xz', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00225', pl='dk', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00225', pl='xz', z=41, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'

  compare_luminosities_interp, sim='00178', pl='dk', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00178', pl='xz', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00119', pl='dk', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00119', pl='xz', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00197', pl='dk', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00197', pl='xz', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00225', pl='dk', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'
  compare_luminosities_interp, sim='00225', pl='xz', z=1230, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice, lm='12'

end
