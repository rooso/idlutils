function affiche_resol, pos1, pos2, resol
;##################################################################
;cette fonction affiche un cercle blanc d'un rayon egal a la taille
;des plus petites celules AMR sur l'agn 
;##################################################################
  for k = 1, 100 do begin
     PLOTS, CIRCLE(pos1, pos2, k*resol/100.d), color=!white,thick=2
  endfor
  return, 0
end
