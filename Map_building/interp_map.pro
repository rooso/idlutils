function my_interp, map
  
  ni = (size(map))[1]
  nj = (size(map))[2]
  
  map2=map
  
  for i=0, ni-1 do begin
     for j=0, nj-1 do begin 
        
        if map(i,j) eq 0 then begin
           
           if i eq 0 then begin
              
              if j eq      0 then all_neighbours = [map(i,j+1),map(i+1,j),map(i+1,j+1)] else $
                 if j eq nj-1 then all_neighbours = [map(i,j-1),map(i+1,j-1),map(i+1,j)] else $
                    all_neighbours = [map(i,j-1),map(i,j+1),map(i+1,j-1),map(i+1,j),map(i+1,j+1)] 
              
           endif 
           if i eq ni-1 then begin
              
              if j eq nj-1 then all_neighbours = [map(i-1,j-1),map(i-1,j),map(i,j-1)] else $ 
                 if j eq      0 then all_neighbours = [map(i-1,j),map(i-1,j+1),map(i,j+1)] else $
                    all_neighbours = [map(i-1,j-1),map(i-1,j),map(i-1,j+1),map(i,j-1),map(i,j+1)] 
              
           endif
           
           if i ne ni-1 and i ne 0 then begin
if j eq nj-1 then all_neighbours = [map(i-1,j-1),map(i-1,j),map(i,j-1),map(i+1,j-1),map(i+1,j)]  else $ 
   if j eq      0 then all_neighbours = [map(i-1,j),map(i-1,j+1),map(i,j+1),map(i+1,j),map(i+1,j+1)]  else $
      all_neighbours = [map(i-1,j-1),map(i-1,j),map(i-1,j+1),map(i,j-1),map(i,j+1),map(i+1,j-1),map(i+1,j),map(i+1,j+1)] 

endif
           
           map2(i,j) = avg(all_neighbours[where(all_neighbours ne 0)])
           
;print, map2(i,j), map(i,j)
;print, avg(all_neighbours[where(all_neighbours ne 0)])
;print, all_neighbours
           
        endif
     endfor
  endfor
  
  return, map2
end


pro interp_map, sim=sim, plane=plane, zoom=zoom, factor=factor, threshold=threshold, ps=ps, what_to_plot=what_to_plot, quasar=quasar, contour=contour, lmax_name=lmax_name
;#############################################################################################
;test interpolation des lignes sur carte cartesienne
;#############################################################################################
  
;----------------------------------------------------------------------------------------------PARAMETRES
  colden = 0
  offset = 0                    ;offset = 25
  compact = 1                   ;compact = 0
                                ;WHAT_TO_PLOT =   'compare_lum2' ;'compare_lum' ;'den_temp_sfr' ;'den_em'       ;'colden' ;
;---------------------------------------------------------------------------------------------------------
  
  if compact ne 0 then nm_compact = '_comp' else nm_compact = ''
  
  zoom = strcompress(string(fix(zoom)),/remove_all)
  
  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
  endif else begin
     nm = ''
  endelse
  
  if not keyword_set(contour) then contour = -42
  if not keyword_set(lmax_name) then lmax_name = '13'  

  if keyword_set(quasar) then begin
     qsr = 'quasar_'
     if quasar eq 0 or quasar eq '' then qsr=''
  endif else begin
     qsr = ''
  endelse
  
  if WHAT_TO_PLOT eq 'compare_lum' or WHAT_TO_PLOT eq 'compare_lum2' then nm = 'compare_lum_'
  
  if plane eq 'xz' then dir = 'y'
  if plane eq 'zy' then dir = 'x'
  if plane eq 'dk' then dir = 'z'
  
  if sim eq '00100' or sim eq '00075w' $
     or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
  if sim eq '00085' or sim eq '00075n' then name = 'No'
  
  den_map=readfits('/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/map_'+sim+'_rho_central_'+dir+'_thinslice_lmax'+lmax_name+'.fits.gz', header) 
  
;xz : abscisse x, ordonnee z
;zy : abscisse z, ordonnee y
  taille_pixel = sxpar(header, 'cdelt1')
  if taille_pixel ne sxpar(header, 'cdelt2') then stop
  x_kpc=sxpar(header, 'xsink')
  y_kpc=sxpar(header, 'ysink')
  z_kpc=sxpar(header, 'zsink')
  
  lmax = sxpar(header, 'lmax')
  lmax = strcompress(string(fix(lmax)),/remove_all)
  
  n_pixel = sxpar(header, 'naxis1')
  if n_pixel ne sxpar(header, 'naxis2') then stop
  
  
  if plane eq 'xz' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = z_kpc - offset
  endif
  if plane eq 'yz' or plane eq 'zy' then begin
     pos1_agn = z_kpc - offset
     pos2_agn = y_kpc - offset
     plane = 'zy'
  endif 
  if plane eq 'dk' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = y_kpc - offset
  endif 
  
if lmax_name eq '13' then levelmax = '' else levelmax = 'lmax'+lmax_name+'_'

;ouverture de l'arriere-plan :
  if ps ne 0 then begin
     if WHAT_TO_PLOT eq 'den_temp_sfr' then ps_start, nm+levelmax+qsr+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=29, ysize=31, /helvetica, /bold
     if WHAT_TO_PLOT eq 'compare_lum' then ps_start, nm+levelmax+qsr+'den_temp_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=28, ysize=35, /helvetica, /bold
     if WHAT_TO_PLOT eq 'compare_lum2' then ps_start, nm+levelmax+qsr+'comparison_of_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'_zoom='+zoom+nm_compact+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=28, ysize=30, /helvetica, /bold   
     
     if WHAT_TO_PLOT eq 'den_em' then begin
        ps_start, nm+levelmax+qsr+'den_em'+sim+plane+'_zoom='+zoom+nm_compact+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=50, ysize=30, /helvetica, /bold
        threshold = 0
     endif
     !p.font=0
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1.5
     !y.charsize=1.5
     !p.thick=6
  endif
  if WHAT_TO_PLOT eq 'colden' then begin
     win=0
     colden = 1
     threshold = 0
  endif else begin
     
                                ;win = create_windows(den_map, x_kpc, y_kpc, z_kpc, sim, plane, zoom, 0, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, ps)
     
     
     rg=[-6,6]
     rg=-rg                     ;inverser les couleurs ?
     
     if offset eq 0 then imgxr = [7.5,42.5] else imgxr = [-17.5,17.5]
     imgyr = imgxr
     
     if zoom eq 0 then begin
        xr = imgxr
        yr = xr
     endif else begin
        if offset eq 0 then xr = [pos1_agn-zoom*taille_pixel,pos1_agn+zoom*taille_pixel] else xr = [-zoom*taille_pixel,zoom*taille_pixel]
        if offset eq 0 then yr = [pos2_agn-zoom*taille_pixel,pos2_agn+zoom*taille_pixel] else yr = [-zoom*taille_pixel,zoom*taille_pixel]
     endelse
     
;NB :
;zoom de l'image : (offset = 0)
;zoom=0 -> image = (0.85-0.15)*50 kpc = 35 kpc de large
;sinon :
; image = 2 * zoom * taille_pixel = 2 * zoom * 50 kpc / 2^13 = zoom * 12.2 pc de large.
;soit : zoom = 50 -> largeur = 610 pc
;       zoom = 100 -> largeur = 1.22 kpc 
     
     
     if ps eq 0 then window, 0, xsize = 1000, ysize = 1000
     
     loadct, 0
     
     zoomn = strcompress(string(fix(zoom)),/remove_all)
     xt = strmid(plane, 0, 1)+' axis [kpc]'
     yt = strmid(plane, 1, 1)+' axis [kpc]'
     if plane eq 'dk' then begin
        xt = 'x axis [kpc]'
        yt = 'y axis [kpc]'
     endif
     
     xtickn = ''
     ytickn = ''                ;affiche les valeurs sur les graduations
     !x.omargin = 1
     !y.omargin = 1
     
     if compact ne 0 then begin
        xt = ' '
        yt = ' '
        blank = replicate(' ',8)
        titles = blank          ;supprime les annotations sur les graduations
        xtickn = blank
        ytickn = blank
        !x.margin = 0
        !y.margin = 0
     endif
     
     nm_strct=['test']
     
     
     k_max = n_elements(nm_strct)
     !p.multi = [0,2,k_max/2]
     if WHAT_TO_PLOT eq 'compare_lum2' then begin
        !p.multi=0
        !X.MARGIN=[0,20]
     endif
     
     
     titles=strarr(k_max)
     
     
     for k = 0, k_max-1 do begin
        plotimage, alog10(den_map), range=rg, imgxrange=imgxr, imgyrange=imgyr, xr=xr, yr=yr, title=titles[k], xtitle=xt, ytitle=yt, /isotropic, /save, xtickn=xtickn, ytickn=ytickn
        plots, pos1_agn, pos2_agn, psym=1, symsize=3
        if k eq 0 then begin
           strct = CREATE_STRUCT('p'+nm_strct[0], !p, 'x'+nm_strct[0], !x, 'y'+nm_strct[0], !y)
        endif else begin
           strct = CREATE_STRUCT(strct,'p'+nm_strct[k], !p, 'x'+nm_strct[k], !x, 'y'+nm_strct[k], !y)
        endelse
     endfor
     
     
     !p.multi = 0
     !X.MARGIN=[10,3] & !Y.MARGIN=[4,2] 
     !X.OMARGIN=0 & !Y.OMARGIN=0
     
     
     
  endelse
  
  
  if nm eq 'compare_lum_' then nm = ['', 'x10_', 'x100_']  
  for jj = 0, n_elements(nm)-1 do begin
     colorbar = 1
     
     print, 'Snapshot : ', nm(jj)+levelmax+sim+plane
     
;ouverture des fichiers de lop pour le remplissage
     print, 'ouverture fichier post'
     file_name_post = '/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'post_cloudy_parameters_'+nm(jj)+sim+'_all.dat'
                                ;print, file_name_post
     
     dat_file_info = file_info(file_name_post)
     sav_file_info = file_info('/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav')
                                ;print, '/Users/oroos/Post-stage/LOPs'+sim+'/'+qsr+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'
     
     if dat_file_info.mtime gt sav_file_info.mtime then begin
        readcol, file_name_post, name_lop, x_all, y_all, z_all, theta_all, phi_all, depth_all, den_all, temp_all, $
                 Hneutre_all, Oneutre_all, Hb_4861A_all, OIII_5007A_all, Ha_6563A_all, NII_6584A_all, remaining_lop_points, flag, $
                 format='(A35,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A3)'
        save, name_lop, x_all, y_all, z_all, theta_all, phi_all, depth_all, den_all, temp_all, $
              Hneutre_all, Oneutre_all, Hb_4861A_all, OIII_5007A_all, Ha_6563A_all, NII_6584A_all, remaining_lop_points, flag, $
              file='/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endif else begin
        restore, '/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'post_cloudy_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endelse
     print, 'fin lecture fichier post'
     
     print, 'ouverture fichier ascii'
     file_name_ascii = '/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'ascii_parameters_'+nm(jj)+sim+'_all.dat'
                                ;print, file_name_ascii
     
     dat_file_info = file_info(file_name_ascii)
     sav_file_info = file_info('/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav')
                                ;print, '/Users/oroos/Post-stage/LOPs'+sim+'/'+qsr+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     if dat_file_info.mtime gt sav_file_info.mtime then begin
        readcol, file_name_ascii, name_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, $
                 cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, remaining_lop_points_ascii, flag_ascii, $
                 format='(A35,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A3)'
        save, name_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, $
              cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, remaining_lop_points_ascii, flag_ascii, file='/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endif else begin
        restore, '/Users/oroos/Post-stage/LOPs'+sim+'/'+levelmax+qsr+'ascii_parameters_'+nm(jj)+sim+'_all_dat.sav'
     endelse
     print, 'fin lecture fichier ascii'
     
     
     if plane eq 'xz' then t = where(strmatch(name_lop, '*xz*') eq 1 or strmatch(name_lop, '*px*') eq 1 or strmatch(name_lop, '*cx*') eq 1, nt)
     if plane eq 'zy' then t = where(strmatch(name_lop, '*zy*') eq 1 or strmatch(name_lop, '*py*') eq 1 or strmatch(name_lop, '*cy*') eq 1, nt)
     if plane eq 'dk' then t = where(strmatch(name_lop, '*dk*') eq 1, nt) ; or strmatch(name_lop, '*px*') eq 1 or strmatch(name_lop, '*py*') eq 1, nt)
     
     print, nt, ' points trouves pour la simulation ', qsr+nm(jj)+sim+plane
     
     if nt gt 0 then begin      ;optimiser ca 
        x_all = x_all[t]
        y_all = y_all[t]
        z_all = z_all[t]
        theta_all = theta_all[t] 
        phi_all = phi_all[t]
        depth_all = depth_all[t]
        den_all = den_all[t]
        temp_all = temp_all[t]
        Hneutre_all = Hneutre_all[t]
        Oneutre_all = Oneutre_all[t]
        Hb_4861A_all = Hb_4861A_all[t]
        OIII_5007A_all = OIII_5007A_all[t]
        Ha_6563A_all = Ha_6563A_all[t]
        NII_6584A_all = NII_6584A_all[t]
        remaining_lop_points = remaining_lop_points[t] 
        flag = flag[t]
        name_lop = name_lop[t]
     endif
     
     if plane eq 'xz' then t = where(strmatch(name_ascii, '*xz*') eq 1 or strmatch(name_ascii, '*px*') eq 1 or strmatch(name_ascii, '*cx*') eq 1, nt)
     if plane eq 'zy' then t = where(strmatch(name_ascii, '*zy*') eq 1 or strmatch(name_ascii, '*py*') eq 1 or strmatch(name_ascii, '*cy*') eq 1, nt)
     if plane eq 'dk' then t = where(strmatch(name_ascii, '*dk*') eq 1 , nt) ;or strmatch(name_ascii, '*px*') eq 1 or strmatch(name_ascii, '*py*') eq 1, nt)
     if nt gt 0 then begin
        depth_ascii = depth_ascii[t]
        density_ascii = density_ascii[t]
        temperature_ascii  = temperature_ascii[t] 
        remaining_lop_points_ascii = remaining_lop_points_ascii[t] 
        x_ascii = x_ascii[t]
        y_ascii = y_ascii[t]
        z_ascii = z_ascii[t]
        vx_ascii = vx_ascii[t]
        vy_ascii = vy_ascii[t]
        vz_ascii = vz_ascii[t]
        cell_size_ascii = cell_size_ascii[t] 
        x_lop_ascii = x_lop_ascii[t]
        y_lop_ascii = y_lop_ascii[t]
        z_lop_ascii = z_lop_ascii[t]
        theta_lop_ascii =  theta_lop_ascii[t]
        phi_lop_ascii = phi_lop_ascii[t]
        flag_ascii = flag_ascii[t]
        name_ascii = name_ascii[t]
     endif
     
     
     if colden eq 1 then begin
        get_lun, col_den
        openw, col_den, '/Users/oroos/Post-stage/LOPs'+sim+'/'+nm(jj)+levelmax+qsr+'total_column_densities_sfr_'+strn(threshold)+'Hcc_'+sim+plane+'.txt'
        totl = 0.
     endif
     
  endfor

  
;tests
;zoom=82 <=> zoom * 2 * resol (6pc) = 1 kpc
;pour 1 kpc on veut des pixels d'environ 2*resol --> 82 pixels
  
  resol= 50/2^lmax              ;kpc
  
  eps = 2d*resol                ;kpc

div = 10.

  if zoom lt 1000 then map = dblarr(zoom,zoom) else map = dblarr(round(zoom/div),round(zoom/div))
  nb = map

  if zoom ge 1000 then eps = eps*div
  
  print, minmax(x_all)
  
  t = where(x_all gt x_kpc-zoom*taille_pixel and x_all le x_kpc+zoom*taille_pixel and y_all gt y_kpc-zoom*taille_pixel and y_all le y_kpc+zoom*taille_pixel)


  
  x=x_All[t]
  y=y_all[t]
  den=den_all[t]
  
  print, minmax(x)

  x = x - min(x)
  y = y - min(y)

  print, minmax(x)

  
  ni = (size(map))[1]
  nj = (size(map))[2]
  
  print, 'Debut du mappage...'
  for i=0, ni-1 do begin
     print, float(i+1)/ni*100, ' % ... ', systime()
     for j=0, nj-1 do begin 
        t = where(floor(x/eps) eq i and floor(y/eps) eq j,nt)
        if nt gt 0 then map(i,j) = total(den[t])
        nb(i,j) = nt
        ;if nt gt 0 then print, i,j, nt, den[t]
        
     endfor
  endfor
  
  t=where(map ne 0)
  map[t] = map[t]/nb[t]
  
  log_map = alog10(map)
  t = where(map eq 0,nt0)
  if nt0 gt 0 then log_map[t] = -10
  
  window, 1
  loadct,39
  plotimage, log_map, range=[-10,5], imgxr=[x_kpc-zoom*taille_pixel,x_kpc+zoom*taille_pixel], imgyr=[y_kpc-zoom*taille_pixel,y_kpc+zoom*taille_pixel], /is
  plots, x_kpc, y_kpc, psym=1, color=!white, thick=4, syms=4
  
  cntr=0
  while nt0 ne 0 do begin
     print, 'Iteration #',cntr+1 
     
     map2 = my_interp(map)
     
     log_map2 = alog10(map2)
     t = where(map2 eq 0, nt0)
     if nt0 gt 0 then log_map2[t] = -10
     
     
     window,3
     plotimage, log_map2, range=[-10,5], imgxr=[x_kpc-zoom*taille_pixel,x_kpc+zoom*taille_pixel], imgyr=[y_kpc-zoom*taille_pixel,y_kpc+zoom*taille_pixel], /is
     plots, x_kpc, y_kpc, psym=1, color=!white, thick=4, syms=4
     
     map=map2
     cntr+=1
     
  endwhile
  
  
  stop
  
  
  
  
end
