function create_windows, den_map, x_kpc, y_kpc, z_kpc, sim, plane, zoom, w0, offset, threshold, compact, WHAT_TO_PLOT, taille_pixel, ps
;###########################################################
;cette fonction cree la fenetre ou seront tracees les cartes
;###########################################################

  
  if plane eq 'xz' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = z_kpc - offset
  endif
  if plane eq 'yz' or plane eq 'zy' then begin
     pos1_agn = z_kpc - offset
     pos2_agn = y_kpc - offset
     plane = 'zy'
  endif
  if plane eq 'dk' then begin
     pos1_agn = x_kpc - offset
     pos2_agn = y_kpc - offset
  endif
  
  rg=[-6,6]
  rg=-rg ;inverser les couleurs ?
  
  if offset eq 0 then imgxr = [7.5,42.5] else imgxr = [-17.5,17.5]
  imgyr = imgxr
  
  if zoom eq 0 then begin
     xr = imgxr
     yr = xr
  endif else begin
     if offset eq 0 then xr = [pos1_agn-zoom*taille_pixel,pos1_agn+zoom*taille_pixel] else xr = [-zoom*taille_pixel,zoom*taille_pixel]
     if offset eq 0 then yr = [pos2_agn-zoom*taille_pixel,pos2_agn+zoom*taille_pixel] else yr = [-zoom*taille_pixel,zoom*taille_pixel]
  endelse

 if ps eq 0 then begin
        chars = 2
        textt = 1
     endif else begin
        chars = 5
        textt = 5
     endelse 
  
;NB :
;zoom de l'image : (offset = 0)
;zoom=0 -> image = (0.85-0.15)*50 kpc = 35 kpc de large
;sinon :
; image = 2 * zoom * taille_pixel = 2 * zoom * 50 kpc / 2^13 = zoom * 12.2 pc de large.
;soit : zoom = 50 -> largeur = 610 pc
;       zoom = 100 -> largeur = 1.22 kpc 
  
xs = 800
ys=xs
if what_to_plot eq 'den_em' then xs = 1200
if what_to_plot eq 'compare_lum2' then ys = 500
if what_to_plot eq 'compare_lum2' then xs = 700
if what_to_plot eq 'all_den_temp_sfr' then xs = 1000
if what_to_plot eq 'all_den_temp_sfr' then ys = 0.196/0.31*xs
  if ps eq 0 then window, w0, xsize=xs, ysize=ys, ypos=245, xpos=0

  loadct, 0
  
  zoomn = strcompress(string(fix(zoom)),/remove_all)
  xt = strmid(plane, 0, 1)+' axis [kpc]'
  yt = strmid(plane, 1, 1)+' axis [kpc]'
  if plane eq 'dk' then begin
     xt = 'x axis [kpc]'
     yt = 'y axis [kpc]'
  endif
  
  xtickn = ''
  ytickn = '' ;affiche les valeurs sur les graduations
  !x.omargin = 1
  !y.omargin = 1

  if compact ne 0 then begin
     xt = ' '
     yt = ' '
     blank = replicate(' ',8)
     titles = blank ;supprime les annotations sur les graduations
     xtickn = blank
     ytickn = blank
     !x.margin = 0
     !y.margin = 0
  endif

 if WHAT_TO_PLOT eq 'compare_lum' then nm_strct = ['ti', 'sfri', 'tf', 'sfrf', 'tfx10', 'sfrfx10', 'tfx100', 'sfrfx100'] 
 if WHAT_TO_PLOT eq 'den_em' then nm_strct = ['den','ofr','emo','emn']
 if WHAT_TO_PLOT eq 'den_temp_sfr' then nm_strct = ['den', 'hfr', 'ti', 'tf', 'sfri', 'sfrf']
 if WHAT_TO_PLOT eq 'all_den_temp_sfr' then nm_strct = ['den', 'hfr', 'hfrx10', 'hfrx100', 'ti', 'tf', 'tfx10', 'tfx100', 'sfri', 'sfrf', 'sfrfx10', 'sfrfx100']


if WHAT_TO_PLOT eq 'compare_lum2' then nm_strct = ['one']
 
 
 k_max = n_elements(nm_strct)
 !p.multi = [0,2,k_max/2]
if WHAT_TO_PLOT eq 'compare_lum2' then begin
!p.multi=0
!X.MARGIN=[0,20]
endif
 
 titles=strarr(k_max)
 
;afficher les titres ?
 affiche_titre = 0
 if affiche_titre eq 1 then begin
 if WHAT_TO_PLOT eq 'den_temp_sfr' then titles = [sim+" : log Gas density in cm"+textoidl('^{-3}'),sim+" : log fraction of neutral H",sim+" : log Initial temperature in K",sim+" : log Final temperature in K",sim+" : log Initial SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')+" (> "+strn(threshold, length=6)+" cm"+textoidl('^{-3}')+')',sim+" : log Final SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')+" (> "+strn(threshold, length=6)+" cm"+textoidl('^{-3}')]
    
;titles = [sim+" : log Gas density in cm'+textoidl('^{-3}')",sim+" : log fraction of
;neutral O",sim+" : Emergent emissivity of OIII",sim+" : Intrinsic
;emissivity of OIII",sim+" : Emergent emissivity of NII",sim+" :
;Intrinsic emissivity of NII"]
    if WHAT_TO_PLOT eq 'den_em' then titles = [sim+" : log Gas density in cm"+textoidl('^{-3}'),sim+" : log fraction of neutral O",sim+" : Emergent emissivity of OIII",sim+" : Emergent emissivity of NII"]
if WHAT_TO_PLOT eq 'compare_lum' then titles = [sim+" : log Initial temperature in K", sim+" : log Initial SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log Final_x1 temperature in K", sim+" : log Final_x1 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log Final_x10 temperature in K", sim+" : log Final_x10 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}'),sim+" : log Final_x100 temperature in K", sim+" : log Final_x100 SFR in M"+textoidl('_\odot')+" yr"+textoidl('^{-1}')]

if WHAT_TO_PLOT eq 'one' then titles=[sim+' : log '+textoidl('\rho_{SFR}')+' in M'+textoidl('_\odot')+' yr'+textoidl('^{-1}')+'']
 endif

if WHAT_TO_PLOT eq 'all_den_temp_sfr' then begin
;den, hfr, hfrx10, hfr100, ti, tf, tfx10, tfx100, sfri, sfrf, sfrx10, sfrfx100
left = [0.01,0.296,0.497,0.698,0.01,0.296,0.497,0.698,0.01,0.296,0.497,0.698]
bottom = reverse([0.01,0.01,0.01,0.01,0.33,0.33,0.33,0.33,0.65,0.65,0.65,0.65])
right = [0.197,0.492,0.693,0.894,0.197,0.492,0.693,0.894,0.197,0.492,0.693,0.894]
top = reverse([0.32,0.32,0.32,0.32,0.64,0.64,0.64,0.64,0.96,0.96,0.96,0.96])


titles=['Before RT', 'L'+textoidl('_{AGN}'), '10 x L'+textoidl('_{AGN}'), '100 x L'+textoidl('_{AGN}'), '', '', '', '', '', '', '', '']
endif
 
 for k = 0, k_max-1 do begin

;if WHAT_TO_PLOT eq 'all_den_temp_sfr' then begin
;if k eq 0 or k eq 4 or k eq 8 or k eq 3 or k eq 7 or k eq 11 then !x.margin=[0,10] else !x.margin = 0
;if k le 7 then !y.margin=[0.5,0] else !y.margin = 0
;endif

    if WHAT_TO_PLOT eq 'all_den_temp_sfr' then plotimage, alog10(den_map), range=rg, imgxrange=imgxr, imgyrange=imgyr, xr=xr, yr=yr, title=titles[k], xtitle=xt, ytitle=yt, /save, xtickn=xtickn, ytickn=ytickn, position=[left(k),bottom(k),right(k),top(k)], /noeras, chars=2*chars, textt=textt else $
plotimage, alog10(den_map), range=rg, imgxrange=imgxr, imgyrange=imgyr, xr=xr, yr=yr, title=titles[k], xtitle=xt, ytitle=yt, /isotropic, /save, xtickn=xtickn, ytickn=ytickn
    plots, pos1_agn, pos2_agn, psym=1, symsize=3
    if k eq 0 then begin
       strct = CREATE_STRUCT('p'+nm_strct[0], !p, 'x'+nm_strct[0], !x, 'y'+nm_strct[0], !y)
    endif else begin
       strct = CREATE_STRUCT(strct,'p'+nm_strct[k], !p, 'x'+nm_strct[k], !x, 'y'+nm_strct[k], !y)
    endelse
 endfor
 
 return, strct
end
