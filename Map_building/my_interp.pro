function my_interp, map,nb
  
  ni = (size(map))[1]
  nj = (size(map))[2]
  
  map2=map
  nb2=nb
  
  for i=0, ni-1 do begin
     for j=0, nj-1 do begin 
        
        if nb(i,j) eq 0 then begin
           
           if i eq 0 then begin
              
              if j eq      0 then all_neighbours = [map(i,j+1),map(i+1,j),map(i+1,j+1)] else $
                 if j eq nj-1 then all_neighbours = [map(i,j-1),map(i+1,j-1),map(i+1,j)] else $
                    all_neighbours = [map(i,j-1),map(i,j+1),map(i+1,j-1),map(i+1,j),map(i+1,j+1)] 
              
           endif 
           if i eq ni-1 then begin
              
              if j eq nj-1 then all_neighbours = [map(i-1,j-1),map(i-1,j),map(i,j-1)] else $ 
                 if j eq      0 then all_neighbours = [map(i-1,j),map(i-1,j+1),map(i,j+1)] else $
                    all_neighbours = [map(i-1,j-1),map(i-1,j),map(i-1,j+1),map(i,j-1),map(i,j+1)] 
              
           endif
           
           if i ne ni-1 and i ne 0 then begin
if j eq nj-1 then all_neighbours = [map(i-1,j-1),map(i-1,j),map(i,j-1),map(i+1,j-1),map(i+1,j)]  else $ 
   if j eq      0 then all_neighbours = [map(i-1,j),map(i-1,j+1),map(i,j+1),map(i+1,j),map(i+1,j+1)]  else $
      all_neighbours = [map(i-1,j-1),map(i-1,j),map(i-1,j+1),map(i,j-1),map(i,j+1),map(i+1,j-1),map(i+1,j),map(i+1,j+1)] 

endif
t_neigh = where(all_neighbours ne 0, nt_neigh)
if nt_neigh gt 0 then begin          
           map2(i,j) = avg(all_neighbours[t_neigh])
           nb2(i,j) = 1
endif
           
;print, map2(i,j), map(i,j)
;print, avg(all_neighbours[where(all_neighbours ne 0)])
;print, all_neighbours
           
        endif
     endfor
  endfor
  
strct = {map:map2,nb:nb2}

  return, strct
end

