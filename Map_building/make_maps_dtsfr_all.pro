pro make_maps_dtsfr_all, threshold=threshold, temp_choice=temp_choice

;NB : heating rate == relative temperature change -> just a rewording.

  compare_luminosities_interp, sim='00100', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00100', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00100', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00100', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice

  compare_luminosities_interp, sim='00210', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00210', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00150', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00150', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00075w', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00075w', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice

  compare_luminosities_interp, sim='00210', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00210', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00150', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00150', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00075w', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00075w', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
;;;;;;;;;;;;;;;;;;;;;;;;;
  compare_luminosities_interp, sim='00108', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00108', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice

  compare_luminosities_interp, sim='00170', pl='dk', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00170', pl='xz', z=82, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice

  compare_luminosities_interp, sim='00108', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00108', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice

  compare_luminosities_interp, sim='00170', pl='dk', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice
  compare_luminosities_interp, sim='00170', pl='xz', z=2460, f='', thr=threshold, ps=1, w='all_den_temp_sfr', contour=-42, /interp, temp_choice=temp_choice




end
