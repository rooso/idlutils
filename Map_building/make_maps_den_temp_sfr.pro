pro make_maps_den_temp_sfr, factor=factor, zoom=zoom

compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00100', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp

compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00210', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp

compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00075w', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp

compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00108', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp

compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00150', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp

compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='dk', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f=factor, thr=100, ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f=factor, thr=10,  ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f=factor, thr=1,   ps=1, w='den_temp_sfr', contour=-42, /interp
compare_luminosities_interp, sim='00170', pl='xz', z=zoom, f=factor, thr=0,   ps=1, w='den_temp_sfr', contour=-42, /interp


end
