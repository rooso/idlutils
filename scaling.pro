pro scaling, ps=ps

ps = keyword_set(ps)

;col 1 : N_CPU
;col 2 : time with 128 CPU / time with N_CPU

;128	 1.00
;256	 1.97
;350	 2.53
;512	 3.83
;750	 5.46
;1024	 7.13
;1400    8.63

n_cpu = [128,256,350,512,750,1024,1400]
time_ratio = [1.00,1.97,2.53,3.83,5.46,7.13,8.63]


if ps eq 1 then ps_thick = 16 else ps_thick = 1

if ps eq 0 then begin
window, 1 
endif else begin
   ps_start, 'scaling_prace.eps', /encapsulated, /color, /decomposed, /cm, xsize=27, ysize=22, /helvetica, /bold
   !p.font=0
   !p.charsize=4
   !p.charthick=2
   !x.thick=15
   !y.thick=15
   !p.thick=6
endelse 
plot, n_cpu, n_cpu/128., xtitle='Number of cores', ytitle='Speedup normalized to 128 cores', thick=ps_thick
oplot, n_cpu, time_ratio, color=!red, psym=-1, thick=ps_thick, syms=ps_thick/2.

items = ['Ideal', 'Speedup']
colors = [!black,!red]
psyms = [0,-1]
lines = [0,0]
thicks = ps_thick
syms = ps_thick/2.
  AL_Legend, items, PSym=psyms, Color=colors, /top, textcolor=!black, outline_color=!black, /clear, background_color=!white, /left, /device, thick=thicks, linsize=linsize, syms=syms, lines=lines

if ps eq 1 then ps_end, /png
if ps eq 1 then spawn, 'open scaling_prace.eps'

end
