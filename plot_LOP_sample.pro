function trace, file, theta, phi, color, ps, list=list, center=center

  defplotcolors

     ascii = read_file_ascii(file,/kpc)
     depth = ascii(*,0)
     density = ascii(*,1)


;if max(depth) lt 10 then begin
;window, 3
;plot, depth, density, /xlog, /ylog, xr=[0.001,20], yr=[1e-6,1e6], psym=1
;xyouts, 0.002, 1d4, file
;;stop
;endif

 readcol, "/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/output_00100/sink_00100.out", rien, rien2, x_c, y_c, z_c, /silent
;print, 'x_C, y_c, z_c : ', x_c, y_c, z_c

  depth_max = max(depth)
  ;print, 'depth_max :', depth_max
  x = depth_max*cos(phi)*sin(theta)
  y = depth_max*sin(phi)*sin(theta)
  z = depth_max*cos(theta)
;print, minmax(depth)

    if ps eq 0 then wset,0
     device, decompose=1
     defplotcolors
print, x, y, z
x = [x_c-25, x]
y = [y_c-25, y]
z = [z_c-25, z]
     device, decompose=1
     defplotcolors
     plots, X, Y, Z, /T3D, PSYM=-3, COLOR=color, thick=4
     plots, x_c-25, y_c-25, z_c-25, psym=1, thick=4, /t3d, syms=8

     if center eq 0 then printf, list, "x_BH, y_BH, z_BH : ", x_c-25, y_c-25, z_c-25
     printf, list, x, y, z
     
     return, [x,y,z,minmax(depth)]

end

pro plot_LOP_sample,  ps=ps

  if not keyword_Set(ps) then ps = 0

  if ps ne 0 then begin
     !p.font=0
     !p.charsize=4
     !p.charthick=2
     !p.thick=4
     !x.thick=20
     !y.thick=20
  endif


  device, decompose=1
  defplotcolors
  
  files = findfile('/Users/oroos/Post-stage/LOP_sample/density_profile_*_LOP*.ascii',count=nf)
  colors = fltarr(nf) + !red
  
  print, 'On recherche les fichiers :'
  print, '/Users/oroos/Post-stage/LOP_sample/density_profile_*_LOP*.ascii'
  
  files_phyc = files
  strreplace, files_phyc, 'ascii', 'phyc'

  files_phyc_x10 = files_phyc
  strreplace, files_phyc_x10, 'density', 'x10_density'


  files_phyc_x100 = files_phyc
  strreplace, files_phyc_x100, 'density', 'x100_density'
  
  print, 'En tout, il y en a ', nf, '.'
  
  theta=dblarr(nf)
  phi=dblarr(nf)

  
  if ps eq 0 then window, 0
  
  if ps eq 1 then ps_start, '/Users/oroos/Post-stage/LOP_sample/plot_lops.eps', /encapsulated, /color, /cm, xsize=20, ysize=20
  
  center=0
  openw, list, '/Users/oroos/Post-stage/LOP_sample/list_of_lops.txt', /get_lun     

  device, decompose=1
  defplotcolors
  Plot_3dbox, [-25,25], [-25,25], [-25,25], psym=3, /nodata,   $
              GRIDSTYLE=1, $    ;/SOLID_WALLS
              YZSTYLE=5, AZ=40, TITLE="Distribution of LOPs",      $
              Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
              Ztitle="Z Coordinate" 
                                ;/YSTYLE, ZRANGE=[-25,25],
                                ;XRANGE=[-25,25]
  
  for k = 0, nf-1 do begin
     openr, f, files(k), /get_lun
                                ;print, files(k)
     line1 = ''
     readf, f, line1
                                ;print, line1
     theta(k) = double(strmid(line1,strpos(line1,':')+1,25))
                                ;print, 'theta_strmid = ', strmid(line1,strpos(line1,':')+1,25)
                                ;print, 'theta = ', theta(k)
     phi(k) = double(strmid(line1,strpos(line1,':')+26,25))
                                ;print, 'phi_strmid = ', strmid(line1,strpos(line1,':')+26,25)
                                ;print, 'phi = ', phi(k)

     func = trace(files(k), theta(k), phi(k), colors(k),ps, list=list, center=center)
     if k eq 0 then graph = func else graph = [[graph],[func]]
     center = center + 1

     close, f
     free_lun, f
     
  endfor


  close, list
  free_lun, list

  print, min(graph(4,*)), max(graph(4,*))

yes=0
if ps eq 0 and yes eq 1 then begin
  window, 2
  Plot_3dbox, [-25,25], [-25,25], [-25,25], psym=3, /nodata,   $
              GRIDSTYLE=1, $    ;/SOLID_WALLS
              YZSTYLE=5, AZ=40, TITLE="Distribution of LOPs",      $
              Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
              Ztitle="Z Coordinate" 
  plot_3dbox, graph(0,*), graph(1,*), graph(2,*), /t3d, psym=4, /xy_plane, /xz_plane, /yz_plane, /nodata
endif


;print, 'Min/max de theta : ', minmax(theta)
;print, 'Min/max de phi : ', minmax(phi)
  
;plots,  [25,25], [25,50], [25,25], psym=-4

  if ps eq 1 then ps_end, /png

if ps eq 0 then begin
window, 2
plot, [0.001,20], [1e-6,1e6], /xlog, /ylog, xr=[0.001,20], yr=[1e-6,1e6], psym=1, /nodata
for k=0, nf-1 do begin
     ascii = read_file_ascii(files(k),/kpc)
     depth = ascii(*,0)
     density = ascii(*,1)

     phyc = read_file_phyc(files_phyc(k),/kpc)
     depth_phyc = phyc(*,0)
     density_phyc = phyc(*,2)
oplot, depth, density, psym=1
oplot, depth_phyc, density_phyc, psym=-3, color=!blue
;stop
endfor

endif

  print, 'Fin du programme, tout est OK.'
  
end
