;lecture des LOPs et creation des scripts avec les profils de densite
;pour Cloudy

function make_windows, den_map, w0, taille_pixel,zoom,pos1_agn,pos2_agn,sim,plane, ps

rg=[-6,6]
rg=-rg

if ps eq 0 then window, w0, xpos=0, ypos=0, xsize=1400, ysize=500

!p.multi=[0,2,1]

loadct, 0

;;;;lmax=13
!x.margin=[10,0]
plotimage, alog10(den_map), range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], title="log Column density in cm^-2 ("+sim+")",  xtitle=strmid(plane, 0, 1)+' axis', ytitle=strmid(plane, 1, 1)+' axis', /isotropic, /save
plots, pos1_agn, pos2_agn, psym=1, symsize=3
;cgColorbar, range=rg, title='log Gas density in H/cc', tloc='top', ncol=255, divisions=10, color=0, /fit, /vertical, /reverse
p1_13 = !p & x1_13 = !x & y1_13 = !y

!x.margin=[10,3]
plotimage, alog10(den_map), range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=[pos1_agn-zoom*taille_pixel,pos1_agn+zoom*taille_pixel], yr=[pos2_agn-zoom*taille_pixel,pos2_agn+zoom*taille_pixel], title="log Column density in cm^-2 ("+sim+", zoom)", xtitle=strmid(plane, 0, 1)+' axis', ytitle=strmid(plane, 1, 1)+' axis', /isotropic, /save
plots, pos1_agn, pos2_agn, psym=1, symsize=3
;cgColorbar, range=rg, title='log Gas density in H/cc', tloc='top', ncol=255, divisions=10, color=!p.color, /fit, /vertical, /reverse
p2_13 = !p & x2_13 = !x & y2_13 = !y


;window, w0+1, xsize=600, ysize=600
;loadct, 39
;defplotcolors
;plot, [0.03, 15], [0, 1e6], /xstyle, /ystyle, /nodata, /xlog, /ylog, xtitle="Depth in kpc", ytitle="Gas density in H/cc", title='Density profile along LOPs'
;p3 = !p & x3 = !x & y3 = !y

;strct = {p1:p1,x1:x1,y1:y1,p2:p2,x2:x2,y2:y2};,p3:p3,x3:x3,y3:y3}

strct = {p1_13:p1_13,x1_13:x1_13,y1_13:y1_13,p2_13:p2_13,x2_13:x2_13,y2_13:y2_13}

!p.multi=0

return, strct
end


function affiche_resol, pos1, pos2, resol
for k = 1, 1000 do begin
defplotcolors
PLOTS, CIRCLE(pos1, pos2, k*resol/1000.), color=!black,thick=1
endfor
return, 0
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION read_lop_curie, lop, w1, strct, sim, lmax, plane, random, header, zoom, den_map, filename, color_map, size_map, ps
;read_lops=read_lop_curie(float(lop), 0,  win00100_xz, '00100', 10, 'xz', '', header3, zoom, den_map00100_xz, ps)

;;;utiliser nom fichier directement

lop = strcompress(string(fix(lop)),/remove_all)
lmax = strcompress(string(fix(lmax)),/remove_all)

print, 'lop fonction : ', lop

if plane eq 'zy' then plane = 'yz'


print, 'filename fonction : ', filename

readcol, filename, depth, density, x, y, z, temperature, vx, vy, vz, cell_size, x_lop, y_lop, z_lop, theta_lop, phi_lop, /silent
taille_pixel=50./2^13
x_kpc=sxpar(header, 'xsink')
y_kpc=sxpar(header, 'ysink')
z_kpc=sxpar(header, 'zsink')


filling_factor = 1./5. ;;si jamais il change, le cherche dans file_cloudy
file_cloudy = filename
strreplace, file_cloudy, '.ascii', '.in'
readcol, file_cloudy, cont, cloudy_depth, cloudy_density, format = '(A8,D,D)', /silent
t_cont = where(cont eq 'continue', nt_cont)
if nt_cont gt 0 then begin
cloudy_depth = cloudy_depth[t_cont]
cloudy_density = cloudy_density[t_cont]
endif

;ignorer le premier point arbitraire
cloudy_depth = cloudy_depth(1:n_elements(cloudy_depth)-1)
cloudy_density = cloudy_density(1:n_elements(cloudy_density)-1)

;if min(abs(x_lop - x_kpc)) gt 0.01 or min(abs(y_lop - y_kpc)) gt 0.01 or min(abs(z_lop - z_kpc)) gt 0.01 then begin 
;    print, 'TN : ', x_lop(0), y_lop(0), z_lop(0), ' Attention, la position de centrale ne correspond pas au TN !!'
;    print, filename
;endif else begin  

t_sort = sort(depth)
sorted_depth = depth(t_sort)
sorted_density = density(t_sort)
x_lops = x_lop(t_sort)
y_lops = y_lop(t_sort)
z_lops = z_lop(t_sort)
theta_lops = theta_lop(t_sort)
phi_lops = phi_lop(t_sort)


if plane eq 'xz' then begin
    pos1 = x_lops
    pos2 = z_lops
    pos1_agn = x_kpc
    pos2_agn = z_kpc
 endif
if plane eq 'yz' then begin
    pos1 = z_lops
    pos2 = y_lops
    pos1_agn = z_kpc
    pos2_agn = y_kpc
    plane = 'zy'
endif 

count = 0

for i = 0, (size(sorted_depth))[1]-1 do begin
for j = 0, (size(sorted_depth))[1]-1 do begin   
   if (sorted_depth(i) eq sorted_depth(j) && j ne i) then begin
      count++
      print, sorted_depth(i), sorted_density(i), sorted_density(j)
   endif
endfor
endfor

if count gt 0 then print, 'Il y a ', count, ' problemes'


if ps eq 0 then wset, w1
device, /decompose
defplotcolors
!p = strct.p1 & !x = strct.x1 & !y = strct.y1
oplot, pos1, pos2, col=!white;, line = 2

!p = strct.p2 & !x = strct.x2 & !y = strct.y2
oplot, pos1, pos2, col=!white;, line = 2

;plot, sorted_depth, sorted_density, /xlog, /ylog, xr=[0.01,20],
;xtitle="Depth in kpc", ytitle="Gas density in H/cc", title='Density
;profile along LOP'+lop
;wset, w1+1
;!p = strct.p3 & !x = strct.x3 & !y = strct.y3
;oplot, sorted_depth, sorted_density, color=lop*100
;here, the depth is the distance between the AGN and the current point of the line of sight



;predire la profondeur a laquelle la limite d'ionisation sera
;atteinte en utilisant le critere 10^22 cm-2
taille = n_elements(cloudy_depth)
taille2 = n_elements(sorted_depth)
lg = fltarr(taille)
lg2 = fltarr(taille2)
column_density = fltarr(taille)
column_density2 = fltarr(taille2)

;help, taille
;help, taille2

lg(0) = 10^cloudy_depth(0)
column_density(0) = 10d^(cloudy_density(0))*filling_factor*lg(0)

lg2(0) = sorted_depth(0)/3.24d-22
column_density2(0) = sorted_density*lg2(0)

if ps eq 1 then begin
chars = 2
thick = 5
endif
if ps eq 0 then begin
wset, w1
chars = 0.7
thick = 1
endif

device,decompose=0
cgloadct, 39, /silent, /reverse

for i = 1, taille-1 do begin
      if cloudy_depth(i) eq !VALUES.F_INFINITY then print, 'Warning, infinite value for LOP ', lop

      lg(i) = 10d^cloudy_depth(i)-10d^cloudy_depth(i-1)
      lg2(i) = (sorted_depth(i)-sorted_depth(i-1))/3.24d-22
      ;print, lg(i)

       column_density(i) = column_density(i-1) + 10d^(cloudy_density(i))*filling_factor*lg(i)
       column_density2(i) = column_density2(i-1) + sorted_density(i)*lg2(i)
       ;print, alog10(column_density(i)), alog10(column_density2(i))
    endfor

;print, minmax(column_density2), minmax(column_density)
;if abs(max(column_density2) - max(column_density)) ge 1e22 then begin
;print,  abs(max(column_density2) - max(column_density))
;stop
;endif
if max(column_density2) ge 1e24 then print, max(column_density2) , max(column_density) 

rg = [16,26]

; Here is how we set up the elevation colors.   
   colors = cgScaleVector(Findgen(taille2), rg(0), rg(1))
   ;print, colors
   elevColors = Value_Locate(colors, alog10(column_density2))
   ;print, taille
   ;help, elevColors
   ;print, elevColors

print, min(elevColors), max(elevColors)
print, min(column_density2), max(column_density2)

if min(elevColors) eq -1 then color_min = 0.
if min(elevColors) ge 0 then color_min = float(min(elevColors))/(taille2-1)*255.

color_max = float(max(elevColors))/(taille2-1)*255.

;print, color_min, color_max

   elevColors = Byte(Round(cgScaleVector(elevColors, color_min, color_max)))

rg = reverse(rg)

;help, elevColors
;help, taille

;for i = 0, taille-1 do begin
;print, alog10(column_density2(i)), elevColors(i)
;endfor
   

!p = strct.p1 & !x = strct.x1 & !y = strct.y1
   FOR j=0,taille-2 DO oplot, [pos1[j], pos1[j+1]], [pos2[j], pos2[j+1]], $
       Color=elevColors[j], Thick=thick
resol = 50./2^lmax
res=affiche_resol(pos1_agn, pos2_agn, resol)

device,decompose=0
cgloadct, 39, /silent
cgColorbar, range=rg, title='log column density in H/cm2', tloc='top', ncol=255, divisions=10, color=!p.color, /fit, /vertical, /reverse

device,decompose=0
cgloadct, 39, /silent, /reverse

!p = strct.p2 & !x = strct.x2 & !y = strct.y2
   FOR j=0,taille-2 DO oplot, [pos1[j], pos1[j+1]], [pos2[j], pos2[j+1]], $
       Color=elevColors[j], Thick=thick
resol = 50./2^lmax
res=affiche_resol(pos1_agn, pos2_agn, resol)
device,decompose=0
cgloadct, 39, /silent
;cgDCBar, COLORS=colors, NColors=10, Bottom=1, Labels=labs, /vertical, /fit, /right, /treverse, labpos=0, chars=chars, tchars=chars
cgColorbar, range=rg, title='log column density in H/cm2', tloc='top', ncol=255, divisions=10, color=!p.color, /fit, /vertical, /reverse

return, 0

end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro twoD_colden_map, sim=sim, plane=plane, zoom=zoom, ps=ps

;;;zoom = 50


if plane eq 'xz' then dir = 'y'
if plane eq 'zy' then dir = 'x'

if sim eq '00100' or sim eq '00075w' then name = 'With'
if sim eq '00085' or sim eq '00075n' then name = 'No'

zoom = strcompress(string(fix(zoom)),/remove_all)

den_map=readfits('/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/map_'+sim+'_rho_central_'+dir+'_thinslice_lmax13.fits.gz', header) 
taille_pixel = 50./2^13
x_kpc=sxpar(header, 'xsink')
y_kpc=sxpar(header, 'ysink')
z_kpc=sxpar(header, 'zsink')

;lmax=13
files_13_random = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+plane+'__*.ascii',count = nf_13)

totl=1
if totl eq 1 then begin
if plane eq 'xz' then xt=strmid(plane, 0, 1);x
if plane eq 'zy' then xt=strmid(plane, 1, 1);y

files_13_cone = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*c'+xt+'__*.ascii',count = nf_13_c)
files_13_gal_plane = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*p'+xt+'__*.ascii',count = nf_13_p)

files_13_random = [files_13_random,files_13_cone,files_13_gal_plane]
nf_13 = nf_13 + nf_13_c + nf_13_p
endif

print, "Il y a ", nf_13, ' fichiers'

if plane eq 'xz' then begin
    pos1_agn = x_kpc
    pos2_agn = z_kpc
 endif
if plane eq 'yz' or plane eq 'zy' then begin
    pos1_agn = z_kpc
    pos2_agn = y_kpc
    plane = 'zy'
endif

;keywds = pswindow(/metric)
if ps eq 1 then begin
ps_start, '2D_colden_map'+sim+plane+'_zoom='+zoom+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=45, ysize=15
!p.charsize=4 
!p.charthick=2
endif
;device, _extra=keywds

win=make_windows(den_map, 0, taille_pixel,zoom,pos1_agn,pos2_agn,sim,plane, ps)

defplotcolors

color_map = den_map*0.0
size_map = den_map*0.0

;lmax=13
win_13 = {p1:win.p1_13, x1:win.x1_13, y1:win.y1_13, p2:win.p2_13, x2:win.x2_13, y2:win.y2_13}
for file = 0, nf_13-1 do begin 
    f = files_13_random(file)
    n = strpos(f,sim+'_LOP') + 9
    m = strpos(f, plane+'__')
    len = m - n
    lop = strmid(f, n, len)
    if sim eq '00075w' or sim eq '00075n' then lop = strmid(f, n+1, len-1)
    random = strmid(f, m+4, 20)
    ;print, 'lop, random : ', lop, ' ', random
    ;print, 'filename : ', f
    if strpos(random, '_') ne 0 then read_lops=read_lop_curie(float(lop), 0,  win_13, sim, 13, plane, random, header, zoom, den_map, f, color_map, size_map, ps)
    ;;;dans le cone superieur
    ;if sim eq '00100' and plane eq 'xz' and random eq 'd20130426t095625.750' and lop eq 39 then stop
;/Users/oroos/Curie/Codes_sur_Curie/2D_LOPs/From_cube/LOPs13/density_profile_00100_LOP39xz__d20130426t095625.750.ascii
    
    ;;;dans le disque vers la gauche
    ;if sim eq '00100' and plane eq 'xz' and random eq 'd20130429t151123.502' and lop eq 12 then stop
;/Users/oroos/Curie/Codes_sur_Curie/2D_LOPs/From_cube/LOPs13/density_profile_00100_LOP12xz__d20130429t151123.502.ascii
 endfor

!p.multi=0
;window, 27
;plotimage, read_lops, range=[0.005,0.196], imgxrange=[7.5,42.5], imgyrange=[7.5,42.5]
;print, read_lops

!p.multi=0
if ps eq 1 then print,  'Conversion en png...'
if ps eq 1 then ps_end, /png

print, 'Fin du programme. Tout est OK.'
end
