pro plot_denpro_example, ps=ps

ps = keyword_set(ps)
if ps eq 1 then ps_thick = 16 else ps_thick = 1
if ps eq 1 then linsize = 0.1 else linsize = 1

disk_lop_file = '/Users/oroos/Post-stage/LOPs00100/density_profile_00100_LOP100dk__d20131226t204120.070.ascii'
cone_lop_file = '/Users/oroos/Post-stage/LOPs00100/density_profile_00100_LOP100cd__d20131226t203844.501.ascii'

disk_lop = read_file_ascii(disk_lop_file)
cone_lop = read_file_ascii(cone_lop_file)

items = ['Typical AMR profile in the plane of the disk', 'Interpolated version', 'Typical AMR profile inclined wrt. the disk', 'Interpolated version']
psyms = [1,0,1,0]
lines = [0,0,0,0]
colors = [!black,!blue,!red,!orange]
thicks = ps_thick
syms = ps_thick/4.

if ps eq 1 then begin
   ps_start, 'example_of_density_profile.eps', /encapsulated, /color, /decomposed, /cm, xsize=27, ysize=22, /helvetica, /bold
   !p.font=0
   !p.charsize=4
   !p.charthick=2
   !x.thick=15
   !y.thick=15
   !p.thick=6
endif else begin
   window, 1
endelse
plot, [0.002,20], [6d-5,2d3], /nodata, /xlog, /ylog, /xstyle, /ystyle, xtitle='Depth [kpc]', ytitle='Gas number density [cm'+textoidl('^{-3}')+']'
oplot, disk_lop(*,0), disk_lop(*,1), psym=-3, color=!blue, thick=ps_thick, syms=ps_thick/4.
oplot, disk_lop(*,0), disk_lop(*,1), psym=10, color=!black, thick=ps_thick, syms=ps_thick/4.
oplot, disk_lop(*,0), disk_lop(*,1), psym=1, color=!black, thick=ps_thick, syms=ps_thick/4.
oplot, cone_lop(*,0), cone_lop(*,1), psym=-3, color=!orange, thick=ps_thick, syms=ps_thick/4.
oplot, cone_lop(*,0), cone_lop(*,1), psym=10, color=!red, thick=ps_thick, syms=ps_thick/4.
oplot, cone_lop(*,0), cone_lop(*,1), psym=1, color=!red, thick=ps_thick, syms=ps_thick/4.
  AL_Legend, items, PSym=psyms, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, background_color=!white, /left, /device, thick=thicks, linsize=linsize, syms=syms, lines=lines

if ps eq 1 then ps_end, /png
if ps eq 1 then spawn, 'open example_of_density_profile.png'
stop

end
