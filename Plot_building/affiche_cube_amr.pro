pro affiche_cube_amr, sim=sim, part=part, ps=ps
  
  ps_true = ps
  part_true = part
  if part eq 'xz' or part eq 'zy' then slice = 0.2
  if part eq 'xy' then slice = 0.04
  if part ne 'xz' and part ne 'xy' and part ne 'zy' then slice = 0

if slice lt 50.d3/2d^13 then print, 'Probleme ! Tranche plus fine que la taille minimale des cellules.'
if slice mod 50.d3/2d^13 ne 0 then print, 'Attention, la taille de la tranche n est pas un multiple de la taille minimale des cellules'

  if sim eq '00100' or sim eq '00075w' $
     or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then fb_case = 'With'
  if sim eq '00085' or sim eq '00075n' $
     or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then fb_case = 'No'
  
  sm = sim
  
  if sim eq '00075w' or sim eq '00075n' then sm='00075'
  
  file_sink = '/Users/oroos/Post-stage/orianne_data/'+fb_case+'_AGN_feedback/output_'+sm+'/sink_'+sm+'.out'
  readcol, file_sink,  var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  
  x_agn = x_center(0)
  y_agn = y_center(0)
  z_agn = z_center(0)
  
  print, x_agn, y_agn, z_agn
  
  lmax='13'

print, 'ouverture du cube amr'

file_name_amr = '/Users/oroos/Post-stage/orianne_data/'+fb_case+'_AGN_feedback/gas_part_'+sim+'.ascii.lmax'+lmax
dat_file_info = file_info(file_name_amr)
  sav_file_info = file_info('/Users/oroos/Post-stage/LOPs'+sim+'/amr_'+sim+'_all.sav')
  
  if dat_file_info.mtime gt sav_file_info.mtime then begin 
print, 'readcol'
readcol, file_name_amr, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, cell_size_amr, ilevel_amr, format='(D,D,D,D,D,D,D,D,D,I)'
  
save, /variables, file='/Users/oroos/Post-stage/LOPs'+sim+'/amr_'+sim+'_all.sav'
endif else begin 
print, 'restore'
restore, '/Users/oroos/Post-stage/LOPs'+sim+'/amr_'+sim+'_all.sav'
endelse
print, 'fin lecture cube amr'

  part = part_true
  ps = ps_true
  
  print, 'minmax x_amr', minmax(x_amr)
  
;correction du polytrope de jeans
  alpha = (0.041666*sqrt(32*!dpi)*(50.d3/2^13)^2)
  rho_0 = 900./alpha            ;rho(T = 900 K) H/cc
;print, 'rho_0 : ', rho_0
  t_rho = where(density_amr ge rho_0, nt_rho)
  if nt_rho gt 0 then begin
     T_polytrope = alpha*density_amr[t_rho]
     t_temp = where(temperature_amr[t_rho] le 2*T_polytrope, nt_temp)
     if nt_temp gt 0 then temperature_amr[t_rho(t_temp)] = 900
  endif
  
  
  param = density_amr
  rg=[-7,6]
  ;rg = [min(alog10(param)), max(alog10(param))]


  print, 'minmax param', minmax(param)
  
  taille = n_elements(x_amr)
  
  
  colors = cgScaleVector(dindgen(taille), rg(0), rg(1))
                                ;print, 'colors', colors
  cols = Value_Locate(colors, alog10(param))
  
  print, 'minmax cols', min(cols), max(cols)
  print, 'minmax log(param)', min(alog10(param)), max(alog10(param))
  
  if min(cols) eq -1 then color_min = 0.
  if min(cols) ge 0 then color_min = float(min(cols))/(taille-1)*255.
  
  color_max = float(max(cols))/(taille-1)*255.
  print, 'color_min, color_max', color_min, color_max
  
  if color_min ne color_max then cols = Byte(Round(cgScaleVector(cols, color_min, color_max)))
  if color_min eq color_max then cols = Byte(Round(cols))
  
  print,  'Doivent etre entre 0 et 255 :', minmax(cols)
  
if ps eq 1 then begin
thicks = 2
endif
if ps eq 0 then begin
thicks = 1
endif

 if ps eq 1 then begin
        ps_start, 'cube_amr'+sim+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=30
        !p.charsize=4 
        !p.charthick=2
     endif

  if part eq 'all' then begin
     if ps eq 0 then window, 0, xsize=800, ysize=800
     Plot_3dbox, [0,50], [0,50], [0,50], psym=3, /nodata, /ystyle, /xstyle, /zstyle, /isotropic,  $
                 GRIDSTYLE=1, $ ;/SOLID_WALLS
                 YZSTYLE=5, AZ=40, TITLE="CUBE AMR "+sim,      $
                 Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
                 Ztitle="Z Coordinate"
     
     select_10 = 10*lindgen(n_elements(x_amr)/10)  ;ne prendre qu'un point sur 10
     select_100 = 100*lindgen(n_elements(x_amr)/100) ;ne prendre qu'un point sur 100
     select_1000 = 1000*lindgen(n_elements(x_amr)/1000) ;ne prendre qu'un point sur 100
     
     select = select_10
     
     device,decompose=0
     cgloadct, 39, /silent, /reverse
     cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
     plots, X_amr[select], Y_amr[select], Z_amr[select], /T3D, PSYM=3, COLOR=cols[select], thick=thicks
     ;plots, X_amr, Y_amr, Z_amr, /T3D, PSYM=3, COLOR=cols, thick=1
     device, /decompose

 if ps eq 1 then ps_end, /png
 if ps eq 1 then print, 'Conversion en png...'

yes = 0
if yes eq 1 then begin     
print, 'depart'
cartesian_grid = grid3(x_amr,y_amr,z_amr,param,start=[0.15*50+0.5*50./2d^13,0.15*50.+0.5*50./2d^13,0.15*50.+0.5*50./2d^13],delta=50./2d^13, ngrid=0.7*2d^13)
print, 'ok !!'

save, cartesian_grid, file='cartesian_grid_'+sim+part+'.sav'
;restore, 'cartesian_grid_'+sim+part+'.sav'

help, cartesian_grid
print, 'Nombre attendu : ', 0.7*2d^13

part = 'xy'
slice = 0.08

 if part eq 'xz' then begin
        pos1 = x_amr
        pos2 = z_amr
        proj_dir = y_amr
        pos1_agn = x_agn
        pos2_agn = z_agn
        proj_agn = y_agn
     endif else begin
        if part eq 'zy' then begin
           pos1 = z_amr
           pos2 = y_amr
           proj_dir = x_amr
           pos1_agn = z_agn
           pos2_agn = y_agn
           proj_agn = x_agn
        endif else begin
           if part eq 'xy' then begin
              pos1 = x_amr
              pos2 = y_amr
              proj_dir = z_amr
              pos1_agn = x_agn
              pos2_agn = y_agn
              proj_agn = z_agn
           endif else begin
              print, 'Tranche non reconnue.'
              stop
           endelse
        endelse
     endelse
     
     t_slice = where(abs(proj_dir-proj_agn) le slice/2., nt_slice)

print, nt_slice
     
     if nt_slice gt 0 then begin
        x = pos1[t_slice]
        y = pos2[t_slice]
        c = cols[t_slice]
        p = param[t_slice]
        n = density_amr[t_slice]

p_proj = p*0.0
count_proj = 0

for k = 0L, n_elements(x)-1 do begin
   if p_proj(k) eq 0 then begin
      t_proj = where(x(k) eq x and y(k) eq y, nt_proj)
      if nt_proj gt 0 then begin 
         p_proj[t_proj] = total(n[t_proj]*p[t_proj])/total(n[t_proj])
         ;p_proj[t_proj] = avg(p[t_proj])
         count_proj = count_proj+nt_proj
         
      endif 
   endif
endfor

x_proj1 = x(0)
y_proj1 = y(0)
p_proj1 = p_proj(0)
c_proj1 = c(0)
for j = 1L, n_elements(x)-1 do begin
if p_proj(j) ne p_proj(j-1) then begin
x_proj1 = [x_proj1,x(j)]
y_proj1 = [y_proj1,y(j)]
p_proj1 = [p_proj1,p_proj(j)]
c_proj1 = [c_proj1,c(j)]
endif
endfor

help, x_proj1
print, 'proj', count_proj, n_elements(x)

        res=50./2.D^13          ;kpc
    

         window, 2
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plot, [7.5,42.5],[7.5,42.5], psym=3, /nodata, xtitle='x axis', ytitle='z axis', title='xz plane +/- 1', /xstyle,/ystyle, /isotropic
        plots, x_proj1, y_proj1, psym=3, color=c_proj1
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose

        image_proj = tri_surf(alog10(p_proj1), X_proj1, Y_proj1, GS=[res,res])
        window,20
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plotimage, image_proj, range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=xr, yr=yr, title=sim, /isotropic, /save
        plots, pos1_agn, pos2_agn, psym=1, symsize=3
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose
     endif

endif
;-----------------------------------------------------------

  endif else begin

     if part eq 'xz' then begin
        pos1 = x_amr
        pos2 = z_amr
        proj_dir = y_amr
        pos1_agn = x_agn
        pos2_agn = z_agn
        proj_agn = y_agn
     endif else begin
        if part eq 'zy' then begin
           pos1 = z_amr
           pos2 = y_amr
           proj_dir = x_amr
           pos1_agn = z_agn
           pos2_agn = y_agn
           proj_agn = x_agn
        endif else begin
           if part eq 'xy' then begin
              pos1 = x_amr
              pos2 = y_amr
              proj_dir = z_amr
              pos1_agn = x_agn
              pos2_agn = y_agn
              proj_agn = z_agn
           endif else begin
              print, 'Tranche non reconnue.'
              stop
           endelse
        endelse
     endelse
     
     t_entire_cells = where(proj_dir+cell_size_amr/2. le proj_agn+slice/2. and proj_dir-cell_size_amr/2. ge proj_agn-slice/2., nt_entire_cells) ;prendre meme les cellules dont le centre n'est pas dans la tranche pour le mass-weighting
     t_slice = where(abs(proj_dir-proj_agn) le slice/2., nt_slice) ;mais ne pas oublier de ne compter que les cellules dans la tranche
     
     print, nt_slice
     
     if nt_slice gt 0 then begin
        x = pos1[t_slice]
        y = pos2[t_slice]
        c = cols[t_slice]
        p = param[t_slice]
        n = density_amr[t_slice]

        ;points superposes
        window, 1
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plot, [7.5,42.5],[7.5,42.5], psym=3, /nodata, xtitle='x axis', ytitle='z axis', title='xz plane +/- 1', /xstyle,/ystyle, /isotropic
        plots, x, y, psym=3, color=c
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose

;test image
        res=50./2.D^13          ;kpc
        
;image = MIN_CURVE_SURF(p, X, Y);, GS=[50./2.D^13,50./2.D^13])
        image = tri_surf(alog10(p), X, Y, GS=[res,res])

       
        help, x
        help, y
        help, c
        
        print, 'min/max p : ', minmax(alog10(p))
        
        help, image
        window,10
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plotimage, image, range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=xr, yr=yr, title=sim, /isotropic, /save
        plots, pos1_agn, pos2_agn, psym=1, symsize=3
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        p1 = !p & x1 = !x & y1 = !y
        device, /decompose


       mass_weighted = 1
        if mass_weighted eq 1 then begin
           
;faire une projection sur le plan voulu du parametre A
; projection mass-weighted : (Somme_i A_i*density_i) / (Somme_i
; density_i) 

;test : histogrammes
print, minmax(x)
print, minmax(y)

binsize=50./2.d^13
                   
data = transpose([[x],[y]])
help, data


xy_hist = hist_nd(data, binsize, min=7.5, max=42.5, REVERSE_INDICES=ri)
;REVERSE_INDICES: Set to a named variable to receive the
;         reverse indices, for mapping which points occurred in a
;         given bin.  Note that this is a 1-dimensional reverse index
;         vector (see HISTOGRAM).  E.g., to find the indices of points
;         which fell in a histogram bin [i,j,k], look up:
;
;             ind=[i+nx*(j+ny*k)]
;             ri[ri[ind]:ri[ind+1]-1]

t_double = where(xy_hist ge 1, nt_double)
print, nt_double

help, xy_hist
print, minmax(xy_hist)

window,3
cgloadct, 39
plotimage, xy_hist, range=minmax(xy_hist)


;on a tous les points ou il y a des doubles (+ceux ou il y a un point
;-> mass-weighting ne change rien)
;avec reverse_indices, retrouver les positions correspondantes et
;moyenner p (mass-weighted)

help, t_double
rri = ri[t_double]
rri1 = ri[t_double+1]

x_double = dblarr(nt_double)
y_double = dblarr(nt_double)
p_double = dblarr(nt_double)
c_double = dblarr(nt_double)

;for tt = 0L, nt_double-1 do begin
;if rri(tt) ne rri1(tt) then begin 
;print, 'rri(tt)', rri(tt)
;print, 'rri1(tt)-1', rri1(tt)-1
;print, data[ri[rri(tt):rri1(tt)-1]]
;x_double(tt) =  (data[ri[rri(tt):rri1(tt)-1]])(0)
;y_double(tt) =  (data[ri[rri(tt):rri1(tt)-1]])(1)
;t_param_double = where(x eq x_double(tt) and y eq y_double(tt), nt_param_double)
;print, nt_param_double
;if nt_param_double gt 0 then begin
;p_double(tt) = total(n[t_param_double]*p[t_param_double])/total(n[t_param_double])
;c_double(tt) = c[t_param_double]
;endif
;endif else begin
 ;print, 'No data in bin ', t_double(tt)
;endelse
;endfor

;window, 3
;        device,decompose=0
;        cgloadct, 39, /silent
;        plot, [7.5,42.5],[7.5,42.5], psym=3, /nodata, xtitle='x axis', ytitle='z axis', title='xz plane +/- 1', /xstyle,/ystyle, /isotropic
;        plots, x_double, y_double, psym=3, color=c_double
;        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
;        device, /decompose

 ;       image_double = tri_surf(alog10(p_double), X_double, Y_double, GS=[res,res])
        

 ;       window,30
 ;       device,decompose=0
 ;       cgloadct, 39, /silent
 ;       plotimage, image_double, range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=xr, yr=yr, title=sim, /isotropic, /save
 ;       plots, pos1_agn, pos2_agn, psym=1, symsize=3
 ;       cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
 ;       device, /decompose

;stop

;mass-weighting (uniquement pour les centres de cellules AMR
;superposes)
;--> prendre en compte l'etendue des cellules !!
p_proj = p*0.0
count_proj = 0

t_lmax = where(ilevel_amr[t_entire_cells] eq 13, nt_lmax)
if nt_lmax gt 0 then begin
x_cartesian = pos1[t_lmax]
y_cartesian = pos2[t_lmax]
p_cartesian = param[t_lmax]
c_cartesian = cols[t_lmax]
endif

l_min=min(ilevel_amr)
l_max=max(ilevel_amr)

;comment prendre en compte les cellules de la tranche dont le
;centre n'est PAS dans la tranche ?

for j = l_min, lmax-1 do begin
print, 'La cellule de niveau ', j, ' sera divisee en ', (2^3)^(lmax-j), ' cellules de niveau de raffinement max.'
t_l = where(ilevel_amr[t_entire_cells] eq j, nt_l)
if nt_l gt 0 then begin
x_l = x[t_l]
y_l = y[t_l]

x_l_divided = dblarr(nt_l)
y_l_divided = dblarr(nt_l)


endif
x_cartesian = [x_cartesian, x_l_divided]
y_cartesian = [y_cartesian, y_l_divided]
z_cartesian = [z_cartesian, z_l_divided]
p_cartesian = [p_cartesian, p_l_divided]
c_cartesian = [c_cartesian, c_l_divided]
endfor

for k = 0L, n_elements(x)-1 do begin
   if p_proj(k) eq 0 then begin
      t_proj = where(x(k) eq x and y(k) eq y, nt_proj)
      if nt_proj gt 0 then begin 
         p_proj[t_proj] = total(n[t_proj]*p[t_proj])/total(n[t_proj])
         ;p_proj[t_proj] = avg(p[t_proj])
         count_proj = count_proj+nt_proj
         
      endif 
   endif
endfor

x_proj1 = x(0)
y_proj1 = y(0)
p_proj1 = p_proj(0)
c_proj1 = c(0)
for j = 1L, n_elements(x)-1 do begin
if p_proj(j) ne p_proj(j-1) then begin
x_proj1 = [x_proj1,x(j)]
y_proj1 = [y_proj1,y(j)]
p_proj1 = [p_proj1,p_proj(j)]
c_proj1 = [c_proj1,c(j)]
endif
endfor

help, x_proj1



print, 'proj', count_proj, n_elements(x)
           
   

         window, 2
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plot, [7.5,42.5],[7.5,42.5], psym=3, /nodata, xtitle='x axis', ytitle='z axis', title='xz plane +/- 1', /xstyle,/ystyle, /isotropic
        plots, x_proj1, y_proj1, psym=3, color=c_proj1
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose

        image_proj = tri_surf(alog10(p_proj1), X_proj1, Y_proj1, GS=[res,res])
        

        window,20
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plotimage, image_proj, range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=xr, yr=yr, title=sim, /isotropic, /save
        plots, pos1_agn, pos2_agn, psym=1, symsize=3
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose

        endif
     endif
     
  endelse
  
  
end
