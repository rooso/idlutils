pro rotation_curve, ps=ps

readcol, '/Users/oroos/Post-stage/orianne_data/vrot_00100.txt', radius_kpc, vrot_kmps

if ps eq 0 then window, 0
if ps eq 1 then begin
ps_start, 'rotation_curve.eps', /encapsulated, xsize=20, ysize=15
!p.charsize=4 
!p.charthick=2
endif

plot, radius_kpc, vrot_kmps, /nodata, title='Rotation curve of the simulated galaxy', xtitle='Radius (kpc)', ytitle='v_rot (km/s)'
oplot, radius_kpc, vrot_kmps

if ps eq 1 then ps_end, /png

end
