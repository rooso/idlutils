pro bpt, sim=sim, part=part, factor=factor, ps=ps

if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse

;./LOPs/density_profile_00100_LOP32zy_amrd20130422t150139.945.in 
;density_profile_00100_LOP9yz__d20130426t163332.614.ascii
filenames = '/Users/oroos/Post-stage/LOPs'+sim+'/'+nm+'density_profile_'+sim+'_LOP*'+part+'_*'

files_em_em = findfile(filenames+'.em_emergent',count = nf_em)
files_em_int = files_em_em
strreplace, files_em_int, '.em_emergent', '.em_intrinsic'

demarcation_x = findgen(100)/100*3.5-2.5
demarcation_y1 = 0.61/(demarcation_x-0.47)+1.19 ;Kewley 2001
demarcation_y2 = 0.61/(demarcation_x-0.05)+1.3  ;Kaufmann 2003

t1 = where(demarcation_y1 lt 1.1)
t2 = where(demarcation_y2 lt 1.1)

if ps eq 0 then window, 0, xsize=450, ysize=400
if ps eq 1 then ps_start, nm+'bpt_emergent_emissivites'+sim+part+'.eps', /encapsulated;, /cm, xsize=15, ysize=15

plot, [-2.5,1], [-1.5,1.4], /nodata, title='BPT diagram : Emergent emissivities', xtitle='log [NII]/Ha', ytitle='log [OIII]/Hb', /xstyle, /ystyle, /isotropic
defplotcolors
for k = 0, nf_em-1 do begin
readcol, files_em_em(k), depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
;warning, depth should be corrected before use
djs_xyouts, 0.4, -1.4, 'AGN', chars=2, thick=10
djs_xyouts, -1.7, -1.4, 'HII region', chars=2, thick=10
oplot, alog10(NII_6584A/Ha_6563A), alog10(OIII_5007A/Hb_4861A);, psym=3, color=100*k
oplot, demarcation_x[t1], demarcation_y1[t1], color=!blue, line=2
oplot, demarcation_x[t2], demarcation_y2[t2], color=!blue

endfor
if ps eq 1 then ps_end, /png

if ps eq 0 then window, 1, xsize=450, ysize=400
if ps eq 1 then ps_start, nm+'bpt_intrinsic_emissivites'+sim+part+'.eps', /encapsulated;, /cm, xsize=15, ysize=15

plot, [-2.5,1], [-1.5,1.4], /nodata, title='BPT diagram : Intrinsic emissivities', xtitle='log [NII]/Ha', ytitle='log [OIII]/Hb', /xstyle, /ystyle, /isotropic
defplotcolors
for k = 0, nf_em-1 do begin
readcol, files_em_int(k), depth_int, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
;warning, depth should be corrected before use
djs_xyouts, 0.4, -1.4, 'AGN', chars=2, thick=10
djs_xyouts, -1.7, -1.4, 'HII region', chars=2, thick=10
oplot, alog10(NII_6584A/Ha_6563A), alog10(OIII_5007A/Hb_4861A), psym=3;, color=100*k
oplot, demarcation_x[t1], demarcation_y1[t1], color=!blue, line=2
oplot, demarcation_x[t2], demarcation_y2[t2], color=!blue

endfor
if ps eq 1 then ps_end, /png

end
