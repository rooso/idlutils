pro plot_bh

x_w = [25.3026,24.8715,25.0139,25.2143,25.6459,24.8671]
y_w = [25.2173,25.1231,25.0954,24.6755,25.3214,25.1959]
z_w = [25.0439,25.0196,24.9797,24.9365,24.9146,24.9195]

x_wo = [25.2822,24.8484,24.9811,25.4939,25.8518,25.6422]
y_wo = [25.2246,25.1187,25.0920,24.9374,25.2402,25.6940]
z_wo = [25.0473,25.0991,25.0745,25.0677,25.0616,25.0582]

window, 1, xsize=800, ysize=800
Plot_3dbox, [24,26], [24,26], [24,26], psym=3, /nodata, /ystyle, /xstyle, /zstyle,   $
                  GRIDSTYLE=1, $ ;/SOLID_WALLS
                  YZSTYLE=5, AZ=40, TITLE="BH Position",      $
                  Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
                  Ztitle="Z Coordinate"
plots, x_w, y_w, z_w, /T3D, PSYM=-1, COLOR=!red, thick=1
plots, x_wo, y_wo, z_wo, /T3D, PSYM=-1, COLOR=!blue, thick=1

print, 'avg w position : ', avg(x_w),avg(y_w),avg(z_w)
print, 'avg wo position : ', avg(x_wo),avg(y_wo),avg(z_wo)

end
