pro affiche_cube_post_cloudy, sim=sim, part=part, factor=factor, ps=ps

  part_true = part
  
if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse

  if sim eq '00100' or sim eq '00075w' $
     or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then fb_case = 'With'
  if sim eq '00085' or sim eq '00075n' $
     or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then fb_case = 'No'
  
  sm = sim
  
  if sim eq '00075w' or sim eq '00075n' then sm='00075'
  
  file_sink = '/Users/oroos/Post-stage/orianne_data/'+fb_case+'_AGN_feedback/output_'+sm+'/sink_'+sm+'.out'
  readcol, file_sink,  var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  
  x_agn = x_center(0)
  y_agn = y_center(0)
  z_agn = z_center(0)
  
  print, x_agn, y_agn, z_agn
  
print, 'ouverture du cube near'

file_name_near = '/Volumes/TRANSCEND/'+nm+'cloudy_near_amr_'+sim+'_all.dat'
dat_file_info = file_info(file_name_near)
  sav_file_info = file_info('/Volumes/TRANSCEND/'+nm+'cloudy_near_'+sim+'_all.sav')
  
  if dat_file_info.mtime gt sav_file_info.mtime then begin
print, 'readcol'
readcol, file_name_near, x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,D,I,A5,A35)'
;readcol, '/Volumes/TRANSCEND/'+nm+'cloudy_near_amr_'+sim+'_all.dat',
;x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop,
;phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop,
;Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop,
;NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr,
;SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid,
;in_cell, flag, name


save, x_amr, y_amr, z_amr, distance_min, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, temperature_post, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, SFR100_amr, SFR100_cloudy, SFR100_hybrid, SFR10_amr, SFR10_cloudy, SFR10_hybrid, SFR1_amr, SFR1_cloudy, SFR1_hybrid, SFR0_amr, SFR0_cloudy, SFR0_hybrid, in_cell, flag, name, file='/Volume/TRANSCEND/LOPs'+sim+'/'+nm+'cloudy_near_'+sim+'_all.sav'
  endif else begin 
print, 'restore' 
restore, '/Volumes/TRANSCEND/'+nm+'cloudy_near_'+sim+'_all.sav'
endelse
print, 'fin lecture cube near'
  
  part = part_true
  
  
  print, 'minmax x_amr', minmax(x_amr)
  print, 'minmax x_lop', minmax(x_lop)
  
  ;param = temperature_post      ;prendre nouvelle == post
  ;rg=[2,9]
  
param = Hneutre_lop
rg=[-13,0]
param_name = 'frac_of_neutralH'


  print, 'minmax param', minmax(param)
  
  taille = n_elements(x_amr)
  
  
  colors = cgScaleVector(dindgen(taille), rg(0), rg(1))
                                ;print, 'colors', colors
  cols = Value_Locate(colors, alog10(param))
  
  print, 'minmax cols', min(cols), max(cols)
  print, 'minmax log(param)', min(alog10(param)), max(alog10(param))
  
  if min(cols) eq -1 then color_min = 0.
  if min(cols) ge 0 then color_min = float(min(cols))/(taille-1)*255.
  
  color_max = float(max(cols))/(taille-1)*255.
  print, 'color_min, color_max', color_min, color_max
  
  if color_min ne color_max then cols = Byte(Round(cgScaleVector(cols, color_min, color_max)))
  if color_min eq color_max then cols = Byte(Round(cols))
  
  print,  'Doivent etre entre 0 et 255 :', minmax(cols)
  
  if part eq 'all' then begin

   if ps eq 1 then begin
        ps_start, nm+'cube_near_'+param_name+'_'+sim+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=30
        !p.charsize=4 
        !p.charthick=4
     endif

!x.margin = [10,10]

     if ps eq 0 then window, 0, xsize=800, ysize=800
     Plot_3dbox, [7.5,42.5], [7.5,42.5], [7.5,42.5], psym=3, /nodata, /ystyle, /xstyle, /zstyle,   $
                 GRIDSTYLE=1, $ ;/SOLID_WALLS
                 YZSTYLE=5, AZ=40, TITLE="CUBE POST-CLOUDY "+sim;,      $
                 ;Xtitle="X Coordinate", Ytitle="Y Coordinate",      $
                 ;Ztitle="Z Coordinate"
     
     select_10 = 10*lindgen(n_elements(x_amr)/10)  ;ne prendre qu'un point sur 10
     select_100 = 100*lindgen(n_elements(x_amr)/100) ;ne prendre qu'un point sur 100
     select_1000 = 1000*lindgen(n_elements(x_amr)/1000) ;ne prendre qu'un point sur 1000
     
     ;if ps eq 0 then select = select_1000 
     ;if ps eq 1 then select = where(in_cell eq 1 or in_cell eq 4,nt)

select = where(in_cell eq 1 or in_cell eq 4,nt)
print, nt


;/!\ ICI ON A TOUS LES POINTS ! HORS CHAQUE CELLULE AMR CONTIENT LE
;POINT CLOUDY LE PLUS PROCHE INDEPENDAMMENT DE LA DISTANCE QUI LES
;SEPARE... FILTRER AVEC IN_CELL

     device,decompose=0
     cgloadct, 39, /silent, /reverse
     cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
     plots, X_amr[select], Y_amr[select], Z_amr[select], /T3D, PSYM=3, COLOR=cols[select], thick=1
     plots, X_amr, Y_amr, Z_amr, /T3D, PSYM=3, COLOR=cols, thick=1
     device, /decompose

 if ps eq 1 then ps_end, /png
 if ps eq 1 then print, 'Conversion en png...'

  endif else begin
     
     t_xz = where(abs(z_amr-z_agn) le 0.1, nt_xz)
     
;tester data ??? lire directement sous forme  de matrice
     
     print, nt_xz
     
     if nt_xz gt 0 then begin
        window, 1
        x=x_amr[t_xz]
        y=y_amr[t_xz]
        c = cols[t_xz]
        p = param[t_xz]
        
        mass_weighted = 0
        if mass_weighted eq 1 then begin
           
;faire une projection sur le plan voulu du parametre A
; projection mass-weighted : (Somme_i A_i*density_i) / (Somme_i
; density_i) 
;for k = 0, n_elements(x)-1 do begin
;
;endfor
           
           
        endif
        window, 1
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plot, [7.5,42.5],[7.5,42.5], psym=3, /nodata, xtitle='x axis', ytitle='z axis', title='xz plane +/- 1', /xstyle,/ystyle, /isotropic
        plots, x, y, psym=3, color=c
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        device, /decompose
        
;test image
        res=50./2.D^13          ;kpc
        
;image = MIN_CURVE_SURF(p, X, Y);, GS=[50./2.D^13,50./2.D^13])
        image = tri_surf(alog10(p), X, Y, GS=[res,res])
        
        help, x
        help, y
        help, c
        
        print, 'min/max p : ', minmax(alog10(p))
        
        help, image
        window,10
        device,decompose=0
        cgloadct, 39, /silent, /reverse
        plotimage, image, range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=xr, yr=yr, title=sim, /isotropic, /save
        plots, x_agn, z_agn, psym=1, symsize=3
        cgColorbar, range=rg, ncol=255, divisions=10, color=!p.color, /vertical, /fit, title=title
        p1 = !p & x1 = !x & y1 = !y
        device, /decompose
        
     endif
  endelse
  
  device, /decompose
  
end
