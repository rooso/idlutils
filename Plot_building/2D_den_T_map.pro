function show_map, den_map, x_kpc, y_kpc, z_kpc, sim, lmax, plane, zoom, w0, title, compact, ps

taille_pixel = 50./2^lmax

if plane eq 'xz' then begin
    pos1_agn = x_kpc
    pos2_agn = z_kpc
 endif
if plane eq 'yz' or plane eq 'zy' then begin
    pos1_agn = z_kpc
    pos2_agn = y_kpc
    plane = 'zy'
endif 
if plane eq 'xy' or plane eq 'dk' then begin
    pos1_agn = x_kpc
    pos2_agn = y_kpc
    plane = 'xy'
endif 

xt=strmid(plane, 0, 1)+' axis [kpc]'
yt=strmid(plane, 1, 1)+' axis [kpc]'

  xtickn = ''
  ytickn = '' ;affiche les valeurs sur les graduations
  !x.omargin = 1
  !y.omargin = 1

  if compact ne 0 then begin
     xt = ' '
     yt = ' '
     blank = replicate(' ',20)
     titles = blank ;supprime les annotations sur les graduations
     xtickn = blank
     ytickn = blank
     !x.margin = 0
     !y.margin = 0
  endif

carre=600
rg=[-7,6]

;rg=[-6,4];contraste eleve

rg=reverse(rg)

if ps eq 0 then window, w0, xpos=0, ypos=-450, xsize = 2*carre, ysize = carre+100

!p.multi = [0,2,1]
loadct, 0, /silent
;loadct, 39, /silent

plotimage, alog10(den_map), range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=[pos1_agn-2460*taille_pixel,pos1_agn+2460*taille_pixel], yr=[pos2_agn-2460*taille_pixel,pos2_agn+2460*taille_pixel], title="Large view", xtitle=xt, ytitle=yt, /isotropic, xtickn=xtickn, ytickn=ytickn, chars=2
plots, pos1_agn, pos2_agn, psym=1, symsize=5, thick=20
lab=print_label(!black, 0, ps)

plotimage, alog10(den_map), range=rg, imgxrange=[7.5,42.5], imgyrange=[7.5,42.5], xr=[pos1_agn-zoom*taille_pixel,pos1_agn+zoom*taille_pixel], yr=[pos2_agn-zoom*taille_pixel,pos2_agn+zoom*taille_pixel], title="Zoom in", xtitle=xt, ytitle=yt, /isotropic, xtickn=xtickn, ytickn=ytickn, chars=2;, charthick=400
plots, pos1_agn, pos2_agn, psym=1, symsize=5, thick=20
lab=print_label(!black, 0, ps)

 ; add color bar
loadct, 0, /silent
;loadct, 39, /silent
   cgColorbar, range=rg, title='log Gas density in H/cc', tloc='bottom', ncol=255, color=!p.color, pos=[0.1,0.96,0.9,0.99], div=13, tcharsize=2, charpercent=0.3 ;,textthick=400, charthick=400,



!p.multi=0

return, 0
end


pro twoD_den_T_map, sim=sim, zoom=zoom, compact=compact, ps=ps

lmax=13

if sim eq '00100' or sim eq '00075w' or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
if sim eq '00085' or sim eq '00075n' or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then name = 'No'

if ps ne 0 then begin
     !p.font=0
     !p.charsize=5 
     !p.charthick=2
     !x.charsize=1.5
     !y.charsize=1.5
     !x.thick=20
     !y.thick=20
     !p.thick=2
endif





 ;;;;;;;;;/!\ OLD !!!
;T_map_xz=readfits('/Users/oroos/Curie/orianne_data/map_'+sim+'_T_central_y_thinslice.fits.gz', header_xz) ;abscisse x, ordonnee z
;T_map_zy=readfits('/Users/oroos/Curie/orianne_data/map_'+sim+'_T_central_x_thinslice.fits.gz', header_zy) ;abscisse z, ordonnee y
;taille_pixel = sxpar(header_xz, 'cdelt1')
;x_kpc=sxpar(header_xz, 'xsink')
;y_kpc=sxpar(header_xz, 'ysink')
;z_kpc=sxpar(header_xz, 'zsink')

;, T_map, x_kpc, y_kpc, z_kpc, sim, lmax, plane, zoom, w0, title

;if ps eq 1 then ps_start, '2D_Tinit_map_'+sim+'xz.eps', /encapsulated, /cm, xsize=17, ysize=10
;affiche_xz = show_T_map(T_map_xz, x_kpc, y_kpc, z_kpc, sim, lmax, 'xz', zoom, 0, '', ps)
;if ps eq 1 then ps_end, /png

;if ps eq 1 then ps_start, '2D_Tinit_map_'+sim+'zy.eps', /encapsulated, /cm, xsize=17, ysize=10
;affiche_zy = show_T_map(T_map_zy, x_kpc, y_kpc, z_kpc, sim, lmax, 'zy', zoom, 1, '', ps)
;if ps eq 1 then ps_end, /png

den_map_xz=readfits('/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/map_'+sim+'_rho_central_y_thinslice_lmax13.fits.gz', header_xz)  ;abscisse x, ordonnee z
den_map_zy=readfits('/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/map_'+sim+'_rho_central_x_thinslice_lmax13.fits.gz', header_zy)  ;abscisse z, ordonnee y
den_map_dk=readfits('/Users/oroos/Post-stage/orianne_data/'+name+'_AGN_feedback/map_'+sim+'_rho_central_z_thinslice_lmax13.fits.gz', header_zy)  ;abscisse x, ordonnee y
taille_pixel = sxpar(header_xz, 'cdelt1')
x_kpc=sxpar(header_xz, 'xsink')
y_kpc=sxpar(header_xz, 'ysink')
z_kpc=sxpar(header_xz, 'zsink')
if ps eq 1 then ps_start, '2D_den_map_'+sim+'_z='+strn(zoom)+'_xz.eps', /encapsulated, /cm, xsize=17, ysize=10
affiche_xz = show_map(den_map_xz, x_kpc, y_kpc, z_kpc, sim, lmax, 'xz', zoom, 2, '', compact, ps)
if ps eq 1 then ps_end, /png

if ps eq 1 then ps_start, '2D_den_map_'+sim+'_z='+strn(zoom)+'_zy.eps', /encapsulated, /cm, xsize=17, ysize=10
affiche_zy = show_map(den_map_zy, x_kpc, y_kpc, z_kpc, sim, lmax, 'zy', zoom, 3, '', compact, ps)
if ps eq 1 then ps_end, /png

if ps eq 1 then ps_start, '2D_den_map_'+sim+'_z='+strn(zoom)+'_dk.eps', /encapsulated, /cm, xsize=17, ysize=10
affiche_dk = show_map(den_map_dk, x_kpc, y_kpc, z_kpc, sim, lmax, 'xy', zoom, 4, '', compact, ps)
if ps eq 1 then ps_end, /png

if ps eq 1 then spawn, 'open 2D_den_map_'+sim+'_z='+strn(zoom)+'_*.png'

end
