function read_lines, filename, w1, strct, linestyle, pl, radius_inner, fill_fact, ps

plt=pl

readcol, filename, depth, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent

depth = depth + radius_inner

if plt eq 1 then begin
if ps eq 0 then wset, w1
!p = strct.p & !x = strct.x & !y = strct.y
;oplot, depth, Lya_1216A, color=!red, line=linestyle
;oplot, depth, Lyb_1026A, color=!green, line=linestyle
oplot, depth, Hb_4861A*fill_fact, color=!black, line=linestyle
oplot, depth, OIII_5007A*fill_fact, color=!red, line=linestyle
oplot, depth, Ha_6563A*fill_fact, color=!blue, line=linestyle
oplot, depth, NII_6584A*fill_fact, color=!green, line=linestyle
;oplot, depth, OI_6300A, color=!cyan, line=linestyle
;oplot, depth, SII_6731A_6716A, color=!yellow, line=linestyle
;oplot, depth, NeIII_3869A, color=!orange, line=linestyle
;oplot, depth, OII_3727A_multiplet, color=!gray, line=linestyle
;oplot, depth, NeV_3426A, color=!slate, line=linestyle
;oplot, depth, SiVII_2481m, color=!brown, line=linestyle
;oplot, depth, Brg_2166m, color=!tan, line=linestyle
;oplot, depth, SiVI_1963m, color=!dred, line=linestyle
;oplot, depth, AlIX_2040m, color=!lpink, line=linestyle
;oplot, depth, CaVIII_2321m, color=!dcyan, line=linestyle
;oplot, depth, NeVI_7652m, color=!lcyan, line=linestyle

;oplot, depth, OIII_5007A/Hb_4861A, color=!red, line=linestyle
;oplot, depth, NII_6584A/Ha_6563A, color=!green, line=linestyle

endif

return,  0

end

function read_phys_conditions, file_phyc, file_fionH, file_fionO, file_density, file_continua, file_emergent, file_intrinsic, file_ascii, rion, w0, linestyle, pl, value1, value2, nm, ps

lmax=13

  plt=pl
  resolution = 50./2^lmax          ;in kpc
  resolution = resolution / 3.24e-22 ;in cm

  readcol, file_phyc, depth, temperature, hden, eden, heating, rad_acc, fill_fact, /silent
  ;units : cm, K, cm-3, cm-3, erg/cm3/s, number
  hden = hden*fill_fact

  print, 'Filling factor : ', fill_fact(1)

  READCOL, file_fionH, depth_H, HI, HII, H2, /silent
  READCOL, file_fionO, depth_O, OI, OII, OIII, OIV, OV, OVI, OVII, OVIII, OIX, O1, O1e, O1ee, /silent
  ;units : cm, log numbers

  readcol, file_density, col_continue, depth_den, density, format='(A8,D,D)', /silent
  ;units : none, log cm, log H/cc
  cmp = strcmp(col_continue, 'continue')
  t = where( cmp eq 1 and depth_den gt -35, nt)
  col_continue = col_continue[t]
  depth_den = depth_den[t]
  density = density[t]

  ;pas de correction de pfdeur car pre-cloudy

  ;correction de la densite !!
  density = density + alog10(fill_fact)

;le nuage et l'AGN sont separes de ??? -> depend du profil de densite
;'radius inner'
  openr, fin, file_density, /get_lun
  line1=''
  readf, fin, line1
  line2=''
  readf, fin, line2
  line3=''
  readf, fin, line3
if nm ne '' then begin
  line4=''
  readf, fin, line4
endif
  line_radius = ''
  readf, fin, line_radius
  close, fin
  free_lun, fin

;print, line1, line2, line3, line_radius

  inner = strpos(line_radius, 'inner')
  outer = strpos(line_radius, 'outer')
  len = outer - inner
  radius_inner = double(strmid(line_radius,inner+5,len-5))
  print, 'rayon', radius_inner

if radius_inner eq 0 then stop
  
  radius_inner = 10^radius_inner ;en cm
;print, radius_inner
;print, depth_H, depth_H + radius_inner


;profondeur depuis l'AGN
  depth_H = depth_H + radius_inner ;cm
  depth_O = depth_O + radius_inner ;cm
  depth = depth + radius_inner     ;cm
                                  ;=incident
  readcol, file_continua, lambda, nuFnu, attenuated, diffuse, transmitted, reflected, /silent
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s -> incident radiation
    ;transmitted = diffuse + attenuated

  readcol, file_emergent, depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
  readcol, file_intrinsic, depth_int, Lya_1216A_int, Lyb_1026A_int, Hb_4861A_int, OIII_5007A_int, Ha_6563A_int, NII_6584A_int, OI_6300A_int, SII_6731A_6716A_int, NeIII_3869A_int, OII_3727A_multiplet_int, NeV_3426A_int, SiVII_2481m_int, Brg_2166m_int, SiVI_1963m_int, AlIX_2040m_int, CaVIII_2321m_int, NeVI_7652m_int, /silent
;depth : cm
;emissivities

  depth_em = depth_em + radius_inner ;cm
  depth_int = depth_int + radius_inner ;cm

  readcol, file_ascii, depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, format='(D,D,D,D,D,D,D,D,D,D,D,D,D)', /silent

  t_sort = sort(depth_ascii)
  depth_ascii = depth_ascii[t_sort]/3.24e-22 ;ici pas de correction car donnees pre-cloudy
  density_ascii = density_ascii[t_sort]
  temperature_ascii = temperature_ascii[t_sort]


;determination de la profondeur a laquelle le milieu devient neutre
;et des profondeurs definissant les regions reionisees, le cas echeant

  critere99 = 0.99              ;critere neutre/ionise : une region est ionisee si elle contient 99% de HII.
  critere95 = 0.95              ;critere neutre/ionise : une region est ionisee si elle contient 95% de HII.
  critere90 = 0.90              ;critere neutre/ionise : une region est ionisee si elle contient 90% de HII.

  print, 'H atomique : ', minmax(HI)
  print, 'H+ : ', minmax(HII)
  print, 'H2 moleculaire : ', minmax(H2)

  Hneutre = HI + H2
  Hionise = HII
;print, (HI-H2)/HI*100.

  bornes_inf = 0
  bornes_sup = 0

  pfdeur_inf = Hionise*0.
  pfdeur_sup = Hionise*0.

  critere = 0.90

  if value1 eq 1 then begin
     if ps eq 0 then wset, w0

     plot, [1e-7,1e8], [1e-15,1e5], /nodata, xtitle='Wavelength (micron)', ytitle="SED (erg/cm2/s)", /ylog, /xlog, position = [0.1,0.79,0.95,0.99], /noerase, /xstyle, /ystyle
 ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["Incident spectrum", "Transmitted spectrum", "Reflected spectrum" ]
     psyms = [-3, -3, -3]
     colors = [!green, !red, !blue]
     linestyles = [0, 0, 0]   
    ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     if plt eq 1 then oplot, lambda, nuFnu, color=!green, line=linestyle
     if plt eq 1 then oplot, lambda, transmitted, color=!red, line=linestyle
     if plt eq 1 then oplot, lambda, reflected, color=!blue, line=linestyle
     p1 = !p & x1 = !x & y1 = !y


     plot, [19,23],  [-17,0.5], /nodata, /xstyle, title='Ionization fraction of H and density profile', xtitle='log Depth (cm)', ytitle='log Ioniziation fraction', ystyle=9, ymargin=[4,4], position = [0.1,0.26,0.45,0.46], /noerase
     oplot, [alog10(resolution),alog10(resolution)], [-17,0.5], color=!cyan
     if plt eq 1 then oplot, alog10(depth_H), alog10(HI+H2), line=linestyle
     if plt eq 1 then oplot, alog10(depth_H), alog10(HII), color=!red, line=linestyle
     ;if plt eq 1 then oplot, alog10(depth_H), alog10(H2), color=!green, line=linestyle
                              ;unit cm
     if Hionise(0) gt critere then begin
        print, 'Attention, borne inferieure = debut de la LOP !'
        printf, rion, 'Attention, borne inferieure = debut de la LOP !'
        bornes_inf = bornes_inf + 1
        pfdeur_inf(0) = depth_H(0) 
        if plt eq 1 then oplot, [alog10(depth_H(0)),alog10(depth_H(0))], [-17,0.5], line=0, color=!red
     endif
     for k = 1, (size(Hionise))[1]-1 do begin
        if Hionise(k) ge critere and Hionise(k-1) lt critere then begin
           print, 'borne inferieure'
           bornes_inf = bornes_inf + 1
           pfdeur_inf(k) = depth_H(k) 
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
        if (Hionise(k-1) ge critere and Hionise(k) lt critere) then begin 
           print, 'borne superieure'
           bornes_sup = bornes_sup + 1
           pfdeur_sup(k) = depth_H(k) 
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
        
        if bornes_sup ne bornes_inf and k eq (size(Hionise))[1]-1 then begin
           print, 'Attention, borne superieure = fin de la LOP !'
           printf, rion, 'Attention, borne superieure = fin de la LOP !'
           pfdeur_sup(k) = depth_H(k)  
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
     endfor
     print, 'nombre de bornes : ', bornes_inf, bornes_sup
     
     if bornes_sup eq 0 and bornes_inf eq 0 then begin
        print, 'Attention, critere ', critere*100, ' % de Hionise trop eleve ! La ligne est totalement neutre. Longueur : ', max(depth_H), " cm."
        printf, rion, 'Attention, critere ', critere*100, ' % de Hionise trop eleve ! La ligne est totalement neutre. Longueur : ', max(depth_H), " cm."
     endif else begin
        
        print, "Critere utilise : > ", critere*100, " % de Hionise == region ionisee." 
        printf, rion, "Critere utilise : > ", critere*100, " % de Hionise == region ionisee."
        
        t_inf = where(pfdeur_inf gt 0, nt_inf)
        t_sup = where(pfdeur_sup gt 0)
;print, pfdeur_sup[t_sup], pfdeur_inf[t_inf]
        pfdeur_sup = pfdeur_sup[t_sup]
        pfdeur_inf = pfdeur_inf[t_inf]
        taille_ionise = pfdeur_sup - pfdeur_inf 
        print, 'Taille des zones ionisees en cm : ', taille_ionise
        printf, rion, 'Taille des zones ionisees en cm : ', taille_ionise

        for i = 0, (size(pfdeur_inf))[1]-1 do begin
           if (i eq 0) then begin
              if pfdeur_inf(0) ne depth_H(0) then print, "Taille de la premiere zone neutre en cm : ", pfdeur_inf(0)
              if pfdeur_inf(0) ne depth_H(0) then printf, rion, "Taille de la premiere zone neutre en cm : ", pfdeur_inf(0)
           endif
           if (i gt 0 and i lt (size(pfdeur_inf))[1]-1) then begin
              print, "Taille des zones neutres suivantes en cm : ", pfdeur_inf(i) - pfdeur_sup(i-1)
              printf, rion, "Taille des zones neutres suivantes en cm : ", pfdeur_inf(i) - pfdeur_sup(i-1)
           endif
           if (i eq (size(pfdeur_inf))[1]-1) then begin
              print, "Taille de la derniere zone neutre en cm : ", max(depth_H) - pfdeur_sup(i)
              printf, rion, "Taille de la derniere zone neutre en cm : ", max(depth_H) - pfdeur_sup(i)
           endif
        endfor
     endelse 
      ;for k = 0, bornes_inf-1 do begin
      ;  if plt eq 1 then oplot, [alog10(pfdeur_inf(k)),alog10(pfdeur_inf(k))], [-17,0.5], line=2, color=!red
      ;  if plt eq 1 then oplot, [alog10(pfdeur_sup(k)),alog10(pfdeur_sup(k))], [-17,0.5], line=2, color=!red
     ;endfor
     ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["neutral H", "Ionized H", "Density profile"]
     psyms = -3
     colors = [0, !red, !blue]
     linestyles = 0
    ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     axis, yaxis=1, yrange=[min(density)-0.5,max(density)+0.5], ytitle='log Gas density (H/cc)', /save
     if plt eq 1 then oplot, depth_den, density, color=!blue, line=linestyle
     p4 = !p & x4 = !x & y4 = !y
     
     plot, [19,23],  [-17,0.5], /nodata, /xstyle, title='Ionization fraction of O and density profile', xtitle='log Depth (cm)', ytitle='log Ioniziation fraction', ystyle=9, ymargin=[4,4], position = [0.55,0.26,0.95,0.46], /noerase
     oplot, [alog10(resolution),alog10(resolution)], [-17,0.5], color=!cyan
     if plt eq 1 then begin 
        oplot, alog10(depth_O), alog10(OI), line=linestyle
         oplot, alog10(depth_O), alog10(1-OI), line=linestyle, color=!red
        ;oplot, alog10(depth_O), alog10(OII), color=!red, line=linestyle
        ;oplot, alog10(depth_O), alog10(OIII), color=!green, line=linestyle
        ;oplot, alog10(depth_O), alog10(OIV), color=!purple, line=linestyle
        ;oplot, alog10(depth_O), alog10(OV), color=!pink, line=linestyle
        ;oplot, alog10(depth_O), alog10(OVI), color=!cyan, line=linestyle
        ;oplot, alog10(depth_O), alog10(OVII), color=!orange, line=linestyle
        ;oplot, alog10(depth_O), alog10(OVIII), color=!yellow, line=linestyle
        ;oplot, alog10(depth_O), alog10(OIX), color=!blue, line=linestyle
        ;oplot, alog10(depth_O), alog10(O1), color=!magenta, line=linestyle
        ;oplot, alog10(depth_O), alog10(O1e), color=!brown, line=linestyle
        ;oplot, alog10(depth_O), alog10(O1ee), color=!tan, line=linestyle
     endif
                                ;unit cm
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [alog10(pfdeur_inf(k)),alog10(pfdeur_inf(k))], [-17,0.5], line=2, color=!red
        if plt eq 1 then oplot, [alog10(pfdeur_sup(k)),alog10(pfdeur_sup(k))], [-17,0.5], line=2, color=!red
     endfor
     ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["Neutral O", "Ionized O", "Density profile"];["OI", "OII","OIII","OIV","OV","OVI","OVII","OVIII","OIX","O1","O1e","O1ee" ]
     psyms = -3
     colors = [0, !red, !blue];, !green, !purple, !pink, !cyan, !orange, !yellow, !blue, !magenta, !brown, !dgreen]
     linestyles = 0
                                ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     axis, yaxis=1, yrange=[min(density)-0.5,max(density)+0.5], ytitle='log Gas density (H/cc)', /save
     if plt eq 1 then oplot, depth_den, density, color=!blue, line=linestyle
     p5 = !p & x5 = !x & y5 = !y
     
     plot, [1e19,1e23], [1,1e8], /nodata, /ylog, /xlog, xtitle='Depth (cm)', ytitle='Electron temperature (K)', position = [0.1,0.52,0.45,0.72], /noerase, title='Equilibrium temperature of the gas'
     oplot, [resolution,resolution], [1,1e8], color=!cyan
     if plt eq 1 then oplot, depth, temperature, line=linestyle
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [1e-4,1e8], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [1e-4,1e8], line=2, color=!red
     endfor
     p2 = !p & x2 = !x & y2 = !y
     
     plot, [1e19,1e23], [1e-3,1e6], /nodata, /xlog, /ylog, xtitle='Depth (cm)', ytitle='Hydrogen and electron density (cm-3)', position = [0.55,0.52,0.95,0.72], /noerase, title='Hydrogen and electron density without correction for the filling factor', /ystyle
     oplot, [resolution,resolution], [1e-3,1e6], color=!cyan
     if plt eq 1 then oplot, depth, hden/fill_fact, line=linestyle
     if plt eq 1 then oplot, depth, eden, color=!red, line=linestyle
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [1e-3,1e6], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [1e-3,1e6], line=2, color=!red
     endfor
     ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["H density", "e- density"]
     psyms = [-3, -3]
     colors = [0, !red]
     linestyles = [0, 0]
                                ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     p3 = !p & x3 = !x & y3 = !y
     
     plot, [1e-5,1e5], [0.1,1e9], /ystyle, /xlog, /ylog, /xstyle, /nodata, title='Temperature - density diagram', xtitle='Density in H/cc', ytitle='Temperature in K', position = [0.1,0.05,0.45,0.20], /noerase
                   
     ;n-T diagram
;density est en log[H/cc] et density_ascii en H/cc
;construction de la temperature post-cloudy : max des valeurs avt/apres
;pb : pas le mm echantillonnage
     ;temp_resample = resample(depth_ascii, temperature_ascii, depth)
     ;den_resample = resample(depth_ascii, density_ascii, depth)

;;;interpoler comme cloudy
yy = alog10(density_ascii[1:n_elements(depth_ascii)-1])       ;density_amr
xx = alog10(depth_ascii[1:n_elements(depth_ascii)-1])         ;depth_amr
den_resample = 10d^(interpol(yy,xx,alog10(depth)))  ;density resampled onto Cloudy depth variable

yy2 = alog10(temperature_ascii[1:n_elements(depth_ascii)-1])      ;temperature_amr
xx = alog10(depth_ascii[1:n_elements(depth_ascii)-1])             ;depth_amr
temp_resample = 10d^(interpol(yy2,xx,alog10(depth)))  ;temperature resampled onto Cloudy depth variable

t=where(finite(temp_resample) eq 0, nt)
if nt gt 0 then stop
     temp_post_cloudy = temperature*0
     den_post_cloudy = hden*0

     taille_amr=(size(depth_ascii))[1]
     taille_cloudy=(size(depth))[1] ;inferieure a taille_amr
     for k = 0, taille_cloudy-1 do begin
        if temp_resample(k) ge temperature(k) then begin
           temp_post_cloudy(k) =  temp_resample(k)
           den_post_cloudy(k) = den_resample(k)
        endif
        if temp_resample(k) lt temperature(k) then begin
           temp_post_cloudy(k) =  temperature(k)
           den_post_cloudy(k) =  hden(k)
        endif
     endfor

;density_ascii, temperature_ascii : valeurs initiales
;hden, temperature_electrons : valeurs sorties de cloudy
;hden, temp_post_cloudy : max entre valeur cloudy et valeur initiale :
;valeur physique post-cloudy
;temp_all_post_cloudy : valeur physique + valeur initiale la ou
;cloudy n'est pas alle (s'arrete a T = 4000 K)
     ;if plt eq 1 then oplot, density_ascii, temperature_ascii, psym=1, color=!blue ;pre-cloudy
     if plt eq 1 then oplot, den_resample, temp_resample, psym=3, color=!blue ;pre-cloudy re-echantillonne
     if plt eq 1 then oplot, den_post_cloudy, temp_post_cloudy, psym=3, color=!red            ;post-cloudy
     if plt eq 1 then oplot, [1e-10,1e10], [1e4,1e4], color=!black
     if plt eq 1 then oplot, [10,10], [0.1,1e10], color=!black
     ;POINTS QUI ONT VRAIMENT ETE CHAUFFES
     t = where(temperature ge temp_resample, nt)
     if plt eq 1 and nt gt 0 then oplot, den_post_cloudy[t], temp_post_cloudy[t], psym=3, color=!red
 ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["Resampled initial values", "Final values" ]
     psyms = [1, 1]
     colors = [!blue, !red]
     linestyles = [0,0]
                                ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear

     print, 'T ap/avt : ', minmax(temp_post_cloudy), minmax(temperature_ascii)
     print, 'rho ap/avt : ',minmax(hden), minmax(density_ascii)
;print, temperature
     t = where(depth_ascii ge max(depth))
     temp_all_post_cloudy = [temp_post_cloudy,temperature_ascii[t]]
     depth_all_post_cloudy = [depth,depth_ascii[t]]
     density_all_post_cloudy = [hden,density_ascii[t]] 
     p6 = !p & x6 = !x & y6 = !y

     plot, [1e19,1e23], [-2.5,1.5], /ystyle, /xlog, /nodata, title='Diagnostic ratios', xtitle='Depth in cm', ytitle='log Emergent emissivity ratio', position = [0.55,0.05,0.95,0.20], /noerase
     oplot, [resolution,resolution], [-2.5,1.5], color=!cyan
     ;BPT ratios
     oplot, depth, alog10(OIII_5007A/Hb_4861A), color=!black
     oplot, depth, alog10(NII_6584A/Ha_6563A), color=!red

;TBT ratios
     ;oplot, depth,  NeIII_3869A/OII_3727A_multiplet, color=!red

;traceur individuel
     ;oplot, depth, NeV_3426A, color=!cyan

;blue diagram
     ;oplot, depth, OIII_5007A/Hb_4861A, color=!green
     ;oplot, depth, OII_3727A_multiplet/Hb_4861A, color=!pink
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [-3,1.5], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [-3,1.5], line=2, color=!red
     endfor
                           ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["log [OIII]/Hb", "log [NII]/Ha" ]
     psyms = [1, 1]
     colors = [!black, !red]
     linestyles = [0,0]
                                ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     p7 = !p & x7 = !x & y7 = !y

  endif

  if value2 eq 1 then begin
  if ps eq 0 then wset, w0+1
     !p.multi=[0,2,3]
     plot, [19,23],  [-17,0.5], /nodata, /xstyle, title='Ionization fraction of H and density profile', xtitle='log Depth (cm)', ytitle='log Ioniziation fraction', ystyle=9, ymargin=[4,4], /noerase
     oplot, [alog10(resolution),alog10(resolution)], [-17,0.5], color=!cyan
     if plt eq 1 then oplot, alog10(depth_H), alog10(HI), line=linestyle
     if plt eq 1 then oplot, alog10(depth_H), alog10(HII), color=!red, line=linestyle
     if plt eq 1 then oplot, alog10(depth_H), alog10(H2), color=!green, line=linestyle
                              ;unit cm
     bornes_inf = 0
     bornes_sup = 0

     pfdeur_inf = Hionise*0.
     pfdeur_sup = Hionise*0.
     
     critere = 0.90
     if Hionise(0) gt critere then begin
        print, 'Attention, borne inferieure = debut de la LOP !'
        bornes_inf = bornes_inf + 1
        pfdeur_inf(0) = depth_H(0) 
        if plt eq 1 then oplot, [alog10(depth_H(0)),alog10(depth_H(0))], [-17,0.5], line=0, color=!red
     endif
     for k = 1, (size(Hionise))[1]-1 do begin
        if Hionise(k) ge critere and Hionise(k-1) lt critere then begin
           print, 'borne inferieure'
           bornes_inf = bornes_inf + 1
           pfdeur_inf(k) = depth_H(k) 
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
        if (Hionise(k-1) ge critere and Hionise(k) lt critere) then begin 
           print, 'borne superieure'
           bornes_sup = bornes_sup + 1
           pfdeur_sup(k) = depth_H(k) 
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
        
        if bornes_sup ne bornes_inf and k eq (size(Hionise))[1]-1 then begin
           print, 'Attention, borne superieure = fin de la LOP !'
           pfdeur_sup(k) = depth_H(k)  
           if plt eq 1 then oplot, [alog10(depth_H(k)),alog10(depth_H(k))], [-17,0.5], line=0, color=!red
        endif
     endfor
     print, 'nombre de bornes : ', bornes_inf, bornes_sup
     
     if bornes_sup eq 0 and bornes_inf eq 0 then begin
        print, 'Attention, critere ', critere*100, ' % de Hionise trop eleve ! La ligne est totalement neutre. Longueur : ', max(depth_H), " cm."
        printf, rion, 'Attention, critere ', critere*100, ' % de Hionise trop eleve ! La ligne est totalement neutre. Longueur : ', max(depth_H), " cm."
     endif else begin
        
        print, "Critere utilise : > ", critere*100, " % de Hionise == region ionisee." 
        printf, rion, "Critere utilise : > ", critere*100, " % de Hionise == region ionisee."
        
        t_inf = where(pfdeur_inf gt 0, nt_inf)
        t_sup = where(pfdeur_sup gt 0)
;print, pfdeur_sup[t_sup], pfdeur_inf[t_inf]
        pfdeur_sup = pfdeur_sup[t_sup]
        pfdeur_inf = pfdeur_inf[t_inf]
        taille_ionise = pfdeur_sup - pfdeur_inf 
        print, 'Taille des zones ionisees en cm : ', taille_ionise
        printf, rion, 'Taille des zones ionisees en cm : ', taille_ionise

        for i = 0, (size(pfdeur_inf))[1]-1 do begin
           if (i eq 0) then begin
              if pfdeur_inf(0) ne depth_H(0) then print, "Taille de la premiere zone neutre en cm : ", pfdeur_inf(0)
              if pfdeur_inf(0) ne depth_H(0) then printf, rion, "Taille de la premiere zone neutre en cm : ", pfdeur_inf(0)
           endif
           if (i gt 0 and i lt (size(pfdeur_inf))[1]-1) then begin
              print, "Taille des zones neutres suivantes en cm : ", pfdeur_inf(i) - pfdeur_sup(i-1)
              printf, rion, "Taille des zones neutres suivantes en cm : ", pfdeur_inf(i) - pfdeur_sup(i-1)
           endif
           if (i eq (size(pfdeur_inf))[1]-1) then begin
              print, "Taille de la derniere zone neutre en cm : ", max(depth_H) - pfdeur_sup(i)
              printf, rion, "Taille de la derniere zone neutre en cm : ", max(depth_H) - pfdeur_sup(i)
           endif
        endfor
     endelse 
     ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["HI", "HII", "H2", "Density profile"]
     psyms = [-3, -3, -3, -3]
     colors = [0, !red, !green, !blue]
     linestyles = [0,0,0,0]
    ; Add the legend.
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear
     axis, yaxis=1, yrange=[min(density)-0.5,max(density)+0.5], ytitle='log Gas density (H/cc)', /save
     if plt eq 1 then oplot, depth_den, density, color=!blue, line=linestyle
     p0 = !p & x0 = !x & y0 = !y

     plot, [1e19,1e23], [-37,-15], /ystyle, /xlog, /nodata, title='Intrinsic and emergent', xtitle='Depth in cm', ytitle='Emissivity in erg/cm2/s'
     oplot, [resolution,resolution], [-37,-15], color=!cyan
     p1 = !p & x1 = !x & y1 = !y
     strct_iem = {p:p1, x:x1, y:y1}
     em_em = read_lines(file_emergent, w0+1, strct_iem, 2, plt, radius_inner, fill_fact, ps)
     em_int = read_lines(file_intrinsic, w0+1, strct_iem, 0, plt, radius_inner, fill_fact, ps)
     p1 = !p & x1 = !x & y1 = !y

     plot, [1e19,1e23], [-2,0.5], /ystyle, /xlog, /nodata, title='Emergent over Intrinsic', xtitle='Depth in cm', ytitle='Emissivity ratio'
     oplot, [resolution,resolution], [-2,0.5], color=!cyan
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [-37,-15], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [-37,-15], line=2, color=!red
     endfor

     if plt eq 1 then begin
        ;oplot, depth_int, Lya_1216A/Lya_1216A_int, color=!red   
        ;oplot, depth_int, Lyb_1026A/Lyb_1026A_int, color=!green   
        oplot, depth_int, Hb_4861A/Hb_4861A_int, color=!black   
        oplot, depth_int, OIII_5007A/OIII_5007A_int, color=!red   
        oplot, depth_int, Ha_6563A/Ha_6563A_int, color=!blue   
        oplot, depth_int, NII_6584A/NII_6584A_int, color=!green   
        ;oplot, depth_int, OI_6300A/OI_6300A_int, color=!cyan   
        ;oplot, depth_int, SII_6731A_6716A/SII_6731A_6716A_int, color=!yellow   
        ;oplot, depth_int, NeIII_3869A/NeIII_3869A_int, color=!orange   
        ;oplot, depth_int, OII_3727A_multiplet/OII_3727A_multiplet_int, color=!gray   
        ;oplot, depth_int, NeV_3426A/NeV_3426A_int, color=!slate   
        ;oplot, depth_int, SiVII_2481m/SiVII_2481m_int, color=!brown   
        ;oplot, depth_int, Brg_2166m/Brg_2166m_int, color=!tan   
        ;oplot, depth_int, SiVI_1963m/SiVI_1963m_int, color=!dred   
        ;oplot, depth_int, AlIX_2040m/AlIX_2040m_int, color=!lpink   
        ;oplot, depth_int, CaVIII_2321m/CaVIII_2321m_int, color=!dcyan   
        ;oplot, depth_int, NeVI_7652m/NeVI_7652m_int, color=!lcyan   
        for k = 0, bornes_inf-1 do begin
           oplot, [pfdeur_inf(k),pfdeur_inf(k)], [-2,0.5], line=2, color=!red
           oplot, [pfdeur_sup(k),pfdeur_sup(k)], [-2,0.5], line=2, color=!red
        endfor
     endif
     p2 = !p & x2 = !x & y2 = !y

     plot, [1e19,1e23], [1e-25,1e-14], /xlog, /ylog, /ystyle, /nodata, xtitle='Depth (cm)', ytitle='Heating'
     oplot, [resolution,resolution], [1e-25,1e-14], color=!cyan
     if plt eq 1 then oplot, depth, heating, line=linestyle
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [1e-28,1e-14], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [1e-28,1e-14], line=2, color=!red
     endfor
     p3 = !p & x3 = !x & y3 = !y

     plot, [1e19,1e23], [1e-8,1e-4], /xlog, /ylog, /nodata, xtitle='Depth (cm)', ytitle='Radiative acceleration'
     oplot, [resolution,resolution], [1e-8,1e-4], color=!cyan
     if plt eq 1 then oplot, depth, rad_acc, line=linestyle
     for k = 0, bornes_inf-1 do begin
        if plt eq 1 then oplot, [pfdeur_inf(k),pfdeur_inf(k)], [1e-8,1e-4], line=2, color=!red
        if plt eq 1 then oplot, [pfdeur_sup(k),pfdeur_sup(k)], [1e-8,1e-4], line=2, color=!red
     endfor
     p4 = !p & x4 = !x & y4 = !y

  endif   

;readcol, file_rec, coeff...
;total recombination coefficients
; == sum of radiative, dielectronic and three-body recombination
; coefficients in cm3/s
;for all elements in the code
;evaluated at the current e- temperature

;not a practical format of data

end

pro plot_phyc, sim=sim, part=part, v1=value1, v2=value2, factor=factor,  ps=ps

lmax=13
if keyword_set(factor) then begin
 nm = 'x'+factor+'_'
endif else begin
nm = ''
endelse

plt = 1

if ps eq 1 then plt = 1
 
filenames = '/Users/oroos/Post-stage/LOPs'+sim+'/'+nm+'density_profile_'+sim+'_LOP*'+part+'_*'

files_phyc = findfile(filenames+'.phyc',count = nfiles)

print, 'Il y a ', nfiles, ' fichiers.'

files_con = files_phyc
strreplace, files_con, '.phyc', '.con'
files_fionH = files_phyc
strreplace, files_fionH, '.phyc', '_H.el'
files_fionO = files_phyc
strreplace, files_fionO, '.phyc', '_O.el'
files_density = files_phyc
strreplace, files_density, '.phyc', '.in'
files_em_em = files_phyc
strreplace, files_em_em, '.phyc', '.em_emergent'
files_em_int = files_phyc
strreplace, files_em_int, '.phyc', '.em_intrinsic'

files_rion = files_phyc
strreplace, files_rion, 'density_profile', 'rion_cloudy'
strreplace, files_rion, '.phyc', '.txt'

files_ascii = files_phyc
strreplace, files_ascii, '.phyc', '.ascii'
if factor ne '' then strreplace, files_ascii, nm, ''

;for k = 0, nfiles-1 do begin
;          ;print, files_density(k)
;          ;print, files_phyc(k)
;          f = files_ascii(k)
;          n = strpos(f,sim+'_LOP') + 9
;          if part eq 'xz' then m = strpos(f, 'xz__')
;          if part eq 'zy' then m = strpos(f, 'yz__')
;          len = m - n
;          lop = strmid(f, n, len)
;          random = strmid(f, m+4, 20)
;
;          angle=show_angle(13, sim, part, random, lop)
;          ;dans le cone superieur
;          ;if angle(2) ge -!dpi/2.-0.1 and angle(2) le -!dpi/2.+0.1 then stop
;          ;;; Nom fichier : /Users/oroos/Curie/Codes_sur_Curie/2D_LOPs/From_cube/LOPs13/density_profile_00100_LOP39xz__d20130426t095625.750.ascii
;          ;;; Theta : moyenne, minimum, maximum      -1.5089963      -1.5101844      -1.5089375
;          ;dans le disque cers la gauche
;          ;if angle(2) ge !dpi-0.1 and angle(2) le !dpi+0.1 then stop
;          ;;; Nom fichier : /Users/oroos/Curie/Codes_sur_Curie/2D_LOPs/From_cube/LOPs13/density_profile_00100_LOP12xz__d20130429t151123.502.ascii
;          ;;; Theta : moyenne, minimum, maximum       3.1014555       2.4980915       3.1030015
;       endfor
;stop

    n_win = 0
for k = 0, nfiles-1 do begin
;if (k eq 1 ) then stop
       if n_win lt 30 then begin 
                ;quels parametres ?
          if value1 eq 1 and value2 eq 0 and ps eq 0 then window, n_win, xsize=1200, ysize=1500
          if value1 eq 0 and value2 eq 1 and ps eq 0 then window, n_win, xsize=1200, ysize=1500
          if value1 eq 1 and value2 eq 1 and ps eq 0 then begin
             if ps eq 0 then window, n_win, xsize=1200, ysize=1500
             if ps eq 0 then window, n_win+1, xsize=1200, ysize=1500
          endif
          sed_frac_etc = value1
          reste = value2
          print, files_density(k)
          print, files_phyc(k)
          f = files_ascii(k)
          n = strpos(f,sim+'_LOP') + 9
          m = strpos(f, part+'__')
          len = m - n
          lop = strmid(f, n, len)
          random = strmid(f, m+4, 20)

          if ps eq 1 and value1 eq 1 and value2 eq 0 then ps_start, nm+'plot_phyc'+sim+'_'+part+'_'+random+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20
          if ps eq 1 and value1 eq 0 and value2 eq 1 then ps_start, nm+'plot_phyc_2nd_'+sim+'_'+part+'_'+random+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=20
          openw, rion, files_rion(k), /get_lun
          f=read_phys_conditions(files_phyc(k), files_fionH(k), files_fionO(k), files_density(k), $
                                 files_con(k), files_em_em(k), files_em_int(k), files_ascii(k), $
                                 rion, n_win, 0, plt, sed_frac_etc, reste, nm, ps)
          close, rion
          free_lun, rion
          print, 'C etait la fenetre ', n_win
          if sed_frac_etc eq 1 or reste eq 1 then n_win = n_win + 1
          if sed_frac_etc eq 1 and reste eq 1 then n_win = n_win + 2
          if ps eq 1 then print, 'Conversion en png...'
          if ps eq 1 then ps_end, /png
       endif else begin
          print, files_density(k)
          print, files_phyc(k)
          print, "Attention, le nombre maximal de fenetres de IDL est atteint !"
          print, "Voulez-vous effacer les fenetres et continuer ? Oui : 1, non : 0"
          if plt eq 1 then read, go_on
          if plt eq 0 then begin
              go_on = 1
              n_win = 0
          endif
          if (go_on eq 0) then begin
             print, "Au revoir"
             stop
          endif
          if (go_on eq 1) then begin
             for i = 0, n_win-1 do begin
                 window, i
                 wdelete, i
             endfor
             n_win = 0
             k = k-1
          endif
       endelse
    endfor

print, 'Fin du programme. Tout est OK.'
end
