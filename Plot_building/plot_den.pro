pro plot_den, sim=sim, part=part, ps=ps


device, decompose=1
defplotcolors


print, 'On recherche les fichiers :'
print, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*.in'

colors = fltarr(1)
colors_x10 = fltarr(1)
colors_x100 = fltarr(1)

part2 = [ 'xz', 'zy', 'cx', 'cy', 'px', 'py', 'up', 'dn', 'cu', 'cd', 'dk'] ;les *.in sont les memes pour x1, x10, x100 !!
                                ;en theorie.... parce que certaines ont plante a une luminosite et pas a une autre !

            files_phyc = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=n_files)
            files_phyc_x10 = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/x10_density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=n_files_x10)
            files_phyc_x100 = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/x100_density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=n_files_x100)
           if n_files gt 0 then colors=fltarr(n_files)+!red
           if n_files_x10 gt 0 then colors_x10=fltarr(n_files_x10)+!red
           if n_files_x100 gt 0 then colors_x100=fltarr(n_files_x100)+!red


nf = n_files
nf_x10 = n_files_x10
nf_x100 = n_files_x100
     for k = 1, n_elements(part2)-1 do begin
           
           files_phyc = [files_phyc,findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files)]
           files_phyc_x10 = [files_phyc_x10,findfile('/Users/oroos/Post-stage/LOPs'+sim+'/x10_density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files_x10)]
           files_phyc_x100 = [files_phyc_x100,findfile('/Users/oroos/Post-stage/LOPs'+sim+'/x100_density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files_x100)]


if n_files gt 0 then begin
           if (k eq 1 or k eq 6 or k eq 7)           then colors=[colors,fltarr(n_files)+!red]
           if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors=[colors,fltarr(n_files)+!blue]
           if (k eq 4 or k eq 5 or k eq 10)          then colors=[colors,fltarr(n_files)+!green]
endif
        nf = nf + n_files
if n_files_x10 gt 0 then begin
           if (k eq 1 or k eq 6 or k eq 7)           then colors_x10=[colors_x10,fltarr(n_files_x10)+!red]
           if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors_x10=[colors_x10,fltarr(n_files_x10)+!blue]
           if (k eq 4 or k eq 5 or k eq 10)          then colors_x10=[colors_x10,fltarr(n_files_x10)+!green]
endif
        nf_x10 = nf_x10 + n_files_x10
if n_files_x100 gt 0 then begin
           if (k eq 1 or k eq 6 or k eq 7)           then colors_x100=[colors_x100,fltarr(n_files_x100)+!red]
           if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors_x100=[colors_x100,fltarr(n_files_x100)+!blue]
           if (k eq 4 or k eq 5 or k eq 10)          then colors_x100=[colors_x100,fltarr(n_files_x100)+!green]
endif
        nf_x100 = nf_x100 + n_files_x100



    endfor

files = files_phyc
strreplace, files, '.phyc', '.in'
files_x10 = files_phyc_x10
strreplace, files_x10, '.phyc', '.in'
files_x100 = files_phyc_x100
strreplace, files_x100, '.phyc', '.in'

print, 'En tout, il y a ', nf, ' x1, ', nf_x10, ' x10 et ', nf_x100, ' x100.'
;print, files

if ps eq 0 then begin
window, 1
plot, [-3,1.5], [-6,6], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Density (H/cc)', title='OK x1'
p_den = !p & x_den = !x & y_den = !y

window, 2
plot, [-3,1.5], [-6,6], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Density (H/cc)', title='OK x10'
p_den_x10 = !p & x_den_x10 = !x & y_den_x10 = !y

window, 3
plot, [-3,1.5], [-6,6], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Density (H/cc)', title='OK x100'
p_den_x100 = !p & x_den_x100 = !x & y_den_x100 = !y


window, 11
plot, [-3,1.5], [15,30], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Column density (H/cm2)', title='OK x1', /ystyle
p_colden = !p & x_colden = !x & y_colden = !y

window, 12
plot, [-3,1.5], [15,30], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Column density (H/cm2)', title='OK x10', /ystyle
p_colden_x10 = !p & x_colden_x10 = !x & y_colden_x10 = !y

window, 13
plot, [-3,1.5], [15,30], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Column density (H/cm2)', title='OK x100', /ystyle
p_colden_x100 = !p & x_colden_x100 = !x & y_colden_x100 = !y

window, 21
plot, [-3,1.5], [2,10], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', title='OK x1'
p_temp = !p & x_temp = !x & y_temp = !y

window, 22
plot, [-3,1.5], [2,10], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', title='OK x10'
p_temp_x10 = !p & x_temp_x10 = !x & y_temp_x10 = !y

window, 23
plot, [-3,1.5], [2,10], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', title='OK x100'
p_temp_x100 = !p & x_temp_x100 = !x & y_temp_x100 = !y
endif


for k = 0, nf-1 do begin
readcol, files(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

if k eq 0 then all_densities = 10d^density else all_densities = [all_densities,10d^density]

colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2
  
if ps eq 0 then begin
   wset, 1
   !p = p_den & !x = x_den & !y = y_den
   oplot, depth_den, density, color=colors(k)

    wset, 11
   !p = p_colden & !x = x_colden & !y = y_colden
   oplot, depth_den, colden, color=colors(k)

endif
endfor

help, density
help, all_densities
print, 'Densite moyenne de toutes les lops tracees : ', mean(all_densities)

for k = 0, nf-1 do begin
openr, fin, files(k), /get_lun
line1=''
readf, fin, line1
line2=''
readf, fin, line2
line3=''
readf, fin, line3
line_radius = ''
readf, fin, line_radius
free_lun, fin
;print, line_radius
inner = strpos(line_radius, 'inner')
outer = strpos(line_radius, 'outer')
len = outer - inner
radius_inner = float(strmid(line_radius,inner+5,len-5))
;print, 'rayon', radius_inner

if radius_inner eq 0 then begin
   print, 'rayon nul !'
   stop
endif

radius_inner = 3.24d-22*10^radius_inner ;conversion en kpc
;print, radius_inner
readcol, files_phyc(k), depth_phyc, temperature_electrons, hden, eden, heating, rad_acc, fill_fact, /silent, format='(D,D,D,D,D,D)'
;units :             cm,            K,                 cm-3,
;cm-3,erg/cm3/s, cm/s2, number
depth_phyc = depth_phyc*3.24d-22 + radius_inner ;kpc
hden = hden*fill_fact
t_max = where(depth_phyc eq max(depth_phyc))

length_phyc = depth_phyc*0
colden_phyc = hden*0

length_phyc(0) = depth_phyc/3.24d-22 ;cm
colden_phyc(0) = hden(0)*length_phyc(0) ;H/cm2

for j = 1, n_elements(depth_phyc)-1 do begin
length_phyc(j) = (depth_phyc(j) - depth_phyc(j-1))/3.24d-22 ;cm
colden_phyc(j) = colden_phyc(j-1) + hden(j)*length_phyc(j) ;H/cm2
endfor

readcol, files(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

  colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2

t500 = where(depth_den eq max(depth_den))
if ps eq 0 then begin
   wset, 1
   !p = p_den & !x = x_den & !y = y_den
   plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!black, psym=1
   if n_elements(depth_den) ge 497 then begin
      plots, depth_den[t500], density[t500], color=!orange, psym=2 
      plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!dred, psym=1
      t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], density[t], color=!gray, psym=-3
   endif else begin
      print, 'Non tronquee'
   endelse
endif
   
   wset, 11
   !p = p_colden & !x = x_colden & !y = y_colden
                                ;oplot, depth_den, colden, color=colors(k)
   plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!black, psym=1, thick=4
                                ;oplot, alog10(depth_phyc), alog10(colden_phyc), color=colors(k), psym=1
   if n_elements(depth_den) ge 497 then begin
      plots, depth_den[t500], colden[t500], color=!orange, psym=2 
      plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!dred, psym=1
      t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], colden[t], color=!gray, psym=-3
      endif
      
      wset, 21
      !p = p_temp & !x = x_temp & !y = y_temp
 oplot, depth_phyc, temperature_electrons, color=colors(k), psym=-3
      plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!black, psym=1, thick=4
                                ;oplot, alog10(depth_phyc), alog10(temperature_electrons), color=colors(k), psym=1
      if n_elements(depth_den) ge 497 then begin
         plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!dred, psym=1
      endif
      
   
endfor

for k = 0, nf_x10-1 do begin
readcol, files_x10(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2

  
if ps eq 0 then begin
   wset, 2
   !p = p_den_x10 & !x = x_den_x10 & !y = y_den_x10
   oplot, depth_den, density, color=colors_x10(k)

  wset, 12
   !p = p_colden_x10 & !x = x_colden_x10 & !y = y_colden_x10
   oplot, depth_den, colden, color=colors_x10(k)

endif
endfor
for k = 0, nf_x10-1 do begin
openr, fin, files_x10(k), /get_lun
line1=''
readf, fin, line1
line2=''
readf, fin, line2
line3=''
readf, fin, line3
;if nm ne '' then begin
line4=''
readf, fin, line4
;endif
line_radius = ''
readf, fin, line_radius
free_lun, fin
;print, line_radius
inner = strpos(line_radius, 'inner')
outer = strpos(line_radius, 'outer')
len = outer - inner
radius_inner = float(strmid(line_radius,inner+5,len-5))
;print, 'rayon', radius_inner

if radius_inner eq 0 then begin
   print, 'rayon nul !'
   stop
endif

radius_inner = 3.24d-22*10^radius_inner ;conversion en kpc
;print, radius_inner
readcol, files_phyc_x10(k), depth_phyc, temperature_electrons, hden, eden, heating, rad_acc, fill_fact, /silent, format='(D,D,D,D,D,D)'
;units :             cm,            K,                 cm-3,
;cm-3,erg/cm3/s, cm/s2, number
depth_phyc = depth_phyc*3.24d-22 + radius_inner ;kpc
hden = hden*fill_fact
t_max = where(depth_phyc eq max(depth_phyc))

length_phyc = depth_phyc*0
colden_phyc = hden*0

length_phyc(0) = depth_phyc/3.24d-22 ;cm
colden_phyc(0) = hden(0)*length_phyc(0) ;H/cm2

for j = 1, n_elements(depth_phyc)-1 do begin
length_phyc(j) = (depth_phyc(j) - depth_phyc(j-1))/3.24d-22 ;cm
colden_phyc(j) = colden_phyc(j-1) + hden(j)*length_phyc(j) ;H/cm2
endfor


readcol, files_x10(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

  colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2


t500 = where(depth_den eq max(depth_den))
if ps eq 0 then begin
   wset, 2
   !p = p_den_x10 & !x = x_den_x10 & !y = y_den_x10
   plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!black, psym=1
if n_elements(depth_den) ge 497 then begin
plots, depth_den[t500], density[t500], color=!orange, psym=2 
plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!dred, psym=1
t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], density[t], color=!gray, psym=-3
endif else begin
print, 'Non tronquee'
endelse
endif

wset, 12
!p = p_colden_x10 & !x = x_colden_x10 & !y = y_colden_x10
 plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!black, psym=1, thick=4
if n_elements(depth_den) ge 497 then begin
plots, depth_den[t500], colden[t500], color=!orange, psym=2 
plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!dred, psym=1
t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], colden[t], color=!gray, psym=-3
endif

wset, 22
!p = p_temp_x10 & !x = x_temp_x10 & !y = y_temp_x10
 oplot, depth_phyc, temperature_electrons, color=colors_x10(k), psym=-3
 plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!black, psym=1, thick=4
if n_elements(depth_den) ge 497 then begin
plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!dred, psym=1
endif

endfor

for k = 0, nf_x100-1 do begin
readcol, files_x100(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

   colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2
  
if ps eq 0 then begin
   wset, 3
   !p = p_den_x100 & !x = x_den_x100 & !y = y_den_x100
   oplot, depth_den, density, color=colors_x100(k)

   wset, 13
   !p = p_colden_x100 & !x = x_colden_x100 & !y = y_colden_x100
   oplot, depth_den, colden, color=colors_x100(k)


endif
endfor
for k = 0, nf_x100-1 do begin
openr, fin, files_x100(k), /get_lun
line1=''
readf, fin, line1
line2=''
readf, fin, line2
line3=''
readf, fin, line3
;if nm ne '' then begin
line4=''
readf, fin, line4
;endif
line_radius = ''
readf, fin, line_radius
free_lun, fin
;print, line_radius
inner = strpos(line_radius, 'inner')
outer = strpos(line_radius, 'outer')
len = outer - inner
radius_inner = float(strmid(line_radius,inner+5,len-5))
;print, 'rayon', radius_inner

if radius_inner eq 0 then begin
   print, 'rayon nul !'
   stop
endif

radius_inner = 3.24d-22*10^radius_inner ;conversion en kpc
;print, radius_inner
readcol, files_phyc_x100(k), depth_phyc, temperature_electrons, hden, eden, heating, rad_acc, fill_fact, /silent, format='(D,D,D,D,D,D)'
;units :             cm,            K,                 cm-3,
;cm-3,erg/cm3/s, cm/s2, number
depth_phyc = depth_phyc*3.24d-22 + radius_inner ;kpc
hden = hden*fill_fact
t_max = where(depth_phyc eq max(depth_phyc))

length_phyc = depth_phyc*0
colden_phyc = hden*0

length_phyc(0) = depth_phyc/3.24d-22 ;cm
colden_phyc(0) = hden(0)*length_phyc(0) ;H/cm2

for j = 1, n_elements(depth_phyc)-1 do begin
length_phyc(j) = (depth_phyc(j) - depth_phyc(j-1))/3.24d-22 ;cm
colden_phyc(j) = colden_phyc(j-1) + hden(j)*length_phyc(j) ;H/cm2
endfor

readcol, files_x100(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

  colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2

t500 = where(depth_den eq max(depth_den))
if ps eq 0 then begin
   wset, 3
   !p = p_den_x100 & !x = x_den_x100 & !y = y_den_x100
   plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!black, psym=1
   if n_elements(depth_den) ge 497 then begin
      plots, depth_den[t500], density[t500], color=!orange, psym=2 
      plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!dred, psym=1
      t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], density[t], color=!gray, psym=-3
   endif else begin
      print, 'Non tronquee'
   endelse
endif


wset, 13
!p = p_colden_x100 & !x = x_colden_x100 & !y = y_colden_x100
 plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!black, psym=1
if n_elements(depth_den) ge 497 then begin
plots, depth_den[t500], colden[t500], color=!orange, psym=2 
plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!dred, psym=1
t = where(depth_den ge max(alog10(depth_phyc)))
;oplot, depth_den[t], colden[t], color=!gray, psym=-3
endif

wset, 23
!p = p_temp_x100 & !x = x_temp_x100 & !y = y_temp_x100
 oplot, depth_phyc, temperature_electrons, color=colors_x100(k), psym=-3
 plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!black, psym=1
if n_elements(depth_den) ge 497 then begin
plots, alog10(depth_phyc[t_max]), alog10(temperature_electrons[t_max]), color=!dred, psym=1
endif

endfor

   ;print, 'pause'
   ;read, go_on



;print, 'Min/max de theta : ', minmax(theta)
;print, 'Min/max de phi : ', minmax(phi)

;plots,  [25,25], [25,50], [25,25], psym=-4
if ps eq 1 then ps_end, /png

print, 'Fin du programme, tout est OK.'




end
