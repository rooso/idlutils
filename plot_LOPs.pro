function trace, file, sim, part, theta, phi, color, dim, ps, list=list, center=center

defplotcolors

if sim eq '00100' or sim eq '00075w' $
or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
if sim eq '00085' or sim eq '00075n' $
or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then name = 'No'

sm = sim

if sim eq '00075w' or sim eq '00075n' then sm='00075'

if strmatch(file, '*ascii*') eq 1 then begin
ascii = read_file_ascii(file,/kpc)
depth = ascii(*,0)
density = ascii(*,1)
endif


if strmatch(file, '*phyc*') eq 1 then begin
phyc = read_file_phyc(file,/kpc)
depth = phyc(*,0)
density = phyc(*,2)
endif

;if max(depth) lt 10 then begin
;window, 3
;plot, depth, density, /xlog, /ylog, xr=[0.001,20], yr=[1e-6,1e6], psym=1
;xyouts, 0.002, 1d4, file
;;stop
;endif

readcol, "/Users/oroos/Post-stage/orianne_data/"+name+"_AGN_feedback/output_"+sm+"/sink_"+sm+".out", rien, rien2, x_c, y_c, z_c, /silent

depth_max = max(depth)
x = depth_max*cos(phi)*sin(theta)
y = depth_max*sin(phi)*sin(theta)
z = depth_max*cos(theta)
;print, minmax(depth)

if dim eq '3d' then begin
   wset,0
   device, decompose=1
   defplotcolors
;print, x;, y, z
;x = [x_c-25, x]
;y = [y_c-25, y]
;z = [z_c-25, z]
   device, decompose=1
   defplotcolors
   plots, X, Y, Z, /T3D, PSYM=3, COLOR=color, thick=2
   plots, x_c-25, y_c-25, z_c-25, psym=1, thick=4, /t3d, syms=4

if center eq 0 then printf, list,  x_c-25, y_c-25, z_c-25
printf, list, x, y, z
   
   return, [x,y,z,minmax(depth)]
endif else begin

if ps eq 0 then symb = 3 else symb=sym(1)

if part eq 'xz' then begin
if strmatch(file, '*density*x*') eq 1 then begin
   oplot, replicate(x_c,n_elements(depth))+depth*cos(phi)*sin(theta),  replicate(z_c,n_elements(depth))+depth*cos(theta), psym=symb, color=color, thick=1,syms=0.25
   ;print, replicate(x_c,n_elements(depth))+depth*cos(phi)*sin(theta), 'et', replicate(z_c,n_elements(depth))+depth*cos(theta)
   ;help, depth
   ;stop
endif
endif else begin
if strmatch(file, '*density*dk*') eq 1 then begin
   oplot, replicate(x_c,n_elements(depth))+depth*cos(phi)*sin(theta),  replicate(y_c,n_elements(depth))+depth*sin(phi)*sin(theta), psym=symb, color=color, thick=1, syms=0.25

endif
endelse
endelse

end

pro plot_LOPs, sim=sim, part=part, dim=dim, ps=ps

if not keyword_Set(part) then part = 'all'
if not keyword_Set(ps) then ps = 0
if not keyword_set(sim) then sim = '00100'
if not keyword_set(dim) then dim='3d'

      if ps ne 0 then begin
         !p.font=0
         !p.charsize=4
         !p.charthick=2
         !p.thick=4
         !x.thick=20
         !y.thick=20
      endif


  device, decompose=1
  defplotcolors
  if part ne 'all' then begin
     
     device, decompose=1
     defplotcolors
     
     files = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part+'*.ascii',count=nf)
     colors = fltarr(nf) + !red
     
     print, 'On recherche les fichiers :'
     print, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part+'*.ascii', nf
     
     tout = 1
     if tout eq 1 and part ne 'dk' then begin
        if part ne 'zy' then xt=strmid(part, 0, 1) $ ;x, u, d
        else xt=strmid(part, 1, 1) ;y
        
        files_cone = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*c'+xt+'__*.ascii',count = nf_c)
        print, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*c'+xt+'__*.ascii', nf_c
        
        if part ne 'up' and part ne 'dn' then begin 
           files_gal_plane = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*p'+xt+'__*.ascii',count = nf_p)
           print, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*p'+xt+'__*.ascii', nf_p
           
           files = [files,files_cone,files_gal_plane]
           nf = nf + nf_c + nf_p
           colors = [colors,fltarr(nf_c)+!blue,fltarr(nf_p)+!green]
        endif else begin
           files = [files,files_cone]
           nf = nf + nf_c
           colors = [colors,fltarr(nf_c)+!blue]
        endelse
     endif
     
  endif else begin
     
     
     device, decompose=1
     defplotcolors
    

     print, 'On recherche les fichiers :'
     print, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*.ascii'
     
     colors = fltarr(1)
               ;0      1     2     3     4    5     6     7    8     9   10 
     part2 = ['xz', 'zy', 'cx', 'cy', 'px', 'py', 'up', 'dn', 'cu', 'cd', 'dk']
     
     files = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(0)+'*.ascii',count=n_files)
     colors=fltarr(n_files)+!red
     nf = n_files
     for k = 1, n_elements(part2)-1 do begin
        files = [files,findfile( '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(k)+'*.ascii',count=n_files)]
        if (k eq 1 or k eq 6 or k eq 7) then colors=[colors,fltarr(n_files)+!red]
        if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors=[colors,fltarr(n_files)+!blue]
        if (k eq 4 or k eq 5 or k eq 10) then colors=[colors,fltarr(n_files)+!green]
        nf = nf + n_files
     endfor
  endelse
  
files_phyc = files
strreplace, files_phyc, 'ascii', 'phyc'

files_phyc_x10 = files_phyc
strreplace, files_phyc_x10, 'density', 'x10_density'


files_phyc_x100 = files_phyc
strreplace, files_phyc_x100, 'density', 'x100_density'
  
  print, 'En tout, il y en a ', nf, '.'
  
  theta=dblarr(nf)
  phi=dblarr(nf)

  
if dim eq '3d' then begin
  if ps eq 0 then window, 0
  
  if ps eq 1 then ps_start, '/Users/oroos/Post-stage/plot_lops_'+sim+'_'+part+'.eps', /encapsulated, /color, /cm, xsize=20, ysize=20
  
center=0
openw, list, '/Users/oroos/Post-stage/LOPs'+sim+'/list_of_lops_'+sim+part+'.txt', /get_lun     

  device, decompose=1
  defplotcolors
  Plot_3dbox, [-25,25], [-25,25], [-25,25], psym=3, /nodata,   $
              GRIDSTYLE=1, $    ;/SOLID_WALLS
              YZSTYLE=5, AZ=40, TITLE="Distribution of LOPs for sim "+sim+part,      $
              Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
              Ztitle="Z Coordinate" 
                                ;/YSTYLE, ZRANGE=[-25,25],
                                ;XRANGE=[-25,25]
endif
              
              for k = 0, nf-1 do begin
     openr, f, files(k), /get_lun
                                ;print, files(k)
     line1 = ''
     readf, f, line1
                                ;print, line1
     theta(k) = double(strmid(line1,strpos(line1,':')+1,25))
                                ;print, 'theta_strmid = ', strmid(line1,strpos(line1,':')+1,25)
                                ;print, 'theta = ', theta(k)
     phi(k) = double(strmid(line1,strpos(line1,':')+26,25))
                                ;print, 'phi_strmid = ', strmid(line1,strpos(line1,':')+26,25)
                                ;print, 'phi = ', phi(k)
if dim eq '3d' then begin
       func = trace(files(k), sim, part, theta(k), phi(k), colors(k),dim,ps, list=list, center=center)
       if k eq 0 then graph = func else graph = [[graph],[func]]
       center = center + 1
endif
     close, f
     free_lun, f
     
  endfor

if dim eq '3d' then begin
close, list
free_lun, list

print, min(graph(4,*)), max(graph(4,*))

window, 2
Plot_3dbox, [-25,25], [-25,25], [-25,25], psym=3, /nodata,   $
              GRIDSTYLE=1, $    ;/SOLID_WALLS
              YZSTYLE=5, AZ=40, TITLE="Distribution of LOPs for sim "+sim+part,      $
              Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
              Ztitle="Z Coordinate" 
plot_3dbox, graph(0,*), graph(1,*), graph(2,*), /t3d, psym=4, /xy_plane, /xz_plane, /yz_plane, /nodata
endif

if dim eq '2d' then begin
if sim eq '00100' then begin

map_xz = readfits('/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/map_00100_level_central_y_thinslice_lmax13.fits.gz',header_xz)
map_xy = readfits('/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/map_00100_level_central_z_thinslice_lmax13.fits.gz',header_xy)

rho = 0 ;rho=1 pour remplacer par la densite
if rho eq 1 then begin
map_xz = alog10(readfits('/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/map_00100_rho_central_y_thinslice_lmax13.fits.gz',header_xz))
map_xy = alog10(readfits('/Users/oroos/Post-stage/orianne_data/With_AGN_feedback/map_00100_rho_central_z_thinslice_lmax13.fits.gz',header_xy))

endif

;print, minmax(map_xz)
;print, minmax(map_xy)

x_agn = sxpar(header_xz, 'xsink')
y_agn = sxpar(header_xz, 'ysink')
z_agn = sxpar(header_xz, 'zsink')
taille_pixel = sxpar(header_xz, 'cdelt1')
boxlen = sxpar(header_xz,'boxlen')
lmax = sxpar(header_xz,'lmax')

;zoom=0
zoom=2460
;zoom=82


 imgxr = [7.5,42.5]
     imgyr = imgxr
  
  if zoom eq 0 then begin
 xr_xz = [7.5,42.5]
 yr_xz = xr_xz

xr_xy = xr_xz
yr_xy = yr_xz
  endif else begin
 xr_xz = [x_agn-zoom*taille_pixel,x_agn+zoom*taille_pixel] 
yr_xz = [z_agn-zoom*taille_pixel,z_agn+zoom*taille_pixel] 


 xr_xy = [x_agn-zoom*taille_pixel,x_agn+zoom*taille_pixel] 
yr_xy = [y_agn-zoom*taille_pixel,y_agn+zoom*taille_pixel] 
endelse


     xt = ' '
     yt = ' '
     blank = replicate(' ',10)
     titles = blank ;supprime les annotations sur les graduations
     xtickn = blank
     ytickn = blank
     !x.margin = 0
     !y.margin = 0

 
 if ps eq 0 then begin
        chars = 2
        textt = 1
     endif else begin
        chars = 5
        textt = 5
     endelse 

if rho eq 1 then cells = 'den' else cells = 'cells'

if ps eq 0 then window, 1, ysize=570, xsize=900
  if ps eq 1 then ps_start, '/Users/oroos/Post-stage/plot_lops_'+cells+'_'+sim+'_'+part+'.eps', /encapsulated, /color, /cm, xsize=20, ysize=15

 if ps eq 0 then syms = 4 else syms=10
 if ps eq 0 then thick = 4 else thick=40

!p.multi = [0,2,1]
rg=[7.9,13.1]
if rho eq 1 then rg = [-7,6]

rg_bar = boxlen*1d3/2d^rg
if rho eq 1 then rg_bar = [-7,6]

title = 'Cell size [pc]'
if rho eq 1 then title = 'Gas density [cm'+textoidl('^{-3}')+']'

n=39
if rho eq 1 then n=0

pos_bar = [0.10, 0.81, 0.90, 0.88]
if rho eq 1 then pos_bar = [0.01, 0.81, 0.24, 0.88]
if rho eq 1 then nb = 0.5 else nb = 1 

if part eq 'all' or part eq 'xz' then begin
   device, /decompose
 cgloadct,n, /silent, /reverse
 plotimage, map_xz, range=rg, imgxrange=imgxr, imgyrange=imgyr, xr=xr_xz, yr=yr_xz, xtitle=xt, ytitle=yt, /isotropic, /save, xtickn=xtickn, ytickn=ytickn, ncolors=256, bottom=0
for k = 0, nf-1 do begin
       func = trace(files(k), sim, 'xz', theta(k), phi(k), !white,dim,ps)
       ;func = trace(files_phyc_x100(k), sim, 'xz', theta(k), phi(k), !black,dim,ps)
       ;func = trace(files_phyc_x10(k), sim, 'xz', theta(k), phi(k), !black,dim,ps)
       ;func = trace(files_phyc(k), sim, 'xz', theta(k), phi(k), !black,dim,ps)
    endfor
device,decompose=0
    loadct, 0, /silent
    plots, x_agn, z_agn, psym=1, color=!white, thick=thick/2., syms=syms/2.
    lab=print_label(!black, 0, ps)
    device, /decompose
 endif
yes = 1 ;yes=1 : superposer la boite de zoom
if yes eq 1 then begin

zoom_plot = 82

oplot, [x_agn-zoom_plot*taille_pixel,x_agn+zoom_plot*taille_pixel], [z_agn-zoom_plot*taille_pixel,z_agn-zoom_plot*taille_pixel], thick=thick/2.
oplot, [x_agn-zoom_plot*taille_pixel,x_agn+zoom_plot*taille_pixel], [z_agn+zoom_plot*taille_pixel,z_agn+zoom_plot*taille_pixel], thick=thick/2.
oplot, [x_agn-zoom_plot*taille_pixel,x_agn-zoom_plot*taille_pixel], [z_agn-zoom_plot*taille_pixel,z_agn+zoom_plot*taille_pixel], thick=thick/2.
oplot, [x_agn+zoom_plot*taille_pixel,x_agn+zoom_plot*taille_pixel], [z_agn-zoom_plot*taille_pixel,z_agn+zoom_plot*taille_pixel], thick=thick/2.
oplot, [0,x_agn-zoom_plot*taille_pixel], [0,z_agn-zoom_plot*taille_pixel], thick=thick/2.
oplot, [x_agn+zoom_plot*taille_pixel,50], [z_agn-zoom_plot*taille_pixel,0], thick=thick/2.
endif

if ps eq 0 then loc_ps = 'top' else loc_ps = 'bottom'

if part eq 'all' or part eq 'dk' then begin
 cgloadct,n, /silent, /reverse
 plotimage, map_xy, range=rg, imgxrange=imgxr, imgyrange=imgyr,  xr=xr_xy, yr=yr_xy, xtitle=xt, ytitle=yt, /isotropic, /save, xtickn=xtickn, ytickn=ytickn, ncolors=256, bottom=0
    cgColorbar, range=rg_bar, color=!p.color, title=title, /top, chars=chars*nb, textt=textt*nb, pos=pos_bar, tloc=loc_ps
for k = 0, nf-1 do begin
       func = trace(files(k), sim, 'dk', theta(k), phi(k), !white,dim,ps)
       ;func = trace(files_phyc_x100(k), sim, 'dk', theta(k), phi(k), !black,dim,ps)
       ;func = trace(files_phyc_x10(k), sim, 'dk', theta(k), phi(k), !black,dim,ps)
       ;func = trace(files_phyc(k), sim, 'dk', theta(k), phi(k), !black,dim,ps)
    endfor
    device,decompose=0
    loadct, 0, /silent
    plots, x_agn, y_agn, psym=1, color=!white, thick=thick/2., syms=syms/2.
lab=print_label(!black, 0, ps)
    device, /decompose

endif


endif
endif

;print, 'Min/max de theta : ', minmax(theta)
;print, 'Min/max de phi : ', minmax(phi)
  
;plots,  [25,25], [25,50], [25,25], psym=-4
  if ps eq 1 then ps_end, /png

;;;
if ps eq 1 and part eq 'all' and dim eq '2d' and sim eq '00100' then spawn, 'open /Users/oroos/Post-stage/plot_lops_'+cells+'_00100_all.png'
;;;
  print, 'Fin du programme, tout est OK.'
  
end
