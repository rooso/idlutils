;lecture des LOPs et creation des scripts avec les profils de densite
;pour Cloudy
FUNCTION read_lop, file, sim, part, lop, random, ps

print, file
readcol, file, depth, density;, x, y, z, temperature, vx, vy, vz, cell_size, x_lop, y_lop, z_lop, theta_los, phi_los, /silent
if n_elements(depth) eq 0 then begin
print, 'fichier vide !!'
print, file
stop
endif


t_sort = sort(depth)
sorted_depth = depth(t_sort)
sorted_density = density(t_sort)

count = 0

for i = 0, (size(sorted_depth))[1]-1 do begin
for j = 0, (size(sorted_depth))[1]-1 do begin   
   if (sorted_depth(i) eq sorted_depth(j) && j ne i) then begin
      count++
      print, 'pfdeur, densite ', sorted_depth(i), sorted_density(i), sorted_density(j)
stop
   endif
endfor
endfor

print, 'Il y a ', count, ' problemes'


if lop le 5 then begin
if ps eq 0 then window, lop
if ps eq 1 then ps_start, '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP'+lop+part+'__'+random+'.eps', /encapsulated
defplotcolors

plot, sorted_depth, sorted_density, psym=10, /xlog, /ylog, xr=[0.01,20], xtitle="Depth in kpc", ytitle="Gas density in H/cc", title='Density profile'
oplot, depth, density, psym=1
;here, the depth is the distance between the AGN and the current point
;of the line of prop
if ps eq 1 then ps_end, /png
endif

;preparing data for Cloudy

;correction de la densite (car on a des densites moyennes)
filling_factor = 1./5.
sorted_density = sorted_density/filling_factor ;;;avant d'utiliser une densite cloudy, il faudra TOUJOURS remultiplier par ff !!!

;cloudy reads logs + depth in cm and rho in cm^-3
print, "premier point : ", sorted_depth(0)
cloudy_depth = alog10(double(sorted_depth)/3.24d-22) ; in log(cm)
cloudy_density = alog10(double(sorted_density)) ; in H/cc = cm^-3

t_debut = where(sorted_depth eq 0., nt_debut)
if nt_debut gt 0 then cloudy_depth[t_debut] = -30. ;;le premier point cloudy est arbitraire et est a -35.
;;si le premier vrai point amr est a 0, cela pose un probleme (valeurs
;;doivent etre triees par pfdeur croissante)

;print, cloudy_depth 
taille = (size(cloudy_depth))[1]
print, "taille", taille

;print, "premier point : ", cloudy_depth(0)
;if cloudy_depth(0) le -35. then stop ;;valeur de depart exigee par cloudy


;sortie du script lisible par cloudy : spectre realiste de Sy1 (AGN +
;BB + transmitted) + profil de densite issu de la LOP

;On veut : L_tot = 10^43.5 erg/s = L_AGN + somme L_BB_i + L_transmis avec
;L_AGN + L_BB = L_transmis et L_AGN/L_BB_i donne par l'ajustement improved_final_realistic_sy1_spectrum

;       L_BB = 4*pi*r_0^2*sigma*T^4

;on veut commencer le calcul du transfert de rayonnement au niveau de
;la premiere cellule
radius_inner = cloudy_depth(0)
if cloudy_depth(0) eq -30 then radius_inner = cloudy_depth(1)
radius_outer = cloudy_depth(taille-1) - 0.05

;predire la profondeur a laquelle la limite d'ionisation sera
;atteinte en utilisant le critere 10^22 cm-2
lg = fltarr(taille)
lg(0) = 10^cloudy_depth(0)
column_density = 0

;troncature des lignes trop longues pour cloudy
taille_500 = 0
if taille ge 497 then begin
   taille_500 = taille
   taille = 497
   print, "/!\ LOP tronquee a 500 paires"
   radius_outer = cloudy_depth(taille-1) - 0.05
if radius_outer gt max(cloudy_depth(0:taille-1)) then radius_outer = cloudy_depth(taille-2) - 0.05
endif

first_depth = -35.

;;;L_tot = L_incident + L_transmis = 44.5 (corresp a L_X = 43.5)
;;;avec L_transmis = L_incident = L_AGN + somme L_BB
;;;L_BB en fonction de L_AGN donne par realistic_AGN.in


;filename = 'density_profile_'+sim+'_LOP'+lop+part+'__'+random
filename = 'transp6_profile_'+sim+'_LOP'+lop+part+'__'+random

openw, data, '/Users/oroos/Post-stage/LOPs'+sim+'/'+filename+'.in', /get_lun
printf, data, 'title Realistic AGN+central pc spectrum for LOP number ', lop+part+random
printf, data, 'AGN T = 5.5e5 k, a(ox) = -0.7, a(uv)=-0.3 a(x)=-0.48'
printf, data, 'luminosity (total) 44.12 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=3e2 K , radius 18.20 //luminosities adjusted so that Ltot = 44.5'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, '////////autres bb pour combler vide IR et rattraper pente FIR'
printf, data, 'blackbody, T=90 K , radius 18.90 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=200 K , radius 18.39 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=900 K , radius 17.10'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=1400 K , radius 16.10' 
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'blackbody, T=2900 K , radius 14.98 '
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'CMB'
printf, data, 'table read "../realistic_AGN_transmitted.txt"'
printf, data, 'luminosity (total) 44.2 //erg/s'
printf, data, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, data, 'sphere'
printf, data, 'filling factor ', filling_factor
printf, data, 'dlaw table //density is divided by the filling factor'
printf, data, 'continue', first_depth, cloudy_density(0) ;;premier point exige par cloudy, densite du premier vrai point (en double, du coup)

for i = 0, taille-1 do begin
      if cloudy_depth(i) eq !VALUES.F_INFINITY then print, 'Warning, infinite value for LOP ', lop
      
if i eq 0 then begin
if cloudy_depth(0) ne -30 then printf, data, 'continue', cloudy_depth(i), cloudy_density(i)
endif else begin
printf, data, 'continue', cloudy_depth(i), cloudy_density(i)
endelse
      if i gt 0 then lg(i) = 10^cloudy_depth(i)-10^cloudy_depth(i-1)
      ;print, lg(i)
   if column_density lt 1e22 then begin
        column_density = column_density +  10^(cloudy_density(i))*filling_factor*lg(i)
        i_critique = i
        ;print, column_density
   endif
endfor

if taille_500 gt 0 then begin
trunc_name = filename
strreplace, trunc_name, '*profile', 'trunc500'
openw, trunc500, '/Users/oroos/Post-stage/LOPs'+sim+'/'+trunc_name+'.ascii', /get_lun
print, 'Attention, ', taille_500-497, ' points ont ete tronques'
printf, trunc500, 'Attention, ', taille_500-497, ' points ont ete tronques'
print, 'Les points restants sont : '
printf, trunc500, 'Les points restants sont : '
for j = taille, taille_500-1 do begin
printf, trunc500, cloudy_depth(j), cloudy_density(j)
endfor
close, trunc500
free_lun, trunc500
endif


rion_name = filename
strreplace, rion_name, '*profile', 'rion_prediction'
openw, rion, '/Users/oroos/Post-stage/LOPs'+sim+'/'+rion_name+'.txt', /get_lun
print, "LOP ", lop, " : La densite de colonne critique ", column_density, " cm^-2 est atteinte a une profondeur de ", 10^cloudy_depth(i_critique), " cm (log = ", cloudy_depth(i_critique), "), soit ", 10^cloudy_depth(i_critique)*3.24e-22, " kpc."
printf, rion, "LOP ", lop, " : La densite de colonne critique ", column_density, " cm^-2 est atteinte a une profondeur de ", 10^cloudy_depth(i_critique), " cm (log = ", cloudy_depth(i_critique), "), soit ", 10^cloudy_depth(i_critique)*3.24e-22, " kpc."
if column_density lt 1e22 then begin
    print, "LOP ", lop, " : Attention, la limite d'ionisation n'est pas passee !"
    printf, rion, "LOP ", lop, " : Attention, la limite d'ionisation n'est pas passee !"
endif
close, rion

printf, data, 'end of dlaw table'
printf, data, 'abundances ISM no grains no qheat'
printf, data, 'grains ISM function sublimation'
printf, data, 'no grain qheat'
printf, data, 'iterate to convergence'
printf, data, '//save line labels "line_labels.txt"'
printf, data, 'save lines, emissivity "'+filename+'.em_intrinsic" last //default == intrinsinc'
printf, data, 'TOTL	 1216A'
printf, data, 'H  1	 1026A'
printf, data, 'TOTL	 4861A'
printf, data, 'O  3	 5007A'
printf, data, 'H  1	 6563A'
printf, data, 'N  2	 6584A'
printf, data, 'O  1	 6300A'
printf, data, 'S  2	 6720A'
printf, data, 'Ne 3	 3869A'
printf, data, 'TOTL	 3727A'
printf, data, 'Ne 5	 3426A'
printf, data, 'Si 7	2.481m'
printf, data, 'H  1	2.166m'
printf, data, 'Si 6	1.963m'
printf, data, 'Al 9	2.040m'
printf, data, 'Ca 8	2.321m'
printf, data, 'Ne 6	7.652m'
printf, data, 'end of lines'
printf, data, 'save lines, emissivity emergent "'+filename+'.em_emergent" last //default == intrinsinc'
printf, data, 'TOTL	 1216A'
printf, data, 'H  1	 1026A'
printf, data, 'TOTL	 4861A'
printf, data, 'O  3	 5007A'
printf, data, 'H  1	 6563A'
printf, data, 'N  2	 6584A'
printf, data, 'O  1	 6300A'
printf, data, 'S  2	 6720A'
printf, data, 'Ne 3	 3869A'
printf, data, 'TOTL	 3727A'
printf, data, 'Ne 5	 3426A'
printf, data, 'Si 7	2.481m'
printf, data, 'H  1	2.166m'
printf, data, 'Si 6	1.963m'
printf, data, 'Al 9	2.040m'
printf, data, 'Ca 8	2.321m'
printf, data, 'Ne 6	7.652m'
printf, data, 'end of lines'
printf, data, 'set nend 4400'
printf, data, 'save overview "'+filename+'.ovr" last'
printf, data, 'save continuum "'+filename+'.con" units micron last'
printf, data, 'save lines, array "'+filename+'.lin" units micron last'
printf, data, 'save element Hydrogen "'+filename+'_H.el" last'
printf, data, 'save element Oxygen "'+filename+'_O.el" last'
printf, data, 'save physical conditions "'+filename+'.phyc" last'
printf, data, 'print last'
if taille_500 gt 0 then printf, data, '//Cette LOP a ete tronquee de ',taille_500-497,' points.'
close, data

free_lun, data
free_lun, rion

return, 0

end


pro transp_ascii_to_in, sim=sim, part=part, ps=ps

;filenames = '/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part+'_*'
filenames = '/Users/oroos/Post-stage/LOPs'+sim+'/transp6_profile_'+sim+'_LOP*'+part+'_*'

files = findfile(filenames+'.ascii',count = nfiles)

for k = 0, nfiles-1 do begin
    f = files(k)
    n = strpos(f,sim+'_LOP') + 9
    m = strpos(f, part+'__')
    len = m - n
    lop = strmid(f, n, len)
    if sim eq '00075w' or sim eq '00075n' then lop = strmid(f, n+1, len-1)
    random = strmid(f, m+4, 20)
y=read_lop(files(k),sim, part, lop, random, ps)

;print, 'nom ', files(k)
;print, 'sim ', sim
;print, 'partie ', part
;print, 'lop ', lop
;print, 'random ', random
;stop

endfor

print, 'Fin du programme. Tout est OK.'

end


