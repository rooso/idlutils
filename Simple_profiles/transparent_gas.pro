;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ce programme prend des LOPs et fabrique des transp_LOPs en mettant       ;;
;; a tres faible densite les parties > T_seuil.                             ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


function trace, file, sim, part, theta, phi, color,  p_plot,  x_plot,  y_plot, ps
  
  defplotcolors
  
  if sim eq '00100' or sim eq '00075w' $
     or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
  if sim eq '00085' or sim eq '00075n' $
     or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then name = 'No'
  
  sm = sim
  
  if sim eq '00075w' or sim eq '00075n' then sm='00075'
  
  readcol, file, depth, /silent
  readcol, "/Users/oroos/Post-stage/orianne_data/"+name+"_AGN_feedback/output_"+sm+"/sink_"+sm+".out", rien, rien2, x_c, y_c, z_c, /silent
  
  depth_max = max(depth)
  x = depth_max*cos(phi)*sin(theta) + x_c
  y = depth_max*sin(phi)*sin(theta) + y_c
  z = depth_max*cos(theta) + z_c
  
  device, decompose=1
  defplotcolors
;print, x;, y, z
  x = [x_c, x]
  y = [y_c, y]
  z = [z_c, z]
  device, decompose=1
  defplotcolors
  if ps eq 0 then wset, 0
  !p = p_plot & !x = x_plot & !y = y_plot
  plots, X, Y, Z, /T3D, PSYM=-4, COLOR=color, thick=2 ;color=color+long(100*phi);
  
  
end

pro transparent_gas, sim=sim, part=part, ps=ps
  
  device, decompose=1
  defplotcolors
  
if not keyword_set(sim) then sim = '00100'
if not keyword_set(ps) then ps = 0
if not keyword_set(part) then part = 'dk'

  phyc = '/Users/oroos/Post-stage/LOPs'+sim+'/*density_profile_'+sim+'_LOP*.phyc'
  
  print, 'On recherche les fichiers :'
  print, phyc
  
  colors = fltarr(1)
  
  part2 = ['xz', 'zy', 'cx', 'cy', 'px', 'py', 'up', 'dn', 'cu', 'cd', 'dk']
  

  files_phyc = findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=n_files)
  if n_files gt 0 then colors=fltarr(n_files)+!red

one_file = files_phyc(0)
  
  nf = n_files
  for k = 1, n_elements(part2)-1 do begin
     
     files_phyc = [files_phyc,findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files)]
     one_file = [one_file,(findfile('/Users/oroos/Post-stage/LOPs'+sim+'/density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files))(0)];ne prendre qu'un seul fichier
     
     if n_files gt 0 then begin
        if (k eq 1 or k eq 6 or k eq 7) then colors=[colors,fltarr(n_files)+!red]
        if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors=[colors,fltarr(n_files)+!blue]
        if (k eq 4 or k eq 5 or k eq 10) then colors=[colors,fltarr(n_files)+!green]
     endif
     nf = nf + n_files
  endfor
 
  
  files_phyc = files_phyc[where(files_phyc ne '')]
  help, files_phyc
  
files_phyc=one_file
nf = n_elements(one_file)
colors=[!red,!red,!blue,!blue,!green,!green,!red,!red,!blue,!blue,!green] 
 
  files = files_phyc
  strreplace, files, '.phyc', '.ascii'
  files_density = files_phyc
  strreplace, files_density, '.phyc', '.in'
  
  
  print, 'En tout, il y en a ', nf, '.'
;print, files
  
  
  
  theta=dblarr(nf)
  phi=dblarr(nf)
  
  if ps eq 0 then window, 0
  if ps eq 1 then ps_start, '/Users/oroos/Post-stage/plot_lops_test'+sim+'_'+part+'.eps', /encapsulated, /color, /cm, xsize=20, ysize=20
  
  device, decompose=1
  defplotcolors
  
  Plot_3dbox, [0,50], [0,50], [0,50], psym=3, /nodata,   $
              GRIDSTYLE=1, $    ;/SOLID_WALLS
              YZSTYLE=5, AZ=40, TITLE="Distribution of sww_LOPs for sim "+sim+part,      $
              Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
              Ztitle="Z Coordinate" ;,     $
                                ;/YSTYLE, ZRANGE=[-25,25], XRANGE=[-25,25]
  
  p_plot = !p & x_plot = !x & y_plot = !y 
  
  if ps eq 0 then begin
     window, 1
     plot, [-3,1.5], [-6,6], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Density (H/cc)'
     p_den = !p & x_den = !x & y_den = !y
     
     window, 3
     plot, [-3,1.5], [2,10], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', /ystyle
     p_temp = !p & x_temp = !x & y_temp = !y
     
  endif
  
  
  for k = 0, nf-1 do begin
     openr, f, files(k), /get_lun ;fichier ascii
     print, files(k)
     line1 = ''
     readf, f, line1
                                ;print, line1
     theta(k) = double(strmid(line1,strpos(line1,':')+1,25))
                                ;print, 'theta_strmid = ', strmid(line1,strpos(line1,':')+1,25)
     print, 'theta = ', theta(k)
     phi(k) = double(strmid(line1,strpos(line1,':')+26,25))
                                ;print, 'phi_strmid = ', strmid(line1,strpos(line1,':')+26,25)
     print, 'phi = ', phi(k)
     func = trace(files(k), sim, part, theta(k), phi(k), colors(k), p_plot, x_plot, y_plot, ps)
     close, f
     free_lun, f
     
     readcol, files_density(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
                                ;units : none, log cm, log H/cc
     cmp = strcmp(col_continue, 'continue')
     t = where( cmp eq 1 and depth_den gt -35, nt)
     col_continue = col_continue[t]
     depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
     density = density[t]+alog10(1./5.)        ;log H/cc*fill_fact
     
     readcol, files(k), depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, theta_lop_ascii, phi_lop_ascii, /silent, format='(D,D,D,D,D,D,D,D,D,D,D,D,D,D,D)'
     
     t_sort = sort(depth_ascii)
     sorted_depth = depth_ascii[t_sort]
     sorted_density = density_ascii[t_sort]
     sorted_x_ascii = x_ascii[t_sort]
     sorted_y_ascii = y_ascii[t_sort]
     sorted_z_ascii = z_ascii[t_sort]
     sorted_temperature = temperature_ascii[t_sort]
     sorted_vx_ascii = vx_ascii[t_sort]
     sorted_vy_ascii = vy_ascii[t_sort]
     sorted_vz_ascii = vz_ascii[t_sort]
     sorted_cell_size = cell_size_ascii[t_sort]
     x_lops = x_lop_ascii[t_sort]
     y_lops = y_lop_ascii[t_sort]
     z_lops = z_lop_ascii[t_sort]
     theta_lops = theta_lop_ascii[t_sort]
     phi_lops = phi_lop_ascii[t_sort]
     
     count=0
     for i = 0, (size(sorted_depth))[1]-1 do begin
        for j = 0, (size(sorted_depth))[1]-1 do begin   
           if (sorted_depth(i) eq sorted_depth(j) && j ne i) then begin
              count++
              print, sorted_depth(i), sorted_density(i), sorted_density(j)
           endif
        endfor
     endfor
     
     if count gt 0 then begin
        print, 'Il y a ', count, ' problemes'
        stop
     endif

     openr, fin, files_density(k), /get_lun
     line1=''
     readf, fin, line1
     line2=''
     readf, fin, line2
     line3=''
     readf, fin, line3
     line_radius = ''
     readf, fin, line_radius
     free_lun, fin
;print, line_radius
     inner = strpos(line_radius, 'inner')
     outer = strpos(line_radius, 'outer')
     len = outer - inner
     radius_inner = float(strmid(line_radius,inner+5,len-5))
;print, 'rayon', radius_inner
     
     if radius_inner eq 0 then begin
        print, 'rayon nul !'
        stop
     endif
     
     radius_inner = 3.24d-22*10^radius_inner ;conversion en kpc
;print, radius_inner
     readcol, files_phyc(k), depth_phyc, temperature_electrons, hden, eden, heating, rad_acc, fill_fact, /silent, format='(D,D,D,D,D,D)'
;units :             cm,            K,                 cm-3,
;cm-3,erg/cm3/s, cm/s2, number
     depth_phyc = depth_phyc*3.24d-22 + radius_inner ;kpc
     hden = hden*fill_fact
     t_max = where(depth_phyc eq max(depth_phyc))
          
     if ps eq 0 then begin
        wset, 1
        !p = p_den & !x = x_den & !y = y_den
        oplot, depth_den, density, color=colors(k)
        oplot, alog10(sorted_depth), alog10(sorted_density), color=colors(k), line=2
        oplot, alog10(depth_phyc), alog10(hden),  color=colors(k), line=1
     p_den = !p & x_den = !x & y_den = !y

        wset, 3
        !p = p_temp & !x = x_temp & !y = y_temp
        oplot, alog10(sorted_depth), alog10(sorted_temperature), color=colors(k), line=2
       ; oplot, alog10(depth_phyc), alog10(temperature_electrons),  color=colors(k), line=1
     p_temp = !p & x_temp = !x & y_temp = !y
      endif
     
;;;;le gaz ionise dans la simu (== T_amr > 10^6 K) devrait etre
;;;;transparent au rayonnement CLOUDY. ---> mettre une densite
;;;;arbitrairement basse


t_transparent4 = where(sorted_temperature ge 5d4)
density_transparent4 = sorted_density
density_transparent4[t_transparent4] = 1d-6
file_transp4 = files(k)
strreplace, file_transp4, 'density', 'transp4' 
openw, transp4, file_transp4, /get_lun

t_transparent5 = where(sorted_temperature ge 1d5)
density_transparent5 = sorted_density
density_transparent5[t_transparent5] = 1d-6
file_transp5 = files(k)
strreplace, file_transp5, 'density', 'transp5' 
openw, transp5, file_transp5, /get_lun

t_transparent6 = where(sorted_temperature ge 1d6)
density_transparent6 = sorted_density
density_transparent6[t_transparent6] = 1d-6
file_transp6 = files(k)
strreplace, file_transp6, 'density', 'transp6' 
openw, transp6, file_transp6, /get_lun


t_transparent7 = where(sorted_temperature ge 1d7)
density_transparent7 = sorted_density
density_transparent7[t_transparent7] = 1d-6
file_transp7 = files(k)
strreplace, file_transp7, 'density', 'transp7' 
openw, transp7, file_transp7, /get_lun

for ii = 0, n_elements(sorted_depth)-1 do begin
printf, transp4, sorted_depth(ii), density_transparent4(ii)
printf, transp5, sorted_depth(ii), density_transparent5(ii)
printf, transp6, sorted_depth(ii), density_transparent6(ii)
printf, transp7, sorted_depth(ii), density_transparent7(ii)

print, sorted_depth(ii), density_transparent4(ii)
print, sorted_depth(ii), density_transparent5(ii)
print, sorted_depth(ii), density_transparent6(ii)
print, sorted_depth(ii), density_transparent7(ii)

;stop
endfor
close, transp4
close, transp5
close, transp6
close, transp7

free_lun, transp4
free_lun, transp5
free_lun, transp6
free_lun, transp7



        wset, 1
        !p = p_den & !x = x_den & !y = y_den
        oplot, alog10(sorted_depth), alog10(density_transparent6), color=colors(k), line=2
     p_den = !p & x_den = !x & y_den = !y


  endfor
     
;print, 'Min/max de theta : ', minmax(theta)
;print, 'Min/max de phi : ', minmax(phi)
     
;plots,  [25,25], [25,50], [25,25], psym=-4
     if ps eq 1 then ps_end, /png
     
print, 'Fin du programme, tout est OK.'

end
  
  
