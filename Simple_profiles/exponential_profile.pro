pro exponential_profile, ff=ff
 
print, 'Are you sur you want to delete the exponential*.in and create them again ?'
read, go_on
if go_on eq 1 then begin

 z0 = 500d-3
 r0 = 4.5
 M = 2d10
 cte = M/2./!dpi/r0^2/z0/(1.-exp(-1.4))/(1-2.4*exp(-1.4))
 print, cte

 z = findgen(497)/497.*1.4*z0                                           
 r = findgen(497)/497.*1.4*r0   

rho = dblarr(497,497)

for i = 0, 496 do begin
for j = 0, 496 do begin
 rho(i,j) = cte*exp(-r(i)/r0)*exp(-z(j)/z0)*4.07d-8
endfor
endfor
 print, minmax(rho)

 readcol, '/Users/oroos/Post-stage/LOPs00100/density_profile_00100_LOP1dk__d*.ascii', dpth, den
window, 2, ypos=425
 plot, r, cte*exp(-r/r0)*exp(-z(0)/z0)*4.07d-8, /xlog, /ylog, xr=[1e-3,50], yr=[1e-6,1e6]
 oplot, dpth, den, psym=3, color=!red   
 oplot, r, cte*exp(-r/r0)*exp(-z(248)/z0)*4.07d-8, color=!blue
 oplot, r, cte*exp(-r/r0)*exp(-z(496)/z0)*4.07d-8, color=!purple
 oplot, r, cte*exp(-r/r0)*exp(-2.*z(496)/z0)*4.07d-8, color=!magenta
 oplot, r, cte*exp(-r/r0)*exp(-4.*z(496)/z0)*4.07d-8, color=!pink

print, avg(cte*exp(-r/r0)*exp(-z(0)/z0)*4.07d-8), avg(cte*exp(-r/r0)*exp(-z(248)/z0)*4.07d-8), avg(cte*exp(-r/r0)*exp(-z(496)/z0)*4.07d-8), avg(cte*exp(-r/r0)*exp(-2.*z(496)/z0)*4.07d-8), avg(cte*exp(-r/r0)*exp(-4.*z(496)/z0)*4.07d-8), avg(den)


log_r_cm = alog10(r/3.24d-22) ;kpc to log cm 
if ff eq 1 then log_rho_h_cc = alog10(rho) else log_rho_h_cc = alog10(rho/(1./5.)) ;H/cc to log H/cc/fill_fact

;print, log_r_cm(0)
log_r_cm(0) = -35.

;print, log_r_cm
;print, log_rho_h_cc(*,0)
 
if ff eq 0.2 then begin
  openw, lun, 'exponential_profile_z=0.txt', /get_lun  
  openw, lun2, 'exponential_profile_before_z=0.in', /get_lun                         
  openw, lun3, 'x10_exponential_profile_before_z=0.in', /get_lun 
  openw, lun4, 'x100_exponential_profile_before_z=0.in', /get_lun 
endif
if ff eq 1 then begin
  openw, lun, 'expff_z=0.txt', /get_lun  
  openw, lun2, 'expff_before_z=0.in', /get_lun                         
  openw, lun3, 'x10_expff_before_z=0.in', /get_lun 
  openw, lun4, 'x100_expff_before_z=0.in', /get_lun 
endif
 for i = 0, n_elements(r)-1 do begin
printf, lun, r(i), z(0), rho(i,0)
printf, lun2, 'continue', log_r_cm(i), log_rho_h_cc(i,0)
printf, lun3, 'continue', log_r_cm(i), log_rho_h_cc(i,0)
printf, lun4, 'continue', log_r_cm(i), log_rho_h_cc(i,0)
 endfor
close, lun, lun2, lun3, lun4                                                                 
 free_lun, lun, lun2, lun3, lun4 

if ff eq 0.2 then begin
  openw, lun, 'exponential_profile_z=z_max.txt', /get_lun  
  openw, lun2, 'exponential_profile_before_z=z_max.in', /get_lun                         
  openw, lun3, 'x10_exponential_profile_before_z=z_max.in', /get_lun 
  openw, lun4, 'x100_exponential_profile_before_z=z_max.in', /get_lun 
endif
if ff eq 1 then begin
  openw, lun, 'expff_z=z_max.txt', /get_lun  
  openw, lun2, 'expff_before_z=z_max.in', /get_lun                         
  openw, lun3, 'x10_expff_before_z=z_max.in', /get_lun 
  openw, lun4, 'x100_expff_before_z=z_max.in', /get_lun 
endif                      
 for i = 0, n_elements(r)-1 do begin
printf, lun, r(i), z(496), rho(i,496)
printf, lun2, 'continue', log_r_cm(i), log_rho_h_cc(i,496)
printf, lun3, 'continue', log_r_cm(i), log_rho_h_cc(i,496)
printf, lun4, 'continue', log_r_cm(i), log_rho_h_cc(i,496)
 endfor
close, lun, lun2, lun3, lun4                                                                 
 free_lun, lun, lun2, lun3, lun4                                                                    
                                                                                 
if ff eq 0.2 then begin
  openw, lun, 'exponential_profile_z=half_z_max.txt', /get_lun  
  openw, lun2, 'exponential_profile_before_z=half_z_max.in', /get_lun                         
  openw, lun3, 'x10_exponential_profile_before_z=half_z_max.in', /get_lun 
  openw, lun4, 'x100_exponential_profile_before_z=half_z_max.in', /get_lun 
endif
if ff eq 1 then begin
  openw, lun, 'expff_z=half_z_max.txt', /get_lun  
  openw, lun2, 'expff_before_z=half_z_max.in', /get_lun                         
  openw, lun3, 'x10_expff_before_z=half_z_max.in', /get_lun 
  openw, lun4, 'x100_expff_before_z=half_z_max.in', /get_lun 
endif                       
 for i = 0, n_elements(r)-1 do begin
printf, lun, r(i), z(248), rho(i,248)
printf, lun2, 'continue', log_r_cm(i), log_rho_h_cc(i,248)
printf, lun3, 'continue', log_r_cm(i), log_rho_h_cc(i,248)
printf, lun4, 'continue', log_r_cm(i), log_rho_h_cc(i,248)
 endfor
close, lun, lun2, lun3, lun4                                                                 
 free_lun, lun, lun2, lun3, lun4 



if ff eq 0.2 then begin
  openw, lun, 'exponential_profile_z=2z_max.txt', /get_lun  
  openw, lun2, 'exponential_profile_before_z=2z_max.in', /get_lun                         
  openw, lun3, 'x10_exponential_profile_before_z=2z_max.in', /get_lun 
  openw, lun4, 'x100_exponential_profile_before_z=2z_max.in', /get_lun 
endif
if ff eq 1 then begin
  openw, lun, 'expff_z=2z_max.txt', /get_lun  
  openw, lun2, 'expff_before_z=2z_max.in', /get_lun                         
  openw, lun3, 'x10_expff_before_z=2z_max.in', /get_lun 
  openw, lun4, 'x100_expff_before_z=2z_max.in', /get_lun 
endif                       
 for i = 0, n_elements(r)-1 do begin
printf, lun, r(i), 2.*z(496), cte*exp(-r(i)/r0)*exp(-2.*z(496)/z0)*4.07d-8
printf, lun2, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-2.*z(496)/z0)*4.07d-8/(1./5.))
printf, lun3, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-2.*z(496)/z0)*4.07d-8/(1./5.))
printf, lun4, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-2.*z(496)/z0)*4.07d-8/(1./5.))
 endfor
close, lun, lun2, lun3, lun4                                                                 
 free_lun, lun, lun2, lun3, lun4    



if ff eq 0.2 then begin
  openw, lun, 'exponential_profile_z=4z_max.txt', /get_lun  
  openw, lun2, 'exponential_profile_before_z=4z_max.in', /get_lun                         
  openw, lun3, 'x10_exponential_profile_before_z=4z_max.in', /get_lun 
  openw, lun4, 'x100_exponential_profile_before_z=4z_max.in', /get_lun 
endif
if ff eq 1 then begin
  openw, lun, 'expff_z=4z_max.txt', /get_lun  
  openw, lun2, 'expff_before_z=4z_max.in', /get_lun                         
  openw, lun3, 'x10_expff_before_z=4z_max.in', /get_lun 
  openw, lun4, 'x100_expff_before_z=4z_max.in', /get_lun 
endif                         
 for i = 0, n_elements(r)-1 do begin
printf, lun, r(i), 4.*z(496), cte*exp(-r(i)/r0)*exp(-4.*z(496)/z0)*4.07d-8
printf, lun2, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-4.*z(496)/z0)*4.07d-8/(1./5.))
printf, lun3, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-4.*z(496)/z0)*4.07d-8/(1./5.))
printf, lun4, 'continue', log_r_cm(i), alog10(cte*exp(-r(i)/r0)*exp(-4.*z(496)/z0)*4.07d-8/(1./5.))
 endfor
close, lun, lun2, lun3, lun4                                                                 
 free_lun, lun, lun2, lun3, lun4    


endif

end
