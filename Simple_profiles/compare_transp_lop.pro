pro compare_transp_lop, ps=ps, compact=compact
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; ce programme compare les profils semi-transparents au profil   ;;;;
;;;; original correspondant.                                        ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if not keyword_set(ps) then ps = 0
  if not keyword_set(compact) then compact = 0

  if compact eq 1 then nm_comp = '_compact' else nm_comp = ''

  if ps eq 1 then ps_thick = 16 else ps_thick = 1

  dir = '/Users/oroos/Post-stage/LOPs00100/'

;transpX :
;;;;le gaz ionise dans la simu (== T_amr > 10^X K) devrait etre
;;;;transparent au rayonnement CLOUDY. ---> mettre une densite
;;;;arbitrairement basse (10^-6 H/cc, independamment du ff)

  transp4 = findfile(dir+'*transp4*.in',count=nf_transp4)
  transp5 = findfile(dir+'*transp5*.in',count=nf_transp5)
  transp6 = findfile(dir+'*transp6*.in',count=nf_transp6)
  transp7 = findfile(dir+'*transp7*.in',count=nf_transp7)

  comp = transp4
  strreplace, comp, 'transp4', 'density'

;print, comp
;print, transp4

  title = ['cd', 'cu', 'cx', 'cy', 'dk', 'dn', 'px', 'py', 'up', 'xz', 'zy']
  title = [title,'x100_'+title,'x10_'+title]

;print, comp
;print, transp4
;stop

  if compact eq 0 then begin
     items = ['Transparent for T > 5 x '+textoidl('10^{4}')+' K', 'Transparent for T > 1 x '+textoidl('10^{5}')+' K',  'Transparent for T > 1 x '+textoidl('10^{6}')+' K', 'Transparent for T > 1 x '+textoidl('10^{7}')+' K', 'Original LOP :', 'L'+textoidl('_{AGN}')+' x 1', 'L'+textoidl('_{AGN}')+' x 10', 'L'+textoidl('_{AGN}')+' x 100', 'AMR (initial state)', 'Transparent : n = '+textoidl('10^{-6}')+' cm'+textoidl('^{-3}') ]
     psyms = 0
     colors = [!red, !brown, !blue, !green,!white,!black, !black, !black, !black, !white]
     linestyles = [0,0,0,0,0,0,0,0,2,0]
     thicks = [1,1,1,1,1,4,2,1,1,1]*ps_thick
  endif else begin
     items = ['Transparent for T > 5 x '+textoidl('10^{4}')+' K', 'Transparent for T > 1 x '+textoidl('10^{5}')+' K',  'Transparent for T > 1 x '+textoidl('10^{6}')+' K', 'Transparent for T > 1 x '+textoidl('10^{7}')+' K', 'Original LOP, L'+textoidl('_{AGN}')+' x 100', 'AMR (initial state)', 'Transparent : n = '+textoidl('10^{-6}')+' cm'+textoidl('^{-3}') ]
     psyms = 0
     colors = [!red, !brown, !blue, !green,!black,!black, !white]
     linestyles = [0,0,0,0,0,2,0]
     thicks = [1,1,1,1,1,1,1]*ps_thick

  endelse


  if ps eq 1 then linsize = 0.07 else linsize = 1
  if compact eq 0 then begin
     if ps eq 1 then pos = [500,33000] else pos = [20,505]
  endif else begin
     if ps eq 1 then pos = [500,35500] else pos = [20,535]
  endelse

  if nf_transp4 ne nf_transp5 or nf_transp4 ne nf_transp6 or nf_transp4 ne nf_transp7 then stop

  for k = 0, 10 do begin

     if file_test(repstr(transp4(k),'.in','.phyc')) eq 1 then begin

        r_in4 = search_rinner(transp4(k),/silent)
        r_in5 = search_rinner(transp5(k),/silent)
        r_in6 = search_rinner(transp6(k),/silent)
        r_in7 = search_rinner(transp7(k),/silent)
        r_comp = search_rinner(comp(k),/silent)
;print, r_in4, r_in5, r_in6, r_in7, r_comp

        if r_in4 ne r_in5 or r_in4 ne r_in6 or r_in4 ne r_in7 or r_in4 ne r_comp then stop

;lecture des profils de densite
        profil_4 = read_file_in(transp4(k))
        profil_5 = read_file_in(transp5(k))
        profil_6 = read_file_in(transp6(k))
        profil_7 = read_file_in(transp7(k))
        profil_comp = read_file_in(comp(k))

        if ps eq 1 then begin
           ps_start, 'transparent_LOPs_'+title(k)+nm_comp+'.eps', /encapsulated, /color, /decomposed, /cm, xsize=22, ysize=22, /helvetica, /bold
           !p.font=0
           !p.charsize=4
           !p.charthick=2
           !x.thick=15
           !y.thick=15
           !p.thick=6
        endif else begin
           window, 1, xsize=800, ysize=800
        endelse

        !p.multi=[3,2,2]
        !x.margin=[8,1]
        !y.margin=[2,2]
;IDL> print, !x.margin, !y.margin
;      10.0000      3.00000
;      4.00000      2.00000
;IDL> print, !x.omargin, !y.omargin
;      0.00000      0.00000
;      0.00000      0.00000

;use | as symbol
        UserSym, [0, 0, 0, 0, 0], [0, 0, 0, -0.7, 0.7] 

        if ps eq 0 then chars = 1.5 else chars = 1.5*ps_thick/8.

        cgText, 0.5, 0.98, ALIGNMENT=0.5, CHARSIZE=chars, /NORMAL, title(k)
        plot, [0.002,30], [1e-7,1e7], /nodata, xtitle='Depth [kpc]', ytitle='Density [cm'+textoidl('^{-3}')+']', /xlog, /ylog, /xstyle, /ystyle
        oplot, profil_4(*,0), profil_4(*,1)*4, color=!red, thick=ps_thick, max_val=5d-6, psym=-8
        oplot, profil_5(*,0), profil_5(*,1)*1.5, color=!brown, thick=ps_thick, max_val=2d-6, psym=-8
        oplot, profil_6(*,0), profil_6(*,1)/1.5, color=!blue, thick=ps_thick, max_val=1d-6, psym=-8
        oplot, profil_7(*,0), profil_7(*,1)/4, color=!green, thick=ps_thick, max_val=0.5d-6, psym=-8 ;filtre et relie les points correctement
        oplot, profil_comp(*,0), profil_comp(*,1), color=!black, thick=ps_thick

                                ; Add the legend.
        AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, /bottom, textcolor=!black, outline_color=!black, /clear, background_color=!white,  position=pos, /device, thick=thicks, linsize=linsize

;stop

;lecture du fichier .phyc
        phyc_4 = read_file_phyc(transp4(k))
        phyc_5 = read_file_phyc(transp5(k))
        phyc_6 = read_file_phyc(transp6(k))
        phyc_7 = read_file_phyc(transp7(k))
        phyc_comp = read_file_phyc(comp(k))

;x10
        phyc_4_x10 = read_file_phyc(transp4(k+22))
        phyc_5_x10 = read_file_phyc(transp5(k+22))
        phyc_6_x10 = read_file_phyc(transp6(k+22))
        phyc_7_x10 = read_file_phyc(transp7(k+22))
        phyc_comp_x10 = read_file_phyc(comp(k+22))

;x100
        phyc_4_x100 = read_file_phyc(transp4(k+11))
        phyc_5_x100 = read_file_phyc(transp5(k+11))
        phyc_6_x100 = read_file_phyc(transp6(k+11))
        phyc_7_x100 = read_file_phyc(transp7(k+11))
        phyc_comp_x100 = read_file_phyc(comp(k+11))

;lecture du fichier .ascii
        ascii_comp = read_file_ascii(comp(k))

        verif = 0               ;profil ok.
        if verif eq 1 then begin
;densite
           oplot, phyc_4(*,0), phyc_4(*,2), color=!red, line=1, thick=ps_thick
           oplot, phyc_5(*,0), phyc_5(*,2), color=!brown, line=1, thick=ps_thick
           oplot, phyc_6(*,0), phyc_6(*,2), color=!blue, line=1, thick=ps_thick
           oplot, phyc_7(*,0), phyc_7(*,2), color=!green, line=1, thick=ps_thick
           oplot, phyc_comp(*,0), phyc_comp(*,2), color=!black, line=1, thick=ps_thick
           oplot, ascii_comp(*,0), ascii_comp(*,1), color=!black, line=2, thick=ps_thick
        endif
;stop

        !y.margin=[4,2]

        plot, [0.002,30], [1e2,1e10], /nodata, xtitle='Depth [kpc]', ytitle='Temperature [K]', /xlog, /ylog, /xstyle, /ystyle
        oplot, [0.002,30], [1e4,1e4], color=!gray, thick=4*ps_thick
        if compact eq 0 then begin
           oplot, phyc_4(*,0), phyc_4(*,1), color=!red, thick=4*ps_thick
           oplot, phyc_5(*,0), phyc_5(*,1), color=!brown, thick=4*ps_thick
           oplot, phyc_6(*,0), phyc_6(*,1), color=!blue, thick=4*ps_thick
           oplot, phyc_7(*,0), phyc_7(*,1), color=!green, thick=4*ps_thick
           oplot, phyc_comp(*,0), phyc_comp(*,1), color=!black, thick=4*ps_thick

           oplot, phyc_4_x10(*,0), phyc_4_x10(*,1), color=!red, thick=2*ps_thick
           oplot, phyc_5_x10(*,0), phyc_5_x10(*,1), color=!brown, thick=2*ps_thick
           oplot, phyc_6_x10(*,0), phyc_6_x10(*,1), color=!blue, thick=2*ps_thick
           oplot, phyc_7_x10(*,0), phyc_7_x10(*,1), color=!green, thick=2*ps_thick
           oplot, phyc_comp_x10(*,0), phyc_comp_x10(*,1), color=!black, thick=2*ps_thick
        endif

        oplot, phyc_4_x100(*,0), phyc_4_x100(*,1), color=!red, thick=1*ps_thick
        oplot, phyc_5_x100(*,0), phyc_5_x100(*,1), color=!brown, thick=1*ps_thick
        oplot, phyc_6_x100(*,0), phyc_6_x100(*,1), color=!blue, thick=1*ps_thick
        oplot, phyc_7_x100(*,0), phyc_7_x100(*,1), color=!green, thick=1*ps_thick
        oplot, phyc_comp_x100(*,0), phyc_comp_x100(*,1), color=!black, thick=1*ps_thick
        oplot, ascii_comp(*,0), ascii_comp(*,5), color=!black, line=2 ; T_AMR

        oplot, profil_4(*,0), profil_4(*,1)*4*1d15, color=!red, thick=ps_thick, max_val=5d-6*1d15, psym=-8
        oplot, profil_5(*,0), profil_5(*,1)*1.5*1d15, color=!brown, thick=ps_thick, max_val=2d-6*1d15, psym=-8
        oplot, profil_6(*,0), profil_6(*,1)/1.5*1d15, color=!blue, thick=ps_thick, max_val=1d-6*1d15, psym=-8
        oplot, profil_7(*,0), profil_7(*,1)/4*1d15, color=!green, thick=ps_thick, max_val=0.5d-6*1d15, psym=-8

;lecture du fichier _H.el
        Hfrac_4 = read_file_hfrac(transp4(k))
        Hfrac_5 = read_file_hfrac(transp5(k))
        Hfrac_6 = read_file_hfrac(transp6(k))
        Hfrac_7 = read_file_hfrac(transp7(k))
        Hfrac_comp = read_file_hfrac(comp(k))

        Hfrac_4_x10 = read_file_hfrac(transp4(k+22))
        Hfrac_5_x10 = read_file_hfrac(transp5(k+22))
        Hfrac_6_x10 = read_file_hfrac(transp6(k+22))
        Hfrac_7_x10 = read_file_hfrac(transp7(k+22))
        Hfrac_comp_x10 = read_file_hfrac(comp(k+22))

        Hfrac_4_x100 = read_file_hfrac(transp4(k+11))
        Hfrac_5_x100 = read_file_hfrac(transp5(k+11))
        Hfrac_6_x100 = read_file_hfrac(transp6(k+11))
        Hfrac_7_x100 = read_file_hfrac(transp7(k+11))
        Hfrac_comp_x100 = read_file_hfrac(comp(k+11))

;limite inferieure du plot : 1d-13 pour distinguer la partie neutre, 1d-22 pour voir la courbe en entier
        plot, [0.002,30], [1d-13,1d0], /nodata, xtitle='Depth [kpc]', ytitle='Fraction of neutral H', /xlog, /ylog, /xstyle, /ystyle
        oplot, [0.002,30], [1e-1,1e-1], color=!gray, thick=4*ps_thick
        if compact eq 0 then begin
           oplot, Hfrac_4(*,0), Hfrac_4(*,1), color=!red, thick=4*ps_thick
           oplot, Hfrac_5(*,0), Hfrac_5(*,1), color=!brown, thick=4*ps_thick
           oplot, Hfrac_6(*,0), Hfrac_6(*,1), color=!blue, thick=4*ps_thick
           oplot, Hfrac_7(*,0), Hfrac_7(*,1), color=!green, thick=4*ps_thick
           oplot, Hfrac_comp(*,0), Hfrac_comp(*,1), color=!black, thick=4*ps_thick

           oplot, Hfrac_4_x10(*,0), Hfrac_4_x10(*,1), color=!red, thick=2*ps_thick
           oplot, Hfrac_5_x10(*,0), Hfrac_5_x10(*,1), color=!brown, thick=2*ps_thick
           oplot, Hfrac_6_x10(*,0), Hfrac_6_x10(*,1), color=!blue, thick=2*ps_thick
           oplot, Hfrac_7_x10(*,0), Hfrac_7_x10(*,1), color=!green, thick=2*ps_thick
           oplot, Hfrac_comp_x10(*,0), Hfrac_comp_x10(*,1), color=!black, thick=2*ps_thick
        endif

        oplot, Hfrac_4_x100(*,0), Hfrac_4_x100(*,1), color=!red, thick=1*ps_thick
        oplot, Hfrac_5_x100(*,0), Hfrac_5_x100(*,1), color=!brown, thick=1*ps_thick
        oplot, Hfrac_6_x100(*,0), Hfrac_6_x100(*,1), color=!blue, thick=1*ps_thick
        oplot, Hfrac_7_x100(*,0), Hfrac_7_x100(*,1), color=!green, thick=1*ps_thick
        oplot, Hfrac_comp_x100(*,0), Hfrac_comp_x100(*,1), color=!black, thick=1*ps_thick

        if compact eq 1 then begin
           if title(k) eq 'dk' or title(k) eq 'py' then shift = 1d-6 else shift=1d3
           oplot, profil_4(*,0), profil_4(*,1)*4*shift, color=!red, thick=ps_thick, max_val=5d-6*shift, psym=-8
           oplot, profil_5(*,0), profil_5(*,1)*1.5*shift, color=!brown, thick=ps_thick, max_val=2d-6*shift, psym=-8
           oplot, profil_6(*,0), profil_6(*,1)/1.5*shift, color=!blue, thick=ps_thick, max_val=1d-6*shift, psym=-8
           oplot, profil_7(*,0), profil_7(*,1)/4*shift, color=!green, thick=ps_thick, max_val=0.5d-6*shift, psym=-8
        endif


;compute SFR
        threshold=10
        lmax=13
        boxlen=50

        deltaSFR_4 = compute_sfr(transp4(k),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_5 = compute_sfr(transp5(k),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_6 = compute_sfr(transp6(k),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_7 = compute_sfr(transp7(k),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_comp = compute_sfr(comp(k),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)

        deltaSFR_4_x10 = compute_sfr(transp4(k+22),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_5_x10 = compute_sfr(transp5(k+22),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_6_x10 = compute_sfr(transp6(k+22),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_7_x10 = compute_sfr(transp7(k+22),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_comp_x10 = compute_sfr(comp(k+22),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)

        deltaSFR_4_x100 = compute_sfr(transp4(k+11),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_5_x100 = compute_sfr(transp5(k+11),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_6_x100 = compute_sfr(transp6(k+11),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_7_x100 = compute_sfr(transp7(k+11),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)
        deltaSFR_comp_x100 = compute_sfr(comp(k+11),threshold=threshold,/SFR,lmax=lmax,boxlen=boxlen)


        print, 'SFRavant, SFRapres [SFR en M_sun/yr/kpc^3] , deltaSFR = (SFRavant - SFRapres)/SFRavant et delta C/T = (deltaSFR_comp - deltaSFR_transp)/deltaSFR_comp'
        print, '1 x L_AGN'
        print, 'transp 4 : ', deltaSFR_4, (deltaSFR_comp(2)-deltaSFR_4(2))/deltaSFR_comp(2)
        print, 'transp 5 : ', deltaSFR_5, (deltaSFR_comp(2)-deltaSFR_5(2))/deltaSFR_comp(2)
        print, 'transp 6 : ', deltaSFR_6, (deltaSFR_comp(2)-deltaSFR_6(2))/deltaSFR_comp(2)
        print, 'transp 7 : ', deltaSFR_7, (deltaSFR_comp(2)-deltaSFR_7(2))/deltaSFR_comp(2)
        print, 'comp : ', deltaSFR_comp

        print, '10 x L_AGN'
        print, 'transp 4 : ', deltaSFR_4_x10, (deltaSFR_comp_x10(2)-deltaSFR_4_x10(2))/deltaSFR_comp_x10(2)
        print, 'transp 5 : ', deltaSFR_5_x10, (deltaSFR_comp_x10(2)-deltaSFR_5_x10(2))/deltaSFR_comp_x10(2)
        print, 'transp 6 : ', deltaSFR_6_x10, (deltaSFR_comp_x10(2)-deltaSFR_6_x10(2))/deltaSFR_comp_x10(2)
        print, 'transp 7 : ', deltaSFR_7_x10, (deltaSFR_comp_x10(2)-deltaSFR_7_x10(2))/deltaSFR_comp_x10(2)
        print, 'comp : ', deltaSFR_comp_x10

        print, '100 x L_AGN'
        print, 'transp 4 : ', deltaSFR_4_x100, (deltaSFR_comp_x100(2)-deltaSFR_4_x100(2))/deltaSFR_comp_x100(2)
        print, 'transp 5 : ', deltaSFR_5_x100, (deltaSFR_comp_x100(2)-deltaSFR_5_x100(2))/deltaSFR_comp_x100(2)
        print, 'transp 6 : ', deltaSFR_6_x100, (deltaSFR_comp_x100(2)-deltaSFR_6_x100(2))/deltaSFR_comp_x100(2)
        print, 'transp 7 : ', deltaSFR_7_x100, (deltaSFR_comp_x100(2)-deltaSFR_7_x100(2))/deltaSFR_comp_x100(2)
        print, 'comp : ', deltaSFR_comp_x100

        if ps eq 1 then begin 
           ps_end, /png
;spawn, 'open transparent_LOPs_'+title(k)+nm_comp+'.eps'
        endif

        stop
     endif

  endfor


  print, 'Fin du programme, tout est OK.'

end
