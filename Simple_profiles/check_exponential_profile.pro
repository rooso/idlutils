pro check_exponential_profile, ps=ps
  
  dir='/Users/oroos/Post-stage/Exponential_profile/'
  
exp = 'expff' ; exp = 'exponential_profile'

  file_in = dir+exp+'_z=0.in'
  file_in_x10 = dir+'x10_'+exp+'_z=0.in'
  file_in_x100 = dir+'x100_'+exp+'_z=0.in'

  file_in_half = dir+exp+'_z=half_z_max.in'
  file_in_x10_half = dir+'x10_'+exp+'_z=half_z_max.in'
  file_in_x100_half = dir+'x100_'+exp+'_z=half_z_max.in'

  file_in_max = dir+exp+'_z=z_max.in'
  file_in_x10_max = dir+'x10_'+exp+'_z=z_max.in'
  file_in_x100_max = dir+'x100_'+exp+'_z=z_max.in'

  file_in_2max = dir+exp+'_z=2z_max.in'
  file_in_x10_2max = dir+'x10_'+exp+'_z=2z_max.in'
  file_in_x100_2max = dir+'x100_'+exp+'_z=2z_max.in'

  file_in_4max = dir+exp+'_z=4z_max.in'
  file_in_x10_4max = dir+'x10_'+exp+'_z=4z_max.in'
  file_in_x100_4max = dir+'x100_'+exp+'_z=4z_max.in'

  
  r_inner = 10d^search_rinner(file_in)         ;cm
  r_inner_x10 = 10d^search_rinner(file_in_x10) ;cm
  r_inner_x100 = 10d^search_rinner(file_in_x100) ;cm

  r_outer = 10d^search_router(file_in)         ;cm  

  file_comp = dir+'density_profile_00100_LOP1dk__d20131226t204119.840.in'
  file_comp_x10 = dir+'x10_density_profile_00100_LOP1dk__d20131226t204119.840.in'
  file_comp_x100 = dir+'x100_density_profile_00100_LOP1dk__d20131226t204119.840.in'
  
  r_innercomp = 10d^search_rinner(file_in)         ;cm
  r_innercomp_x10 = 10d^search_rinner(file_in_x10) ;cm
  r_innercomp_x100 = 10d^search_rinner(file_in_x100) ;cm
  
  if r_inner ne r_inner_x10 or r_inner_x10 ne r_inner_x100 then stop

  

if ps ne 0 then begin
ps_start, 'compare_exponential_profile.eps', /encapsulated, /color, /decomposed, /cm, xsize=30, ysize=60, /helvetica, /bold
     !p.charsize=12
     !p.charthick=2
     !p.thick=12

endif


if ps eq 0 then thick = 2 else thick = 18
if ps eq 0 then lin = 1 else lin = 0.1

if ps eq 0 then window, 0, ypos=425, ysize=600
!p.multi=[0,2,4]
!x.margin=[7,3]
!y.margin=[4,1]

plot, [0.01,10], [-7,0], ytitle='log fraction of neutral H', /nodata, /xlog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_H = !p & x_H = !x & y_H = !y

plot, [0.01,10], [1e3,1e8], ytitle='Temperature [K]', /nodata, /xlog, /ylog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_temp = !p & x_temp = !x & y_temp = !y

!y.margin=[4,0]

plot, [0.01,10], [-7,0], ytitle='log fraction of neutral H', /nodata, /xlog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_H_x10 = !p & x_H_x10 = !x & y_H_x10 = !y

plot, [0.01,10], [1e3,1e8], ytitle='Temperature [K]', /nodata, /xlog, /ylog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_temp_x10 = !p & x_temp_x10 = !x & y_temp_x10 = !y

plot, [0.01,10], [-7,0], ytitle='log fraction of neutral H', /nodata, /xlog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_H_x100 = !p & x_H_x100 = !x & y_H_x100 = !y

plot, [0.01,10], [1e3,1e8], ytitle='Temperature [K]', /nodata, /xlog, /ylog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_temp_x100 = !p & x_temp_x100 = !x & y_temp_x100 = !y

plot, [0.01,10], [1e-4,1e4], ytitle='Density [H/cc]', /nodata, /xlog, /ylog, /ystyle, /xstyle, xtitle='Depth [kpc]'
p_den = !p & x_den = !x & y_den = !y

!y.margin=0
!x.margin=0
plot, [0.01,10],[1e-4,1e4], /nodata, color=!white, xtickn=replicate(' ',10), ytickn=replicate(' ',10)
 ; Create the legend with NASA Astronomy routine AL_LEGEND.
     items = ["Exp. profile (0.1 H/cc)","Exp. profile (1.4 H/cc)","Exp. profile (5.5 H/cc)","Exp. profile (11 H/cc)", "Exp. profile (22 H/cc)","Simulated profile", "Seyfert regime", "wQuasar regime", "sQuasar regime" ]
     psyms = [0,0,0,0,0, 0, 0,0,0]
     colors = [!purple,!orange,!red,!brown, !black,!blue, !black, !black, !black]
     linestyles = [0,0,0, 0, 0, 0, 0, 2, 4]   
    ; Add the legend.
if ps eq 1 then   !p.charsize=6
     AL_Legend, items, PSym=psyms, Lines=linestyles, Color=colors, textcolor=!black, outline_color=!black, /clear, back=!white, /top, /left, linsize=lin, thick=thick
if ps eq 1 then   !p.charsize=12


  files = [file_in, file_in_half, file_in_max, file_in_2max, file_in_4max, file_comp, $
           file_in_x10, file_in_x10_half, file_in_x10_max, file_in_x10_2max, file_in_x10_4max, file_comp_x10, $
           file_in_x100, file_in_x100_half, file_in_x100_max, file_in_x100_2max, file_in_x100_4max, file_comp_x100]
  col   = [!black,!brown,!red,!orange,!purple,!blue,!black,!brown,!red,!orange,!purple,!blue,!black,!brown,!red,!orange,!purple,!blue]
  line  = [0,0,0,0,0,0,2,2,2,2,2,2,4,4,4,4,4,4]
  for k = 0, n_elements(files)-1 do begin
     files_phyc = files
     strreplace, files_phyc, '.in', '.phyc'
     files_con = files_phyc
     strreplace, files_con, '.phyc', '.con'
     files_fionH = files_phyc
     strreplace, files_fionH, '.phyc', '_H.el'
     files_fionO = files_phyc
     strreplace, files_fionO, '.phyc', '_O.el'
     files_em_em = files_phyc
     strreplace, files_em_em, '.phyc', '.em_emergent'
     files_em_int = files_phyc
     strreplace, files_em_int, '.phyc', '.em_intrinsic'
     
     readcol, files(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
                                ;units : none, log cm, log H/cc
     cmp = strcmp(col_continue, 'continue')
     t = where( cmp eq 1 and depth_den gt -35, nt)
     col_continue = col_continue[t]
     depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
     density = density[t]+alog10(1./5.)

     depth_den = 10d^depth_den
     density = 10d^density

print, files(k), avg(density)
print, minmax(density)
print, 10d^(avg(alog10(density)))
;stop

!p = p_den & !x = x_den & !y = y_den
oplot, depth_den,density, color=col(k), line=line(k), thick=thick
     
     readcol, files_phyc(k), depth_phyc, temperature_phyc, hden_phyc, eden_phyc, heating_phyc, rad_acc_phyc, fill_fact, /silent
                                ;units :                cm,           K, cm-3, cm-3, erg/cm3/s, number
     hden_phyc = hden_phyc*fill_fact
     eden_phyc = eden_phyc*fill_fact
     depth_phyc = depth_phyc + r_inner       ;cm


if k lt 6 then begin 
!p = p_temp & !x = x_temp & !y = y_temp
oplot, depth_phyc*3.24d-22,temperature_phyc, color=col(k), line=line(k), thick=thick
endif
if k ge 6 and k lt 12 then begin 
!p = p_temp_x10 & !x = x_temp_x10 & !y = y_temp_x10
oplot, depth_phyc*3.24d-22,temperature_phyc, color=col(k), line=line(k), thick=thick
endif
if k ge 12 and k lt 18 then begin 
!p = p_temp_x100 & !x = x_temp_x100 & !y = y_temp_x100
oplot, depth_phyc*3.24d-22,temperature_phyc, color=col(k), line=line(k), thick=thick
endif
     
     READCOL, files_fionH(k), depth_H, HI, HII, H2, /silent, format='(D,D,D,D)'
     Hneutre = HI + H2
     
     depth_H = depth_H + r_inner             ;cm

if k lt 6 then begin 
!p = p_H & !x = x_H & !y = y_H
oplot, depth_H*3.24d-22,alog10(Hneutre), color=col(k), line=line(k), thick=thick
endif
if k ge 6 and k lt 12 then begin 
!p = p_H_x10 & !x = x_H_x10 & !y = y_H_x10
oplot, depth_H*3.24d-22,alog10(Hneutre), color=col(k), line=line(k), thick=thick
endif
if k ge 12 and k lt 18 then begin 
!p = p_H_x100 & !x = x_H_x100 & !y = y_H_x100
oplot, depth_H*3.24d-22,alog10(Hneutre), color=col(k), line=line(k), thick=thick
endif


     
     readcol, files_em_em(k), depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
     readcol, files_em_int(k), depth_int, Lya_1216A_int, Lyb_1026A_int, Hb_4861A_int, OIII_5007A_int, Ha_6563A_int, NII_6584A_int, OI_6300A_int, SII_6731A_6716A_int, NeIII_3869A_int, OII_3727A_multiplet_int, NeV_3426A_int, SiVII_2481m_int, Brg_2166m_int, SiVI_1963m_int, AlIX_2040m_int, CaVIII_2321m_int, NeVI_7652m_int, /silent
;depth : cm
;emissivities
     
     depth_em = depth_em + r_inner         ;cm
     depth_int = depth_int + r_inner       ;cm
     
;cols = find_colors(temperature_phyc, 2, 8)
;  device,decompose=0
;  cgloadct, 39, /silent, /reverse
;
;data = fltarr(n_elements(depth_phyc),100)
;
;window, 10, ypos=425
;theta = findgen(100)/100.*2.*!dpi
; cgPlot, max(depth_phyc)*3.24d-22+fltarr(100), theta, /polar, XStyle=5, YStyle=5, $
;       /NoData, Aspect=1.0
;    ; Draw axis through center.
;    cgAxis, /XAxis, 0, 0
;    cgAxis, /YAxis, 0, 0
;for i = 0, 99 do begin
;    
;    ; Plot data.
;  kkmin = 0
;  for kk = 0, n_elements(depth_phyc)-2 do begin
;
;data(kk,i) = temperature_phyc(kk)
;if kk eq n_elements(depth_phyc)-2 then data(kk+1,i) = temperature_phyc(kk+1)
;
;     ;if cols(kk) ne cols(kk+1) or kk eq (size(param))[1]-2 then begin
;     ;   cgplot, [depth_phyc(kkmin)*3.24d-22, depth_phyc(kk+1)*3.24d-22], [theta(i), theta(i)], color=cols(kk), /Overplot, /Polar, PSym=-3
;     ;   kkmin = kk
;     ;endif 
;  endfor
;  device,/decompose  
;
;endfor
;    ; Draw 25 and 75 percent circles.
;    ;dataMax = Max(depth_phyc*3.24d-22)
;    ;percent25 = Circle(0, 0, 0.25*dataMax)
;    ;percent75 = Circle(0, 0, 0.75*dataMax)
;    ;cgPlotS, percent25, Color='red'
;    ;cgPlotS, percent75, Color='red'
;
;plotimage, alog10(data), range=[2,8];,  imgxrange=imgxr, imgyrange=imgyr
;
;cols = find_colors(Hneutre, -10, 0)
;  device,decompose=0
;  cgloadct, 39, /silent, /reverse
;
;data = fltarr(n_elements(depth_H),100)
;
;window, 20, ypos=425
;theta = findgen(100)/100.*2.*!dpi
; cgPlot, max(depth_H)*3.24d-22+fltarr(100), theta, /polar, XStyle=5, YStyle=5, $
;       /NoData, Aspect=1.0
;    ; Draw axis through center.
;    cgAxis, /XAxis, 0, 0
;    cgAxis, /YAxis, 0, 0
;for i = 0, 99 do begin
;    
;    ; Plot data.
;  kkmin = 0
;  for kk = 0, n_elements(depth_H)-2 do begin
;
;data(kk,i) = Hneutre(kk)
;if kk eq n_elements(depth_H)-2 then data(kk+1,i) = Hneutre(kk+1)
;
;     ;if cols(kk) ne cols(kk+1) or kk eq (size(param))[1]-2 then begin
;     ;   cgplot, [depth_H(kkmin)*3.24d-22, depth_H(kk+1)*3.24d-22], [theta(i), theta(i)], color=cols(kk), /Overplot, /Polar, PSym=-3
;     ;   kkmin = kk
;     ;endif 
;  endfor
;  device,/decompose  
;
;endfor
;    ; Draw 25 and 75 percent circles.
;    ;dataMax = Max(depth_phyc*3.24d-22)
;    ;percent25 = Circle(0, 0, 0.25*dataMax)
;    ;percent75 = Circle(0, 0, 0.75*dataMax)
;    ;cgPlotS, percent25, Color='red'
;    ;cgPlotS, percent75, Color='red'
;
;window, 20, ypos=425
;plotimage, alog10(data), range=[-10,0];,  imgxrange=imgxr, imgyrange=imgyr
;
;;stop

  endfor
if ps ne 0 then ps_end, /png
if ps ne 0 then spawn, 'open compare_exponential_profile.png'
stop
end

