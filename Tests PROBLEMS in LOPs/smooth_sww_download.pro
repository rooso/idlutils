pro smooth_sww

files_to_smooth = findfile('/Users/oroos/Post-stage/Test_smooth_sww/*.in', count=nf)

window, 0
plot, [8e-3,50],[1e-6,1e6], /nodata, /xlog, /ylog, /xstyle

for k = 0, nf-1 do begin

readcol, files_to_smooth(k), col_cont, depth, density, format='(A8,D,D)'
t = where(col_cont eq 'continue' and depth gt -35)
col_cont = col_cont[t]
depth = (10d^depth[t])*3.24d-22
density = (10d^density[t])*1./5.

smoothed_density = smooth(density,3)
smoothed_density2 = 10d^(smooth(alog10(density),3))
smoothed_density3 = poly_smooth(density)
;smoothed_density4 = asmooth(density,3) ;does not work : !image ?
;smoothed_density4 = m_smooth(density,3) ;does not work either

wset, 0
oplot, depth, density
oplot, depth, smoothed_density, color=!red
oplot, depth, smoothed_density2, color=!magenta
oplot, depth, smoothed_density3, color=!blue
;oplot, depth, smoothed_density4, color=!green
print, 'pause'
read, go_on
erase
endfor

print, 'Fin ?'
read, go_on

end
