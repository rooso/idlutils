pro hist_negative, ps=ps

restore, 'vals_negative.sav'

;L'erreur apparait quand vals est < -2e-9


if ps eq 0 then window, 0 else ps_start, '/Users/oroos/Post-stage/Test_negative/histo_vals_negative_ion_pop.eps', /encapsulated
plothist, alog10(-vals), title='Negative ion pop. : log(abs(vals))', xtitle='Erreur si vals < -2e-9. Explicable par erreur de prec num ?', /ylog, yr=[1,1e5]
if ps ne 0 then ps_end, /png

print, 'pause'
read, go_on

end
