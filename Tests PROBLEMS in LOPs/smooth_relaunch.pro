pro smooth_relaunch

file_to_smooth = './Before_smoothing/relaunch_x100_density_profile_00150_LOP283dk__d20131021t095312.936.in'

file_smoothed = file_to_smooth
strreplace, file_smoothed, 'Before_smoothing/', ''

window, 0
plot, [7,22],[1e-6,1e6], /nodata, /ylog, /xstyle


readcol, file_to_smooth, col_cont, depth, density, format='(A8,D,D)', /silent
;print, col_cont
t = where(col_cont eq 'continue')
col_cont = col_cont[t]
depth = (10d^depth[t])*3.24d-22
density = (10d^density[t])*1./5.

smoothed_density = smooth(density,3)
smoothed_density2 = 10d^(smooth(alog10(density),3))
smoothed_density3 = poly_smooth(density)

help, density
help, smoothed_density2

nm = FILE_LINES(file_to_smooth)
content_before = strarr(nm)

openr, lun, file_to_smooth, /get_lun
for i = 0, nm-1 do begin
line = ''
readf, lun, line
;print, line
content_before(i) = line
print, content_before(i) 
endfor
close, lun
free_lun, lun

;print, content_before
print, 'is it ok ??'


content_smoothed = content_before

t = where(strmatch(content_before, '*continue*') eq 1, nt)
print, nt
help, density

for j = 0, n_elements(density)-1 do begin
content_smoothed[t(j)] = 'continue'+'	'+strtrim(alog10(depth(j)/3.24d-22),2)+'	'+strtrim(alog10(smoothed_density2(j)/(1./5.)),2)
endfor

help, content_before
help, content_smoothed

openw, lun, file_smoothed, /get_lun
for i = 0, n_elements(content_smoothed)-1 do begin
printf, lun, content_smoothed(i)
endfor
close, lun
free_lun, lun

wset, 0
oplot, depth, density, psym=3
oplot, depth, smoothed_density, color=!red, psym=3
oplot, depth, smoothed_density2, color=!magenta, psym=3
oplot, depth, smoothed_density3, color=!blue, psym=3
oplot, depth, density-smoothed_density+3000, color=!red, psym=3
oplot, depth, density-smoothed_density2+4000, color=!magenta, psym=3
oplot, depth, density-smoothed_density3+5000, color=!blue, psym=3

;oplot, depth, smoothed_density4, color=!green


print, 'Fin ?'
read, go_on

end

