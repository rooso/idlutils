pro relaunch

;ce programme relance les lignes cloudy qui se sont arretees au rayon exterieur alors qu'elles etaient tronquees.
;les lignes concernees sont toutes des x100
;seules les lignes dignes d'interet sont relancees, ie celles dont la densite de colonne restante est > 10^22 H/cc (celles ou il risque d'y avaoir changement d'etat

;dans les lignes tronquees, on a :
; r_outer = max(depth<497 paires) - 0.05 (chevauchement voulu par cloudy)

;pour relancer, on pose :
;r_inner = r_outer_truncated (ci-dessus)
;r_outer = max(depth_all) - 0.05
;depth = [depth_truncated>r_outer_truncated, depth]
;ie on prend en compte les points qui ont ete ignores avant a cause du r_outer < max(depth)

dir='/Users/oroos/Post-stage/Extend_trunc500/Colden_sup22'
files_val = findfile(dir+'/Relaunch/trunc500*.ascii', count=nf)


files_in = files_val
strreplace, files_in, 'trunc500', 'relaunch_x100_density_profile'
strreplace, files_in, '.ascii', '.in'

files = files_val
strreplace, files, dir+'/Relaunch/trunc500', 'transmitted_continuum'
strreplace, files,  '.ascii', '.txt'


filling_factor = 1./5.

out_files = files_in
strreplace, out_files, '.in', ''
strreplace, out_files, './Relaunch/', ''

files_truncated = files_val
strreplace, files_truncated, './Relaunch/trunc500', './Transmitted_Done/x100_density_profile'
strreplace, files_truncated, '.ascii', '.in'
files_phyc_truncated = files_truncated
strreplace, files_phyc_truncated, '.in', '.phyc'

window, 0
plot, [22.3,22.6], [-4,4], /nodata, /xstyle, xtitle='log Depth (cm)', ytitle='log Density (H/cc)'

for k = 0, n_elements(files_in)-1 do begin

openw, fin, files_in(k), /get_lun
pos1 = strpos(files_in(k), 'relaunch')
endd = strlen(files_in(k)) - pos1
printf, fin, '//Cette ligne est un prolongement : '
printf, fin, 'title ', strmid(files_in(k), pos1, endd)

;lire les valeurs a relancer
readcol, files_val(k), depth, den, /silent, format='(D,D)' ;already multiplied by filling factor

;verifier jusqu'ou le calcul s'est fait
readcol, files_truncated(k), col_continue, depth_truncated, den_truncated, /silent, format='(A5,D,D)' ;already multiplied by filling factor
radius_outer_truncated = search_router(files_truncated(k))
radius_inner_truncated = search_rinner(files_truncated(k))
t = where(col_continue eq 'continue')
depth_truncated = depth_truncated[t]
den_truncated = den_truncated[t]

;le nouveau r_inner est l'ancien r_outer
radius_inner = radius_outer_truncated

;le nouveau r_outer est defini comme toujours
radius_outer = max(depth) - 0.05

;NB : le calcul ne s'est pas forcement arrete a r_outer, remettre les points manquants
readcol, files_phyc_truncated(k), depth_phyc_truncated, temp_phyc_truncated, hden_phyc_truncated, /silent, format='(D,D)'
hden_phyc_truncated = hden_phyc_truncated
depth_phyc_truncated = depth_phyc_truncated + 10d^radius_inner_truncated ;cm
max_depth_phyc_trunc = alog10(depth_phyc_truncated(n_elements(depth_phyc_truncated)-1))
t = where(depth_truncated ge max_depth_phyc_trunc, nt)

print,  max_depth_phyc_trunc
print, depth_truncated[t]

if nt gt 0 then begin

depth = [depth_truncated[t], depth]
den = [den_truncated[t], den]

print, nt, ' points remis sur ', n_elements(depth)
endif

last_temp_truncated = alog10(temp_phyc_truncated(n_elements(depth_phyc_truncated)-1))


print, depth
;wset, 0
window, 0
plot, [22.3,22.6], [-4,4], /nodata, /xstyle, xtitle='log Depth (cm)', ytitle='log Density (H/cc)'
oplot, depth_truncated, den_truncated
oplot, alog10(depth_phyc_truncated), alog10(hden_phyc_truncated), color=!green
oplot, depth, den, color=!red

;print, 'radius outer truncated ', radius_outer_truncated
;print, depth_truncated
;print, 'trunc500'
;print, depth
;print, 'r inner/outer : ', radius_inner, radius_outer

;tronquer a nouveau ?
if n_elements(depth) eq 497 then begin
print, 'Oh non ! il faut de nouveau tronquer'
stop
endif

pos1 = strpos(files(k), '__d')
f_k = strmid(files(k),0,pos1)+'.txt'

printf, fin, 'table read "'+f_k+'" //need somthing else ??? need renormalization? total lum ??'
printf, fin, 'luminosity (total) 46.5 //erg/s == luminosity of x100 truncated line bec. transmitted spectrum is in erg/s/cm2'
printf, fin, 'radius inner ', radius_inner, ' outer ', radius_outer
printf, fin, 'CMB'
printf, fin, 'sphere'
printf, fin, 'force temperature ', last_temp_truncated
printf, fin, 'filling factor ', filling_factor
printf, fin, 'dlaw table //density is divided by the filling factor'

printf, fin, 'continue', -35., den(0) ;;premier point exige par cloudy, densite du premier vrai point (en double, du coup)
for i = 0, n_elements(depth)-1 do begin
printf, fin, 'continue', depth(i), den(i)
endfor
printf, fin, 'end of dlaw'
printf, fin, 'abundances ISM no grains no qheat'
printf, fin, 'grains ISM function sublimation'
printf, fin, 'no grain qheat'
printf, fin, 'iterate to convergence'
printf, fin, '//save line labels "line_labels.txt"'
printf, fin, 'save lines, emissivity "'+out_files(k)+'.em_intrinsic" last //default == intrinsinc'
printf, fin, 'TOTL  1216A'
printf, fin, 'H  1  1026A'
printf, fin, 'TOTL  4861A'
printf, fin, 'O  3  5007A'
printf, fin, 'H  1  6563A'
printf, fin, 'N  2  6584A'
printf, fin, 'O  1  6300A'
printf, fin, 'S  2  6720A'
printf, fin, 'Ne 3  3869A'
printf, fin, 'TOTL  3727A'
printf, fin, 'Ne 5  3426A'
printf, fin, 'Si 7 2.481m'
printf, fin, 'H  1 2.166m'
printf, fin, 'Si 6 1.963m'
printf, fin, 'Al 9 2.040m'
printf, fin, 'Ca 8 2.321m'
printf, fin, 'Ne 6 7.652m'
printf, fin, 'end of lines'
printf, fin, 'save lines, emissivity emergent "'+out_files(k)+'.em_emergent" last //default == intrinsinc'
printf, fin, 'TOTL  1216A'
printf, fin, 'H  1  1026A'
printf, fin, 'TOTL  4861A'
printf, fin, 'O  3  5007A'
printf, fin, 'H  1  6563A'
printf, fin, 'N  2  6584A'
printf, fin, 'O  1  6300A'
printf, fin, 'S  2  6720A'
printf, fin, 'Ne 3  3869A'
printf, fin, 'TOTL  3727A'
printf, fin, 'Ne 5  3426A'
printf, fin, 'Si 7 2.481m'
printf, fin, 'H  1 2.166m'
printf, fin, 'Si 6 1.963m'
printf, fin, 'Al 9 2.040m'
printf, fin, 'Ca 8 2.321m'
printf, fin, 'Ne 6 7.652m'
printf, fin, 'end of lines'
printf, fin, 'set nend 4400'
printf, fin, 'save overview "'+out_files(k)+'.ovr" last'
printf, fin, 'save continuum "'+out_files(k)+'.con" units micron last'
printf, fin, 'save lines, array "'+out_files(k)+'.lin" units micron last'
printf, fin, 'save element Hydrogen "'+out_files(k)+'_H.el" last'
printf, fin, 'save element Oxygen "'+out_files(k)+'_O.el" last'
printf, fin, 'save physical conditions "'+out_files(k)+'.phyc" last'
printf, fin, 'print last'
close, fin
free_lun, fin

;print, 'pause'
;read, go_on

endfor

print, 'Fin ?'
read, go_on


end
