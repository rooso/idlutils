pro negative

;problemes : negative ion population in ion_solver
readcol, './negative_ion_population', files, format='(A100)', /silent


for k = 0, n_elements(files)-1 do begin


;PROBLEM negative ion population in ion_solver, nelem=6, N  ion=0 val=XXX Search?F zone=560 iteration=2
readcol, files(k), problem, nelem, reste, format='(A50,A10,A50)', delimiter=',', /silent

;print, problem
t = where(problem eq 'PROBLEM negative ion population in ion_solver')
;print, problem[t], '=problem'
;print, nelem[t], '=nelem'
;print, reste[t], '=reste'

problem=problem[t]
nelem=nelem[t]
reste=reste[t]

;reste = unique(reste,/sort)

pos_val = strpos(reste,'val=')
pos_zone = strpos(reste,'zone=')
pos_itr = strpos(reste, 'iteration=')
val = strmid(reste,pos_val+4,10)
zone = strmid(reste,pos_zone+5,strpos(reste,'iteration')-pos_zone-5)
iteration = strmid(reste,pos_itr+10,2)

;print, val,'/', zone,'/', iteration

val = unique(val,/sort)
zone = unique(zone,/sort)
iteration = unique(iteration,/sort)

if k eq 0 then begin
vals = val
zones = zone
iterations = iteration
endif else begin
vals = [vals,val]
zones = [zones,zone]
iterations = [iterations,iteration]
endelse

print, k+1, n_elements(files)
;print, reste
;print, val, zone, iteration
;print, '---'

;print, 'pause'
;read, go_on

endfor

t = where(strmatch(iterations, '*2*') eq 1)
iterations = iterations[t]
vals = vals[t]
zones = zones[t]

;vals = unique(vals,count_vals, /sort)
;zones = unique(zones, count_zones, /sort)
;iterations = unique(iterations, count_iterations, /sort)

vals = float(strtrim(vals,2))
zones = float(strtrim(zones,2))


print, vals, zones, iterations

histo_vals = histogram(alog10(-vals))

save, vals, file='vals_negative.sav'
help, vals


print, 'pause'
read, go_on

end
