pro check_where_negative

readcol, '/Users/oroos/Post-stage/Test_negative/negative_ion_population', files, format='(A100)', /silent
files_phyc = files
strreplace, files_phyc, '.out', '.phyc'
files_in = files
strreplace, files_in, '.out', '.in'
files_el = files
strreplace, files_el, '.out', '_H.el'

files_ok = find_file('/Users/oroos/Post-stage/LOPs00100/*.in', count=nf_ok)
files_ok_el = files_ok
strreplace, files_ok_el, '.in', '_H.el'

print, nf_ok, n_elements(files)


window, 0
plot, [1e-3,1e2], [1e2,1e8], /nodata, title='Temperature where negative', /xlog, /ylog
p0 = !p & x0 = !x & y0 = !y

window, 1
plot, [1e-3,1e2], [1e-6,1e6], /nodata, title='Density where negative', /xlog, /ylog
p1 = !p & x1 = !x & y1 = !y

window, 2
plot, [1e-3,1e2], [1e-10,1e0], /nodata, title='Hneutre where negative', /xlog, /ylog
p2 = !p & x2 = !x & y2 = !y

window, 3
plot, [1e-3,1e2], [1e-10,1e0], /nodata, title='Hneutre for OK lines', /xlog, /ylog
p3 = !p & x3 = !x & y3 = !y

for k = 0, n_elements(files)-1 do begin

print, k+1, n_elements(files)

radius_inner = search_rinner(files(k))
radius_inner = 10^radius_inner ;en cm


readcol, files_phyc(k), depth_phyc, temp_phyc, hden, format='(D,D,D)', /silent
hden = hden*1./5.
depth_phyc = (depth_phyc + radius_inner)*3.24d-22 ;kpc from BH

readcol, files(k), problem, nelem, reste, format='(A50,A10,A50)', delimiter=',', /silent

;print, problem
t = where(problem eq 'PROBLEM negative ion population in ion_solver')
;print, problem[t], '=problem'
;print, nelem[t], '=nelem'
;print, reste[t], '=reste'

problem=problem[t]
nelem=nelem[t]
reste=reste[t]

;reste = unique(reste,/sort)

pos_val = strpos(reste,'val=')
pos_zone = strpos(reste,'zone=')
pos_itr = strpos(reste, 'iteration=')
val = strmid(reste,pos_val+4,10)
zone = strmid(reste,pos_zone+5,strpos(reste,'iteration')-pos_zone-5)
iteration = strmid(reste,pos_itr+10,2)

;print, val,'/', zone,'/', iteration

val = unique(val,/sort)
zone = unique(zone,/sort)
iteration = unique(iteration,/sort)


wset, 0
!p = p0 & !x = x0 & !y = y0
oplot, depth_phyc, temp_phyc, psym=3
oplot, depth_phyc[zone-1], temp_phyc[zone-1], color=!red, psym=1
p0 = !p & x0 = !x & y0 = !y

wset, 1
!p = p1 & !x = x1 & !y = y1
oplot, depth_phyc, hden, psym=3
oplot, depth_phyc[zone-1], hden[zone-1], color=!red, psym=1
p1 = !p & x1 = !x & y1 = !y

READCOL, files_el(k), depth_H, HI, HII, H2, /silent
depth_H = (depth_H+radius_inner)*3.24d-22
Hneutre = HI + H2

wset, 2
!p = p2 & !x = x2 & !y = y2
oplot, depth_H, Hneutre, psym=3
oplot, depth_H[zone-1], Hneutre[zone-1], color=!red, psym=1
p2 = !p & x2 = !x & y2 = !y


radius_inner_ok = search_rinner(files_ok(k))
radius_inner_ok = 10^radius_inner_ok ;en cm
READCOL, files_ok_el(k), depth_H_ok, HI_ok, HII_ok, H2_ok, /silent
depth_H_ok = (depth_H_ok+radius_inner_ok)*3.24d-22
Hneutre_ok = HI_ok + H2_ok
wset, 3
!p = p3 & !x = x3 & !y = y3
oplot, depth_H_ok, Hneutre_ok, psym=3
p3 = !p & x3 = !x & y3 = !y
endfor

print, 'pause'
read, go_on

end
