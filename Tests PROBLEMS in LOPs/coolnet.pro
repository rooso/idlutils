pro coolnet, factor=factor

if factor eq '' or factor eq '0' then nm = '' else nm = 'x'+factor+'_'

sims = ['00100', '00075w', '00108', '00150', '00170', '00210']
;sims_transcend = [ '00085', '00075n']

nfiles = 0

yes = 0
if yes eq 1 then begin
;tous les fichiers
for j = 0, n_elements(sims)-1 do begin
if j eq 0 then files = find_file('/Users/oroos/Post-stage/LOPs'+sims(j)+'/'+nm+'density*.in', count=nf) $ 
          else files = [files,find_file('/Users/oroos/Post-stage/LOPs'+sims(j)+'/'+nm+'density*.in', count=nf)]
nfiles = nfiles + nf
endfor
print, nfiles

;for j = 0, n_elements(sims_transcend)-1 do begin
;files = [files, find_file('/Volumes/TRANSCEND/LOPs'+sims_transcend(j)+'/'+nm+'density*.in', count=nf2)]
;nfiles = nfiles + nf2
;endfor
;
;print, nfiles

log_depth_all = dblarr(nfiles)
log_density_all = dblarr(nfiles)
log_temp_all_ascii = dblarr(nfiles)
log_depth_all_ascii = dblarr(nfiles)
log_density_all_ascii = dblarr(nfiles)
;
files_ascii = files
strreplace, files_ascii, '.in', '.ascii'
strreplace, files_ascii, 'x100_', ''
strreplace, files_ascii, 'x10_', ''


for k = 0L, nfiles-1 do begin
readcol, files(k), cont_all, depth_all, den_all, format='(A8,D,D)', /silent
t =  where(cont_all eq 'continue')
cont_all = cont_all[t]
depth_all = depth_all[t] ;log cm
den_all = den_all[t]+alog10(1./5.) ;log H/cc

log_depth_all(k) = depth_all(1)
log_density_all(k) = den_all(1)

readcol, files_ascii(k), depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, format='(D,D,D,D,D,D)', /silent
t = where(depth_ascii eq min(depth_ascii))
log_temp_all_ascii(k) = alog10(temperature_ascii[t])
log_density_all_ascii(k) = alog10(density_ascii[t])
log_depth_all_ascii(k) = alog10(depth_ascii[t])

if min(depth_ascii) eq 0 then log_depth_all_ascii(k) = -30.
;print, log_depth_all_ascii(k), depth_ascii[t]
endfor

endif
;save, log_depth_all, log_density_all, log_depth_all_ascii, log_density_all_ascii, log_temp_all_ascii, file='/Users/oroos/Post-stage/CoolNet/'+nm+'coolnet_comp.sav'
restore, '/Users/oroos/Post-stage/CoolNet/'+nm+'coolnet_comp.sav'

nm2 = nm
strreplace, nm2, '_', ''
;fichiers avec probleme CoolNet
if nm eq '' then readcol, 'coolnet_first_two_depths_den', cont, log_depth, log_density, format='(A100,D,D)' else readcol, 'coolnet_first_two_depths_'+nm2, cont, log_depth, log_density, format='(A100,D,D)'

t = where (log_depth ne -35)

cont = cont[t]
log_depth = log_depth[t] ;log cm
log_density = log_density[t]+alog10(1./5.) ;log H/cc

nm2 = nm
strreplace, nm2, '_', ''

if nm eq '' then readcol, 'coolnet_den', files_coolnet_ascii, format='(A100)' else  readcol, 'coolnet_'+nm2, files_coolnet_ascii, format='(A100)'
log_temp_all_coolnet = fltarr(n_elements(files_coolnet_ascii))

for k = 0, n_elements(files_coolnet_ascii)-1 do begin
readcol, files_coolnet_ascii(k), depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, format='(D,D,D,D,D,D)', /silent
t = where(depth_ascii eq min(depth_ascii))
log_temp_all_coolnet(k) = alog10(temperature_ascii[t])
;print, files_coolnet_ascii(k)
;print, log_temp_all_coolnet(k)
endfor

window, 0
plothist, log_depth_all, /ylog, yr=[0.9,1e6], /ystyle
plothist, log_depth, /overplot, color=!red ;where coolnet pbs
plothist, log_depth_all_ascii, /overplot, color=!blue

window, 1
plothist, log_density_all, /ylog, yr=[0.9,1e6], /ystyle
plothist, log_density, /overplot, color=!red
plothist, log_density_all_ascii, /overplot, color=!blue

nm2 = nm
strreplace, nm2, '_', ''

if nm eq '' then readcol, 'coolnet_first_temp_phyc_den', depth_phyc, temp_phyc, format='(D,D)', /silent else  readcol, 'coolnet_first_temp_phyc_'+nm2, depth_phyc, temp_phyc, format='(D,D)', /silent 
wset, 0
plothist, alog10(depth_phyc), /overplot, color=!cyan

window, 2 ;valeur de temperature du point le plus proche du BH
plothist, log_temp_all_ascii, /ylog, yr=[0.9,1e6], xr=[1,10], /ystyle, color=!blue ;;valeur amr pour toutes les LOP (de tous les snapshots) qui ont 'exited OK'
plothist, alog10(temp_phyc), /overplot, color=!cyan ;;valeur cloudy pour les LOP qui ont eu un pb de CoolNet
plothist, log_temp_all_ascii[where(log_depth_all_ascii eq -30)], /overplot, color=!green ;;valeur amr pour les toutes les LOP 'ex OK' dont le 1er point est sur le BH
plothist, log_temp_all_coolnet, /overplot, color=!red ;;valeur amr pour les LOP qui ont eu un pb de CoolNet

;print, 'pause'
;read, go_on

end
