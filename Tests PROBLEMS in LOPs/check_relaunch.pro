pro check_relaunch

files_relaunched = findfile('./Relaunch_Done/*.in', count=nf_relaunched)

files_truncated = files_relaunched
strreplace, files_truncated, 'Relaunch_Done', 'Transmitted_Done'
strreplace, files_truncated, 'relaunch_', ''

files_trunc_phyc = files_truncated
strreplace, files_trunc_phyc, '.in', '.phyc'
files_rel_phyc = files_relaunched
strreplace, files_rel_phyc, '.in', '.phyc'


window, 0
plot, [1e-3,50], [1e2,1e8], /xlog, /ylog, xtitle='Depth (kpc)', ytitle='Temperature (K)', /nodata, /xstyle

for k = 0, nf_relaunched-1 do begin
readcol, files_trunc_phyc(k), depth_trunc, temp_trunc, hden_trunc, /silent
readcol, files_rel_phyc(k), depth_relaunched, temp_relaunched, hden_relaunched, /silent

print, files_trunc_phyc(k)
r_inner_trunc = search_rinner(files_truncated(k))
depth_trunc = (depth_trunc + 10d^r_inner_trunc)*3.24d-22
hden_trunc = hden_trunc*1./5.

print, files_rel_phyc(k)
r_inner_relaunched = search_rinner(files_relaunched(k))
depth_relaunched = (depth_relaunched + 10d^r_inner_relaunched + 10d^r_inner_trunc)*3.24d-22
hden_relaunched = hden_relaunched* 1./5.

;wset, 0
window, 0
plot, [5,50], [1e2,1e10], /xlog, /ylog, xtitle='Depth (kpc)', ytitle='Temperature (K)', /nodata, /xstyle
oplot, depth_trunc, temp_trunc
oplot, depth_relaunched, temp_relaunched, color=!red

window, 2
plot, [5,50], [1e-6,1e6], /xlog, /ylog, xtitle='Depth (kpc)', ytitle='Densite (H/cc)', /nodata, /xstyle
oplot, depth_trunc, hden_trunc
oplot, depth_relaunched, hden_relaunched, color=!red

if strmatch(files_rel_phyc(k), '*00150_LOP283dk__d20131021t095312.936*') eq 1 then begin ;ligne a smoother. a ete deplacee et lissee donc ok.
help, depth_relaunched
help, depth_trunc
stop
endif


;stop

endfor

print, 'Ligne lissee :'
files_relaunched = findfile('./Relaunch_Smoothed_Done/*.in', count=nf_relaunched)
if nf_relaunched ne 1 then stop

files_truncated = files_relaunched
strreplace, files_truncated, 'Relaunch_Smoothed_Done', 'Transmitted_Done'
strreplace, files_truncated, 'relaunch_', ''

files_trunc_phyc = files_truncated
strreplace, files_trunc_phyc, '.in', '.phyc'
files_rel_phyc = files_relaunched
strreplace, files_rel_phyc, '.in', '.phyc'

readcol, files_trunc_phyc, depth_trunc, temp_trunc, hden_trunc, /silent
readcol, files_rel_phyc, depth_relaunched, temp_relaunched, hden_relaunched, /silent

print, files_trunc_phyc
r_inner_trunc = search_rinner(files_truncated)
depth_trunc = (depth_trunc + 10d^r_inner_trunc)*3.24d-22
hden_trunc = hden_trunc*1./5.

print, files_rel_phyc
r_inner_relaunched = search_rinner(files_relaunched)
depth_relaunched = (depth_relaunched + 10d^r_inner_relaunched + 10d^r_inner_trunc)*3.24d-22
hden_relaunched = hden_relaunched* 1./5.

;wset, 0
window, 0
plot, [5,50], [1e2,1e10], /xlog, /ylog, xtitle='Depth (kpc)', ytitle='Temperature (K)', /nodata, /xstyle
oplot, depth_trunc, temp_trunc
oplot, depth_relaunched, temp_relaunched, color=!red

window, 2
plot, [5,50], [1e-6,1e6], /xlog, /ylog, xtitle='Depth (kpc)', ytitle='Densite (H/cc)', /nodata, /xstyle
oplot, depth_trunc, hden_trunc
oplot, depth_relaunched, hden_relaunched, color=!red


end
