pro convfail

;density_profile_00170_LOP177dk__d20131024t083209.282.out: PROBLEM  ConvFail 1, Temp not converged itr 3 zone 915 fnzone 918.99 Te=5.0000e+03 Htot=2.645e-21 Ctot=2.659e-21 rel err=-5.289e-03 rel tol:5.000e-03

openr, out, 'convfail_den_with_zone', /get_lun
lines = file_lines('./convfail_den_with_zone')

print, lines

file_out = strarr(lines)
zone = fltarr(lines)

for k = 0, lines-1 do begin
line1 = ''
readf, out, line1
pos = strpos(line1,': PROBLEM')
file_out(k) = strmid(line1,0,pos)
pos1 = strpos(line1,'zone')
pos2 = strpos(line1,'fnzone')
;print, strmid(line1,pos1+4, pos2-pos1-4)
zone(k) = float(strmid(line1,pos1+4, pos2-pos1-4))
;print, line1
;print, file_out(k)
;print, zone(k)

endfor
close, out
free_lun, out

readcol, './convfail_den_on_mac', file_in, format='(A100)'
;print, file_in

file_phyc = file_in
strreplace, file_phyc, '.in', '.phyc'
file_el = file_in
strreplace, file_el, '.in', '_H.el'
file_ascii = file_in
strreplace, file_ascii, '.in', '.ascii'


for k = 0, n_elements(file_in)-1 do begin

print, file_in(k)
print, file_out(k)

radius_inner = search_rinner(file_in(k))
readcol, file_phyc(k), depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22
hden = hden*1.d/5.
READCOL, file_el(k), depth_H, HI, HII, H2, /silent
depth_H = (depth_H+10d^radius_inner)*3.24d-22
Hneutre = HI + H2

readcol, file_ascii(k), depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, format='(D,D,D,D,D,D,D,D,D,D,D,D,D)', /silent

  t_sort = sort(depth_ascii)
  depth_ascii = depth_ascii[t_sort] ;ici pas de correction car donnees pre-cloudy
  density_ascii = density_ascii[t_sort]
  temperature_ascii = temperature_ascii[t_sort]

window, 0
plot, depth, hden, /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Density (H/cc)'
plots, depth[zone(k)-1], hden[zone(k)-1], color=!red, psym=1, syms=4
oplot, depth_ascii, density_ascii, color=!green

window, 1
plot, depth, temp, /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Temperature (K)', yr=[1e3,1e8]
plots, depth[zone(k)-1], temp[zone(k)-1], color=!red, psym=1, syms=4
oplot, depth_ascii, temperature_ascii, color=!green

window, 2
plot, depth_H, Hneutre,  /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Fraction of Hneutral=HI+H2', yr=[1e-10,5], /ystyle
plots, depth_H[zone(k)-1], Hneutre[zone(k)-1], color=!red, psym=1, syms=4

print, 'pause'
read, go_on

endfor

print, 'Fin ?'
read, go_on

end


