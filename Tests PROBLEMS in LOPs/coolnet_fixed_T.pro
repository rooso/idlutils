function read, file_phyc

file_in = file_phyc
strreplace, file_in, '.phyc', '.in'
radius_inner = search_rinner(file_in)

readcol, file_phyc, depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22 ;kpc
hden = hden*1.d/5. ;H/cc corrected for filling factor

;help, depth
;help, hden

return, [[depth],[temp]]

end


pro coolnet_fixed_t
  
;this program shows the impact of setting the first temperature to the
;AMR one on the 'coolnet derivative' problem
; 1. no difference on T(depth)
; 2. the 'CoolNet Derivative problem' messages disappear (and do not
; appear where it was not there -> Not_coolnet_fixed_t)

location = '/Users/oroos/Post-stage/CoolNet/Test_coolnet_fixed_temp/'

coolnet_default = findfile(location+'Coolnet/*.phyc', count=nf_cdef)
coolnet_fixed_t = findfile(location+'Coolnet_fixed_T/*.phyc', count=nf_cfix)

no_coolnet_default = findfile(location+'Not_coolnet/*.phyc', count=nf_nocdef)
no_coolnet_fixed_t = findfile(location+'Not_coolnet_fixed_T/*.phyc', count=nf_nocfix)

print, nf_cdef, nf_cfix, nf_nocdef, nf_nocfix

window, 0
;plot, [1e-3,50], [1e2,1e8], /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', /xlog, /ylog
plot, [1e-3,0.02], [1e6,1e8], /nodata, xtitle='Depth (kpc)', ytitle='Temperature (K)', /xlog, /ylog

for k = 0, nf_cdef-1 do begin

depth_temp_cdef = read(coolnet_default(k))
depth_temp_cfix = read(coolnet_fixed_t(k))
depth_temp_nocdef = read(no_coolnet_default(k))
depth_temp_nocfix = read(no_coolnet_fixed_t(k))
;help, depth_temp


oplot, depth_temp_cdef(*,0), depth_temp_cdef(*,1), color=!red, line=2
oplot, depth_temp_cfix(*,0), depth_temp_cfix(*,1), color=!red, line=0
oplot, depth_temp_nocdef(*,0), depth_temp_nocdef(*,1), color=!green, line=2
oplot, depth_temp_nocfix(*,0), depth_temp_nocfix(*,1), color=!green, line=0


endfor

stop
end
