pro compare_smooth

file_ok = findfile('./Not_smoothed/*.phyc', count=nf_ok)
file_ok_smoothed = findfile('./Smoothed/*.phyc', count=nf_ok_smoothed)

if nf_ok ne nf_ok_smoothed then stop

file_ok_in = file_ok
strreplace, file_ok_in, '.phyc', '.in'
file_ok_in_smoothed = file_ok_smoothed
strreplace, file_ok_in_smoothed, '.phyc', '.in'
file_ascii = file_ok
strreplace, file_ascii, 'x10_', ''
strreplace, file_ascii, '.phyc', '.ascii'
strreplace, file_ascii, '/Not_smoothed', ''
file_ok_el = file_ok
strreplace, file_ok_el, '.phyc', '_H.el'
file_ok_el_smoothed = file_ok_smoothed
strreplace, file_ok_el_smoothed , '.phyc', '_H.el'


for k = 0, nf_ok-1 do begin
radius_inner = search_rinner(file_ok_in(k))
radius_inner_smoothed = search_rinner(file_ok_in_smoothed(k))

if radius_inner ne radius_inner_smoothed then stop

readcol, file_ok(k), depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22
hden = hden*1.d/5.
readcol, file_ok_smoothed(k), depth_smoothed, temp_smoothed, hden_smoothed, /silent, format='(D,D,D)'
depth_smoothed = (depth_smoothed+10d^radius_inner_smoothed)*3.24d-22
hden_smoothed = hden_smoothed*1.d/5.

readcol, file_ascii(k), depth_ascii, density_ascii, x_ascii, y_ascii, z_ascii, temperature_ascii, vx_ascii, vy_ascii, vz_ascii, cell_size_ascii, x_lop_ascii, y_lop_ascii, z_lop_ascii, format='(D,D,D,D,D,D,D,D,D,D,D,D,D)', /silent

  t_sort = sort(depth_ascii)
  depth_ascii = depth_ascii[t_sort] ;ici pas de correction car donnees pre-cloudy
  density_ascii = density_ascii[t_sort]
  temperature_ascii = temperature_ascii[t_sort]

window, 0
plot, depth_smoothed, hden_smoothed, color=!red, /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Density (H/cc)'
oplot, depth, hden
oplot, depth_ascii, density_ascii, color=!green

window, 1
plot, depth_smoothed, temp_smoothed, color=!red, /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Temperature (K)', yr=[1e3,1e8]
oplot, depth, temp
oplot, depth_ascii, temperature_ascii, color=!green

;print, file_ok_el(k)

READCOL, file_ok_el(k), depth_H, HI, HII, H2, /silent
depth_H = (depth_H+10d^radius_inner)*3.24d-22
Hneutre = HI + H2
READCOL, file_ok_el_smoothed(k), depth_H_smoothed, HI_smoothed, HII_smoothed, H2_smoothed, /silent
depth_H_smoothed = (depth_H_smoothed+10d^radius_inner_smoothed)*3.24d-22
Hneutre_smoothed = HI_smoothed + H2_smoothed

window, 2
print, minmax(Hneutre)
print, 'smooth'
print, minmax(Hneutre_smoothed)
plot, depth_H_smoothed, Hneutre_smoothed, color=!red, /xlog, /ylog, xr=[0.001,50], xtitle='Depth (kpc)', ytitle='Fraction of Hneutral=HI+H2'
oplot, depth_H, Hneutre

print, 'pause'
read, go_on

endfor

print, 'Fin ?'
read, go_on

end
