function trace, file, sim, part, theta, phi, color,  p_plot,  x_plot,  y_plot, ps

defplotcolors

if sim eq '00100' or sim eq '00075w' $
or sim eq '00108' or sim eq '00150' or sim eq '00170' or sim eq '00210' then name = 'With'
if sim eq '00085' or sim eq '00075n' $
or sim eq '00090' or sim eq '00120' or sim eq '00130' or sim eq '00148' then name = 'No'

sm = sim

if sim eq '00075w' or sim eq '00075n' then sm='00075'

readcol, file, depth, /silent
readcol, "/Users/oroos/Post-stage/orianne_data/"+name+"_AGN_feedback/output_"+sm+"/sink_"+sm+".out", rien, rien2, x_c, y_c, z_c, /silent

depth_max = max(depth)
x = depth_max*cos(phi)*sin(theta) + x_c
y = depth_max*sin(phi)*sin(theta) + y_c
z = depth_max*cos(theta) + z_c

device, decompose=1
defplotcolors
;print, x;, y, z
x = [x_c, x]
y = [y_c, y]
z = [z_c, z]
device, decompose=1
defplotcolors
if ps eq 0 then wset, 0
!p = p_plot & !x = x_plot & !y = y_plot
plots, X, Y, Z, /T3D, PSYM=-4, COLOR=color, thick=2 ;color=color+long(100*phi);


end

pro plot_LOPs_sww, sim=sim, part=part, ps=ps


;dans 90 % des LOP Sww (y compris PROBLEM DISASTER), il suffit de
;lisser le profil (voir tests smooth) --> exited OK

;dans les 10 % restants --> toujours Sww (appartenant uniquement au snapshot 00210 ie
;le plus dense), test en smoothant 2x

device, decompose=1
defplotcolors


print, 'On recherche les fichiers :'
print, '/Users/oroos/Post-stage/Tabden-Sww/Sww/density_profile_'+sim+'_LOP*.phyc'

colors = fltarr(1)

 part2 = ['xz', 'zy', 'cx', 'cy', 'px', 'py', 'up', 'dn', 'cu', 'cd', 'dk']

           files_phyc = findfile('/Users/oroos/Post-stage/Tabden-Sww/Sww/density_profile_'+sim+'_LOP*'+part2(0)+'*.phyc',count=n_files)
           if n_files gt 0 then colors=fltarr(n_files)+!red

nf = n_files
     for k = 1, n_elements(part2)-1 do begin

           files_phyc = [files_phyc,findfile('/Users/oroos/Post-stage/Tabden-Sww/Sww/density_profile_'+sim+'_LOP*'+part2(k)+'*.phyc',count=n_files)]

if n_files gt 0 then begin
           if (k eq 1 or k eq 6 or k eq 7) then colors=[colors,fltarr(n_files)+!red]
           if (k eq 2 or k eq 3 or k eq 8 or k eq 9) then colors=[colors,fltarr(n_files)+!blue]
           if (k eq 4 or k eq 5 or k eq 10) then colors=[colors,fltarr(n_files)+!green]
endif
        nf = nf + n_files
    endfor


files_phyc = files_phyc[where(files_phyc ne '')]
help, files_phyc


files = files_phyc
strreplace, files, '.phyc', '.ascii'
strreplace, files, 'x100', ''
strreplace, files, 'x10', ''
files_density = files_phyc
strreplace, files_density, '.phyc', '.in'


print, 'En tout, il y en a ', nf, '.'
;print, files



theta=dblarr(nf)
phi=dblarr(nf)

if ps eq 0 then window, 0
if ps eq 1 then ps_start, '/Users/oroos/Post-stage/plot_lops_sww'+sim+'_'+part+'.eps', /encapsulated, /color, /cm, xsize=20, ysize=20

device, decompose=1
defplotcolors

Plot_3dbox, [0,50], [0,50], [0,50], psym=3, /nodata,   $
                  GRIDSTYLE=1, $ ;/SOLID_WALLS
                  YZSTYLE=5, AZ=40, TITLE="Distribution of sww_LOPs for sim "+sim+part,      $
                  Xtitle="X Coordinate", Ytitle="Y Coordinate", Charsize=1.9,      $
                  Ztitle="Z Coordinate";,     $
                  ;/YSTYLE, ZRANGE=[-25,25], XRANGE=[-25,25]

p_plot = !p & x_plot = !x & y_plot = !y 

if ps eq 0 then begin
window, 1
plot, [-3,1.5], [-6,6], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Density (H/cc)', title='Sww'
p_den = !p & x_den = !x & y_den = !y

window, 2
plot, [-3,1.5], [15,30], psym=-3, /nodata, xtitle='Depth (kpc)', ytitle='Column density (H/cm2)', title='Sww', /ystyle
p_colden = !p & x_colden = !x & y_colden = !y
endif


for k = 0, nf-1 do begin
   openr, f, files(k), /get_lun
   print, files(k)
   line1 = ''
   readf, f, line1
   ;print, line1
   theta(k) = double(strmid(line1,strpos(line1,':')+1,25))
   ;print, 'theta_strmid = ', strmid(line1,strpos(line1,':')+1,25)
   print, 'theta = ', theta(k)
   phi(k) = double(strmid(line1,strpos(line1,':')+26,25))
   ;print, 'phi_strmid = ', strmid(line1,strpos(line1,':')+26,25)
   print, 'phi = ', phi(k)
   func = trace(files(k), sim, part, theta(k), phi(k), colors(k), p_plot, x_plot, y_plot, ps)
   close, f
   free_lun, f
 ;regler le probleme des UP qui sont contamines avec des DOWN

   readcol, files_density(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.) ;log H/cc*fill_fact

   colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2

if ps eq 0 then begin
   wset, 1
   !p = p_den & !x = x_den & !y = y_den
   oplot, depth_den, density, color=colors(k)

   wset, 2
   !p = p_colden & !x = x_colden & !y = y_colden
   oplot, depth_den, colden, color=colors(k)
endif

   ;print, 'pause'
   ;read, go_on

endfor
for k = 0, nf-1 do begin
   
if strmatch(files_density(k), '*x1*') eq 1 then nm = 'x' else nm = ''

openr, fin, files_density(k), /get_lun
line1=''
readf, fin, line1
line2=''
readf, fin, line2
line3=''
readf, fin, line3
line_radius = ''
if nm ne '' then begin
line4=''
readf, fin, line4
endif
readf, fin, line_radius
free_lun, fin
;print, line_radius
inner = strpos(line_radius, 'inner')
outer = strpos(line_radius, 'outer')
len = outer - inner
radius_inner = float(strmid(line_radius,inner+5,len-5))
;print, 'rayon', radius_inner

if radius_inner eq 0 then begin
   print, 'rayon nul !'
   stop
endif

radius_inner = 3.24d-22*10^radius_inner ;conversion en kpc
;print, radius_inner
readcol, files_phyc(k), depth_phyc, temperature_electrons, hden, eden, heating, rad_acc, fill_fact, /silent, format='(D,D,D,D,D,D)'
;units :             cm,            K,                 cm-3,
;cm-3,erg/cm3/s, cm/s2, number
depth_phyc = depth_phyc*3.24d-22 + radius_inner ;kpc
hden = hden*fill_fact
t_max = where(depth_phyc eq max(depth_phyc))

length_phyc = depth_phyc*0
colden_phyc = hden*0

length_phyc(0) = depth_phyc/3.24d-22 ;cm
colden_phyc(0) = hden(0)*length_phyc(0) ;H/cm2

for j = 1, n_elements(depth_phyc)-1 do begin
length_phyc(j) = (depth_phyc(j) - depth_phyc(j-1))/3.24d-22 ;cm
colden_phyc(j) = colden_phyc(j-1) + hden(j)*length_phyc(j) ;H/cm2
endfor

   readcol, files_density(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
   ;units : none, log cm, log H/cc
   cmp = strcmp(col_continue, 'continue')
   t = where( cmp eq 1 and depth_den gt -35, nt)
   col_continue = col_continue[t]
   depth_den = depth_den[t]+alog10(3.24e-22) ;log kpc
   density = density[t]+alog10(1./5.)

   colden = density*0
   length = depth_den*0

length(0) = 10d^(depth_den(0))/3.24d-22 ;cm
colden(0) = 10d^(density(0))*length(0) ;H/cm2

for j = 1, n_elements(depth_den)-1 do begin
   length(j) = (10d^(depth_den(j)) - 10d^(depth_den(j-1)))/3.24d-22 ;cm
   colden(j) = colden(j-1) + 10d^density(j)*length(j) ;H/cm2
endfor

colden = alog10(colden) ;log H/cm2

t500 = where(depth_den eq max(depth_den))
  
if ps eq 0 then begin
   wset, 1
   !p = p_den & !x = x_den & !y = y_den
   ;oplot, depth_den, density, color=colors(k)
 plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!black, psym=1, thick=4
 ;oplot, alog10(depth_phyc), alog10(hden), color=colors(k), psym=1
if n_elements(depth_den) ge 497 then begin
plots, depth_den[t500], density[t500], color=!orange, psym=2 
plots, alog10(depth_phyc[t_max]), alog10(hden[t_max]), color=!dred, psym=1
endif else begin
print, 'Non tronquee'
endelse

wset, 2
!p = p_colden & !x = x_colden & !y = y_colden
   ;oplot, depth_den, colden, color=colors(k)
 plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!black, psym=1, thick=4
 ;oplot, alog10(depth_phyc), alog10(colden_phyc), color=colors(k), psym=1
if n_elements(depth_den) ge 497 then begin
plots, depth_den[t500], colden[t500], color=!orange, psym=2 
plots, alog10(depth_phyc[t_max]), alog10(colden_phyc[t_max]), color=!dred, psym=1
endif

endif

   ;print, 'pause'
   ;read, go_on

endfor

;print, 'Min/max de theta : ', minmax(theta)
;print, 'Min/max de phi : ', minmax(phi)

;plots,  [25,25], [25,50], [25,25], psym=-4
if ps eq 1 then ps_end, /png

print, 'Fin du programme, tout est OK.'

end
