pro smooth_still_sww


files_to_smooth = findfile('/Users/oroos/Post-stage/Test_smooth_sww/Smoothed/Still_sww/*.in', count=nf)
print, nf

initial_files = files_to_smooth
strreplace, initial_files, 'Smoothed/Still_sww/', ''

files_smoothed = files_to_smooth
strreplace, files_smoothed, 'Still_sww', 'Still_sww/Smoothed'

window, 0
plot, [8e-3,50],[1e-6,1e6], /nodata, /xlog, /ylog, /xstyle

for k = 0, nf-1 do begin

;print, files_to_smooth(k)
readcol, files_to_smooth(k), col_cont, depth, density, format='(A8,D,D)', /silent
;print, col_cont
t = where(col_cont eq 'continue')
col_cont = col_cont[t]
depth = (10d^depth[t])*3.24d-22
density = (10d^density[t])*1./5.

readcol, initial_files(k), col_cont_init, depth_init, density_init, format='(A8,D,D)', /silent
;print, col_cont_init
t = where(col_cont_init eq 'continue')
col_cont_init = col_cont_init[t]
depth_init = (10d^depth_init[t])*3.24d-22
density_init = (10d^density_init[t])*1./5.


smoothed_density = smooth(density,3)
smoothed_density2 = 10d^(smooth(alog10(density),3))
smoothed_density3 = poly_smooth(density)
;smoothed_density4 = asmooth(density,3) ;does not work : !image ?
;smoothed_density4 = m_smooth(density,3) ;does not work either

help, density
help, smoothed_density2

nm = (FILE_LINES(files_to_smooth(k)))
content_before = strarr(nm)

print, files_to_smooth(k)
openr, lun, files_to_smooth(k), /get_lun
for i = 0, nm-1 do begin
line = ''
readf, lun, line
;print, line
content_before(i) = line
print, content_before(i) 
endfor
close, lun
free_lun, lun

;print, content_before
print, 'is it ok ??'


content_smoothed = content_before

t = where(strmatch(content_before, '*continue*') eq 1, nt)
print, nt
help, density

for j = 0, n_elements(density)-1 do begin
content_smoothed[t(j)] = 'continue'+'	'+strtrim(alog10(depth(j)/3.24d-22),2)+'	'+strtrim(alog10(smoothed_density2(j)/(1./5.)),2)
endfor

help, content_before
help, content_smoothed

openw, lun, files_smoothed(k), /get_lun
for i = 0, n_elements(content_smoothed)-1 do begin
printf, lun, content_smoothed(i)
endfor
close, lun
free_lun, lun

wset, 0
oplot, depth, density
oplot, depth, smoothed_density, color=!red
oplot, depth, smoothed_density2, color=!magenta
oplot, depth, smoothed_density3, color=!blue
;oplot, depth, smoothed_density4, color=!green
oplot, depth_init, density_init, color=!green
print, 'pause'
read, go_on
erase
endfor

print, 'Fin ?'
read, go_on

end
