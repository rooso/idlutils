pro check_smooth_on_ok_line

files_to_smooth = findfile('/Users/oroos/Post-stage/Test_smooth_sww/Test_smooth_with_OK_line/*.in', count=nf)
print, nf

files_smoothed = files_to_smooth
strreplace, files_smoothed, 'line', 'line/Smoothed'

window, 0
plot, [8e-3,50],[1e-6,1e6], /nodata, /xlog, /ylog, /xstyle

for k = 0, nf-1 do begin

;print, files_to_smooth(k)
readcol, files_to_smooth(k), col_cont, depth, density, format='(A8,D,D)', /silent
;print, col_cont
t = where(col_cont eq 'continue')
col_cont = col_cont[t]
depth = (10d^depth[t])*3.24d-22
density = (10d^density[t])*1./5.

smoothed_density = smooth(density,3)
smoothed_density2 = 10d^(smooth(alog10(density),3))
smoothed_density3 = poly_smooth(density)
;smoothed_density4 = asmooth(density,3) ;does not work : !image ?
;smoothed_density4 = m_smooth(density,3) ;does not work either

help, density
help, smoothed_density2

nm = (FILE_LINES(files_to_smooth(k)))
content_before = strarr(nm)

print, files_to_smooth(k)
openr, lun, files_to_smooth(k), /get_lun
for i = 0, nm-1 do begin
line = ''
readf, lun, line
;print, line
content_before(i) = line
print, content_before(i) 
endfor
close, lun
free_lun, lun

;print, content_before
print, 'is it ok ??'


content_smoothed = content_before

t = where(strmatch(content_before, '*continue*') eq 1, nt)
print, nt
help, density

for j = 0, n_elements(density)-1 do begin
content_smoothed[t(j)] = 'continue'+'	'+strtrim(alog10(depth(j)/3.24d-22),2)+'	'+strtrim(alog10(smoothed_density2(j)/(1./5.)),2)
endfor

help, content_before
help, content_smoothed

openw, lun, files_smoothed(k), /get_lun
for i = 0, n_elements(content_smoothed)-1 do begin
printf, lun, content_smoothed(i)
endfor
close, lun
free_lun, lun

wset, 0
oplot, depth, density
oplot, depth, smoothed_density, color=!red
oplot, depth, smoothed_density2, color=!magenta
oplot, depth, smoothed_density3, color=!blue
;oplot, depth, smoothed_density4, color=!green
print, 'pause'
read, go_on
erase
endfor

print, 'Fin ?'
read, go_on

end
