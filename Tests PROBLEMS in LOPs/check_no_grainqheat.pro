pro check_no_grainqheat

;ce programme compare des LOPs avec (reference) et sans le quantum
;heating afin de voir s'il est correct de le negliger

;Commentaires :
;desactiver le qheat a regle le probleme de la LOP dense_tabden
;(pourquoi?)

;desactiver le qheat a regle le probleme de la LOP something went wrong
;MAIS : peut-etre juste parce que l'echantillonnage est
;different et que la partie ou il y a une oscillation de
;l'ionisation a ete evitee
;-> peut-etre un hasard

;GLOBALEMENT : il n'y a aucune difference entre les deux
;calculs pour T, Hionfrac, n et Halpha.

;pour les spectres, il y q des differences dans tous les cas
;ATTENTION


;files 'exited OK', no qheat convergence problems / qheat disabled
file_qheat_ok_in = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467_qheatok.in'
file_qheat_ok_phyc = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467.phyc'
file_qheat_ok_H = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467_H.el'
file_qheat_ok_cont = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467.con'
file_qheat_ok_em_em =  '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467.em_emergent'
file_qheat_ok_em_int =  '/Users/oroos/Post-stage/Test_noqheat/density_profile_00210_LOP110cu__d20131021t094639.467.em_intrinsic'
;comparison with active qheat
file_qheat_ok_comp =  '/Users/oroos/Post-stage/LOPs00210/density_profile_00210_LOP110cu__d20131021t094639.467.phyc'
file_qheat_ok_H_comp =  '/Users/oroos/Post-stage/LOPs00210/density_profile_00210_LOP110cu__d20131021t094639.467_H.el'
file_qheat_ok_cont_comp =  '/Users/oroos/Post-stage/LOPs00210/density_profile_00210_LOP110cu__d20131021t094639.467.con'
file_qheat_ok_em_em_comp =  '/Users/oroos/Post-stage/LOPs00210/density_profile_00210_LOP110cu__d20131021t094639.467.em_emergent'
file_qheat_ok_em_int_comp =  '/Users/oroos/Post-stage/LOPs00210/density_profile_00210_LOP110cu__d20131021t094639.467.em_intrinsic'


;files 'exited OK', qheat convergence problems  / qheat disabled
file_qheat_pb_in = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516_qheatdidnotcv.in'
file_qheat_pb_phyc = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516.phyc'
file_qheat_pb_H = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516_H.el'
file_qheat_pb_cont = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516.con'
file_qheat_pb_em_em = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516.em_emergent'
file_qheat_pb_em_int = '/Users/oroos/Post-stage/Test_noqheat/density_profile_00075w_LOP107up__d20130704t155701.516.em_intrinsic'
;comparison with active qheat
file_qheat_pb_comp = '/Volumes/TRANSCEND/LOPs00075w/density_profile_00075w_LOP107up__d20130704t155701.516.phyc'
file_qheat_pb_H_comp = '/Volumes/TRANSCEND/LOPs00075w/density_profile_00075w_LOP107up__d20130704t155701.516_H.el'
file_qheat_pb_cont_comp = '/Volumes/TRANSCEND/LOPs00075w/density_profile_00075w_LOP107up__d20130704t155701.516.con'
file_qheat_pb_em_em_comp = '/Volumes/TRANSCEND/LOPs00075w/density_profile_00075w_LOP107up__d20130704t155701.516.em_emergent'
file_qheat_pb_em_int_comp = '/Volumes/TRANSCEND/LOPs00075w/density_profile_00075w_LOP107up__d20130704t155701.516.em_intrinsic'


;files 'something went wrong'  / qheat disabled
file_sww_in =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471_sww.in'
file_sww_phyc =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471.phyc'
file_sww_H =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471_H.el'
file_sww_cont =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471.con'
file_sww_em_em =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471.em_emergent'
file_sww_em_int =  '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00210_LOP117cd__d20131021t094639.471.em_intrinsic'
;comparison with active qheat
file_sww_comp = '/Users/oroos/Post-stage/LOPs00210/Sww/x100_density_profile_00210_LOP117cd__d20131021t094639.471.phyc'
file_sww_H_comp = '/Users/oroos/Post-stage/LOPs00210/Sww/x100_density_profile_00210_LOP117cd__d20131021t094639.471_H.el'
file_sww_cont_comp = '/Users/oroos/Post-stage/LOPs00210/Sww/x100_density_profile_00210_LOP117cd__d20131021t094639.471.con'
file_sww_em_em_comp =  '/Users/oroos/Post-stage/LOPs00210/Sww/x100_density_profile_00210_LOP117cd__d20131021t094639.471.em_emergent'
file_sww_em_int_comp =  '/Users/oroos/Post-stage/LOPs00210/Sww/x100_density_profile_00210_LOP117cd__d20131021t094639.471.em_intrinsic'
;  Calculation stopped because outer radius reached. Iteration 2 of 2
;   The geometry is spherical.
; W-An ionization oscillation occurred at zone 984, elem Li 4, by 314% from 1.89e-02 to 4.62e-03 to 1.93e-02
; W-An ionization oscillation occurred at zone 984, elem Mg 6, by 440% from 8.55e-03 to 1.66e-03 to 1.13e-02
; W-An ionization oscillation occurred at zone 984, elem Si 7, by 474% from 1.81e-02 to 3.30e-03 to 2.61e-02
; W-An ionization oscillation occurred at zone 984, elem P  7, by 307% from 2.84e-02 to 6.63e-03 to 2.72e-02


;files 'problem in dense_tabden'  / qheat disabled
file_tabden_in = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212_tabden.in'
file_tabden_phyc = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212.phyc'
file_tabden_H = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212_H.el'
file_tabden_cont = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212.con'
file_tabden_em_em = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212.em_emergent'
file_tabden_em_int = '/Users/oroos/Post-stage/Test_noqheat/x100_density_profile_00170_LOP7dk__d20131024t083209.212.em_intrinsic'
;comparison with active qheat
file_tabden_comp =  '/Users/oroos/Post-stage/LOPs00170/Tabden/x100_density_profile_00170_LOP7dk__d20131024t083209.212.phyc'
file_tabden_H_comp =  '/Users/oroos/Post-stage/LOPs00170/Tabden/x100_density_profile_00170_LOP7dk__d20131024t083209.212_H.el'
file_tabden_cont_comp =  '/Users/oroos/Post-stage/LOPs00170/Tabden/x100_density_profile_00170_LOP7dk__d20131024t083209.212.con'
file_tabden_em_em_comp = '/Users/oroos/Post-stage/LOPs00170/Tabden/x100_density_profile_00170_LOP7dk__d20131024t083209.212.em_emergent'
file_tabden_em_int_comp = '/Users/oroos/Post-stage/LOPs00170/Tabden/x100_density_profile_00170_LOP7dk__d20131024t083209.212.em_intrinsic'


;-----------------------------------------------------------------------------------------------------------------------
files_in = [file_qheat_ok_in, file_qheat_pb_in, file_sww_in, file_tabden_in] ;qheat desactive
files_phyc = [file_qheat_ok_phyc, file_qheat_pb_phyc, file_sww_phyc, file_tabden_phyc] ;resultats avec qheat desactive
files_fionH = [file_qheat_ok_H, file_qheat_pb_H, file_sww_H, file_tabden_H] 
files_continua = [file_qheat_ok_cont, file_qheat_pb_cont, file_sww_cont, file_tabden_cont]
files_emergent = [file_qheat_ok_em_em, file_qheat_pb_em_em, file_sww_em_em, file_tabden_em_em]
files_intrinsic = [file_qheat_ok_em_int, file_qheat_pb_em_int, file_sww_em_int, file_tabden_em_int]
colors = [!red, !orange, !magenta, !purple]
files_comp = [file_qheat_ok_comp, file_qheat_pb_comp, file_sww_comp, file_tabden_comp] ;comparaison qheat active
files_fionH_comp = [file_qheat_ok_H_comp, file_qheat_pb_H_comp, file_sww_H_comp, file_tabden_H_comp] 
files_continua_comp = [file_qheat_ok_cont_comp, file_qheat_pb_cont_comp, file_sww_cont_comp, file_tabden_cont_comp]
files_emergent_comp = [file_qheat_ok_em_em_comp, file_qheat_pb_em_em_comp, file_sww_em_em_comp, file_tabden_em_em_comp]
files_intrinsic_comp = [file_qheat_ok_em_int_comp, file_qheat_pb_em_int_comp, file_sww_em_int_comp, file_tabden_em_int_comp]


window, 1
;plot, [1e-3,1e2], [1e-7,1e6], psym=3, /ylog, /xlog, title='density (H/cc) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
;zoom
plot, [10,20], [1e-6,1e-4], psym=3, /ylog, /xlog, title='density (H/cc) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
p_den = !p & x_den = !x & y_den = !y 

window, 2
plot, [1e-3,1e2], [1e2,1e10], psym=3, /ylog, /xlog, title='temperature (K) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
;zoom
;plot, [10,20], [1e2,1e10], psym=3, /ylog, /xlog, title='temperature (K) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
p_temp = !p & x_temp = !x & y_temp = !y 

window, 3
plot, [1e-3,1e2], [1e-10,2], psym=3, /ylog, /xlog, title='neutral fraction of H ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
;zoom
;plot, [10,20], [1e-10,2], psym=3, /ylog, /xlog, title='neutral fraction of H ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
p_H = !p & x_H = !x & y_H = !y 

window, 4
;plot, [1e-9,1e8], [1e-6,1e1], psym=3, /ylog, /xlog, title='transmitted_noqheat/transmitted_withqheat ', /nodata, xtitle='Wavelength (um)', /ystyle, /xstyle
;zoom
plot, [0.05,0.85], [0.4,1.5], psym=3, title='transmitted_noqheat/transmitted_withqheat ', /nodata, xtitle='Wavelength (um)', /ystyle, /xstyle
; Lya_1216A_comp, Lyb_1026A_comp, Hb_4861A_comp, OIII_5007A_comp, Ha_6563A_comp, NII_6584A_comp, OI_6300A_comp, SII_6731A_6716A_comp, NeIII_3869A_comp, OII_3727A_multiplet_comp, NeV_3426A_comp, SiVII_2481m_comp, Brg_2166m_comp, SiVI_1963m_comp, AlIX_2040m_comp, CaVIII_2321m_comp, NeVI_7652m_comp
;overplot emission line location
oplot, [0.1216,0.1216], [1.05,1.8] ;Lya
oplot, [0.4861,0.4861], [1.05,1.8] ;Hb
oplot, [0.1026,0.1026], [1.25,1.8] ;Lyb
oplot, [0.5007,0.5007], [1.05,1.8] ;OIII
oplot, [0.6563,0.6563], [1.15,1.8] ;Ha
oplot, [0.6584,0.6584], [1.15,1.8] ;NII
oplot, [0.6300,0.6300], [1.05,1.8] ;OI
oplot, [0.6731,0.6731], [1.05,1.8] ;SII
oplot, [0.6716,0.6716], [1.05,1.8] ;SII
oplot, [0.3869,0.3869], [1.05,1.8] ;NeIII
oplot, [0.3727,0.3727], [1.05,1.8] ;OII
oplot, [0.3426,0.3426], [1.05,1.8] ;NeV

oplot, [0.1216,0.1216], [0,0.9] ;Lya
oplot, [0.4861,0.4861], [0,0.45] ;Hb
oplot, [0.1026,0.1026], [0,0.9] ;Lyb
oplot, [0.5007,0.5007], [0,0.7] ;OIII
oplot, [0.6563,0.6563], [0,0.55] ;Ha
oplot, [0.6584,0.6584], [0,0.55] ;NII
oplot, [0.6300,0.6300], [0,0.7] ;OI
oplot, [0.6731,0.6731], [0,0.75] ;SII
oplot, [0.6716,0.6716], [0,0.75] ;SII
oplot, [0.3869,0.3869], [0,0.65] ;NeIII
oplot, [0.3727,0.3727], [0,0.75] ;OII
oplot, [0.3426,0.3426], [0,0.9] ;NeV

p_cont = !p & x_cont = !x & y_cont = !y 

window, 5
plot, [1e-3,1e2], [-40,-10], psym=3, /xlog, title='log Halpha emission (int & em) (erg/cm3/s) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
;zoom
;plot, [10,20], [-40,-10], /xlog, psym=3, title='log Halpha emission (int & em) (erg/cm3/s) ', /nodata, xtitle='Depth (kpc)', /ystyle, /xstyle
p_emission = !p & x_emission = !x & y_emission = !y 

for j = 0, n_elements(files_in)-1 do begin

k = n_elements(files_in)-1 - j

 if strmatch(files_in(k), '*x1*') eq 1 then nm = 'x' else nm = ''
openr, fin, files_in(k), /get_lun
           line1=''
           readf, fin, line1
           line2=''
           readf, fin, line2
           line3=''
           readf, fin, line3
           if nm ne '' then begin
              line4=''
              readf, fin, line4
           endif
           line_radius = ''
           readf, fin, line_radius
           close, fin
           free_lun, fin
           
;print, line1, line2, line3, line_radius
           
           inner = strpos(line_radius, 'inner')
           outer = strpos(line_radius, 'outer')
           len = outer - inner
           radius_inner = double(strmid(line_radius,inner+5,len-5))
                                ;print, 'rayon', radius_inner
           
           if radius_inner eq 0 then stop

readcol, files_phyc(k), depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22
hden = hden*1.d/5.
readcol, files_comp(k), depth_comp, temp_comp, hden_comp, /silent, format='(D,D,D)'
depth_comp = (depth_comp+10d^radius_inner)*3.24d-22
hden_comp = hden_comp*1.d/5.

 readcol, files_in(k), col_continue, depth_den, density, format='(A8,D,D)', /silent
  ;units : none, log cm, log H/cc
  cmp = strcmp(col_continue, 'continue')
  t = where( cmp eq 1, nt)
  col_continue = col_continue[t]
;print, depth_den[t]
;print, ''
  depth_den = (10d^depth_den[t]+10d^radius_inner)*3.24d-22
  density = (10d^density[t])*1.d/5.

;print, depth_den

wset, 1
!p = p_den & !x = x_den & !y = y_den 
;oplot, depth_den, density, color=!gray
oplot, depth, hden, color=colors(k), psym=1
oplot, depth_comp, hden_comp, psym=-3
if k eq 2 then plots, depth_comp(984-1), hden_comp(984-1), color=!green, psym=2
p_den = !p & x_den = !x & y_den = !y 

wset, 2
!p = p_temp & !x = x_temp & !y = y_temp
oplot, depth, temp, color=colors(k), psym=1
oplot, depth_comp, temp_comp, psym=-3
if k eq 2 then plots, depth_comp(984-1), temp_comp(984-1), color=!green, psym=2 ;zone du warning qui a fait sww
p_temp = !p & x_temp = !x & y_temp = !y 

READCOL, files_fionH(k), depth_H, HI, HII, H2, /silent
depth_H = (depth_H+10d^radius_inner)*3.24d-22
Hneutre = HI + H2
READCOL, files_fionH_comp(k), depth_H_comp, HI_comp, HII_comp, H2_comp, /silent
depth_H_comp = (depth_H_comp+10d^radius_inner)*3.24d-22
Hneutre_comp = HI_comp + H2_comp

wset, 3
!p = p_H & !x = x_H & !y = y_H
oplot, depth_H, Hneutre, color=colors(k), psym=1
oplot, depth_H_comp, Hneutre_comp, psym=-3
if k eq 2 then plots, depth_comp(984-1), Hneutre_comp(984-1), color=!green, psym=2
p_H = !p & x_H = !x & y_H = !y 

                                      ;=incident
  readcol, files_continua(k), lambda, nuFnu, attenuated, diffuse, transmitted, reflected, /silent
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s -> incident radiation
    ;transmitted = diffuse + attenuated
  readcol, files_continua_comp(k), lambda_comp, nuFnu_comp, attenuated_comp, diffuse_comp, transmitted_comp, reflected_comp, /silent
    ;lambda : units micron !
    ;nu*Lnu/(4pi r0^2) = nuFnu : units erg/cm2/s -> incident radiation
    ;transmitted = diffuse + attenuated

if n_elements(lambda) eq n_elements(lambda_comp) then begin
wset, 4
!p = p_cont & !x = x_cont & !y = y_cont
oplot, lambda, transmitted/transmitted_comp, color=colors(k), psym=-3
p_cont = !p & x_cont = !x & y_cont = !y 
endif else begin
help, lambda
help, lambda_comp
endelse

readcol, files_emergent(k), depth_em, Lya_1216A, Lyb_1026A, Hb_4861A, OIII_5007A, Ha_6563A, NII_6584A, OI_6300A, SII_6731A_6716A, NeIII_3869A, OII_3727A_multiplet, NeV_3426A, SiVII_2481m, Brg_2166m, SiVI_1963m, AlIX_2040m, CaVIII_2321m, NeVI_7652m, /silent
  readcol, files_intrinsic(k), depth_int, Lya_1216A_int, Lyb_1026A_int, Hb_4861A_int, OIII_5007A_int, Ha_6563A_int, NII_6584A_int, OI_6300A_int, SII_6731A_6716A_int, NeIII_3869A_int, OII_3727A_multiplet_int, NeV_3426A_int, SiVII_2481m_int, Brg_2166m_int, SiVI_1963m_int, AlIX_2040m_int, CaVIII_2321m_int, NeVI_7652m_int, /silent

readcol, files_emergent_comp(k), depth_em_comp, Lya_1216A_comp, Lyb_1026A_comp, Hb_4861A_comp, OIII_5007A_comp, Ha_6563A_comp, NII_6584A_comp, OI_6300A_comp, SII_6731A_6716A_comp, NeIII_3869A_comp, OII_3727A_multiplet_comp, NeV_3426A_comp, SiVII_2481m_comp, Brg_2166m_comp, SiVI_1963m_comp, AlIX_2040m_comp, CaVIII_2321m_comp, NeVI_7652m_comp, /silent
  readcol, files_intrinsic_comp(k), depth_int_comp, Lya_1216A_int_comp, Lyb_1026A_int_comp, Hb_4861A_int_comp, OIII_5007A_int_comp, Ha_6563A_int_comp, NII_6584A_int_comp, OI_6300A_int_comp, SII_6731A_6716A_int_comp, NeIII_3869A_int_comp, OII_3727A_multiplet_int_comp, NeV_3426A_int_comp, SiVII_2481m_int_comp, Brg_2166m_int_comp, SiVI_1963m_int_comp, AlIX_2040m_int_comp, CaVIII_2321m_int_comp, NeVI_7652m_int_comp, /silent


depth_em = (depth_em+10d^radius_inner)*3.24d-22
depth_int = (depth_int+10d^radius_inner)*3.24d-22
depth_em_comp = (depth_em_comp+10d^radius_inner)*3.24d-22
depth_int_comp = (depth_int_comp+10d^radius_inner)*3.24d-22
;print, minmax(Ha_6563A)

wset, 5
!p = p_emission & !x = x_emission & !y = y_emission
oplot, depth_em, Ha_6563A, color=colors(k), psym=1
oplot, depth_em_comp, Ha_6563A_comp, psym=-3
if k eq 2 then plots, depth_em_comp(984-1), Ha_6563A_comp(984-1), color=!green, psym=2
oplot, depth_int, Ha_6563A_int, color=colors(k), psym=1
oplot, depth_int_comp, Ha_6563A_int_comp, psym=-3, line=1
if k eq 2 then plots, depth_int_comp(984-1), Ha_6563A_int_comp(984-1), color=!green, psym=2
p_emission = !p & x_emission = !x & y_emission = !y 

if j ne n_elements(files_in)-1 then begin
print, 'pause'
read, go_on
endif

endfor

end
