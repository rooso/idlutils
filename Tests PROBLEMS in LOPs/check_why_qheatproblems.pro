pro check_why_qheatproblems, sim=sim, factor=factor

;ce programme regarde dans quelles zones ont lieu les problemes de
;qheat en fonction de la luminosite 

;--> ils ont lieu a faible densite ou a grande pfdeur
;--> il n'y a pas assew de photons (pfdeur) ou d'atomes (densite) pour faire le calcul
;le quantum heqting peut etre neglige en utilisqant la commade :
;no qheat / no grain qheat
;dans le script d'entree cloudy

  if keyword_set(factor) then begin
     nm = 'x'+factor+'_'
     tt = 'x'+factor
     if factor eq '' then nm = ''
  endif else begin
     factor = ''
     nm = ''
     tt = ''
  endelse

if sim eq '00075w' or sim eq '00085' or sim eq '00075n' then begin
directory = '/Volumes/TRANSCEND/'
endif else begin
directory = '/Users/oroos/Post-stage/'
endelse

readcol, directory+'LOPs'+sim+'/'+nm+'with_qheat_problems', file, format='(A100)'

file = directory+'/LOPs'+sim+'/'+strtrim(file,2)

file_phyc = file
strreplace, file_phyc, '.out', '.phyc'
file_in = file
strreplace, file_in, '.out', '.in'

help, file

readcol, directory+'LOPs'+sim+'/'+nm+'no_qheat_problems', file_no_qheat, format = '(A100)'
strreplace, file_no_qheat, '.out', '.phyc'
file_no_qheat = directory+'LOPs'+sim+'/'+strtrim(file_no_qheat,2)
file_no_in = file_no_qheat
strreplace, file_no_in, '.phyc', '.in'

help, file_no_qheat
color_no = fltarr(n_elements(file_no_qheat))+!green

window, 1
plot, [1e-3,1e2], [1e-6,1e6], psym=3, /ylog, /xlog, title='density (H/cc) '+sim+tt, /nodata, xtitle='Depth (kpc)'
p_den = !p & x_den = !x & y_den = !y 

window, 2
plot, [1e-3,1e2], [1e2,1e10], psym=3, /ylog, /xlog, title='temperature (K) '+sim+tt, /nodata, xtitle='Depth (kpc)'
p_temp = !p & x_temp = !x & y_temp = !y 

window, 3
plot, [1e-3, 1e2], [1e15,1e30], psym=3, /ylog, /xlog, title='column density (H/cm2) '+sim+tt, /nodata, xtitle='Depth (kpc)'
p_colden = !p & x_colden = !x & y_colden = !y

for k = 0, n_elements(file)-1 do begin

 if strmatch(file_in(k), '*x1*') eq 1 then nm = 'x' else nm = ''
openr, fin, file_in(k), /get_lun
           line1=''
           readf, fin, line1
           line2=''
           readf, fin, line2
           line3=''
           readf, fin, line3
           if nm ne '' then begin
              line4=''
              readf, fin, line4
           endif
           line_radius = ''
           readf, fin, line_radius
           close, fin
           free_lun, fin
           
;print, line1, line2, line3, line_radius
           
           inner = strpos(line_radius, 'inner')
           outer = strpos(line_radius, 'outer')
           len = outer - inner
           radius_inner = double(strmid(line_radius,inner+5,len-5))
                                ;print, 'rayon', radius_inner
           
           if radius_inner eq 0 then stop

readcol, file_phyc(k), depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22
hden = hden*1./5.

length = depth*0
colden = hden*0

length(0) = depth/3.24d-22 ;cm
colden(0) = hden(0)*length(0) ;H/cm2

for j = 1, n_elements(depth)-1 do begin
length(j) = (depth(j) - depth(j-1))/3.24d-22 ;cm
colden(j) = colden(j-1) + hden(j)*length(j) ;H/cm2
endfor

wset, 1
!p = p_den & !x = x_den & !y = y_den 
oplot, depth, hden, psym=-3
p_den = !p & x_den = !x & y_den = !y 

wset, 2
!p = p_temp & !x = x_temp & !y = y_temp
oplot, depth, temp, psym=-3
p_temp = !p & x_temp = !x & y_temp = !y 

wset, 3
!p = p_colden & !x = x_colden & !y = y_colden
oplot, depth, colden, psym=-3
p_colden = !p & x_colden = !x & y_colden = !y 

endfor
for k = 0, n_elements(file)-1 do begin

 if strmatch(file_in(k), '*x1*') eq 1 then nm = 'x' else nm = ''
openr, fin, file_in(k), /get_lun
           line1=''
           readf, fin, line1
           line2=''
           readf, fin, line2
           line3=''
           readf, fin, line3
           if nm ne '' then begin
              line4=''
              readf, fin, line4
           endif
           line_radius = ''
           readf, fin, line_radius
           close, fin
           free_lun, fin
           
;print, line1, line2, line3, line_radius
           
           inner = strpos(line_radius, 'inner')
           outer = strpos(line_radius, 'outer')
           len = outer - inner
           radius_inner = double(strmid(line_radius,inner+5,len-5))
                                ;print, 'rayon', radius_inner
           
           if radius_inner eq 0 then stop
           
count = 0
test = '' 

openr, search_final_iteration, file(k), /get_lun 
while (not eof(search_final_iteration)) do begin
readf, search_final_iteration, test, format='(A100)'
count = count + 1

if (strmatch(test, '*Start Iteration Number 2*') eq 1) then break ;;ne garder que l'iteration 2 (la derniere)

endwhile
close, search_final_iteration
free_lun, search_final_iteration

readcol, file(k), problem, qheat, did, nnot, converge, grain, name_grain, in, zzone, zone, $
         format='(A7,A5,A3,A3,A8,A5,A9,A2,A4,I)', /silent, skipline=count
t = where(problem eq 'PROBLEM' and qheat eq 'qheat' and did eq 'did' and nnot eq 'not' and converge eq 'converge', nt)

debut = problem+qheat+did+nnot+converge+grain+name_grain+in+zzone

if nt gt 0 then begin
debut = debut[t]
zone = zone[t]

;on ne veut garder qu'une erreur par zone
zone = unique(zone)

;print, zone

;print, 'pause'
;read, go_on

readcol, file_phyc(k), depth, temp, hden, /silent, format='(D,D,D)'
depth = (depth+10d^radius_inner)*3.24d-22
hden = hden*1./5.

;verif :
print, 'min/max zones avec pb qheat : ', minmax(zone)
help, hden
help, depth
print, 'Soit ', (max(zone)-min(zone))/float(n_elements(depth))*100., ' % des points cloudy de la ligne'


length = depth*0
colden = hden*0

length(0) = depth/3.24d-22 ;cm
colden(0) = hden(0)*length(0) ;H/cm2

for j = 1, n_elements(depth)-1 do begin
length(j) = (depth(j) - depth(j-1))/3.24d-22 ;cm
colden(j) = colden(j-1) + hden(j)*length(j) ;H/cm2
endfor

wset, 1
!p = p_den & !x = x_den & !y = y_den 
if n_elements(zone) gt 1 then color=!dred else color=!red
oplot, depth[zone-1], hden[zone-1], color=!red, psym=3
p_den = !p & x_den = !x & y_den = !y 

wset, 2
!p = p_temp & !x = x_temp & !y = y_temp
if n_elements(zone) gt 1 then color=!dred else color=!red
oplot, depth[zone-1], temp[zone-1], color=!red, psym=3
p_temp = !p & x_temp = !x & y_temp = !y 


 wset, 3
!p = p_colden & !x = x_colden & !y = y_colden 
if n_elements(zone) gt 1 then color=!dred else color=!red
oplot, depth[zone-1], colden[zone-1], color=!red, psym=3
p_colden = !p & x_colden = !x & y_colden = !y 

print, k+1, '/', n_elements(file)

endif else begin

file_no_qheat = [file_no_qheat,file_phyc(k)]
file_no_in = [file_no_in,file_in(k)]
color_no = [!cyan,color_no]
print, 'Plus de probleme a l iteration 2 !'

endelse

endfor

print, 'Afficher les lignes sans probleme de qheat ?'
read, go_on

for k = 0, n_elements(file_no_qheat)-1 do begin

 if strmatch(file_no_in(k), '*x1*') eq 1 then nm = 'x' else nm = ''

openr, fin, file_no_in(k), /get_lun
           line1=''
           readf, fin, line1
           line2=''
           readf, fin, line2
           line3=''
           readf, fin, line3
           if nm ne '' then begin
              line4=''
              readf, fin, line4
           endif
           line_radius = ''
           readf, fin, line_radius
           close, fin
           free_lun, fin
           
;print, line1, line2, line3, line_radius
           
           inner = strpos(line_radius, 'inner')
           outer = strpos(line_radius, 'outer')
           len = outer - inner
           radius_inner = double(strmid(line_radius,inner+5,len-5))
                                ;print, 'rayon', radius_inner
           
           if radius_inner eq 0 then stop
           


readcol, file_no_qheat(k), depth_no_qheat, temp_no_qheat, hden_no_qheat, /silent, format='(D,D,D)'
depth_no_qheat = (depth_no_qheat+10d^radius_inner)*3.24d-22
hden_no_qheat = hden_no_qheat*1./5.

length = depth_no_qheat*0
colden = hden_no_qheat*0

length(0) = depth_no_qheat/3.24d-22 ;cm
colden(0) = hden_no_qheat(0)*length(0) ;H/cm2

for j = 1, n_elements(depth_no_qheat)-1 do begin
length(j) = (depth_no_qheat(j) - depth_no_qheat(j-1))/3.24d-22 ;cm
colden(j) = colden(j-1) + hden_no_qheat(j)*length(j) ;H/cm2
endfor


wset, 1
!p = p_den & !x = x_den & !y = y_den 
oplot, depth_no_qheat, hden_no_qheat, color=color_no(k), line=2
p_den = !p & x_den = !x & y_den = !y 

wset, 2
!p = p_temp & !x = x_temp & !y = y_temp
oplot, depth_no_qheat, temp_no_qheat, color=color_no(k), line=2
p_temp = !p & x_temp = !x & y_temp = !y 

wset, 3
!p = p_colden & !x = x_colden & !y = y_colden 
oplot, depth_no_qheat, colden, color=color_no(k), line=2
p_den = !p & x_colden = !x & y_colden = !y 

print, k+1, '/', n_elements(file_no_qheat)
endfor




end
